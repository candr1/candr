<?php

namespace App\Transcribers;

class TextTranscription {
	protected $transcription;
	protected $logger;
	public $stanza;
	public function __construct($logger = null, $stanza = 1) {
		$this->transcription = new \Ds\Stack();
		$this->logger = $logger;
		$this->stanza = $stanza;
	}
	public function push($item) {
		$this->transcription->push($item);
	}
	public function pop() {
		$popped = $this->transcription->pop();
		return $popped;
	}
	public function finalise() {
		return trim(implode('', array_map(function($item) {
			$space = '';
			if($item->wordstart === true) {
				$space = ' ';
			}
			$txt = $item->text;
			$start = $end = '';
			if(get_class($item) === 'App\Models\EditorialSyllable') {
				$start = '[';
				$end = ']';
			}
			return ($space . $start . $txt . $end);
		}, array_reverse($this->transcription->toArray()))));
	}
}
