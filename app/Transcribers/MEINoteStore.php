<?php

namespace App\Transcribers;

class MEINoteStore {
	private $notes;
	private $nodes;

	public function __construct() {
		$this->notes = new \Hexogen\KDTree\ItemList(2);
		$this->nodes = [];
	}
	public function add_note(float $x, float $y, $node) {
		$this->notes->addItem(new \Hexogen\KDTree\Item(count($this->nodes), [$x, $y]));
		$this->nodes[] = $node;
	}
	public function nearest_note(float $x, float $y) {
		$tree = new \Hexogen\KDTree\KDTree($this->notes);
		$searcher = new \Hexogen\KDTree\NearestSearch($tree);
		$result = $searcher->search(new \Hexogen\KDTree\Point([$x, $y]), 1);
		if(count($result) < 1) {
			return null;
		}
		$result_id = $result[0]->getId();
		return $this->nodes[$result_id];
	}
}
