<?php

namespace \App\Transcribers;

interface InterfaceTranscription {
	public function push($item);
	public function finalise();
}
