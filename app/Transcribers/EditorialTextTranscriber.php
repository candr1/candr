<?php

namespace App\Transcribers;

class EditorialTextTranscriber extends TextTranscriber {
	protected function new_tran($logger, $stanza = 1) {
		return new EditorialTextTranscription($logger, $stanza);
	}
}

