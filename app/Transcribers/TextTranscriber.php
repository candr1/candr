<?php

namespace App\Transcribers;

class TextTranscriber extends AbstractTranscriber implements InterfaceTranscriber {
	public function __construct($logger = null) {
		$this->transcription = new \Ds\Stack([$this->new_tran($logger)]);
		parent::__construct($logger);
	}
	protected function get_items($record) {
		$system = $record->up()->with(['links.setting', 'links.items.up'])->first();
		$links = $system->links()->has('setting')->get();
		$link_items = $links->flatMap(function($a) {
			return $a->items;
		})->filter(function($a) use (&$record) {
			return $a->up->is($record);
		});
		$items = $record->items()
			->with('type')
			->whereIn('itemtype_type', ['App\Models\Syllable', 'App\Models\EditorialSyllable'])
			->get();
		$note_store = new MEINoteStore();
		$record->items()
			->with('type')
			->where('itemtype_type', 'App\Models\Note')
			->get()->map(function($a) use (&$note_store) {
				$type = $a->type;
				$note_store->add_note($type->comp_x, $type->comp_y, $type);
			});
		$syllables = [];
		$seen = new \Ds\Set();
		$all_items = $link_items->merge($items)->reject(function($a) use ($seen) {
			$reject = $seen->contains($a->id);
			if(!$reject) {
				$seen->add($a->id);
			}
			return $reject;
		});
		foreach($all_items as $item) {
			$syllable = $item->type;
			$item_class = get_class($syllable);
			if($item_class === 'App\Models\Syllable' || $item_class === 'App\Models\EditorialSyllable') {
				$nearest_note = $note_store->nearest_note($syllable->centrepoint_x, $syllable->centrepoint_y);
				if($nearest_note) {
					// + 1 to shift it right ever so slightly
					$syllable->centrepoint_x = $nearest_note->comp_x + 5;
					$syllable->centrepoint_y = $nearest_note->comp_y;
				}
			}
			$syllables[] = $syllable;
		}
		usort($syllables, function($a, $b) {
			$c = $a->comp_x <=> $b->comp_x;
			if($c) {
				return $c;
			}
			return $b->comp_y <=> $a->comp_y;
		});
		return $syllables;
	}
	public function transcribe($record, $start = null, $stop = null) {
		$before_count = $this->push_returns->count();
		$ret = parent::transcribe($record, $start, $stop);
		$after_count = $this->push_returns->count();
		$diff = $after_count - $before_count;
		if($diff < 1) {
			return $ret;
		}
		$ret_stack = new \Ds\Stack();
		$this_stack = new \Ds\Stack();
		for($i = 0; $i < $diff; $i++) {
			$ret_pop = $this->push_returns->pop();
			$this_pop = $this->pop();
			$popped_class = get_class($this_pop);
			if($popped_class === 'App\Models\Syllable' ||
			    $popped_class === 'App\Models\EditorialSyllable') {
			    	$this->push($this_pop);
				$this->push_returns->push($ret_pop);
			}
		}
		return $ret;
	}
	protected function new_tran($logger, $stanza = 1) {
		return new TextTranscription($logger, $stanza);
	}
	public function push($item) {
		if($this->transcription->count() === 1 && $item->stanza === 1) {
			return parent::push($item);
		}

		$transcriptions = $this->transcription->toArray();
		$highest_stanza = 1;
		$ret = null;
		foreach($transcriptions as $tran) {
			if($tran->stanza === $item->stanza) {
				$ret = $tran->push($item);
				goto done;
			}
		}
		$new_tran = $this->new_tran($this->logger, $item->stanza);
		$ret = $new_tran->push($item);
		$transcriptions[] = $new_tran;
done:
		$this->transcription = new \Ds\Stack($transcriptions);
		return $ret;
	}
	protected function item_check($record) {
		return in_array(get_class($record), ['App\Models\Syllable', 'App\Models\EditorialSyllable']);
	}
	public function get_transcription() {
		$ret = [];
		$count = 0;
		try {
			while(1) {
				$popped = $this->transcription->pop();
				$stanza = $popped->stanza;
				$ret[intval($stanza)] = $popped->finalise();
			}
		} catch(\UnderflowException $e) { }
		if(count($ret) === 1) {
			return array_values($ret)[0];
		}
		ksort($ret);
		return implode("\n", array_map(function($k, $v) {
			return (strval($k) . ". " . $v);
		}, array_keys($ret), array_values($ret)));
	}
}
