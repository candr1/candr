<?php

namespace App\Transcribers;

class MEIIDFactory
{
  private $ids;
  private $logger;
  private $namespace;

  public function __construct($logger = null, $namespace = 'http://www.w3.org/XML/1998/namespace')
  {
    $this->ids = new \Ds\Set();
    $this->logger = $logger;
    $this->namespace = $namespace;
  }
  public function new($prefix = '', $id = null)
  {
    if (!$id) {
      while (1) {
        $id = uniqid($prefix);
        if ($this->ids->contains($id)) {
          continue;
        }
        break;
      }
    } else {
      $id = $prefix . $id;
    }
    $this->ids->add($id);
    return $id;
  }
  public function get_ns()
  {
    return $this->namespace;
  }
}

class MEILinkStore
{
  private $links;

  public function __construct()
  {
    $this->links = [];
  }
  public function add_link($link_id, $node)
  {
    if (!isset($this->links[$link_id])) {
      $this->links[$link_id] = [];
    }
    $this->links[$link_id][] = $node;
  }
  public function get_links()
  {
    return $this->links;
  }
}

class MEIGuideline
{
  private $line;
  private $pitch;
  private $logger;

  public function __construct($line, $logger)
  {
    usort($line, function ($a, $b) {
      return $a[0] - $b[0];
    });
    $this->line = $line;
    $this->pitch = null;
    $this->logger = $logger;
  }
  public function add_pitch($pitch)
  {
    $this->pitch = clone $pitch;
  }
  public function get_line()
  {
    return $this->line;
  }
  public function get_pitch()
  {
    if ($this->pitch === null) {
      $this->logger->error("Guideline has no pitch");
    }
    return $this->pitch;
  }
}

class MEIGuidelines
{
  private $guidelines;
  private $logger;
  private $mean_dist;
  private $stave_id;

  public function __construct($stave_id, $logger = null)
  {
    $this->guidelines = [];
    $this->stave_id = $stave_id;
    $this->logger = $logger;
    $this->mean_dist = 0;
  }
  private function calc_mean_dist()
  {
    $cumy = 0;
    $n = $this->n_lines();
    if ($n < 3) {
      return 0;
    }
    for ($i = 1; $i < $n; $i++) {
      $a = $this->guidelines[$i - 1]->get_line();
      $b = $this->guidelines[$i]->get_line();
      $cumy += abs(($b[0][1] + $b[1][1]) - ($a[0][1] + $a[1][1]));
    }
    $mean = $cumy / ($n - 2);
    $this->mean_dist = $mean;
    return $mean;
  }
  public function get_mean_dist()
  {
    return $this->mean_dist;
  }
  public function add_line($line)
  {
    $this->guidelines[] = new MEIGuideline($line, $this->logger);
  }
  public function n_lines()
  {
    return count($this->guidelines);
  }
  public function sort($fun)
  {
    $n = strval($this->n_lines());
    usort($this->guidelines, $fun);
    $this->calc_mean_dist();
    $new_n = strval($this->n_lines());
  }
  public function get_guideline($i)
  {
    return $this->guidelines[$i]->get_line();
  }
  public function get_pitch($i, $shift = 0)
  {
    $g_pitch = $this->guidelines[$i]->get_pitch();
    if ($g_pitch === null) {
      $this->logger->error("Failed to get pitch for guideline on stave " . strval($this->stave_id));
      return null;
    }
    $guideline_pitch = clone $g_pitch;
    $this->step_pitch($guideline_pitch, $shift);
    return $guideline_pitch;
  }
  public function guideline_pitches()
  {
    return array_map(function ($a) {
      return $a->get_pitch();
    }, $this->guidelines);
  }
  public function guidelines_json()
  {
    return json_encode(array_map(function ($g) {
      $line = $g->get_line();
      $pitch = $g->get_pitch();
      return [
        "line" => [
          "a" => $line[0],
          "b" => $line[1]
        ],
        "pitch" => [
          "step" => $pitch->step,
          "alter" => $pitch->alter,
          "octave" => $pitch->octave
        ]
      ];
    }, $this->guidelines), JSON_PRETTY_PRINT);
  }
  private function step_pitch($pitch, $d = 1)
  {
    while ($d > 0) {
      $pitch->step = \ianring\Pitch::stepUp($pitch->step);
      if ($pitch->step === 'C') {
        $pitch->octave++;
      }
      $d--;
    }
    while ($d < 0) {
      $pitch->step = \ianring\Pitch::stepDown($pitch->step);
      if ($pitch->step === 'B') {
        $pitch->octave--;
      }
      $d++;
    }
    return $pitch;
  }

  public function add_clef($pitch, $x, $y)
  {
    $thisi = $this->nearest_guideline_i($x, $y);
    $n = $this->n_lines();
    $ipitch = clone $pitch;
    for ($i = $thisi; $i < $n; $i++) {
      $this->guidelines[$i]->add_pitch($ipitch);
      $this->step_pitch($ipitch, 1);
    }
    $ipitch = clone $pitch;
    for ($i = $thisi; $i >= 0; $i--) {
      $this->guidelines[$i]->add_pitch($ipitch);
      $this->step_pitch($ipitch, -1);
    }
    $this->pitched = true;
    return $pitch;
  }
  public function add_accidental($alter, $x, $y)
  {
    $thisi = $this->nearest_guideline_i($x, $y);
    if (!array_key_exists($thisi, $this->guidelines)) {
      $this->logger->error(
        "nearest_guideline_i returned guideline with key " .
          strval($thisi) . " but this set of guidelines only has keys of " .
          json_encode(array_keys($this->guidelines))
      );
      return null;
    }
    $guideline = $this->guidelines[$thisi];
    $pitch = $guideline->get_pitch();
    if (!$pitch) {
      return null;
    }
    // naturals == sharps in this notation, so work out which is
    // most likely
    switch ($pitch->step) {
      case 'C':
      case 'D':
      case 'F':
      case 'G':
        if ($alter == 1) {
          $pitch->alter = 1;
        } else {
          $pitch->alter = 0;
        }
        break;
      case 'A':
      case 'B':
      case 'E':
        if ($alter == -1) {
          $pitch->alter = -1;
        } else {
          $pitch->alter = 0;
        }
        break;
    }
    $guideline->add_pitch($pitch);
    return $pitch;
  }
  public function nearest_guideline_i($x, $y)
  {
    // ported from cleflike.coffee
    $distToSeg = function ($px, $py, $sax, $say, $sbx, $sby) {
      $dx = $sbx - $sax;
      $dy = $sby - $say;
      $l2 = sqrt($dx * $dx + $dy * $dy);
      if ($l2 == 0) {
        return 0;
      }
      $t = abs($dx * ($say - $py) - $dy * ($sax - $px));
      return $t / $l2;
    };
    $minDist = INF;
    $closest = null;
    foreach ($this->guidelines as $i => $guideline) {
      $line = $guideline->get_line();
      $p1 = $line[0];
      $p2 = $line[1];
      $d = $distToSeg($x, $y, $p1[0], $p1[1], $p2[0], $p2[1]);
      if ($d < $minDist) {
        $minDist = $d;
        $closest = $i;
      }
    }
    if ($closest === null) {
      $this->logger->error(
	"nearest_guideline_i returning null on stave " .
	strval($this->stave_id) . ". " .
	strval(count($this->guidelines)) .
	" guides"
      );
      foreach($this->guidelines as $i => $guideline) {
        $line = $guideline->get_line();
	$p1 = $line[0];
	$p2 = $line[1];
	$d = $distToSeg($x, $y, $p1[0], $p1[1], $p2[0], $p2[1]);
	$this->logger->error("Distance " . strval($i) . ": " . strval($d));
      }
    }
    return $closest;
  }
  public function add_centrelines()
  {
    // ported from cleflike.coffee
    $n_stafflines = $this->n_lines();
    if ($n_stafflines < 2) {
      return;
    }
    $sort_by_y_coord = function ($a, $b) {
      $get_mean_y = function ($line) {
        $cum = 0;
        foreach ($line as $point) {
          $cum += $point[1];
        }
        return ($cum / count($line));
      };
      return ($get_mean_y($b->get_line()) - $get_mean_y($a->get_line()));
    };
    $this->sort($sort_by_y_coord);
    $get_midpoint = function ($a, $b) {
      return [($a[0] + $b[0]) / 2, ($a[1] + $b[1]) / 2];
    };
    $cumdy = 0;
    $div = ($n_stafflines - 1) * 4;
    $ret = [];
    for ($i = 1; $i < $n_stafflines; $i++) {
      $a = $this->get_guideline($i - 1);
      $b = $this->get_guideline($i);
      $paa = $a[0];
      $pab = $a[1];
      $pba = $b[0];
      $pbb = $b[1];
      $mida = $get_midpoint($paa, $pba);
      $midb = $get_midpoint($pab, $pbb);
      $cumdy += abs($pba[1] - $paa[1]);
      $cumdy += abs($pbb[1] - $pab[1]);
      $ret[] = [$mida, $midb];
    }
    $havdy = $cumdy / $div;
    $fl = $this->get_guideline(0);
    $ll = $this->get_guideline($n_stafflines - 1);
    $fla = $fl[0];
    $flb = $fl[1];
    $lla = $ll[0];
    $llb = $ll[1];
    $ret[] = [[$fla[0], $fla[1] + $havdy], [$flb[0], $flb[1] + $havdy]];
    $ret[] = [[$lla[0], $lla[1] - $havdy], [$llb[0], $llb[1] - $havdy]];
    foreach ($ret as $r) {
      $this->add_line($r);
    }
    $this->sort($sort_by_y_coord);
  }
}

class MEIDocument
{
  private $document;
  private $logger;
  public $pointer;
  public $namespace;
  public $counter;

  public function __construct($id_factory, $logger = null, $namespace = "http://www.music-encoding.org/ns/mei")
  {
    $this->document = new \DOMDocument('1.0', 'utf-8');
    $this->namespace = $namespace;
    $this->logger = $logger;
    $mei_xml = $this->createElement('mei');
    $this->addAttribute($mei_xml, 'xml:id', $id_factory->new('mei'), $id_factory->get_ns());
    $meiHead_xml = $this->createElement('meiHead');
    $this->addAttribute($meiHead_xml, 'xml:id', $id_factory->new('meiHead'), $id_factory->get_ns());
    $this->addChild($mei_xml, $meiHead_xml);
    $music_xml = $this->createElement('music');
    $this->addAttribute($music_xml, 'xml:id', $id_factory->new('music'), $id_factory->get_ns());
    $body_xml = $this->createElement('body');
    $this->addAttribute($body_xml, 'xml:id', $id_factory->new('body'), $id_factory->get_ns());
    $mdiv_xml = $this->createElement('mdiv');
    $this->addAttribute($mdiv_xml, 'xml:id', $id_factory->new('mdiv'), $id_factory->get_ns());
    $this->addChild($body_xml, $mdiv_xml);
    $this->addChild($music_xml, $body_xml);
    $this->addChild($mei_xml, $music_xml);
    $this->add_root_element($mei_xml);
    $this->pointer = $mdiv_xml;
    $this->counter = 1;
  }
  public function createElement($name)
  {
    return $this->document->createElementNs($this->namespace, $name);
  }
  public function createText($text)
  {
    return $this->document->createTextNode($text);
  }
  public function addAttribute($element, $name, $value, $namespace = null)
  {
    if (!$namespace) {
      $namespace = $this->namespace;
    }
    $element->setAttributeNs($namespace, $name, $value);
    return $element;
  }
  public function addChild($element, $child)
  {
    $element->appendChild($child);
    if ($element->hasChildNodes() === false) {
      $this->logger->error("Appended child, but no children exist");
    }
    return $element;
  }
  public function add_root_element($root)
  {
    $this->document->appendChild($root);
  }
  public function get_xml()
  {
    return $this->document->saveXML();
  }
  public function get_doc()
  {
    return $this->document;
  }
}
class MEITranscriber extends AbstractTranscriber implements InterfaceTranscriber
{
  private $id_factory;
  private $id;
  private $link_store;
  private $guidelines;
  private $mei_doc;

  public function __construct($logger = null)
  {
    // The transcription is a stack of transcriptions of staves
    $this->transcription = new \Ds\Stack();
    $this->guidelines = null;
    $this->id = null;
    $this->id_factory = new MEIIDFactory($logger);
    $this->link_store = new MEILinkStore();
    $this->mei_doc = new MEIDocument($this->id_factory, $logger);
    parent::__construct($logger);
  }
  protected function get_items($stave)
  {
    $items = $stave->items()->with(['type', 'links'])->get()->reject(function ($a) {
      $a === null;
    });
    $note_store = new MEINoteStore();
    $types = [];
    $revisit_syllables = [];
    foreach ($items as $item) {
      $type = $item->type;
      if (!$type) {
        continue;
      }
      $class = get_class($type);
      if ($class === 'App\Models\Staffline') {
        $this->guidelines->add_line([
          [$type->ax, $type->ay],
          [$type->bx, $type->by]
        ]);
      } else {
        if ($class === 'App\Models\Note') {
          $note_store->add_note($type->comp_x, $type->comp_y, $type);
        } else if ($class === 'App\Models\Syllable' || $class === 'App\Models\EditorialSyllable') {
          $revisit_syllables[] = count($types);
        }
        $types[] = $type;
      }
    }
    foreach ($revisit_syllables as $idx) {
      $t = $types[$idx];
      $nearest_note = $note_store->nearest_note($t->centrepoint_x, $t->centrepoint_y);
      if ($nearest_note) {
        // +1 to shift it right ever so slightly
        $types[$idx]->centrepoint_x = $nearest_note->comp_x + 1;
        $types[$idx]->centrepoint_y = $nearest_note->comp_y;
      }
    }
    $this->guidelines->add_centrelines();
    usort($types, function ($a, $b) {
      $c = $a->comp_x <=> $b->comp_x;
      if ($c) {
        return $c;
      }
      return $b->comp_y <=> $a->comp_y;
    });
    $num_types = count($types);
    if ($num_types > 0) {
      $first = $types[0];
      $class = get_class($first);
      if ($class !== 'App\Models\Clef' && $class !== 'App\Models\EditorialClef') {
        $fixed = false;
        for ($i = 1; $i < $num_types; $i++) {
          $itype = $types[$i];
          $iclass = get_class($itype);
          if ($iclass === 'App\Models\Clef' || $iclass === 'App\Models\EditorialClef') {
            $types[$i] = $types[0];
            $types[0] = $itype;
            $fixed = true;
            break;
          }
        }
        if (!$fixed) {
          $this->logger->error("Stave " . strval($stave->id) . " has no clef");
        }
      }
    }
    return $types;
  }
  private function create_new_mei_transcription($id = null)
  {
    $this->guidelines = new MEIGuidelines($id, $this->logger);
    $this->transcription->push(new MEITranscription($id, $this->id_factory, $this->link_store, $this->guidelines, $this->mei_doc, $this->logger));
  }
  public function page($item)
  {
    $xml = $this->mei_doc->createElement('pb');
    $this->mei_doc->addAttribute($xml, 'xml:id', $this->id_factory->new('pb', $item->id), $this->id_factory->get_ns());
    $this->mei_doc->addAttribute($xml, 'n', $item->name);
    $this->mei_doc->addChild($this->mei_doc->pointer, $xml);
  }
  public function transcribe($record, $start = null, $stop = null)
  {
    switch (get_class($record)) {
      case 'App\Models\Stave':
        $this->create_new_mei_transcription($record->id);
        return parent::transcribe($record, $start, $stop);
        break;
      case 'App\Models\System':
        $this->id = $record->id;
        $staves = $record->down()->with('warp')->get()->sort(function ($b, $a) {
          $ac = $a->warp->centroid;
          $bc = $b->warp->centroid;
          $c = $ac[1] <=> $bc[1];
          if ($c) {
            return $c;
          }
          return ($ac[0] <=> $bc[0]);
        });
        // backwards because we're working with stacks
        foreach ($staves as $stave) {
          $this->create_new_mei_transcription($stave->id);
          parent::transcribe($stave, $start, $stop);
        }
        break;
    }
    $xml = $this->mei_doc->createElement('section');
    $this->mei_doc->addAttribute($xml, 'xml:id', $this->id_factory->new('section', $this->id), $this->id_factory->get_ns());
    try {
      $counter = 1;
      while (1) {
        $transcription = $this->transcription->pop();
        $id = $transcription->get_id();
        $layer = $transcription->finalise();
        $stave = $this->mei_doc->createElement('staff');
        $this->mei_doc->addAttribute($stave, 'xml:id', $this->id_factory->new('staff', $id), $this->id_factory->get_ns());
        $this->mei_doc->addAttribute($stave, 'n', $counter);
        $this->mei_doc->addChild($stave, $layer);
        $this->mei_doc->addChild($xml, $stave);
        $counter++;
      }
    } catch (\UnderflowException $e) {
    }
    $system_element = $this->link_items($xml);
    $this->mei_doc->addAttribute($system_element, 'n', $this->mei_doc->counter);
    $this->mei_doc->counter++;
    $this->mei_doc->addChild($this->mei_doc->pointer, $system_element);
    return;
  }
  private function link_items($transcription)
  {
    $links = $this->link_store->get_links();
    foreach ($links as $link_id => $xml_arr) {
      for ($i = 0; $i < count($xml_arr); $i++) {
        $synch_arr = [];
        for ($j = 0; $j < count($xml_arr); $j++) {
          if ($i == $j) {
            continue;
          }
          $synch_arr[] = '#' . $xml_arr[$j]->getAttribute('xml:id');
        }
        $this->mei_doc->addAttribute($xml_arr[$i], 'synch', implode(' ', $synch_arr));
      }
    }
    return $transcription;
  }
  public function get_transcription()
  {
    return $this->mei_doc->get_xml();
  }
}
