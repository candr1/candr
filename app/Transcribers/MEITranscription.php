<?php

namespace App\Transcribers;

class MEITranscription {
	private $transcription;
	private $id_factory;
	private $id;
	private $link_store;
	private $guidelines;
	private $element_factory;
	private $note_store;
	private $syllables;
	private $mei_doc;
	private $logger;

	public function __construct($id, $id_factory, $link_store, $guidelines, $mei_doc, $logger) {
		$this->id = $id;
		$this->id_factory = $id_factory;
		$this->link_store = $link_store;
		$this->guidelines = $guidelines;
		$this->logger = $logger;
		$this->mei_doc = $mei_doc;
		$this->transcription = $this->mei_doc->createElement('layer');
		$root_id = $this->id_factory->new('layer');
		$this->mei_doc->addAttribute($this->transcription, 'xml:id', $root_id, $this->id_factory->get_ns());
		$this->note_store = new MEINoteStore();
		$this->element_factory = new MEIElementFactory($logger, $id_factory, $guidelines, $link_store, $this->note_store, $mei_doc);
		$this->syllables = [];
	}
	public function get_id() {
		return $this->id;
	}
	public function push($item) {
		$ef = $this->element_factory;
		$syllables = &$this->syllables;
		$save_syllables = function($syllable) use (&$syllables) {
			$syllables[] = [$syllable, false];
		};
		$save_editorial_syllables = function($syllable) use (&$syllables) {
			$syllables[] = [$syllable, true];
		};

		$funs = [
			'App\Models\Accidental' => function($i) use (&$ef) { return $ef->accidental($i); },
			'App\Models\Clef' => function($i) use (&$ef) { return $ef->clef($i); },
			'App\Models\Divisione' => function($i) use (&$ef) { return $ef->divisione($i); },
			'App\Models\EditorialAccidental' => function($i) use (&$ef) { return $ef->editorial_accidental($i); },
			'App\Models\EditorialClef' => function($i) use (&$ef) { return $ef->editorial_clef($i); },
			'App\Models\EditorialSyllable' => $save_editorial_syllables,
			'App\Models\EditorialText' => function($i) use (&$ef) { return $ef->editorial_text($i); },
			'App\Models\Ligature' => function($i) use (&$ef) { return $ef->ligature($i); },
			// only transcribe a note if it doesn't belong to a ligature
			'App\Models\Note' => function($i) use (&$ef) { return ($i->ligature()->exists() ? null : $ef->note($i)); },
			'App\Models\Syllable' => $save_syllables
		];
		$class = get_class($item);
		if(array_key_exists($class, $funs)) {
			$xml = $funs[$class]($item);
			if($xml) {
				$this->mei_doc->addChild($this->transcription, $xml);
			}
		}
	}
	private function add_syllables() {
		foreach($this->syllables as $syllable) {
			$note_node = $this->note_store->nearest_note(floatval($syllable[0]->centrepoint_x), floatval($syllable[0]->centrepoint_y));
			if(!$note_node) {
				continue;
			}
			$is_editorial = $syllable[1];
			$xml = null;
			if($is_editorial) {
				$xml = $this->element_factory->editorial_syllable($syllable[0]);
			} else {
				$xml = $this->element_factory->syllable($syllable[0]);
			}
			$this->mei_doc->addChild($note_node, $xml);
		}
	}
	public function finalise() {
		$this->add_syllables();
		return $this->transcription;
	}
}

class MEIElementFactory {
	private $logger;
	private $id_factory;
	private $guidelines;
	private $link_store;
	private $mei_doc;
	private $decimal_precision;
	private $note_store;

	public function __construct($logger, $id_factory, $guidelines, $link_store, $note_store, $mei_doc, $decimal_precision = 2) {
		$this->logger = $logger;
		$this->id_factory = $id_factory;
		$this->guidelines = $guidelines;
		$this->link_store = $link_store;
		$this->note_store = $note_store;
		$this->mei_doc = $mei_doc;
		$this->decimal_precision = $decimal_precision;
	}
	private function float_to_str($fl) {
		return number_format($fl, $this->decimal_precision, '.', '');
	}
	public function accidental($item) {
		$xml = $this->mei_doc->createElement('accid');
		$alter = (function($v) {
			switch(strtolower($v)) {
				case 'sharp':
					return [1, 's'];
				case 'flat':
				default:
					return [-1, 'f'];
			}
			return [0, 'n'];
		})($item->type);
		$pitch = $this->guidelines->add_accidental($alter[0], floatval($item->centrepoint_x), floatval($item->centrepoint_y));
		if(!$pitch) {
			return null;
		}
		$this->mei_doc->addAttribute($xml, 'accid', $alter[1]);
		$this->mei_doc->addAttribute($xml, 'ploc', strtolower($pitch->step));
		$this->mei_doc->addAttribute($xml, 'oloc', $pitch->octave);
		$xmlid = $this->id_factory->new('accid', $item->id);
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function clef($item) {
		$xml = $this->mei_doc->createElement('clef');
		$type = strtoupper($item->type);
		$this->mei_doc->addAttribute($xml, 'shape', $type);
		$pitch = new \ianring\Pitch('C', 0, 3);
		switch($type) {
			case 'C':
				$pitch = new \ianring\Pitch('C', 0, 3);
				break;
			case 'D':
				$pitch = new \ianring\Pitch('D', 0, 2);
				break;
			case 'F':
				$pitch = new \ianring\Pitch('F', 0, 2);
				break;
		}
		$this->guidelines->add_clef($pitch, floatval($item->centrepoint_x), floatval($item->centrepoint_y));
		$loc = $this->guidelines->nearest_guideline_i(floatval($item->centrepoint_x), floatval($item->centrepoint_y)) + 2;
		$clef_line = intdiv($loc, 2);
		$this->mei_doc->addAttribute($xml, 'line', $clef_line);
		$xmlid = $this->id_factory->new('clef', $item->id);
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function divisione($item) {
		$xml = $this->mei_doc->createElement('divisione');
		$loc = $this->guidelines->nearest_guideline_i($item->comp_x, $item->comp_y);
		$this->mei_doc->addAttribute($xml, 'loc', $loc);
		$len_px = sqrt(
			pow($item->ax - $item->bx, 2) +
			pow($item->ay - $item->by, 2)
		);
		$mean_px = $this->guidelines->get_mean_dist();
		$float_len = 2 * $len_px / $mean_px;
		$len = $this->float_to_str($float_len);
		$this->mei_doc->addAttribute($xml, 'len', $len);
		$xmlid = $this->id_factory->new('divisione', $item->id);
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function editorial_accidental($item) {
		$xml = $this->mei_doc->createElement('supplied');
		$accid = $this->accidental($item);
		if(!$accid) {
			return null;
		}
		$this->mei_doc->addChild($xml, $accid);
		$xmlid = $this->id_factory->new('supplied');
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function editorial_clef($item) {
		$xml = $this->mei_doc->createElement('supplied');
		$clef = $this->clef($item);
		$this->mei_doc->addChild($xml, $clef);
		$xmlid = $this->id_factory->new('supplied');
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function editorial_syllable($item) {
		$xml = $this->mei_doc->createElement('supplied');
		$syllable = $this->syllable($item);
		$this->mei_doc->addChild($xml, $syllable);
		$xmlid = $this->id_factory->new('supplied');
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function editorial_text($item) {
		$xml = $this->mei_doc->createElement('supplied');
		$metaMark = $this->mei_doc->createElement('metaMark');
		$this->mei_doc->addAttribute($metaMark, 'xml:id', $this->id_factory->new('metaMark', $item->id), $this->id_factory->get_ns());
		$text = $this->mei_doc->createText($item->text);
		$this->mei_doc->addChild($metaMark, $text);
		$this->mei_doc->addChild($xml, $metaMark);
		$xmlid = $this->id_factory->new('supplied');
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}	
	public function ligature($item) {
		$xml = $this->mei_doc->createElement('ligature');
		$notes = $item->notes()->sorted()->get();
		$type = (strtolower($item->type) == 'currentes' ? 'currentes' : 'square');
		foreach($notes as $note) {
			$this->mei_doc->addChild($xml, $this->note($note));
		}
		$this->mei_doc->addAttribute($xml, 'type', $type);
		$xmlid = $this->id_factory->new('ligature', $item->id);
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	public function note($item) {
		$xml = $this->mei_doc->createElement('note');
		$guideline_i = $this->guidelines->nearest_guideline_i($item->comp_x, $item->comp_y);
		$pitch = $this->guidelines->get_pitch($guideline_i, $item->shift);
		$this->mei_doc->addAttribute($xml, 'pname', strtolower($pitch->step));
		$accid = (function($v) {
			if($v == 1) {
				return 's';
			}
			if($v == -1) {
				return 'f';
			}
			return null;
		})($pitch->alter);
		if($accid) {
			$this->mei_doc->addAttribute($xml, 'accid.ges', $accid);
		}
		$this->mei_doc->addAttribute($xml, 'oct', $pitch->octave);
		if($item->plica != 0) {
			$plica = $this->mei_doc->createElement('plica');
			if($item->plica > 0) {
				$this->mei_doc->addAttribute($plica, 'dir', 'up');
			} else {
				$this->mei_doc->addAttribute($plica, 'dir', 'down');
			}
			$this->mei_doc->addChild($xml, $plica);
		}
		$xmlid = $this->id_factory->new('note', $item->id);
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		$this->note_store->add_note($item->comp_x, $item->comp_y, $xml);
		return $xml;
	}
	public function syllable($item) {
		$xml = $this->mei_doc->createElement('syl');
		if($item->wordstart) {
			$this->mei_doc->addAttribute($xml, 'wordpos', 'i');
		} else {
			$this->mei_doc->addAttribute($xml, 'wordpos', 'm');
		}
		$text = $this->mei_doc->createText($item->text);
		$this->mei_doc->addChild($xml, $text);
		$xmlid = $this->id_factory->new('syl', $item->id);
		$this->mei_doc->addAttribute($xml, 'xml:id', $xmlid, $this->id_factory->get_ns());
		$this->add_links($item, $xml);
		return $xml;
	}
	private function add_links($type, $node) {
		$item = $type->up()->first();
		if(!$item) {
			return;
		}
		$links = $item->links()->get();
		foreach($links as $link) {
			$this->link_store->add_link($link->id, $node);
		}
	}
}
