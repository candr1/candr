<?php

namespace App\Transcribers;

class TranscriberState {
	private $state;
	private $logger;

	public function __construct($logger = null) {
		$this->state = 0;
		$this->logger = $logger;
	}
	public function set_wait() {
		$this->state = 0;
	}
	public function set_continue() {
		$this->state = 1;
	}
	public function set_stop() {
		$this->state = 2;
	}
	public function is_wait() {
		return $this->state === 0;
	}
	public function is_continue() {
		return $this->state === 1;
	}
	public function is_stop() {
		return $this->state === 2;
	}
	public function __toString() {
		switch($this->state) {
			case 0:
				return 'STATE_WAIT';
			case 1:
				return 'STATE_CONT';
			case 2:
				return 'STATE_STOP';
		}
	}
}


abstract class AbstractTranscriber {
	protected $transcription;
	protected $push_returns;
	public $state;
	protected $logger;

	public function __construct($logger = null) {
		$this->state = new TranscriberState($logger);
		$this->logger = $logger;
		$this->push_returns = new \Ds\Stack();
	}
	protected function item_in_nodes($item, $nodes) {
		if(is_iterable($nodes)) {
			foreach($nodes as $node) {
				if($item->is($node)) {
					return true;
				}
			}
		} else {
			if($item->is($nodes)) {
				return true;
			}
		}
		return false;
	}
	public function transcribe($record, $start = null, $stop = null) {
		if($start === null) {
			$this->state->set_continue();
		}
		// All the items to be transcribed
		$items = $this->get_items($record);
		$pushed_or_state_change = false;
		foreach($items as $item) {
			// Sometimes an item is null idk why
			if(!$item) {
				continue;
			}
			// Is it a start node or stop node?
			$in_start = $this->item_in_nodes($item, $start);
			$in_stop = $this->item_in_nodes($item, $stop);

			// Is it a valid node?
			$item_check = $this->item_check($item);

			// Flag to push to transcription
			$push = false;

			$item_class = get_class($item);
			$transcribe_anyway = [
				'App\Models\Clef',
				'App\Models\EditorialClef',
				'App\Models\Accidental',
				'App\Models\EditorialAccidental'
			];
			// Its a start node
			if($in_start) {
				// Continue transcribing
				$this->state->set_continue();
				$pushed_or_state_change = true;
				if($item_check) {
					// Push this node
					$push = true;
				}
			// Its a stop node
			} else if($in_stop) {
				// Stop transcribing
				$this->state->set_stop();
				$push = false;
				$pushed_or_state_change = true;
			// Its neither a start nor stop node, but we're transcribing
			} else if($this->state->is_continue() && $item_check) {
				$push = true;
			} else if(in_array($item_class, $transcribe_anyway)) {
				$push = true;
			}
			if($push === true) {
				$pushed_or_state_change = true;
				$this->push_returns->push($this->push($item));
			}
		}
		// If we've an empty stave, then nothing is pushed, and so we should continue regardless
		if($pushed_or_state_change !== true) {
			$this->state->set_continue();
		}
		return;
	}
	public function container($record) {
		return;
	}
	public function push($item) {
		// Pop a transcription off
		$transcription = $this->transcription->pop();
		// Add this item to the transcription
		$ret = $transcription->push($item);
		// Push it back onto the stack
		$this->transcription->push($transcription);
		return $ret;
	}
	public function pop() {
		// Pop a transcription off
		$transcription = $this->transcription->pop();
		// Get the top item
		$ret = $transcription->pop();
		// Push it back onto the tack
		$this->transcription->push($transcription);
		return $ret;
	}
	public function page($item) {
		return;
	}
	protected function item_check($record) {
		return true;
	}
	public function get_transcription() {
		$ret = [];
		try {
			while(1) {
				$ret[] = $this->transcription->pop()->finalise();
			}
		} catch (\UnderflowException $e) { }
		return $ret;
	}
}
