<?php

namespace App\Transcribers;

interface InterfaceTranscriber {
	function transcribe($record, $start, $stop);
	function get_transcription();
}
