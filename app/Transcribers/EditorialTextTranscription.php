<?php

namespace App\Transcribers;

class EditorialTextTranscription extends TextTranscription {
	public function finalise() {
		return trim(implode('', array_map(function($item) {
			$space = '';
			if($item->wordstart === true) {
				$space = ' ';
			}
			$txt = $item->text;
			return ($space . $txt);
		}, array_reverse($this->transcription->toArray()))));
	}
}
