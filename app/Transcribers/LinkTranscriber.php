<?php

namespace App\Transcribers;

class LinkTranscriber extends AbstractTranscriber implements InterfaceTranscriber {
	private $starting_system_id;

	public function __construct($starting_system_id, $logger = null) {
		parent::__construct($logger);
		$this->transcription = new \Illuminate\Database\Eloquent\Collection();
		$this->starting_system_id = $starting_system_id;
	}
	protected function get_items($record) {
		if($record->id === $this->starting_system_id) {
			return [];
		}
		$links = $record->links()->whereHas('setting')->with(['items.type'])->get();
		$items = new \Illuminate\Database\Eloquent\Collection();
		foreach($links as $link) {
			$items = $items->merge($link->items->map(function($a) {
				return $a->type;
			}))->unique();
		}
		return $items->sort(function($a, $b) {
			$c = $a->comp_x <=> $b->comp_x;
			if($c) {
				return $c;
			}
			return $b->comp_y <=> $a->comp_y;
		});
	}
	public function transcribe($record, $start = null, $stop = null) {
		parent::transcribe($record, $start, $stop);
		if($this->transcription->isEmpty() === false) {
			$this->state->set_stop();
		}
	}
	public function push($item) {
		$this->transcription = $this->transcription->merge($item->up()->first()->links()->get())->unique();
	}
	public function get_transcription() {
		return $this->transcription;
	}
}
