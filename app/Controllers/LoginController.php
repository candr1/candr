<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class LoginController {
	protected $logger;
	protected $view;
	private $order;
	private $logintime;

	public function __construct($logger, Twig $view, $logintime) {
		$this->logger = $logger;
		$this->view = $view;
		$this->logintime = $logintime;
	}

	protected function user_logged_in() {
		if(!isset($_SESSION['login_time']) || !isset($_SESSION['login_user']) || $_SESSION['login_time'] < (time() - $this->logintime)) {
			$_SESSION['login_time'] = 0;
			unset($_SESSION['login_user']);
			return false;
		}
		$_SESSION['login_time'] = time();
		$user = \App\Models\User::with('transcribing')->find($_SESSION['login_user']);
		if(!$user) {
			error_log("Session data was corrupt. Cannot find user already logged in");
			$_SESSION['login_time'] = 0;
			unset($_SESSION['login_user']);
			return false;
		}
		$uarr = $user->toArray();
		if(isset($uarr['transcribing']) && !is_null($uarr['transcribing'])) {
			$uarr['transcribing']['model_id'] = $user->transcribing->model_id;
			$uarr['transcribing']['view_path'] = $user->transcribing->view_path;
		}
		return $uarr;
	}
	public function login(Request $request, Response $response, array $args) {
		$refhead = $request->getHeader('HTTP_REFERER');
		if($refhead) {
			$ref = array_shift($refhead);
			return $response->withRedirect($ref);
		}
		return $response->withRedirect('/');
	}
	public function logout(Request $request, Response $response, array $args) {
		$_SESSION['login_time'] = 0;
		unset($_SESSION['login_user']);
		$refhead = $request->getHeader('HTTP_REFERER');
		$data['redirect'] = '/';
		if($refhead) {
			$ref = array_shift($refhead);
			$data['redirect'] = $ref;
		}
		return $this->view->render($response, 'logout401.twig', $data);
	}
	public function give401(Request $request, Response $response, array $args) {
		return $response->withStatus(401);
	}
	public function clear_transcribe(Request $request, Response $response, array $args) {
		$referer = array_shift($request->getHeader('HTTP_REFERER'));
		$redirect = '/';
		if($referer) {
			$redirect = $referer;
		}
		if($this->user_logged_in()) {
			$user = \App\Models\User::find($_SESSION['login_user']);
			if(!$user) {
				error_log("Session data was corrupt. Cannot find user already logged in");
				$_SESSION['login_time'] = 0;
				unset($_SESSION['login_user']);
			} else {
				$user->transcribing()->dissociate();
				$user->save();
			}
		}
		return $response->withRedirect($redirect);
	}
	protected function request_all_data($stave, $get_links = false) {
		$eager = ['type'];
		if($get_links) {
			$eager[] = 'links';
			$eager[] = 'links.items';
			$eager[] = 'links.items.up';
			$eager[] = 'links.items.type';
		}
		$items = $stave->items()->with($eager)->get();
		$ret = [];
		$get_synch = function($item) {
			return $item
				->links
				->flatMap(function($l) {
					return $l->items
					->reject(function($a) use(&$item) {
						return $a->is($item);
					})
					->map(function($a) {
						$pa = $a->type->parsed_attributes();
						$stave = $a->up;
						$pa['stave'] = $stave->toArray();
						return $pa;
					});
				})->toArray();
		};
		foreach($items as $item) {
			$id = $item->id;
			$comment = $item->editorial_comment;
			$type = $item->type;
			if(!$type) {
				$item->delete();
				continue;
			}
			$attrs = $type->parsed_attributes();
			$type_class = get_class($type);
			if($type_class == \App\Models\Ligature::class) {
				$notes = $type->notes()->sorted()->get();
				$lret = [];
				foreach($notes as $note) {
					$attr = $note->parsed_attributes();
					$noteit = $note->up()->first();
					if(!isset($attr['id'])) {
						$attr['id'] = [];
					}
					$attr['id']['id'] = $noteit->id;
					if($get_links) {
						$attr['synch'] = $get_synch($noteit);
					}
					$lret[] = $attr;
				}
				$attrs['notes'] = $lret;
			} else if($type_class == \App\Models\Note::class) {
				if($type->ligature()->first()) {
					/* Has ligature, do not add, as will be
					 * retrieved in ligature */
					continue;
				}
			}
			$attrs['editorial comment'] = [
				'editorial comment' => $comment
			];
			if(!isset($attrs['id'])) {
				$attrs['id'] = [];
			}
			$attrs['id']['id'] = $id;
			if($get_links) {
				$attrs['synch'] = $get_synch($item);
			}
			$ret[] = $attrs;
		}
		return $ret;
	}
}
