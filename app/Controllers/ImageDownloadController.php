<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class ImageDownloadController {
	protected $logger;
	private $imagestore;

	public function __construct($logger, $imagestore) {
		$this->logger = $logger;
		$this->imagestore = $imagestore;
	}
	public function get_file(Request $request, Response $response, array $args) {
		$fname = realpath($this->imagestore . '/' . $args['fname']);
		if(!$fname) {
			return $response->withStatus(404)->write('File not found');
		}
		$stream = new \Slim\Http\Stream(fopen($fname, 'rb'));
		return $response->withHeader('Content-Description', 'File Transfer')
		  ->withHeader('Content-Type', 'application/force-download')
		  ->withHeader('Content-Type', 'application/octet-stream')
	  	  ->withHeader('Content-Type', 'application/download')
	  	  ->withHeader('Content-Transfer-Encoding', 'binary')
		  ->withHeader('Content-Disposition', 'attachment;filename="'.basename($fname).'"')
	  	  ->withHeader('Expires', '0')
	  	  ->withHeader('Cache-Control', 'must-revalidate')
		  ->withHeader('Pragma', 'public')
	  	  ->withHeader('Content-Length', filesize($fname))
	  	  ->withBody($stream);
	}
}
