<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class EditSettingController extends EditCategoryController {
	public function new_setting_from_link(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$link = \App\Models\Link::with(['setting', 'system'])->find($args['id']);
		if(!$link) {
			return $this->return_message($response, 'error', 'Link not found', 'list_sources', []);
		}
		$system = $link->system;
		if(!$system) {
			return $this->return_message($response, 'error', 'Link does not belong to a system', 'list_sources', []);
		}
		$setting = $link->setting;
		if($setting) {
			return $this->return_message($response, 'error', 'Link already has a setting associated with this moment', 'view_setting', ['id' => $setting->id]);
		}
		$setting = new \App\Models\Setting();
		$setting->link()->associate($link);
		$setting->save();
		$id = $setting->id;
		return $this->do_redirect($response, 'view_setting', ['id' => $id]);
	}
	public function new_setting_from_item(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$item = \App\Models\Item::with('up.up')->find($args['id']);
		if(!$item) {
			return $this->return_message($response, 'error', 'Item not found', 'list_sources', []);
		}
		$link = new \App\Models\Link();
		$system = $item->up()->first()->up()->first();
		$link->system()->associate($system);
		$link->save();
		$link->items()->attach($item->id);
		$setting = new \App\Models\Setting();
		$setting->link()->associate($link);
		$setting->save();
		$id = $setting->id;
		return $this->do_redirect($response, 'view_setting', ['id' => $id]);
	}

	public function edit_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Setting::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Setting not found', 'list_settings', []);
		}
		return $this->view->render($response, 'edit/setting/title.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$title = $this->get_post_or_get($request, 'value');
		if($title) {
			$record = \App\Models\Setting::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Setting not found', 'list_settings', []);
			}
			$record->name = $title;
			$record->save();
			return $this->do_redirect($response, 'view_setting', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_setting_title', ['id' => $args['id']]);
	}
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Setting::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Setting not found', 'list_settings', []);
		}
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'Setting deleted', 'list_settings', []);
		}
		return $this->confirm_message($response, 'Setting will be deleted completely. Continue?', ['delete_setting', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_setting', ['id' => $args['id']]]);
	}
}
