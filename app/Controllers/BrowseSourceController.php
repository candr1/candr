<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseSourceController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		$items = \App\Models\Source::orderBy('name', 'ASC')->get()->toArray();
		return $this->view->render($response, 'browse/listcategory.twig', [
			'items' => $items,
			'routename' => 'view_source',
			'newroutename' => 'new_source',
			'category' => 'source',
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\Source::with([
			'down' => function($q) { return $q->sorted(); },
			'down.down' => function($q) { return $q; },
			'current_transcribers' => function($q) { return $q; },
			'all_transcribers' => function($q) { return $q; }
		])->find($args['id']);
		if(!$record) {
			return $this->view->render($response, 'browse/sources.twig', []);
		}
		$folios = $record->down->makeVisible('down');
		$folio_array = $folios->toArray();
		$image = null;
		foreach($folio_array as &$fol) {
			$facsimiles = $fol['down'];
			foreach($facsimiles as $fac) {
				if($fac['image']) {
					$fol['image'] = $fac['id'];
					break;
				}
			}
		}
		$archive_name = $archive_url = null;
		if($record->archive) {
			try {
				$rdf = \EasyRdf\Graph::newAndLoad($record->archive);
				$tries = [
					['dbp:nativeName', null],
					['dbp:name', null],
					['foaf:name', null],
					['rdfs:label', 'en'],
					['rdfs:label', null]
				];
				\EasyRdf\RdfNamespace::set('dbp', 'http://dbpedia.org/property/');
				$found = False;
				foreach($tries as $lookup_arr) {
					$lookup = $lookup_arr[0];
					$lang = $lookup_arr[1];
					$archive_resource = $rdf->resourcesMatching($lookup);
					if(count($archive_resource) > 0) {
						$archive = $archive_resource[0];
						if($archive) {
							$found = True;
							$archive_name = trim($archive->getLiteral($lookup, $lang));
							if(!$archive_name) {
								$found = False;
							}
							$url_tries = ['foaf:homepage'];
							foreach($url_tries as $url_lookup) {
								$archive_url = $archive->getLiteral($url_lookup);
								if(!$archive_url) {
									$archive_url = $archive->get('foaf:isPrimaryTopicOf');
								} else {
									break;
								}
							}
							if($found) {
								break;
							}
						}
					}
				}
			} catch(Exception $e) { }
		}
		return $this->view->render($response, 'browse/sources.twig', [
			'source' => $record->toArray(),
			'archive' => [
				'name' => $archive_name,
				'url' => $archive_url,
			],
			'up' => [],
			'down' => $folio_array,
			'loggedin' => $this->user_logged_in(),
		]);
	}
}
