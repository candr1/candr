<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseSettingController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		$settings = \App\Models\Setting::with(['link.system.transcribe.up.up', 'link.system.transcribe.down.pivot.warp'])->get();
		foreach($settings as &$setting) {
			$link = $setting->link;
			if(!$link) {
				continue;
			}
			$system = $link->system;
			if(!$system) {
				continue;
			}
			$transcribe = $system->transcribe;
			$folio = $transcribe->up;
			if($folio) {
				$source = $folio->up;
			}
			$setting['source'] = $source;
			$setting['system'] = $system;
			$setting['transcribe'] = $transcribe;
			$setting['folio'] = $folio->makeVisible(['order_column']);
			$setting->makeVisible(['source', 'folio', 'transcribe']);
		}
		$settings = $settings->sort(function($a, $b) {
			$namecmp = strcmp($a['source']['name'], $b['source']['name']);
			if($namecmp) {
				return $namecmp;
			}
			$ordercmp = $a['folio']['order_column'] - $b['folio']['order_column'];
			if($ordercmp) {
				return $ordercmp;
			}
			$ordercmp = strcmp($a['folio']['name'], $b['folio']['name']);
			if($ordercmp) {
				return $ordercmp;
			}
			$get_warp_centroid = function($i) {
				$transcribe = $i['transcribe'];
				foreach($transcribe->down as $other_system) {
					if($other_system->is($i['system'])) {
						return $other_system->pivot->warp->centroid;
					}
				}
				return [0, 0];
			};
			$a_centroid = $get_warp_centroid($a);
			$b_centroid = $get_warp_centroid($b);;
			$ordercmp = $a_centroid[1] - $b_centroid[1];
			if($ordercmp) {
				return $ordercmp;
			}
			$ordercmp = $a_centroid[0] - $b_centroid[0];
			return $ordercmp;
		})->toArray();
		return $this->view->render($response, 'browse/listsettings.twig', [
			"settings" => $settings,
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\Setting::with(['link.system.up', 'link.system.transcribe.up.up'])->find($args['id']);
		if(!$record) {
			return $this->view->render($response, 'browse/settings.twig', []);
		}
		$link = $record->link;
		$system = $facsimiles = $transcribe = $folio = $source = null;
		if($link) {
			$system = $link->system;
			if($system) {
				$facsimiles = $system->up;
				$transcribe = $system->transcribe;
				if($facsimiles && $transcribe) {
					foreach($facsimiles as &$facsimile) {
						$warp = $facsimile->pivot->warp;
						if($warp) {
							$transcribe['warp'] = $warp;
							$transcribe = $transcribe->makeVisible('warp');
							break;
						}
					}
				}
				$folio = $transcribe->up;
				if($folio) {
					$source = $folio->up;
				}
			}
		}
		return $this->view->render($response, 'browse/settings.twig', [
		//return $response->withJson([
			'setting' => $record->toArray(),
			'up' => [
				'view_source' => $source->toArray(),
				'view_folio' => $folio->toArray(),
				'view_facsimile' => $transcribe->toArray(),
				'view_system' => $system->toArray(),
			],
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function text(Request $request, Response $response, array $args) {
		$walker = new \App\Walkers\TextWalker($this->logger);
		return $this->setting_text_walking($request, $response, $args, $walker, 'text');
	}
	public function text_editorial(Request $request, Response $response, array $args) {
		$walker = new \App\Walkers\EditorialTextWalker($this->logger);
		return $this->setting_text_walking($request, $response, $args, $walker, 'text_editorial');
	}
	public function setting_text_walking(Request $request, Response $response, array $args, $walker, $cache_type) {
		$record = \App\Models\Setting::with(['link.items.type', 'link.system'])->find($args['id']);
		if(!$record) {
			return $this->plain_text($response);
		}
		// cache?
		$cache = $this->get_cache($record, $cache_type);
		if($cache) {
			return $this->plain_text($response, $cache->text);
		}
		// no cache

		// link of this setting
		$link = $record->link;
		// starting items are the items related to this link
		$start_items = $link->items->map(function($a) {
		      return $a->type;
		});
		// system on which this link resides
		$system = $link->system;

		$link_walker = new \App\Walkers\LinkWalker($system->id, $this->logger);
		$link_walker->walk($record, true);

		// find all the staves that we visited
		$staves_walked_ids = $link_walker->get_ids_of_class('App\Models\Stave');
		// add them to the related caches table
		$record->related_caches()->sync($staves_walked_ids);

		// back to walking...
		// link walker returns the next links with setting attached
		$end_links = $link_walker->get_transcription();
		$end_items = $end_links->flatMap(function($b) {
			return $b->items->map(function($a) {
				return $a->type;
			});
		});

		// now just the same as usual
		$walker->walk($system, true, $start_items, $end_items);
		$transcription = $walker->get_transcription();
		$this->make_cache($record, $transcription, $cache_type);
		return $this->plain_text($response, $transcription);
	}
	public function mei(Request $request, Response $response, array $args) {
		$record = \App\Models\Setting::with(['link.items.type', 'link.system'])->find($args['id']);
		if(!$record) {
			return $this->plain_text($response);
		}
		// cache?
		$cache = $this->get_cache($record, 'mei');
		if($cache) {
			return $this->xml_response($response, $cache->text);
		}
		// no cache
		// link of this setting
		$link = $record->link;
		// starting items are the items related to this link
		$start_items = $link->items->map(function($a) {
		      return $a->type;
		});
		// system on which this link resides
		$system = $link->system;

		$link_walker = new \App\Walkers\LinkWalker($system->id, $this->logger);
		$link_walker->walk($record, true);

		// link walker returns the next links with setting attached
		$end_links = $link_walker->get_transcription();
		$end_items = $end_links->flatMap(function($b) {
			return $b->items->map(function($a) {
				return $a->type;
			});
		});

		// now just the same as usual
		$mei_walker = new \App\Walkers\MEIWalker($this->logger);
		$mei_walker->walk($system, true, $start_items, $end_items);
		$transcription = $mei_walker->get_transcription();
		$this->make_cache($record, $transcription, 'mei');
		return $this->xml_response($response, $transcription);
	}
}
