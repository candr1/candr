<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class MachineLearningController {
	protected $logger;
	private $public_key;
	private $key_recipient;
	private $key_timeout;
	private $secret_bytes;
	private $gnupg_home;

	public function __construct($logger, $public_key, $key_timeout = 600, $secret_bytes = 256, $gnupg_home = '~/.gnupg/') {
		$this->logger;
		$this->public_key = $public_key;
		$this->key_timeout = $key_timeout;
		$this->secret_bytes = $secret_bytes;
		$this->gnupg_home = $gnupg_home;
		$this->initial_status = "No status set";
	}
	protected function remove_expired_keys() {
		\App\Models\MachineLearningChallenge::where('expires', '<', \Carbon\Carbon::now())->delete();
		return true;
	}
	protected function status_resetter() {
		return $this->status_setter($this->initial_status);
	}
	protected function status_getter() {
		$status = \App\Models\MachineLearningStatus::first();
		if(!$status) {
			return $this->status_resetter();
		}
		return $status;
	}
	protected function status_setter($status) {
		$status = \App\Models\MachineLearningStatus::firstOrCreate([], ["status" => $status]);
		return $status;
	}
	protected function do_encrypt($data) {
		$gpg = new \Crypt_GPG([
			'homedir' => $this->gnupg_home
		]);
		$import_ret = $gpg->importKey($this->public_key);
		if(!$import_ret) {
			return false;
		}
		$fingerprint = $import_ret['fingerprint'];
		$gpg->addEncryptKey($fingerprint);
		$encrypted = $gpg->encrypt($data, false);
		return base64_encode($encrypted);
	}

	public function get_status(Request $request, Response $response) {
		$status = $this->status_getter();
		$diff = \Carbon\Carbon::now()->diffInSeconds($status->last_updated);
		if($diff > $this->key_timeout) {
			return $response->withJson($this->status_resetter());
		}
		return $response->withJson($status);
	}
	public function get_challenge(Request $request, Response $response) {
		$secret = base64_encode(random_bytes($this->secret_bytes));
		$encrypted = $this->do_encrypt($secret);
		$challenge = new \App\Models\MachineLearningChallenge;
		$challenge->challenge = $encrypted;
		$challenge->answer = $secret;
		$challenge->expires = \Carbon\Carbon::now()->addSeconds($this->key_timeout);
		$challenge->save();
		return $response->withJson($challenge);
	}
	public function set_status(Request $request, Response $response) {
		$this->remove_expired_keys();
		$data = json_decode($request->getBody());
		$check_keys = ["answer", "challenge", "status"];
		if(count(array_intersect_key(array_flip($check_keys), $data)) !== count($check_keys)) {
			return $response->withStatus(400)->write(
				"I need the challenge you were answering and " .
				"your answer as well as your status. We have " .
				"keys: " . implode(", ", array_keys($data)));
		}
		$answer = $data['answer'];
		$challenge_response = $data['challenge'];
		$challenge = \App\Models\MachineLearningChallenge::where('challenge', $challenge_response)->first();
		if(!$challenge) {
			return $response->withStatus(400)->write(
				"That challenge was not in the database, it m" .
				"ay have expired");
		}
		if($challenge->answer !== $answer) {
			return $response->withStatus(403)->write("Wrong answer");
		}
		$challenge->delete();

		return $this->status_setter($data['status']);
	}
}
