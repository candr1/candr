<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class EditSystemController extends EditCategoryController {
	use \App\ModelTraits\ModelImage;
	use \App\ModelTraits\ModelNodes;

	private function do_stave_node_set(Request $request, Response $response, array $args, $record) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$json_nodes = $this->get_post_or_get($request, 'nodes');
		if(!$json_nodes) {
			return $this->return_message($response, 'error', 'No nodes submitted', 'edit_facsimile_systems', ['id' => $args['id']]);
		}
		$nodes = json_decode($json_nodes, true);
		if(!is_array($nodes)) {
			return $this->return_message($response, 'error', 'Node data was corrupt', 'edit_facsimile_systems', ['id' => $args['id']]);
		}
		foreach($nodes as $id => $nodevals) {
			if(!is_int($id) && !is_numeric($id)) {
				return $this->return_message($response, 'error', 'Stave ID \'' . $id . '\' is invalid', 'edit_system_staves', ['id' => $args['id']]);
			}
			if(!is_array($nodevals)) {
				return $this->return_message($response, 'error', 'Stave ID \'' . $id . '\' values are not an array', 'edit_system_staves', ['id' => $args['id']]);
			}
			foreach($nodevals as $node) {
				if(!is_array($node) || count($node) != 2) {
					return $this->return_message($response, 'error', 'Stave ID \'' . $id . '\' values do not contain valid node data', 'edit_system_staves', ['id' => $args['id']]);
				}
				if(!is_int($node[0]) || !is_int($node[1])) {
					return $this->return_message($response, 'error', 'Stave ID \'' . $id . '\' values are not integers', 'edit_system_staves', ['id' => $args['id']]);
				}
			}
		}
		$staves = $record->down()->get();
		foreach($staves as $stave) {
			if(array_key_exists($stave['id'], $nodes)) {
				$warp = $stave->warp()->first();
				$warp->nodes = $nodes[$stave['id']];
				$warp->save();
				unset($nodes[$stave['id']]);
			}
		}
		if(!empty($nodes)) {
			$this->return_message($response, 'warning', 'Stave ID(s) ' . implode(', ', array_keys($nodes)) . ' were not found in this system', 'edit_system_staves', ['id' => $record['id']]);
		}
		return true;
	}
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\System::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'System deleted', 'list_sources', []);
		}
		return $this->confirm_message($response, 'System and all its staves will be deleted completely. Continue?', ['delete_system', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_system', ['id' => $args['id']]]);
	}
	public function edit_transcribe(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\System::with(['up', 'transcribe'])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		$facsimiles = $record->up;
		foreach($facsimiles as &$facsimile) {
			if(!$facsimile) {
				continue;
			}
			$warp = $facsimile->pivot->warp()->first();
			$facsimile['warp'] = $warp;
			$facsimile = $facsimile->makeVisible('warp');
		}
		return $this->view->render($response, 'edit/system/transcribe.twig', [
			'record' => $record->makeHidden(['up'])->toArray(),
			'facsimiles' => $facsimiles->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_transcribe(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\System::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		$facsimile = $record->up()->find($args['tid']);
		if(!$facsimile) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$record->transcribe()->associate($facsimile);
		$record->save();
		return $this->do_redirect($response, 'view_system', ['id' => $record['id']]);
	}
	public function add_new_stave(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\System::with('up')->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		$ret = $this->do_stave_node_set($request, $response, $args, $record);
		if($ret !== true) {
			return $ret;
		}
		$n_staves = intval($this->get_post_or_get($request, 'n'));
		if(!$n_staves  || $n_staves < 1) {
			$n_staves = 1;
		}
		$n_present_staves = $record->down()->count();
		$system_warp = $record->up->first()->pivot->warp()->first()->nodes;
		$bound = $this->bounding_box($system_warp);
		$imdim = [$bound[1][0] - $bound[0][0], $bound[2][1] - $bound[0][1]];
		$width = intdiv($imdim[0], 3);
		$height = intdiv($imdim[1], $n_staves * 2 + 1);
		$old_height = $height;
		$old_width = $width;
		for($i = 0;$i < $n_staves; $i++) {
			$stave = new \App\Models\Stave;
			$stave->save();
			$warp = new \App\Models\Warp;
			$x = $width;
			$width *= 2;
			$y = $height * (($i * 2) + 1);
			$height += $y;
			$warp->nodes = [[$x, $y], [$width, $y], [$width, $height], [$x, $height]];
			if($n_staves == 1 && $n_present_staves == 0) {
				$warp->nodes = [[0, 0], [$imdim[0], 0], [$imdim[0], $imdim[1]], [0, $imdim[1]]];
			}
			$warp->save();
			$stave->warp()->associate($warp);
			$stave->save();
			$record->down()->save($stave);
			$height = $old_height;
			$width = $old_width;
		}
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'edit_system_staves', ['id' => $record['id']]);
	}
	public function set_stave_nodes(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\System::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		$ret = $this->do_stave_node_set($request, $response, $args, $record);
		if($ret === true) {
			return $this->do_redirect($response, 'edit_system_staves', ['id' => $record['id']]);
		}
		return $ret;
	}
	public function edit_staves(Request $request, Response $response, array $args) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\System::with(['down.warp', 'up', 'transcribe.up'])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		$transcribe = $record->transcribe;
		if(!$transcribe) {
			return $this->return_message($response, 'error', 'Transcribing system not set', 'view_system', ['id' => $record['id']]);
		}
		$staves = $record->down;
		foreach($staves as &$stave) {
			$warp = $stave->warp;
			$stave['warp'] = $warp;
		}
		$staves = $staves->sort(function($a, $b) {
			$a_centroid = $a->warp->centroid;
			$b_centroid = $b->warp->centroid;
			$c = $a_centroid[1] <=> $b_centroid[1];
			if($c) {
				return $c;
			}
			return ($a_centroid[0] <=> $b_centroid[0]);
		});
		$folio = $transcribe->up;
		$facsimiles = $record->up;
		$warp = null;
		foreach($facsimiles as $f) {
			if(!$f->is($transcribe)) {
				continue;
			}
			$warp = $f->pivot->warp()->first();
			break;
		}
		if(!$warp) {
			return $this->return_message($response, 'error', 'Could not locate warp data of transcribing facsimile', 'view_system', ['id' => $record['id']]);
		}
		$processed = $record->makeHidden(['down', 'up'])->makeVisible('transcribe')->toArray();
		$processed['transcribe']['warp'] = $warp->toArray();
		return $this->view->render($response, 'edit/system/staves.twig', [
			'record' => $processed,
			'down' => $staves->toArray(),
			'folio' => ($folio ? $folio->toArray() : null),
			'loggedin' => $loggedin,
		]);
	}
}
