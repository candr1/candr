<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLChunkController extends XMLGenericChunkController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'chunk-' . $args['id'] . '.xml');
		$record = \App\Models\Link::with(['items.up', 'system.transcribe.set', 'system.up.down', 'system.transcribe.up.up', 'items.links.items'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		/* Write root element */
		$this->xw->startElementNs($this->candrns, "chunk", $this->candrns_endpoint);
		$this->add_id($this->xw, "chunk", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
		$xinclude = !(isset($args['xinclude']) && $args['xinclude'] == 'xinclude');
		$this->make_chunk($record, $xinclude);
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
