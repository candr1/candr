<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLFacsimileSetController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'facsimileset-' . $args['id'] . '.xml');
		$record = \App\Models\FacsimileSet::with(['facsimiles'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->xw->startElementNs($this->candrns, "facsimileSet", $this->candrns_endpoint);
		$this->add_id($this->xw, "facsimileSet", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$this->xw->writeAttributeNs($this->candrns, "name", $this->candrns_endpoint, $record->name);
		$this->xw->writeAttributeNs($this->candrns, "copyrightNotice", $this->candrns_endpoint, $record->copyright_notice);
		$this->xw->writeAttributeNs($this->candrns, "copyrightURL", $this->candrns_endpoint, $record->copyright_url);
		$this->xw->writeAttributeNs($this->candrns, "worldVisible", $this->candrns_endpoint, $record->world_visible ? "true" : "false");
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
		{
			$facsimiles = $record->facsimiles()->get();
			foreach($facsimiles as $facsimile) {
				$this->xw->startElementNs($this->candrns, "facsimileDef", $this->candrns_endpoint);
				$this->add_dates($facsimile);
				$this->add_id($this->xw, "facsimileDef", $facsimile->id);
				$this->xw->endElement();
			}
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
