<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class EditFolioController extends EditCategoryController {
	public function new(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = new \App\Models\Folio;
		$record->save();
		$id = $record->id;
		return $this->do_redirect($response, 'view_folio', ['id' => $id]);
	}
	public function edit_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		return $this->view->render($response, 'edit/folio/title.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$title = $this->get_post_or_get($request, 'value');
		if($title) {
			$record = \App\Models\Folio::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
			}
			$record->name = $title;
			$record->save();
			return $this->do_redirect($response, 'view_folio', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_folio_title', ['id' => $args['id']]);
	}
	public function edit_source(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$first = $record->up();
		$source = $first ? $first->toArray() : null;
		$others = \App\Models\Source::orderBy('name')->get();
		return $this->view->render($response, 'edit/folio/source.twig', [
			'record' => $record->toArray(),
			'source' => $source,
			'history' => $this->get_history_classes($record, 'source_id', \App\Models\Source::class),
			'others' => $others->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_source(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$source = \App\Models\Source::find($args['tid']);
		if(!$source) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		$record->up()->associate($source);
		$record->save();
		return $this->do_redirect($response, 'view_folio', ['id' => $args['id']]);
	}
	public function edit_facsimiles(Request $request, Response $response, array $args) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::with([
			'down' => function($q) { return $q->orderBy('name', 'ASC'); }
		])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$facsimiles = $record->down;
		$others = \App\Models\Facsimile::doesntHave('up')->get();
		return $this->view->render($response, 'edit/folio/facsimiles.twig', [
			'record' => $record->toArray(),
			'facsimiles' => $facsimiles->toArray(),
			'others' => $others->toArray(),
			'loggedin' => $loggedin,
		]);
	}
	public function add_new_facsimile(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$facsimile = new \App\Models\Facsimile;
		$record->down()->save($facsimile);
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'view_facsimile', ['id' => $facsimile['id']]);
	}
	public function add_facsimile(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$facsimile = \App\Models\Facsimile::find($args['tid']);
		if(!$facsimile) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'edit_folio_facsimiles', ['id' => $args['id']]);
		}
		$record->down()->save($facsimile);
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'view_folio', ['id' => $args['id']]);
	}
	public function remove_facsimile(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$facsimile = $record->down()->find($args['tid']);
		if(!$facsimile) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'edit_folio_facsimiles', ['id' => $args['id']]);
		}
		$facsimile->up()->dissociate();
		$facsimile->save();
		return $this->return_message($response, 'success', 'Facsimile removed', 'view_folio', ['id' => $args['id']]);
	}
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'Folio deleted', 'list_sources', []);
		}
		return $this->confirm_message($response, 'Folio will be deleted completely. Continue?', ['delete_folio', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_folio', ['id' => $args['id']]]);
	}
}
