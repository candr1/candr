<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseFolioController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		error_log("Cannot list folios.");
		return $response->withJson(['message' => 'Cannot list folios.']);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\Folio::with([
			'up' => function($q) { return $q; },
			'up.down' => function($q) { return $q->sorted(); },
			'down' => function($q) { return $q->orderBy('name'); },
			'down.set' => function($q) { return $q; },
			'current_transcribers' => function($q) { return $q; },
			'all_transcribers' => function($q) { return $q; },
			'down.all_transcribers' => function($q) { return $q; }, 
			'down.down.all_transcribers' => function($q) { return $q; },
			'down.down.down.all_transcribers' => function($q) { return $q; }
		])->find($args['id']);
		if(!$record) {
			return $this->view->render($response, 'browse/folios.twig', []);
		}
		$facsimiles = $record->down->makeVisible('set');
		$source = $record->up;
		$all_folios = $source->down()->sorted()->get();
		$adjacent = $this->find_adjacent($all_folios, $record);
		$all_transcribers = $this->add_transcribers($record->all_transcribers);
		foreach($facsimiles as $facsimile) {
			$all_transcribers = $this->add_transcribers($facsimile->all_transcribers, $all_transcribers);
			$systems = $facsimile->down()->get();
			foreach($systems as $system) {
				$all_transcribers = $this->add_transcribers($system->all_transcribers, $all_transcribers);
				$staves = $system->down()->get();
				foreach($staves as $stave) {
					$all_transcribers = $this->add_transcribers($stave->all_transcribers, $all_transcribers);
				}
			}
		}
		return $this->view->render($response, 'browse/folios.twig', [
			'folio' => $record->toArray(),
			'adjacent' => $adjacent,
			'up' => [
				'view_source' => $source
			],
			'down' => $facsimiles->toArray(),
			'all_transcribers' => $all_transcribers,
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function text(Request $request, Response $response, array $args) {
		$record = \App\Models\Folio::find($args['id']);
		$walker = new \App\Walkers\TextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text', $walker));
	}
	public function text_editorial(Request $request, Response $response, array $args) {
		$record = \App\Models\Folio::find($args['id']);
		$walker = new \App\Walkers\EditorialTextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text_editorial', $walker));
	}
	public function mei(Request $request, Response $response, array $args) {
		$record = \App\Models\Folio::find($args['id']);
		if(!$record) {
			return $this->plain_text($response);
		}
		// cache?
		$cache = $this->get_cache($record, 'mei');
		if($cache) {
			return $this->xml_response($response, $cache->text);
		}
		// no cache
		$walker = new \App\Walkers\MEIWalker($this->logger);
		$walker->walk($record);
		$transcription = $walker->get_transcription();
		$this->make_cache($record, $transcription, 'mei');
		return $this->xml_response($response, $transcription);
	}
}
