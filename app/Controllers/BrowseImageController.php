<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseImageController extends BrowseCategoryController {
	use \App\ModelTraits\ModelImage;

	public function list(Request $request, Response $response, array $args) {
		error_log("Cannot list images.");
		return $response->withJson(['message' => 'Cannot list images.']);
	}
	public function view(Request $request, Response $response, array $args) {
		$im = \App\Models\Facsimile::where('id', $args['id'])->with('set')->first();
		$get_thumb = false;
		if(isset($args['thumb']) && $args['thumb'] === "thumb") {
			$get_thumb = true;
		}	
		if($im->set && $im->set->world_visible != true && !$this->user_logged_in()) {
			$body = $response->getBody();
			$imagefn = $this->get_image($im, $get_thumb);
			$info = getimagesize($imagefn);
			$bl = imagecreate($info[0], $info[1]);
			$background = imagecolorallocate($bl, 255, 255, 255);
			$black = imagecolorallocate($bl, 0, 0, 0);
			imagestring($bl, 5, 0, 0, "You must be logged in to view this image", $black);
			ob_start();
			imagepng($bl);
			$image_data = ob_get_contents();
			ob_end_clean();
			// write "you must be logged in" image
			$body->write($image_data);
			$mime = "image/png";
			$expiry = (new \Carbon\Carbon('UTC'))->format('D, d M Y H:i:s \G\M\T');
			return $response
				->withHeader('Content-Type', $mime)
				->withHeader('Cache-Control', 'public, max-age=0, must-revalidate, no-cache')
				->withHeader('Pragma', 'no-cache')
				->withHeader('Expires', $expiry);
		}
		$imagefn = $this->get_image($im, $get_thumb);
		$contents = file_get_contents($imagefn);
		$mime = \mime_content_type($imagefn);
		$body = $response->getBody();
		$body->write($contents);
		$expiry = (new \Carbon\Carbon('UTC'))->addSeconds(3600)->format('D, d M Y H:i:s \G\M\T');
		$nr = $response
			->withHeader('Content-Type', $mime)
			->withHeader('Cache-Control', 'public, max-age=3600, immutable')
			->withHeader('Pragma', 'cache')
			->withHeader('Expires', $expiry);
		return $nr;
	}
}
