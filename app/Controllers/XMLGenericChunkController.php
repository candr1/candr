<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

abstract class XMLGenericChunkController extends XMLGenericController {
	protected function make_chunk($record, $xinclude = false) {
		$rec_sys = $record->system()->first();
		$rec_fac = $rec_fol = $rec_src = null;
		if($rec_sys) {
			$rec_fac = $rec_sys->transcribe()->first();
			if($rec_fac) {
				$rec_fol = $rec_fac->up()->first();
				if($rec_fol) {
					$rec_src = $rec_fol->up()->first();
				}
			}
		}
		$ret = false;
		if($rec_src) {
			$ret = $this->make_source($rec_src, $rec_fol, $rec_fac, $rec_sys, $record, $xinclude);
		} elseif($rec_fol) {
			$ret = $this->make_folio($rec_fol, $rec_fac, $rec_sys, $record, $xinclude);
		} elseif($rec_fac) {
			$ret = $this->make_facsimile($rec_fac, $rec_sys, $record, [], $xinclude);
		} elseif($rec_sys) {
			$ret = $this->make_system($rec_sys, $record, $xinclude);
		}
		return $ret;
	}
	protected function make_source($source, $folio_start = null, $facsimile_start = null, $system_start = null, $link_start = null, $xinclude = false) {
		if(!$source) {
			return null;
		}
		$source_folios = $source->down()->sorted()->get();
		if(!$source_folios) {
			return null;
		}
		$this->start_element("source");
		$this->add_id("source", $source->id);
		$this->add_dates($source);
		if($source->name) {
			$this->write_attribute("name", $source->name);
		}
		if($source->archive) {
			$this->write_attribute("archive", $source->archive);
		}
		{
			$this->start_element("folios");
			if($folio_start) {
				$writeit = false;
				foreach($source_folios as $source_folio) {
					if(!$writeit) {
						if($source_folio->is($folio_start)) {
							$writeit = true;
							$mf_ret = $this->make_folio($source_folio, $facsimile_start, $system_start, $link_start, $xinclude);
							if($mf_ret) {
								$this->end_element("folios");
								$this->end_element("source");
								return $mf_ret;
							}
						}
					} else {
						$mf_ret = $this->make_folio($source_folio, null, null, null, $xinclude);
						if($mf_ret) {
							$this->end_element("folios");
							$this->end_element("source");
							return $mf_ret;
						}
					}
				}
			} else {
				foreach($source_folios as $source_folio) {
					$mf_ret = $this->make_folio($source_folio, null, null, null, $xinclude);
					if($mf_ret) {
						$this->end_element("folios");
						$this->end_element("source");
						return $mf_ret;
					}
				}
			}
			$this->end_element("folios");
		}
		$this->end_element("source");
		return false;
	}
	protected function make_folio($folio, $facsimile_start = null, $system_start = null, $link_start = null, $xinclude) {
		if(!$folio) {
			return null;
		}
		$folio_facsimiles = $folio->down()->get();
		if(!$folio_facsimiles) {
			return null;
		}
		$sorted = $folio_facsimiles->sortBy(function($facsimile) {
				return $facsimile->down()->count();
		});
		$this->start_element("folio");
		$this->add_id("folio", $folio->id);
		$this->add_dates($folio);
		if($folio->name) {
			$this->write_attribute("name", $folio->name);
		}
		{
			if($facsimile_start) {
				$facsimile_start_systems = $facsimile_start->down()->get();
				$other_facsimiles = $sorted->filter(function($facsimile) use(&$facsimile_start) {
					if($facsimile->is($facsimile_start)) {
						return false;
					}
					$facsimile_systems = $facsimile->down()->get();
					foreach($facsimile_systems as $system) {
						$system_transcribe = $system->transcribe()->first();
						if($system_transcribe && !$system_transcribe->is($facsimile_start)) {
							return true;
						}
					}
					return false;
				});
				$mf_ret = $this->make_facsimile($facsimile_start, $system_start, $link_start, [], $xinclude);
				if($mf_ret) {
					$this->end_element("folio");
					return $mf_ret;
				}
				foreach($other_facsimiles as $other_facsimile) {
					$mf_ret = $this->make_facsimile($other_facsimile, null, null, $system_start, $xinclude);
					if($mf_ret) {
						$this->end_element("folio");
						return $mf_ret;
					}
				}
			} else {
				foreach($sorted as $facsimile) {
					$mf_ret = $this->make_facsimile($facsimile, null, null, [], $xinclude);
					if($mf_ret) {
						$this->end_element("folio");
						return $mf_ret;
					}
				}
			}

		}
		$this->end_element("folio");
		return false;
	}
	protected function make_facsimileset($facsimileset) {
		$this->start_element("facsimileSet");
		$this->add_id("facsimileSet", $facsimileset->id);
		$this->write_attribute("name", $facsimileset->name);
		$this->write_attribute("copyrightNotice", $facsimileset->copyright_notice);
		$this->write_attribute("copyrightURL", $facsimileset->copyright_url);
		$this->write_attribute("worldVisible", $facsimileset->world_visible ? "true" : "false");
		$this->add_dates($facsimileset);
		$facsimiles = $facsimileset->facsimiles()->get();
		foreach($facsimiles as $facsimile) {
			$this->start_element("facsimileDef");
			$this->add_dates($facsimile);
			$this->add_id("facsimileDef", $facsimile->id);
			$this->end_element("facsimileDef");
		}
		$this->end_element("facsimileSet");
	}
	protected function make_facsimile($facsimile, $system_start = null, $link_start = null, $system_exclude = [], $xinclude = false) {
		if(!$facsimile) {
			return null;
		}
		$facsimile_systems = $facsimile->down()->get();
		if(!$facsimile_systems) {
			return null;
		}
		$this->start_element("facsimile");
		$this->add_id("facsimile", $facsimile->id);
		$this->add_dates($facsimile);
		if($facsimile->name) {
			$this->write_attribute("name", $facsimile->name);
		}
		$this->write_attribute("image", $this->router->pathFor('image', ['id' => $facsimile->id]));
		{
			$facsimile_set = $facsimile->set()->first();
			if($facsimile_set) {
				if($xinclude) {
					$this->include_link($this->router->pathFor('set_get_xml', ['id' => $facsimile_set->id]));
				} else {
					$this->make_facsimileset($facsimile_set);
				}
			}
			$all_minx = $all_miny = INF;
			foreach($facsimile_systems as $system) {
				$warp = $system->pivot->warp()->first();
				if(!$warp) {
					continue;
				}
				$nodes = $warp->nodes;
				foreach($nodes as $node) {
					if($node[0] < $all_minx) {
						$all_minx = $node[0];
					}
					if($node[1] < $all_miny) {
						$all_miny = $node[1];
					}
				}
			}
			$sorted = $facsimile_systems->sortBy(function($system) use(&$all_minx, &$all_miny) {
				$warp = $system->pivot->warp()->first();
				if(!$warp) {
					return "";
				}
				$nodes = $warp->nodes;
				$minx = $miny = INF;
				foreach($nodes as $node) {
					if($node[0] < $minx) {
						$minx = $node[0];
					}
					if($node[1] < $miny) {
						$miny = $node[1];
					}
				}
				$add_x = $minx - $all_minx;
				$add_y = $miny - $all_miny;
				$fmt = str_pad(number_format($add_x, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($add_y, 2, '.', ''), 25, "0", STR_PAD_LEFT);
				return $fmt;
			})->filter(function($system) use(&$system_exclude) { 
				foreach($system_exclude as $sys_ex) {
					if($system->is($sys_ex)) {
						return false;
					}
				}
				return true;
			});
			$this->start_element("systems");
			$writeit = false;
			foreach($facsimile_systems as $facsimile_system) {
				if(!$writeit) {
					if($facsimile_system->is($system_start)) {
						$writeit = true;
						$ms_ret = $this->make_system($system_start, $link_start, $xinclude);
						if($ms_ret) {
							$this->end_element("systems");
							$this->end_element("facsimile");
							return $ms_ret;
						}
					}
				} else {
					$ms_ret = $this->make_system($facsimile_system, null, $xinclude);
					if($ms_ret) {
						$this->end_element("systems");
						$this->end_element("facsimile");
						return $ms_ret;
					}
				}
			}
			$this->end_element("systems");
		}
		$this->end_element("facsimile");
		return false;
	}
	protected function make_warp($warp) {
		$this->start_element("warp");
		$this->add_id("warp", $warp->id);
		$this->add_dates($warp);
		$nodes = $warp->nodes;
		foreach($nodes as $node) {
			$this->start_element("node");
			$this->write_attribute("x", $node[0]);
			$this->write_attribute("y", $node[1]);
			$this->end_element("node");
		}
		$this->end_element("warp");
	}
	protected function make_system($system, $link_from = null, $xinclude = false) {
		$ret = false;
		/* Write initial system element */
		{
			$this->start_element("system");
			$this->add_id("system", $system->id);
			$this->add_dates($system);
			$transcribe = $system->transcribe()->first();
			$warp = null;
			if($transcribe) {
				$facsimiles = $system->up()->get();
				foreach($facsimiles as $facsimile) {
					if($facsimile->is($transcribe)) {
						$warp = $facsimile->pivot->warp()->first();
						break;
					}
				}
			}
			/* Write warp for system */
			if($warp) {
				if($xinclude) {
					$this->include_link($this->router->pathFor('warp_get_xml', ['id' => $warp->id]));
				} else {
					$this->make_warp($warp);
				}
			}
			/* Write staves */
			{
				$this->start_element("staves");
				$staves = $system->down()->with(['warp'])->get();
				$all_minx = $all_miny = INF;
				foreach($staves as $stave) {
					$warp = $stave->warp()->first();
					if(!$warp) {
						continue;
					}
					$nodes = $warp->nodes;
					foreach($nodes as $node) {
						if($node[0] < $all_minx) {
							$all_minx = $node[0];
						}
						if($node[1] < $all_miny) {
							$all_miny = $node[1];
						}
					}
				}
				$sorted = $staves->sortBy(function($stave) use(&$all_minx, &$all_miny) {
					$warp = $stave->warp()->first();
					if(!$warp) {
						return "";
					}
					$nodes = $warp->nodes;
					$minx = $miny = INF;
					foreach($nodes as $node) {
						if($node[0] < $minx) {
							$minx = $node[0];
						}
						if($node[1] < $miny) {
							$miny = $node[1];
						}
					}
					$add_x = $minx - $all_minx;
					$add_y = $miny - $all_miny;
					$fmt = str_pad(number_format($add_x, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($add_y, 2, '.', ''), 25, "0", STR_PAD_LEFT);
					return $fmt;
				});
				$find_item = function($its, $stv) {
					foreach($its as $it) {
						if($it->up()->first()->is($stv)) {
							return $it;
						}
					}
					return null;
				};
				if($link_from) {
					$items = $link_from->items()->get();
					foreach($sorted as $stave) {
						$it = $find_item($items, $stave);
						if(!$it) {
							continue;
						}
						$ms_ret = $this->make_stave($stave, $it, $xinclude);
						if($ms_ret) {
							$ret = $ms_ret;
						}
					}
				} else {
					foreach($sorted as $stave) {
						$ms_ret = $this->make_stave($stave, null, $xinclude);
						if($ms_ret) {
							$ret = $ms_ret;
						}
					}
				}
				/* End staves */
				$this->end_element("staves");
			}
			/* End system */
			$this->end_element("system");
			return $ret;
		}
	}
	protected function make_context($item_start, $xinclude = false) {
		$this->start_element("context");
		$this->add_id("context", $item_start->id);
		$this->add_dates($item_start);
		$stave = $item_start->up()->first();
		{
			$warp = $stave->warp()->first();
			if($warp) {
				if($xinclude) {
					$this->include_link($this->router->pathFor('warp_get_xml', ['id' => $warp->id]));
				} else {
					$this->make_warp($warp);
				}
			}
		}
		{
			$this->start_element("items");
			$items = $stave->items()->with(['type'])->get()->filter(function($item) {
				$type = $item->type()->first();
				$class = get_class($type);
				return in_array($class, [
					\App\Models\Accidental::class,
					\App\Models\Clef::class,
					\App\Models\EditorialAccidental::class,
					\App\Models\EditorialClef::class,
					\App\Models\Staffline::class,
				]);
			});
			$minx = $miny = INF;
			$maxx = $maxy = -INF;
			foreach($items as $item) {
				$type = $item->type()->first();
				$x = $type->comp_x;
				$y = $type->comp_y;
				if($x !== NULL) {
					if($x < $minx) {
						$minx = $x;
					}
					if($x > $maxx) {
						$maxx = $x;
					}
				}
				if($y !== NULL) {
					if($y < $miny) {
						$miny = $y;
					}
					if($y > $maxy) {
						$maxy = $y;
					}
				}
			}
			$sorted = $items->sortBy(function($item) use(&$minx, &$miny, &$maxx, &$maxy) {
				$type = $item->type()->first();
				$x = $type->comp_x - $minx;
				$y = $type->comp_y - $miny;
				$class = get_class($type);
				if($class === \App\Models\Staffline::class) {
					$fmt = "-" . str_pad(number_format($y, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($y, 2, '.', ''), 25, "0", STR_PAD_LEFT);
				} else {
					$addx = $x + $maxx;
					$addy = $y + $maxy;
					$fmt = str_pad(number_format($addx, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($addy, 2, '.', ''), 25, "0", STR_PAD_LEFT);
				}
				return $fmt;
			});
			if($xinclude) {
				foreach($sorted as $item) {
					$this->include_link($this->router->pathFor('item_get_xml', ['id' => $item->id]));
				}
			} else {
				foreach($sorted as $item) {
					$this->make_item($item);
				}
			}
			$this->end_element("items");
		}
		$this->end_element("context");
	}
	private function new_item(&$record) {
		$this->start_element("item");
		$this->add_id("item", $record->id);
		if($record->editorial_comment) {
			$this->write_attribute("editorialComment", $record->editorial_comment);
		}
		$this->add_dates($record);
	}
	private function new_type($record_type) {
		$ns = $this->candrns;
		$ep = $this->candrns_endpoint;
		$xw = $this->xw;
		$add_id = function(&$it, &$id) { return $this->add_id($it, $id); };
		$generic_new = function($type, $name, $attrs) use (&$xw, &$ns, &$ep, &$add_id) {
			$xw->startElementNs($ns, $name, $ep);
			$id = $type->id;
			($add_id)($name, $id);
			foreach($attrs as $tattr => $dat) {
				$xattr = $dat[0];
				$fun = $dat[1];
				$required = $dat[2];
				$transformed = $type->$tattr;
				if($fun) {
					$transformed = $fun($transformed);
				}
				if($required || $transformed !== null) {
					$xw->writeAttributeNs($ns, $xattr, $ep, $transformed);
				}
			}
		};
		$rtwo = function($a) { return round($a, 2); };
		$rint = function($a) { return (int)round($a); };
		$datefmt = function($d) { return $d ? $d->format('c') : null; };
		$end_el = function() { return $this->xw->endElement(); };
		$class = get_class($record_type);
		$funcs = [
			\App\Models\Ligature::class => function($t) use(&$xw, &$ns, &$ep, &$add_id, &$datefmt, &$funcs, &$end_el) {
				$xw->startElementNs($ns, "ligature", $ep);
				$tagname = "ligature";
				$id = $t->id;
				($add_id)($tagname, $id);
				$xw->writeAttributeNs($ns, "ligatureType", $ep, $t->type);
				if($t->created_at) {
					$xw->writeAttributeNs($ns, "createdAt", $ep, $datefmt($t->created_at));
				}
				if($t->updated_at) {
					$xw->writeAttributeNs($ns, "updatedAt", $ep, $datefmt($t->updated_at));
				}
				if($t->deleted_at) {
					$xw->writeAttributeNs($ns, "deletedAt", $ep, $datefmt($t->deleted_at));
				}
				$notes = $t->notes()->get();
				foreach($notes as $note) {
					$funcs[\App\Models\Note::class]($note);
					$end_el();
				}
			},
			\App\Models\Accidental::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'width' => ['width', $rtwo, true],
					'height' => ['height', $rtwo, true],
					'angle' => ['angle', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['accidentalType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "accidental", $attrs);
			},
			\App\Models\Clef::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'width' => ['width', $rtwo, true],
					'height' => ['height', $rtwo, true],
					'angle' => ['angle', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['clefType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "clef", $attrs);
			},
			\App\Models\Divisione::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'ax' => ['ax', $rtwo, true],
					'ay' => ['ay', $rtwo, true],
					'bx' => ['bx', $rtwo, true],
					'by' => ['by', $rtwo, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "divisione", $attrs);
			},
			\App\Models\EditorialAccidental::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['accidentalType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialAccidental", $attrs);
			},
			\App\Models\EditorialClef::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['clefType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialClef", $attrs);
			},
			\App\Models\EditorialSyllable::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'text' => ['text', null, false],
					'wordstart' => ['wordstart', function($a) { return $a ? "true" : "false"; }, false],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialSyllable", $attrs);
			},
			\App\Models\EditorialText::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialText", $attrs);
			},
			\App\Models\Note::class => function($t) use (&$generic_new, &$rtwo, &$rint, &$datefmt) {
				$attrs = [
					'x' => ['x', $rtwo, true],
					'y' => ['y', $rtwo, true],
					'shift' => ['shift', $rint, false],
					'plica' => ['plica', $rint, false],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "note", $attrs);
			},
			\App\Models\Staffline::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'ax' => ['ax', $rtwo, true],
					'ay' => ['ay', $rtwo, true],
					'bx' => ['bx', $rtwo, true],
					'by' => ['by', $rtwo, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "staffline", $attrs);
			},
			\App\Models\Syllable::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'width' => ['width', $rtwo, true],
					'height' => ['height', $rtwo, true],
					'angle' => ['angle', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'text' => ['text', null, false],
					'wordstart' => ['wordstart', function($a) { return $a ? "true" : "false"; }, false],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "syllable", $attrs);
			},
		];
		if(!isset($funcs[$class])) {
			return null;
		}
		return ($funcs[$class])($record_type);
	}
	protected function make_item($item) {
		$this->new_item($item);
		{
			$item_type = $item->type()->first();
			if($item_type) {
				$this->new_type($item_type);
				$this->xw->endElement();
			}
		}
		$this->end_element("item");
	}
	protected function make_stave($stave, $item_start = null, $xinclude = false) {
		$this->start_element("stave");
		$this->add_id("stave", $stave->id);
		$this->add_dates($stave);
		$ret = false;
		/* Write staff content */
		{
			/* Write item context */
			if($item_start) {
				if($xinclude) {
					$this->include_link($this->router->pathFor('context_get_xml', ['id' => $item_start->id]));
				} else {
					$this->make_context($item_start, $xinclude);
				}
			} else {
				$warp = $stave->warp()->first();
				if($warp) {
					if($xinclude) {
						$this->include_link($this->router->pathFor('warp_get_xml', ['id' => $warp->id]));
					} else {
						$this->make_warp($warp);
					}
				}
			}
			/* Write items */
			{
				$this->start_element("items");
				$staff_items = $stave->items()->with(['type'])->get()->filter(function($item) {
					$type = $item->type()->first();
					return (get_class($type) !== \App\Models\Note::class) || !($type->ligature()->first());
				});
				$minx = $miny = INF;
				$maxx = $maxy = -INF;
				foreach($staff_items as $item) {
					$type = $item->type()->first();
					$x = $type->comp_x;
					$y = $type->comp_y;
					if($x !== NULL) {
						if($x < $minx) {
							$minx = $x;
						}
						if($x > $maxx) {
							$maxx = $x;
						}
					}
					if($y !== NULL) {
						if($y < $miny) {
							$miny = $y;
						}
						if($y > $maxy) {
							$maxy = $y;
						}
					}
				}
				$sorted = $staff_items->sortBy(function($item) use(&$minx, &$miny, &$maxx, &$maxy) {
					$type = $item->type()->first();
					$x = $type->comp_x - $minx;
					$y = $type->comp_y - $miny;
					$class = get_class($type);
					if($class === \App\Models\Staffline::class) {
						$fmt = "-" . str_pad(number_format($y, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($y, 2, '.', ''), 25, "0", STR_PAD_LEFT);
					} else {
						$addx = $x + $maxx;
						$addy = $y + $maxy;
						$fmt = str_pad(number_format($addx, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($addy, 2, '.', ''), 25, "0", STR_PAD_LEFT);
					}
					return $fmt;
				});
				$itemis = function($item, $it) {
					if(!$item || !$it) {
						return false;
					}
					if($item->is($it)) {
						return true;
					}
					$item_class = get_class($item);
					$it_class = get_class($it);
					if($item_class === \App\Models\Ligature::class && $it_class === \App\Models\Note::class) {
						$notes = $item->notes()->get();
						foreach($notes as $note) {
							if($note->is($it)) {
								return true;
							}
						}
					}
					if($it_class === \App\Models\Ligature::class && $item_class === \App\Models\Note::class) {
						$notes = $it->notes()->get();
						foreach($notes as $note) {
							if($note->is($item)) {
								return true;
							}
						}
					}
					if($item_class === $it_class && $item_class === \App\Models\Ligature::class) {
						$notesa = $item->notes()->get();
						$notesb = $it->notes()->get();
						foreach($notesa as $na) {
							foreach($notesb as $nb) {
								if($na->is($nb)) {
									return true;
								}
							}
						}
					}
					return false;
				};
				$writeit = $item_start ? false : true;
				foreach($sorted as $item) {
					if(!$writeit && $itemis($item, $item_start)) {
						$writeit = true;
					}
					if($writeit) {
						if($xinclude) {
							$this->include_link($this->router->pathFor('item_get_xml', ['id' => $item->id]));
						} else {
							$this->make_item($item);
						}
						$links = $item->links()->get();
						if($links) {
							/* If this item has links */
							foreach($links as $link) {
								/* If we have item start, then we need
								 * to check that the link we're checking
								 * is not the link we started at in the 
								 * first place */
								if($item_start) {
									$link_items = $link->items()->get();
									foreach($link_items as $link_item) {
										/* If any link item is the link we started at */
										if($link_item->is($item_start)) {
											/* Go to the next link */
											continue 2;
										}
									}
								}
								/* We've checked all the link
								 * items, and none of them are
								 * the starting link so end of
								 * chunk */
								$ret = $link;
								break 2;
							}
						}
					}
				}

			}
			/* End items */
			$this->end_element("items");
		}
		/* End staff */
		$this->end_element("stave");
		return $ret;
	}
}
