<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class EditFacsimileController extends EditCategoryController {
	use \App\ModelTraits\ModelImage;

	private function do_system_node_set(Request $request, Response $response, array $args, $record) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$json_nodes = $this->get_post_or_get($request, 'nodes');
		if(!$json_nodes) {
			return $this->return_message($response, 'error', 'No nodes submitted', 'edit_facsimile_systems', ['id' => $args['id']]);
		}
		$nodes = json_decode($json_nodes, true);
		if(!is_array($nodes)) {
			return $this->return_message($response, 'error', 'Node data was corrupt', 'edit_facsimile_systems', ['id' => $args['id']]);
		}
		foreach($nodes as $id => $nodevals) {
			if(!is_int($id) && !is_numeric($id)) {
				return $this->return_message($response, 'error', 'System ID \'' . $id . '\' is invalid', 'edit_facsimile_systems', ['id' => $args['id']]);
			}
			if(!is_array($nodevals)) {
				return $this->return_message($response, 'error', 'System ID \'' . $id . '\' values are not an array', 'edit_facsimile_systems', ['id' => $args['id']]);
			}
			foreach($nodevals as $node) {
				if(!is_array($node) || count($node) != 2) {
					return $this->return_message($response, 'error', 'System ID \'' . $id . '\' values do not contain valid node data', 'edit_facsimile_systems', ['id' => $args['id']]);
				}
				if(!is_int($node[0]) || !is_int($node[1])) {
					return $this->return_message($response, 'error', 'System ID \'' . $id . '\' values are not integers', 'edit_facsimile_systems', ['id' => $args['id']]);
				}
			}
		}
		$systems = $record->down()->get();
		foreach($systems as $system) {
			if(array_key_exists($system['id'], $nodes)) {
				$warp = $system->pivot->warp()->first();
				$warp->nodes = $nodes[$system['id']];
				$warp->save();
				unset($nodes[$system['id']]);
			}
		}
		if(!empty($nodes)) {
			return $this->return_message($response, 'warning', 'System ID(s) ' . implode(', ', array_keys($nodes)) . ' were not found in this facsimile', 'edit_facsimile_systems', ['id' => $record['id']]);
		}
		return true;
	}
	public function new(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = new \App\Models\Facsimile;
		$record->save();
		$id = $record->id;
		return $this->do_redirect($response, 'view_facsimile', ['id' => $id]);
	}
	public function edit_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		return $this->view->render($response, 'edit/facsimile/title.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$title = $this->get_post_or_get($request, 'value');
		if($title) {
			$record = \App\Models\Facsimile::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
			}
			$record->name = $title;
			$record->save();
			return $this->do_redirect($response, 'view_facsimile', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_facsmimile_title', ['id' => $args['id']]);
	}
	public function edit_set(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::with('set')->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$first = $record->up;
		$set = $first ? $first->toArray() : null;
		$others = \App\Models\FacsimileSet::orderBy('name')->get();
		return $this->view->render($response, 'edit/facsimile/set.twig', [
			'record' => $record->toArray(),
			'set' => $set,
			'history' => $this->get_history_classes($record, 'facsimile_set_id', \App\Models\FacsimileSet::class),
			'others' => $others->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_new(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$set = new \App\Models\FacsimileSet();
		$set->save();
		$record->set()->associate($set);
		$record->save();
		return $this->do_redirect($response, 'view_facsimileset', ['id' => $set['id']]);
	}
	public function set_set(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$set = \App\Models\FacsimileSet::find($args['tid']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'edit_facsimile_facsimileset', ['id' => $args['id']]);
		}
		$record->set()->associate($set);
		$record->save();
		return $this->do_redirect($response, 'view_facsimile', ['id' => $args['id']]);
	}
	public function edit_folio(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::with('up')->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$first = $record->up;
		$folio = $first ? $first->toArray() : null;
		$others = \App\Models\Folio::orderBy('name')->get();
		return $this->view->render($response, 'edit/facsimile/folio.twig', [
			'record' => $record->toArray(),
			'folio' => $folio,
			'history' => $this->get_history_classes($record, 'folio_id', \App\Models\Folio::class),
			'others' => $others->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_folio(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$folio = \App\Models\Folio::find($args['tid']);
		if(!$folio) {
			return $this->return_message($response, 'error', 'Folio not found', 'list_sources', []);
		}
		$record->up()->associate($folio);
		$record->save();
		return $this->do_redirect($response, 'view_facsimile', ['id' => $args['id']]);
	}
	public function edit_image(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		return $this->view->render($response, 'edit/facsimile/image.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_image(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$that = $this;
		$errmsg = function($msg) use ($that, $response, $args) {
			return $that->return_message($response, 'error', $msg, 'edit_facsimile_image', ['id' => $args['id']]);
		};
		$ufs = $request->getUploadedFiles();
		if(!array_key_exists('value', $ufs)) {
			return $errmsg('No uploaded file');
		}
		$uf = $ufs['value'];
		$uferr = $uf->getError();
		switch($uferr) {
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			return $errmsg('File too big');
		case UPLOAD_ERR_PARTIAL:
			return $errmsg('File was only partially uploaded');
		case UPLOAD_ERR_NO_FILE:
			return $errmsg('No file was uploaded');
		case UPLOAD_ERR_NO_TMP_DIR:
		case UPLOAD_ERR_CANT_WRITE:
			return $errmsg('File cannot be written to the server');
		case UPLOAD_ERR_EXTENSION:
			return $errmsg('File upload was stopped for some reason');
		}
		$record->image = $this->moveUploadedFile($this->upload_dir, $uf);
		$this->thumbnailImage($record->image);
		$record->save();
		return $this->do_redirect($response, 'view_facsimile', ['id' => $args['id']]);
	}
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::with([
			'set' => function($q) {
				return $q->withCount('facsimiles');
			}
		])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$set = $record->set;
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'Facsimile deleted', 'list_sources', []);
		}
		$msg = "Facsimile will be deleted completely.";
		if($set->facsimiles_count <= 1) {
			$msg .= " This is the last facsimile in this facsimile set, set will also be deleted.";
		}
		$msg .= " Continue?";
		return $this->confirm_message($response, $msg, ['delete_facsimile', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_facsimile', ['id' => $args['id']]]);
	}
	public function edit_systems(Request $request, Response $response, array $args) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::with(['down', 'up'])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$systems = $record->down;
		foreach($systems as &$system) {
			$warp = $system->pivot->warp()->first();
			$system['warp'] = $warp;
		}
		$first = $record->up;
		$folio = $first ? $first->toArray() : null;
		$processed_systems = $systems->makeVisible('warp')->toArray();
		return $this->view->render($response, 'edit/facsimile/systems.twig', [
			'record' => $record->toArray(),
			'down' => $processed_systems,
			'folio' => $folio,
			'loggedin' => $loggedin,
		]);
	}
	public function edit_systems_from_other_facsimile(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::with(['down', 'up.down.down', 'up.down.warp'])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$ret = $this->do_system_node_set($request, $response, $args, $record);
		if($ret !== true) {
			return $ret;
		}
		// systems from this facsimile
		$systems = $record->down;
		// folio of this facsimile
		$folio = $record->up;
		if(!$folio) {
			return $this->return_message($response, 'error', 'Facsimile does not belong to a folio', 'edit_facsimile_folio', ['id' => $record['id']]);
		}
		// all the facsimiles of this folio
		$allfacs = $folio->down->where('id', '!=', $record['id']);
		$fac_data = [];
		if($allfacs) {
			foreach($allfacs as &$fac) {
				// systems of this other facsimile
				$fac_systems = $fac->down()->get();
				// insert the warp values for each system
				foreach($fac_systems as &$fac_system) {
					$warp = $fac_system->pivot->warp()->first();
					$fac_system['warp'] = $warp;
				}
				// we don't want to see the pivot in the output
				$fac['down'] = $fac_systems->makeHidden('pivot');
			}
			// but we do want to see the systems
			$fac_data = $allfacs->makeVisible(['down', 'warp'])->toArray();
		}
		return $this->view->render($response, 'edit/facsimile/other_systems.twig', [
			'record' => $record->toArray(),
			'facsimiles' => $fac_data,
			'folio' => $folio->toArray(),
			'loggedin' => true,
		]);
	}
	public function add_system(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$system = \App\Models\System::with(['transcribe', 'up'])->find($args['tid']);
		if(!$system) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		if($system->transcribe->count() === 0) {
			$system->transcribe()->save($record);
		}
		$system->save();
		$warp = new \App\Models\Warp;
		$imdim = $this->image_dimensions($record);
		if(!$imdim) {
			return $this->return_message($response, 'error', 'Facsimile image could not be loaded', 'edit_facsimile_systems', ['id' => $record['id']]);
		}
		$width = intdiv($imdim[0], 3);
		$x = $width;
		$width *= 2;
		$height = intdiv($imdim[1], 3);
		$y = $height;
		$height *= 2;
		$warp->nodes = [[$x, $y], [$width, $y], [$width, $height], [$x, $height]];
		$warp->save();
		$record->down()->attach($system);
		$new_relation = $system->up->find($record['id']);
		if(!$new_relation) {
			return $this->return_message($response, 'error', 'Could not find recently-inserted record', 'edit_facsimile_systems', ['id' => $record['id']]);
		}
		$pivot = $new_relation->pivot;
		$pivot->warp()->associate($warp);
		$pivot->save();
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'edit_facsimile_systems', ['id' => $record['id']]);
	}
	public function add_new_system(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::with('set')->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$ret = $this->do_system_node_set($request, $response, $args, $record);
		if($ret !== true) {
			return $ret;
		}
		$n_systems = intval($this->get_post_or_get($request, 'n'));
		if(!$n_systems || $n_systems < 1) {
			$n_systems = 1;
		}
		$imdim = $this->image_dimensions($record);
		if(!$imdim) {
			return $this->return_message($response, 'error', 'Facsimile image could not be loaded', 'edit_facsimile_systems', ['id' => $record['id']]);
		}
		$set = $record->set;
		if($set) {
			$top_margin = $imdim[1] * $set->system_margins[0];
			$right_margin = $imdim[0] * $set->system_margins[1];
			$bottom_margin = $imdim[1] * $set->system_margins[2];
			$left_margin = $imdim[0] * $set->system_margins[3];
		} else {
			$top_margin = $right_margin = $bottom_margin = $left_margin = 0;
		}
		$width = intdiv($imdim[0] - $left_margin - $right_margin, 3);
		$height = intdiv($imdim[1] - $top_margin - $bottom_margin, $n_systems * 2 + 1);
		$old_height = $height;
		$old_width = $width;
		for($i = 0;$i < $n_systems; $i++) {
			$system = new \App\Models\System;
			$system->transcribe()->associate($record);
			$system->save();
			$warp = new \App\Models\Warp;
			$x = $width + $left_margin;
			$width *= 2;
			$y = $top_margin + $height * (($i * 2) + 1);
			$height += $y;
			$warp->nodes = [[$x, $y], [$width, $y], [$width, $height], [$x, $height]];
			$warp->save();
			$record->down()->attach($system);
			foreach($system->up as $facsimile) {
				$pivot = $facsimile->pivot;
				$pivot->warp()->associate($warp);
				$pivot->save();
			}
			$height = $old_height;
			$width = $old_width;
		}
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'edit_facsimile_systems', ['id' => $record['id']]);
	}
	public function set_system_nodes(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$ret = $this->do_system_node_set($request, $response, $args, $record);
		if($ret === true) {
			return $this->do_redirect($response, 'edit_facsimile_systems', ['id' => $record['id']]);
		}
		return $ret;
	}
	public function move_system_up(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$systems_builder = $record->down();
		$systems = $systems_builder->get();
		$this_one = $systems->firstWhere('id', $args['tid']);
		if(!$this_one) {
			return $this->return_message($response, 'error', 'System not found', 'edit_facsimiles', ['id' => $record->id]);
		}
		$prev = $systems->first();
		foreach($systems as $system) {
			if($system->id == $prev->id) {
				continue;
			}
			if($system->id == $this_one->id) {
				$systems_builder->moveBefore($this_one, $prev);
				break;
			}
			$prev = $system;
		}
		return $this->do_redirect($response, 'edit_facsimile_systems', ['id' => $args['id']]);
	}
	public function move_system_down(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'list_sources', []);
		}
		$systems_builder = $record->down();
		$systems = $systems_builder->get()->reverse();
		$this_one = $systems->firstWhere('id', $args['tid']);
		if(!$this_one) {
			return $this->return_message($response, 'error', 'System not found', 'edit_facsimiles', ['id' => $record->id]);
		}
		$next = $systems->first();
		foreach($systems as $system) {
			if($system->id == $next->id) {
				continue;
			}
			if($system->id == $this_one->id) {
				$systems_builder->moveAfter($this_one, $next);
				break;
			}
			$next = $system;
		}
		return $this->do_redirect($response, 'edit_facsimile_systems', ['id' => $args['id']]);
	}
}
