<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseStaveController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		error_log("Cannot list staves.");
		return $response->withJson(['message' => 'Cannot list staves.']);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\Stave::with(['warp', 'up.up.up.up', 'up.transcribe.set', 'current_transcribers', 'all_transcribers'])->find($args['id']);
		if(!$record) {
			return $this->view->render($response, 'browse/staves.twig', []);
		}
		$system = $record->up;
		$all_staves = $system->down;
		$adjacent = $this->find_adjacent($all_staves, $record);
		$all_transcribers = $this->add_transcribers($record->all_transcribers()->get());
		$facsimile = $folio = $source = null;
		if($system) {
			$facsimiles = $system->up;
			$facsimile = $system->transcribe;
			foreach($facsimiles as &$f) {
				if(!$f) {
					continue;
				}
				if($facsimile->is($f)) {
					$facsimile['warp'] = $f->pivot->warp()->first();
					break;
				}
			}
			$facsimile = $facsimile->makeVisible(['warp', 'set']);
			if($facsimile) {
				$folio = $facsimile->up;
				if($folio) {
					$source = $folio->up;
				}
			}
		}
		$stave_lu = $record->updated_at->getTimestamp();
		$stave_w_lu = $record->warp->updated_at->getTimestamp();
		$system_lu = $system->updated_at->getTimestamp();
		$system_w_lu = isset($facsimile['warp']) ? $facsimile['warp']->updated_at->getTimestamp() : 0;
		$lu = max($stave_lu, $stave_w_lu, $system_lu, $system_w_lu);
		return $this->view->render($response, 'browse/staves.twig', [
			'stave' => $record->toArray(),
			'adjacent' => $adjacent,
			'transcribe' => $facsimile,
			'last_updated' => $lu,
			'all_transcribers' => $all_transcribers,
			'up' => [
				'view_source' => $source,
				'view_folio' => $folio,
				'view_facsimile' => $facsimile,
				'view_system' => $system,
			],
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function text(Request $request, Response $response, array $args) {
		$record = \App\Models\Folio::find($args['id']);
		$walker = new \App\Walkers\TextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text', $walker));
	}
	public function text_editorial(Request $request, Response $response, array $args) {
		$record = \App\Models\Folio::find($args['id']);
		$walker = new \App\Walkers\EditorialTextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text_editorial', $walker));
	}
	public function view_rdf(Request $request, Response $response, array $args) {
		$record = \App\Models\Stave::find($args['id']);
		$rdf = new \EasyRdf\Graph();
		$uri = $request->getUri();
		$rdf->resource(strval($uri), 'candr:Stave');
		foreach($this->request_all_data($record, true) as $dat) {
			$fun = function($dat) use (&$fun, &$rdf, &$uri) {
				if(!isset($dat['name']['name']) || !isset($dat['id']['id'])) {
					return;
				}
				$name = $dat['name']['name'];
				$id = $dat['id']['id'];
				$fragged = strval($uri
					->withFragment(
						'name=' . strtolower($name) .
						'&id=' . strval($id)
					));
				$rdf->resource($fragged, 'candr:' . ucfirst($name));
				if(isset($dat['node'])) {
					$rdf->addLiteral($fragged, 'candr:X', $dat['node']['x']);
					$rdf->addLiteral($fragged, 'candr:Y', $dat['node']['y']);
				}
				if(isset($dat['shift']['shift'])) {
					$rdf->addLiteral($fragged, 'candr:Shift', $dat['shift']['shift']);
				}
				if(isset($dat['plica']['plica'])) {
					$rdf->addLiteral($fragged, 'candr:Plica', $dat['plica']['plica']);
				}
				if(isset($dat['notes'])) {
					foreach($dat['notes'] as $note) {
						if(!isset($note['name']['name']) || !isset($note['id']['id'])) {
							continue;
						}
						$subname = $note['name']['name'];
						$subid = $note['id']['id'];
						$subfrag = strval($uri
							->withFragment(
								'name=' . strtolower($subname) .
								'&id=' . strval($subid)
							));
						$rdf->add($fragged, 'candr:contains', $subfrag);
						$fun($note);
					}
				}
				if(isset($dat['editorial comment']['editorial comment']) && !empty($dat['editorial comment']['editorial comment'])) {
					$rdf->addLiteral($fragged, 'candr:EditorialComment', $dat['editorial comment']['editorial comment']);
				}
				if(isset($dat['nodes'])) {
					$rdf->addLiteral($fragged, 'candr:Ax', $dat['nodes']['ax']);
					$rdf->addLiteral($fragged, 'candr:Bx', $dat['nodes']['bx']);
					$rdf->addLiteral($fragged, 'candr:Ay', $dat['nodes']['ay']);
					$rdf->addLiteral($fragged, 'candr:By', $dat['nodes']['by']);
				}
				if(isset($dat['type']['type'])) {
					$rdf->addLiteral($fragged, 'candr:Type', $dat['type']['type']);
				}
				if(isset($dat['position'])) {
					$rdf->addLiteral($fragged, 'candr:Top', $dat['position']['top']);
					$rdf->addLiteral($fragged, 'candr:Left', $dat['position']['left']);
					$rdf->addLiteral($fragged, 'candr:Width', $dat['position']['width']);
					$rdf->addLiteral($fragged, 'candr:Height', $dat['position']['height']);
					$rdf->addLiteral($fragged, 'candr:Angle', $dat['position']['angle']);
				}
				if(isset($dat['centrepoint'])) {
					$subfrag = strval($uri
						->withFragment(
							'name=centrepoint' .
							'&id=' . strval($id)
						));
					$rdf->add($fragged, 'candr:centrepoint', $subfrag);
					$rdf->resource($subfrag, 'candr:Centrepoint');
					$rdf->addLiteral($subfrag, 'candr:X', $dat['centrepoint']['x']);
					$rdf->addLiteral($subfrag, 'candr:Y', $dat['centrepoint']['y']);
				}
				if(isset($dat['text'])) {
					$rdf->addLiteral($fragged, 'candr:Text', $dat['text']['text']);
					$rdf->addLiteral($fragged, 'candr:Wordstart', $dat['text']['wordstart']);
					$rdf->addLiteral($fragged, 'candr:Stanza', $dat['text']['stanza']);
				}
				if(isset($dat['synch'])) {
					foreach($dat['synch'] as $synch) {
						if(!isset($synch['name']['name']) || !isset($synch['id']['id']) || !isset($synch['stave']['id'])) {
							continue;
						}
						$subname = $synch['name']['name'];
						$subid = $synch['id']['id'];
						$exp_uri = explode('/', strval($uri));
						$exp_uri[count($exp_uri) - 3] = strval($synch['stave']['id']);
						$newuri = implode('/', $exp_uri);
						$subfrag = $newuri .
							'#name=' . strtolower($subname) .
							'&id=' . strval($subid);
						$rdf->add($fragged, 'candr:synchronous', $subfrag);
					}
				}
			};
			$fun($dat);
		}
		$data = $rdf->serialise('rdfxml');
		$fname = "stave" . strval($args['id']);
		return $response->withHeader('Content-Type', 'application/rdf+xml')
			->withHeader('Content-Description', 'File Transfer')
	   		->withHeader('Content-Transfer-Encoding', 'binary')
			->withHeader('Content-Disposition', 'attachment; filename="' . $fname . '"')
	   		->write($data);
	}
}
