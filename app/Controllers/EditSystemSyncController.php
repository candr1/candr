<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class EditSystemSyncController extends EditCategoryController {
	public function add_setting(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$sync_data = $this->get_sync_data($args['id']);
		$sync_data['loggedin'] = true;
		if(count($sync_data['links']) < 1 && count($sync_data['down']) > 1) {
			return $this->return_message($response, 'warning', 'This system has no synchronisation links, so adding a setting is not possible. Please add stave synchronisations first', 'system_edit_sync', ['id' => $args['id']]);
		}
		return $this->view->render($response, 'edit/system/addsetting.twig', $sync_data);
	}
	public function edit_sync(Request $request, Response $response, array $args) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$sync_data = $this->get_sync_data($args['id']);
		$sync_data['loggedin'] = $loggedin;
		return $this->view->render($response, 'edit/system/synchronisation.twig', $sync_data);
	}
	private function get_sync_data($id) {
		// This is the same as BrowseSystemSyncController::view
		$record = \App\Models\System::with(['up', 'down', 'down.warp', 'down.items.type', 'transcribe.set', 'links.items', 'links.setting'])->find($id);
		$transcribe = $record->transcribe;
		$facsimiles = $record->up;
		$links = $record->links;
		$fac = $folio = $source = null;
		if($facsimiles) {
			foreach($facsimiles as &$f) {
				if(!$f) {
					continue;
				}
				if($transcribe->is($f)) {
					$warp = $f->pivot->warp()->first();
					$transcribe['warp'] = $warp;
					$fac = $transcribe;
					break;
				}
			}
			if($fac) {
				$folio = $fac->up;
				if($folio) {
					$source = $folio->up;
				}
			}
		}
		$staves = $record->down;
		$output = [];
		foreach($staves as &$stave) {
			$staveout = [];
			$warp = $stave->warp;
			if(!$warp) {
				continue;
			}
			$staveout['warp'] = $warp->nodes;
			$items = $stave->items;
			if(!$items) {
				continue;
			}
			$staveout['items'] = [];
			foreach($items as $item) {
				$type = $item->type;
				if(!$type) {
					continue;
				}
				$nodes = $type->getNodes();
				if(empty($nodes)) {
					continue;
				}
				$staveout['items'][$item->id] = ['name' => $type->model_id, 'nodes' => $nodes];
			}

			$output[$stave->id] = $staveout;
		}
		$processed_links = [];
		foreach($links as $link) {
			$items = $link->items;
			$itemids = [];
			foreach($items as $item) {
				$itemids[] = $item->id;
			}
			$processed_links[$link->id] = [
				"ids" => $itemids,
				"setting" => ($link->setting ? true : false),
			];
		}
		return [
			"system" => $record->toArray(),
			"transcribe" => $transcribe->makeVisible(['warp', 'set'])->toArray(),
			"items" => $output,
			"links" => $processed_links,
			'up' => [
				'view_source' => $source,
				'view_folio' => $folio,
				'view_facsimile' => $transcribe,
			],
			'down' => $staves->toArray(),
			'facsimiles' => $facsimiles,
		];
	}
	public function set_sync(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$json_links = $this->get_post_or_get($request, 'links');
		if(!$json_links) {
			return $this->return_message($response, 'error', 'No links submitted', 'system_edit_sync', ['id' => $args['id']]);
		}
		$new_links = json_decode($json_links, true);
		// Validate submitted links
		if(!is_array($new_links)) {
			return $this->return_message($response, 'error', 'Link data was corrupt', 'system_edit_sync', ['id' => $args['id']]);
		}
		foreach($new_links as &$link) {
			if(count($link) < 2) {
				return $this->return_message($response, 'error', 'Link does not connect at least two items', 'system_edit_sync', ['id' => $args['id']]);
			}
			foreach($link as $id) {
				if(!is_numeric($id)) {
					return $this->return_message($response, 'error', 'Link IDs are not numeric', 'system_edit_sync', ['id' => $args['id']]);
				}
			}
		}
		$record = \App\Models\System::with('links.items')->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'System not found', 'list_sources', []);
		}
		$total_ids = [];
		foreach($new_links as &$link) {
			foreach($link as &$id) {
				$id = intval($id);
				array_push($total_ids, $id);
			}
		}
		$all_items = \App\Models\Item::with('up.up')->find($total_ids);
		$item_by_id = [];
		foreach($all_items as $item) {
			if(!$item->id) {
				continue;
			}
			$item_by_id[intval($item->id)] = $item;
		}
		$canonical_links = array();
		foreach($new_links as &$link) {
			$staves = array();
			$this_link = array();
			foreach($link as &$id) {
				if(!isset($item_by_id[$id])) {
					return $this->return_message($response, 'error', 'Could not find item in database', 'system_edit_sync', ['id' => $args['id']]);
				}
				$item = $item_by_id[$id];
				$stave = $item->up()->first();
				if(!$stave) {
					return $this->return_message($response, 'error', 'Item is not on a stave', 'system_edit_sync', ['id' => $args['id']]);
				}
				$system = $stave->up()->first();
				if(!$record->is($system)) {
					return $this->return_message($response, 'error', 'Item stave is not in this system', 'system_edit_sync', ['id' => $args['id']]);
				}
				$staveid = $stave->id;
				if(in_array($staveid, $staves)) {
					return $this->return_message($response, 'error', 'Trying to link two items from the same stave', 'system_edit_sync', ['id' => $args['id']]);
				}
				$staves[] = $staveid;
				$this_link[] = $id;
			}
			$canonical_links[] = $this_link;
		}
		$find_closest = function($cls, $items) {
			$link_differences = function($a, $b) {
				return count(array_diff($a, $b));
			};
			$min_diff = PHP_INT_MAX;
			$most_similar = null;
			foreach($cls as $cl) {
				$differences = $link_differences($cl, $items);
				if($differences < $min_diff) {
					$min_diff = $differences;
					$most_similar = $cl;
				}
			}
			return $most_similar;
		};
		// Sort links by most items to least
		$current_links = $record->links->sort(function($a, $b) {
			$ai = $a->items->count();
			$bi = $b->items->count();
			return ($a < $b) ? 1 : -1;
		});
		$items_to_id = function($items) {
			$ret = [];
			foreach($items as $item) {
				$ret[] = $item->id;
			}
			return $ret;
		};
		foreach($current_links as $cur_link) {
			$cur_items = $cur_link->items;
			$closest_link = $find_closest($canonical_links, $items_to_id($cur_items));
			if($closest_link) {
				// We found the closest link, update this link
				// to these items
				$cur_link->items()->sync($closest_link);
				$cur_link->save();
				// Delete canonical link
				$key = array_search($closest_link, $canonical_links);
				if($key !== false) {
					unset($canonical_links[$key]);
				}
			} else {
				// Exhausted canonical links. We have too many
				// links in the database, and what remains are
				// the most un-similar links.
				$cur_link->delete();
			}
		}
		// Exhausted current links
		// We may have exhausted $canonical_links too, in which case
		// this next foreach doesn't execute
		foreach($canonical_links as $cl) {
			// What remains in $canonical_links is now any new links
			$link = new \App\Models\Link();
			$link->system()->associate($record);
			$link->save();
			$it = $link->items();
			$it->attach($cl);
		}
		return $this->do_redirect($response, 'system_edit_sync', ['id' => $args['id']]);
	}
}
