<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use GuzzleHttp\Psr7;

abstract class XMLGenericController {
	protected $logger;
	protected $router;
	protected $candrns;
	protected $candrns_endpoint;
	protected $xw;
	protected $xslt_store;
	private $stack;
	protected $view;

	public function __construct($logger, \Slim\Views\Twig $view, $router, $xslt_store) {
		$this->logger = $logger;
		$this->view = $view;
		$this->router = $router;
		$this->xslt_store = $xslt_store;
		$this->candrns = "candr";
		$this->candrns_endpoint = "http://candr.ac.uk/ns/candr/";
		$this->xw = $this->create_xml_writer();
		$this->stack = new \Ds\Stack();
	}
	public function generate(Request $request, Response $response, array $args) { }

	protected function do_xslt($input, $fname, $string = true) {
		$xslt = new \DOMDocument();
		$xslt->load($this->xslt_store . DS . $fname);
		libxml_use_internal_errors(true);
		$proc = new \XSLTProcessor();
		$proc->importStylesheet($xslt);
		if(!is_a($input, 'DOMDocument')) {
			$xml = new \DOMDocument();
			$xml->loadXML($input);
		} else {
			$xml = $input;
		}
		if($string) {
			return $proc->transformToXml($xml);
		}
		return $proc->transformToDoc($xml);
	}
	protected function abc2svg($abc) {
		if(empty(trim($abc))) {
			error_log("Input is empty");
			return false;
		}
		$get_pdf_pages = function($fname) {
			exec('pdfinfo ' . escapeshellarg($fname), $output);
			$n = 0;
			foreach($output as $op) {
				if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1) {
					$n = intval($matches[1]);
					break;
				}
			}
			return $n;
		};
		$tmpdir = sys_get_temp_dir();
		$temp = tempnam($tmpdir, 'candr');
		$abc_in = tempnam($tmpdir, 'candr');
		if($temp === false || $abc_in === false) {
			error_log("Failed to open temporary file");
			return false;
		}
		file_put_contents($abc_in, $abc);
		$cmdline = '(abcm2ps -O - ' . escapeshellarg($abc_in) . ' | ps2pdf - ' . escapeshellarg($temp) . ') 2>&1';
		error_log("Executing: " . $cmdline);
		exec($cmdline, $abcm2ps_output);
		//check validity of PDF
		if(!(is_readable($temp) && (filesize($temp) > 256))) { // a valid PDF will be bigger than 256 bytes
			error_log("abcm2ps output is invalid");
			return false;
		}
		unlink($abc_in);
		//get number of pages in result PDF
		$n = $get_pdf_pages($temp);
		$svgs = [];
		//make temp file to store SVG
		$svg_temp = tempnam($tmpdir, 'candr');
		for($i = 1; $i <= $n; ++$i) {
			//convert PDF page to SVG
			$cmdline = 'pdf2svg ' . escapeshellarg($temp) . ' ' . escapeshellarg($svg_temp) . ' ' . escapeshellarg($i) . ' 2>&1';
			error_log("Executing: " . $cmdline);
			exec($cmdline, $pdf2svg_output);
			//read tempfile
			$svgs[$i] = [
				"svg" => file_get_contents($svg_temp),
				"output" => implode("\n", $pdf2svg_output),
			];
		}
		//delete tmp files
		unlink($svg_temp);
		unlink($temp);
		//return array of svgs as strings
		return [
			"svgs" => $svgs,
			"output" => implode("\n", $abcm2ps_output),
		];
	}
	protected function set_headers(Response &$response, $fname) {
		return $response->withHeader('Content-Type', 'text/xml')
		   ->withHeader('Content-Description', 'File Transfer')
	   	   ->withHeader('Content-Transfer-Encoding', 'binary')
		   ->withHeader('Content-Disposition', 'attachment; filename="' . $fname . '"');
	}
	protected function add_generic_root_attributes($xw) {
		$xw->writeAttributeNs("xmlns", "xml", null, "http://www.w3.org/XML/1998/namespace");
		$xw->writeAttributeNs("xmlns", "xi", null, "http://www.w3.org/2001/XInclude");
		$this->write_attribute("candrversion", "0.0.1");
	}
	protected function add_id($item, $id) {
		$this->write_attribute("id", $item . "-" . $id);
	}
	protected function include_link($href) {
		$this->xw->startElementNs("xi", "include", null);
		$this->xw->writeAttribute("href", $href);
		$this->xw->startElementNs("xi", "fallback", null);
		$this->write_attribute("error", "xinclude: " . $href . " not found");
		$this->xw->endElement();
		$this->xw->endElement();
	}
	protected function create_xml_writer() {
		$xw = new \XMLWriter();
		$xw->openMemory();
		$xw->setIndent(true);
		$xw->setIndentString("\t");
		$xw->startDocument("1.0");
		return $xw;
	}
	protected function end_xml_writer($xw) {
		$xw->endDocument();
		return $xw->outputMemory();
	}
	protected function add_dates($record) {
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
	}
	
	private function get_indent() {
		$str = "";
		$len = $this->stack->count();
		for($i = 0; $i < $len; ++$i) {
			$str .= " ";
		}
		return $str;
	}
	protected function start_element($name) {
		$this->stack->push($name);
		$this->xw->startElementNs($this->candrns, $name, $this->candrns_endpoint);
	}
	protected function end_element($check = null) {
		try {
			$indent = $this->get_indent();
			$el_name = $this->stack->pop();
			if($check !== null && $check !== $el_name) {
				error_log("Element check was incorrect, malformed XML! Expecting: " . $check . ", got " . $el_name);
			}
			$this->xw->endElement();
		} catch(\UnderflowException $e) {
			error_log("Indent underflow!");
		}
	}
	protected function write_attribute($name, $val) {
		$this->xw->writeAttributeNs($this->candrns, $name, $this->candrns_endpoint, $val);
	}
}
