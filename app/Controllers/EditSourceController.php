<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class EditSourceController extends EditCategoryController {
	public function new(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = new \App\Models\Source;
		$record->save();
		$id = $record->id;
		return $this->do_redirect($response, 'view_source', ['id' => $id]);
	}
	public function edit_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		return $this->view->render($response, 'edit/source/title.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$title = $this->get_post_or_get($request, 'value');
		if($title) {
			$record = \App\Models\Source::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
			}
			$record->name = $title;
			$record->save();
			return $this->do_redirect($response, 'view_source', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_source_title', ['id' => $args['id']]);
	}
	public function edit_archive(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		return $this->view->render($response, 'edit/source/archive.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_archive(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$archive = $this->get_post_or_get($request, 'value');
		if($archive) {
			$record = \App\Models\Source::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
			}
			$record->archive = $archive;
			$record->save();
			return $this->do_redirect($response, 'view_source', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_source_archive', ['id' => $args['id']]);
	}
	public function edit_folios(Request $request, Response $response, array $args) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::with([
			'down' => function($q) {
				return $q->orderBy('name', 'ASC');
			}
		])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		$folios = $record->down;
		$others = \App\Models\Folio::doesntHave('up')->get();
		return $this->view->render($response, 'edit/source/folios.twig', [
			'record' => $record->toArray(),
			'folios' => $folios->toArray(),
			'others' => $others->toArray(),
			'loggedin' => $loggedin,
		]);
	}
	public function add_new_folio(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		$folio = new \App\Models\Folio;
		$record->down()->save($folio);
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'view_folio', ['id' => $folio['id']]);
	}
	public function add_folio(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		$folio = \App\Models\Folio::find($args['tid']);
		if(!$folio) {
			return $this->return_message($response, 'error', 'Folio not found', 'edit_source_folios', ['id' => $args['id']]);
		}
		$record->down()->save($folio);
		$record->transcribed = false;
		$record->save();
		return $this->do_redirect($response, 'view_source', ['id' => $args['id']]);
	}
	public function remove_folio(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		$folio = $record->down()->find($args['tid']);
		if(!$folio) {
			return $this->return_message($response, 'error', 'Folio not found', 'edit_source_folios', ['id' => $args['id']]);
		}
		$folio->up()->dissociate();
		$folio->save();
		return $this->return_message($response, 'success', 'Folio removed', 'view_source', ['id' => $args['id']]);
	}
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Source::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Source not found', 'list_sources', []);
		}
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'Source deleted', 'list_sources', []);
		}
		return $this->confirm_message($response, 'Source will be deleted completely. Continue?', ['delete_source', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_source', ['id' => $args['id']]]);
	}
}
