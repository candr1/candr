<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseFacsimileSetController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		$items = \App\Models\FacsimileSet::orderBy('name', 'ASC')->get()->toArray();
		return $this->view->render($response, 'browse/listcategory.twig', [
			'items' => $items,
			'routename' => 'view_facsimileset',
			'category' => 'Facsimile Sets',
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\FacsimileSet::with('facsimiles.up')->find($args['id']);
		if(!$record) {
			return $this->view->render($response, 'browse/root.twig', []);
		}
		$facsimiles = $record->facsimiles->makeVisible('up')->toArray();
		return $this->view->render($response, 'browse/facsimilesets.twig', [
			'set' => $record->toArray(),
			'up' => [
			],
			'down' => $facsimiles,
			'loggedin' => $this->user_logged_in(),
		]);
	}
}
