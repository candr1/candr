<?php

namespace App\Controllers;

use App\Models\User;
use App\Validator;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class TranscribeController extends LoginController {
	protected $logger;
	protected $view;
	protected $router;

	public function __construct($logger, Twig $view, $router, $logintime) {
		parent::__construct($logger, $view, $logintime);
		$this->router = $router;
	}
	private function begin_transcribe($className, $id, $response, $list_path, $edit_path) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$item = $className::find($id);
		if(!$item) {
			return $response->withRedirect($list_path);
		}
		$user = \App\Models\User::find($loggedin['id']);
		if(!$user) {
			error_log("Consistency error: Just found user no longer exists");
			return $response->withRedirect('/login');
		}
		$user->transcribing()->associate($item);
		$user->save();
		return $response->withRedirect($edit_path);
	}
	private function set_transcribe($className, $id, $response, $list_path, $next_path, $val = true) {
		$loggedin = $this->user_logged_in();
		if(!$loggedin) {
			return $response->withRedirect('/login');
		}
		$item = $className::find($id);
		if(!$item) {
			return $response->withRedirect($list_path);
		}
		$item->transcribed = $val;
		$user = \App\Models\User::find($loggedin['id']);
		if(!$user) {
			error_log("Consistency error: Just found user no longer exists");
			return $response->withRedirect('/login');
		}
		$item->all_transcribers()->save($user);
		$item->save();
		$user->transcribing()->dissociate($item);
		$user->save();
		return $response->withRedirect($next_path);
	}
	public function begin_transcribe_source(Request $request, Response $response, array $args) {
		return $this->begin_transcribe(
			\App\Models\Source::class,
			$args['id'],
			$response,
			$this->router->pathFor('list_sources'),
			$this->router->pathFor('view_source', ['id' => $args['id']])
		);
	}
	private function do_set_transcribe_source($response, $id, $val) {
		return $this->set_transcribe(
			\App\Models\Source::class,
			$id,
			$response,
			$this->router->pathFor('list_sources'),
			$this->router->pathFor('list_sources'),
			$val
		);
	}
	public function set_transcribe_source(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_source($response, $args['id'], true);
	}
	public function unset_transcribe_source(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_source($response, $args['id'], false);
	}
	public function begin_transcribe_folio(Request $request, Response $response, array $args) {
		$parent_url = '/';
		$item = \App\Models\Folio::find($args['id']);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_source', ['id' => $parent->id]);
			}
		}
		return $this->begin_transcribe(
			\App\Models\Folio::class,
			$args['id'],
			$response,
			$parent_url,
			$this->router->pathFor('view_folio', ['id' => $args['id']])
		);
	}
	private function do_set_transcribe_folio($response, $id, $val) {
		$parent_url = '/';
		$next_path = '/';
		$item = \App\Models\Folio::find($id);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_source', ['id' => $parent->id]);
				$next_path = $this->router->pathFor('begin_transcribe_source', ['id' => $parent->id]);
			}
		}
		return $this->set_transcribe(
			\App\Models\Folio::class,
			$id,
			$response,
			$parent_url,
			$next_path,
			$val
		);
	}
	public function set_transcribe_folio(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_folio($response, $args['id'], true);
	}
	public function unset_transcribe_folio(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_folio($response, $args['id'], false);
	}
	public function begin_transcribe_facsimile(Request $request, Response $response, array $args) {
		$parent_url = '/';
		$item = \App\Models\Facsimile::find($args['id']);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_folio', ['id' => $parent->id]);
			}
		}
		return $this->begin_transcribe(
			\App\Models\Facsimile::class,
			$args['id'],
			$response,
			$parent_url,
			$this->router->pathFor('view_facsimile', ['id' => $args['id']])
		);
	}
	private function do_set_transcribe_facsimile($response, $id, $val) {
		$parent_url = '/';
		$next_path = '/';
		$item = \App\Models\Facsimile::find($id);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_folio', ['id' => $parent->id]);
				$next_path = $this->router->pathFor('begin_transcribe_folio', ['id' => $parent->id]);
			}
		}
		return $this->set_transcribe(
			\App\Models\Facsimile::class,
			$id,
			$response,
			$parent_url,
			$next_path,
			$val
		);
	}
	public function set_transcribe_facsimile(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_facsimile($response, $args['id'], true);
	}
	public function unset_transcribe_facsimile(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_facsimile($response, $args['id'], false);
	}
	public function begin_transcribe_system(Request $request, Response $response, array $args) {
		$parent_url = '/';
		$item = \App\Models\System::find($args['id']);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_facsimile', ['id' => $parent->id]);
			}
		}
		return $this->begin_transcribe(
			\App\Models\System::class,
			$args['id'],
			$response,
			$parent_url,
			$this->router->pathFor('view_system', ['id' => $args['id']])
		);
	}
	private function do_set_transcribe_system($response, $id, $val) {
		$parent_url = '/';
		$next_path = '/';
		$item = \App\Models\System::find($id);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_facsimile', ['id' => $parent->id]);
				$next_path = $this->router->pathFor('begin_transcribe_facsimile', ['id' => $parent->id]);
			}
		}
		return $this->set_transcribe(
			\App\Models\System::class,
			$id,
			$response,
			$parent_url,
			$next_path,
			$val
		);
	}
	public function set_transcribe_system(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_system($response, $args['id'], true);
	}
	public function unset_transcribe_system(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_system($response, $args['id'], false);
	}
	public function begin_transcribe_stave(Request $request, Response $response, array $args) {
		$parent_url = '/';
		$item = \App\Models\Stave::find($args['id']);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_system', ['id' => $parent->id]);
			}
		}
		return $this->begin_transcribe(
			\App\Models\Stave::class,
			$args['id'],
			$response,
			$parent_url,
			$this->router->pathFor('view_stave', ['id' => $args['id']])
		);
	}
	private function do_set_transcribe_stave($response, $id, $val) {
		$parent_url = '/';
		$next_path = '/';
		$item = \App\Models\Stave::find($id);
		if($item) {
			$parent = $item->up()->first();
			if($parent) {
				$parent_url = $this->router->pathFor('view_system', ['id' => $parent->id]);
				$next_path = $this->router->pathFor('begin_transcribe_system', ['id' => $parent->id]);
			}
		}
		return $this->set_transcribe(
			\App\Models\Stave::class,
			$id,
			$response,
			$parent_url,
			$next_path,
			$val
		);
	}
	public function set_transcribe_stave(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_stave($response, $args['id'], true);
	}
	public function unset_transcribe_stave(Request $request, Response $response, array $args) {
		return $this->do_set_transcribe_stave($response, $args['id'], false);
	}
}
