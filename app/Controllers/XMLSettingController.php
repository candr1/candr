<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLSettingController extends XMLGenericChunkController {
	private function xml_string($id, $xinclude = true) {
		$record = \App\Models\Setting::with(['link.items.up', 'link.system.transcribe.set', 'link.system.up.down', 'link.system.transcribe.up.up', 'link.items.links.items'])->find($id);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$head = $record->link()->first();
		$this->start_element("setting");
		$this->add_id("setting", $record->id);
		$this->write_attribute("name", $record->name);
		$this->add_generic_root_attributes($this->xw);
		$this->add_dates($record);
		while($head) {
			$this->start_element("chunk");
			$this->add_id("chunk", $head->id);
			$dates = [
				'createdAt' => $record->created_at,
				'updatedAt' => $record->updated_at,
				'deletedAt' => $record->deleted_at,
			];
			foreach($dates as $attr => $val) {
				if(!$val) {
					continue;
				}
				$this->write_attribute($attr, $val->format('c'));
			}
			$head = $this->make_chunk($head, $xinclude);
			$this->end_element("chunk");
		}
		$this->end_element("setting");
		return $this->end_xml_writer($this->xw);
	}
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'setting-' . $args['id'] . '.xml');
		$xinclude = !(isset($args['xinclude']) && $args['xinclude'] == 'xinclude');
		$xml = $this->xml_string($args['id'], $xinclude);
		return $response->write($xml);
	}
	public function toText(Request $request, Response $response, array $args) {
		$response = $response->withHeader('Content-Type', 'text/plain');
		$xml = $this->xml_string($args['id'], false);
		$text = $this->do_xslt($xml, 'candrSettingToText.xsl', true);
		return $response->write($text);
	}
	public function toMEI(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'setting-' . $args['id'] . '.mei');
		$xml = $this->xml_string($args['id'], false);
		$mei = $this->do_xslt($xml, 'candrSettingToMei.xsl', true);

		return $response->write($mei);
	}
	public function toABC(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'setting-' . $args['id'] . '.abc')->withHeader('Content-Type', 'text/vnd.abc');
		$xml = $this->xml_string($args['id'], false);
		$mei = $this->do_xslt($xml, 'candrSettingToMei.xsl', false);
		$abc = $this->do_xslt($mei, 'MeiToABC.xsl', true);
		return $response->write($abc);
	}
	public function toSVG(Request $request, Response $response, array $args) {
		$xml = $this->xml_string($args['id'], false);
		$mei = $this->do_xslt($xml, 'candrSettingToMei.xsl', false);
		$abc = $this->do_xslt($mei, 'MeiToABC.xsl', true);
		$svgs = $this->abc2svg($abc);
		return $this->view->render($response, 'xslt/viewSVG.twig', $svgs);
	}
}
