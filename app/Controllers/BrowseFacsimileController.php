<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseFacsimileController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		error_log("Cannot list facsimiles.");
		return $response->withJson(['message' => 'Cannot list facsimiles.']);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::with([
			'up.up' => function($q) { return $q; }, 
			'down' => function($q) { return $q; },
			'up.down' => function($q) { return $q->orderBy('name'); },
			'set' => function($q) { return $q; },
			'current_transcribers' => function($q) { return $q; },
			'all_transcribers' => function($q) { return $q; }, 
			'down.all_transcribers' => function($q) { return $q; },
			'down.down.all_transcribers' => function($q) { return $q; }
		])->find($args['id']);
		if(!$record) {
			return $this->view->render($response, 'browse/facsimiles.twig', []);
		}
		$image = null;
		if($record['image']) {
			$image = $record;
		}
		$folio = $record->up;
		$all_facsimiles = $folio->down;
		$adjacent = $this->find_adjacent($all_facsimiles, $record);
		$systems = $record->down;
		foreach($systems as &$system) {
			$warp = $system->pivot->warp()->first();
			$system['warp'] = $warp;
		}
		$all_transcribers = $this->add_transcribers($record->all_transcribers()->get());
		foreach($systems as $system) {
			$all_transcribers = $this->add_transcribers($system->all_transcribers()->get(), $all_transcribers);
			$staves = $system->down;
			foreach($staves as $stave) {
				$all_transcribers = $this->add_transcribers($stave->all_transcribers);
			}
		}
		$facsimileset = $record->set;
		$source = null;
		if($folio) {
			$source = $folio->up;
		}
		$processed_systems = $systems->makeVisible('warp')->toArray();
		return $this->view->render($response, 'browse/facsimiles.twig', [
			'facsimile' => $record->toArray(),
			'adjacent' => $adjacent,
			'up' => [
				'view_source' => $source,
				'view_folio' => $folio
			],
			'down' => $processed_systems,
			'all_transcribers' => $all_transcribers,
			'set' => $facsimileset,
			'image' => $image,
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function text(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::find($args['id']);
		$walker = new \App\Walkers\TextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text', $walker));
	}
	public function text_editorial(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::find($args['id']);
		$walker = new \App\Walkers\EditorialTextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text_editorial', $walker));
	}
	public function mei(Request $request, Response $response, array $args) {
		$record = \App\Models\Facsimile::find($args['id']);
		if(!$record) {
			return $this->plain_text($response);
		}
		// cache?
		$cache = $this->get_cache($record, 'mei');
		if($cache) {
			return $this->xml_response($response, $cache->text);
		}
		// no cache
		$walker = new \App\Walkers\MEIWalker($this->logger);
		$walker->walk($record);
		$transcription = $walker->get_transcription();
		$this->make_cache($record, $transcription, 'mei');
		return $this->xml_response($response, $transcription);
	}
}
