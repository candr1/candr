<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class UtilityController extends EditCategoryController {
	use \App\ModelTraits\ModelNodes;

	public function add_staves_to_empty_systems(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$systems = \App\Models\System::doesntHave('down')->with('up')->get();
		$return_message = "Updated systems: ";

		$systems_updated = [];
		$systems_error = [];
		foreach($systems as $system) {
			$stave = new \App\Models\Stave;
			$stave->save();
			$add_to_err = function() use (&$systems_error, &$system) {
				if($system) {
					$systems_error[] = $system->id;
				}
			};
			if(!$system) {
				$add_to_err();
				continue;
			}
			$up = $system->up;
			if($up->count() === 0) {
				$add_to_err();
				continue;
			}
			$first = $up;
			if(!$first) {
				$add_to_err();
				continue;
			}
			$pivot = $first->pivot;
			if(!$pivot) {
				$add_to_err();
				continue;
			}
			$warp = $pivot->warp();
			if(!$warp) {
				$add_to_err();
				continue;
			}
			$warp_first = $warp->first();
			if(!$warp_first) {
				$add_to_err();
				continue;
			}
			$nodes = $warp_first->nodes;
			if(!$nodes) {
				$add_to_err();
				continue;
			}
			$bound = $this->bounding_box($nodes);
			$max_x = array_reduce($bound, function($c, $i) {
				return $c === null ? $i[0] : max($c, $i[0]);
			});
			$min_x = array_reduce($bound, function($c, $i) {
				return $c === null ? $i[0] : min($c, $i[0]);
			});
			$max_y = array_reduce($bound, function($c, $i) {
				return $c === null ? $i[1] : max($c, $i[1]);
			});
			$min_y = array_reduce($bound, function($c, $i) {
				return $c === null ? $i[1] : min($c, $i[1]);
			});
			$width = $max_x - $min_x;
			$height = $max_y - $min_y;
			$warp = new \App\Models\Warp;
			$warp->nodes = [[0, 0], [$width, 0], [$width, $height], [0, $height]];
			$warp->save();
			$stave->warp()->associate($warp);
			$stave->save();
			$system->down()->save($stave);
			$system->transcribed = false;
			$system->save();
			$systems_updated[] = strval($system->id);
		}
		$return_message .= implode(", ", $systems_updated);
		if(count($systems_error) > 0) {
			$return_message .= ". However some systems had an error: ";
			$return_message .= implode(", ", $systems_error);
		}
		return $this->return_message($response, 'success', $return_message, 'index', []);
	}
}
