<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseRootController extends LoginController {
	protected $logger;
	protected $view;
	private $order;

	public function __construct($logger, Twig $view) {
		$this->logger = $logger;
		$this->view = $view;
	}
	public function root(Request $request, Response $response, array $args) {
		$data['loggedin'] = $this->user_logged_in();
		return $this->view->render($response, 'browse/root.twig', $data);
	}
}
