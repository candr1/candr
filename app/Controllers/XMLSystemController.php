<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLSystemController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'system-' . $args['id'] . '.xml');
		$record = \App\Models\System::with(['transcribe', 'up.down'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->xw->startElementNs($this->candrns, "system", $this->candrns_endpoint);
		$this->add_id($this->xw, "system", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
		$transcribe = $record->transcribe()->first();
		$warp = null;
		if($transcribe) {
			$facsimiles = $record->up()->get();
			foreach($facsimiles as $facsimile) {
				if($facsimile->is($transcribe)) {
					$warp = $facsimile->pivot->warp()->first();
					break;
				}
			}
		}
		if($warp) {
			$this->include_link($this->router->pathFor('warp_get_xml', ['id' => $warp->id]));
		}
		{
			$this->xw->startElementNs($this->candrns, "staves", $this->candrns_endpoint);
			$staves = $record->down()->with(['warp'])->get();
			$all_minx = $all_miny = INF;
			foreach($staves as $stave) {
				$warp = $stave->warp()->first();
				if(!$warp) {
					continue;
				}
				$nodes = $warp->nodes;
				foreach($nodes as $node) {
					if($node[0] < $all_minx) {
						$all_minx = $node[0];
					}
					if($node[1] < $all_miny) {
						$all_miny = $node[1];
					}
				}
			}
			$sorted = $staves->sortBy(function($stave) use(&$all_minx, &$all_miny) {
				$warp = $stave->warp()->first();
				if(!$warp) {
					return "";
				}
				$nodes = $warp->nodes;
				$minx = $miny = INF;
				foreach($nodes as $node) {
					if($node[0] < $minx) {
						$minx = $node[0];
					}
					if($node[1] < $miny) {
						$miny = $node[1];
					}
				}
				$add_x = $minx - $all_minx;
				$add_y = $miny - $all_miny;
				$fmt = str_pad(number_format($add_x, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($add_y, 2, '.', ''), 25, "0", STR_PAD_LEFT);
				return $fmt;
			});
			foreach($sorted as $stave) {
				$this->include_link($this->router->pathFor('stave_get_xml', ['id' => $stave->id]));
			}
			$this->xw->endElement();
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
