<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class EditStaveController extends EditCategoryController {
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\Stave::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Stave not found', 'list_sources', []);
		}
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'Stave deleted', 'list_sources', []);
		}
		return $this->confirm_message($response, 'Stave and all its items will be deleted completely. Continue?', ['delete_stave', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_stave', ['id' => $args['id']]]);
	}
}
