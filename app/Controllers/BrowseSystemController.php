<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseSystemController extends BrowseCategoryController {
	public function list(Request $request, Response $response, array $args) {
		error_log("Cannot list systems.");
		return $response->withJson(['message' => 'Cannot list systems.']);
	}
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\System::with(['up.up.up', 'transcribe.set', 'down.warp', 'down.all_transcribers', 'links'])->find($args['id']);
		$facsimiles = $record->up;
		$staves = $record->down;
		foreach($staves as &$stave) {
			$warp = $stave->warp;
			$stave['warp'] = $warp;
		}
		$staves = $staves->sort(function($a, $b) {
			$a_centroid = $a->warp->centroid;
			$b_centroid = $b->warp->centroid;
			$c = $a_centroid[1] <=> $b_centroid[1];
			if($c) {
				return $c;
			}
			return ($a_centroid[0] <=> $b_centroid[0]);
		});
		$all_transcribers = $this->add_transcribers($record->all_transcribers);
		foreach($staves as $stave) {
			$all_transcribers = $this->add_transcribers($stave->all_transcribers, $all_transcribers);
		}
		$transcribe = $record->transcribe;
		$fac = $folio = $source = null;
		$all_systems = [];
		if($facsimiles) {
			foreach($facsimiles as &$f) {
				if(!$f) {
					continue;
				}
				$fac_systems = $f->down;
				foreach($fac_systems as $fac_system) {
					$addme = true;
					foreach($all_systems as $all_system) {
						if($fac_system->is($all_system)) {
							$addme = false;
							break 2;
						}
					}
					if($addme) {
						$all_systems[] = $fac_system;
					}
				}
				$warp = $f->pivot->warp;
				if($transcribe->is($f)) {
					$transcribe['warp'] = $warp;
				}
				$f['warp'] = $warp;
				$f = $f->makeVisible('warp');
				$fac = $f;
			}
			if($fac) {
				$folio = $fac->up;
				if($folio) {
					$source = $folio->up;
				}
			}
		}
		$adjacent = $this->find_adjacent($all_systems, $record);
		return $this->view->render($response, 'browse/systems.twig', [
			'system' => $record->toArray(),
			'adjacent' => $adjacent,
			'transcribe' => $transcribe->makeVisible(['warp', 'set'])->toArray(),
			'up' => [
				'view_source' => $source,
				'view_folio' => $folio,
				'view_facsimile' => $transcribe,
			],
			'down' => $staves->toArray(),
			'all_transcribers' => $all_transcribers,
			'facsimiles' => $facsimiles,
			'loggedin' => $this->user_logged_in(),
		]);
	}
	public function text(Request $request, Response $response, array $args) {
		$record = \App\Models\System::find($args['id']);
		$walker = new \App\Walkers\TextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text', $walker));
	}
	public function text_editorial(Request $request, Response $response, array $args) {
		$record = \App\Models\System::find($args['id']);
		$walker = new \App\Walkers\EditorialTextWalker($this->logger);
		return $this->plain_text($response, $this->text_walking($record, 'text_editorial', $walker));
	}
	public function mei(Request $request, Response $response, array $args) {
		$record = \App\Models\System::find($args['id']);
		if(!$record) {
			return $this->plain_text($response);
		}
		// cache?
		$cache = $this->get_cache($record, 'mei');
		if($cache) {
			return $this->xml_response($response, $cache->text);
		}
		// no cache
		$walker = new \App\Walkers\MEIWalker($this->logger);
		$walker->walk($record);
		$transcription = $walker->get_transcription();
		$this->make_cache($record, $transcription, 'mei');
		return $this->xml_response($response, $transcription);
	}
}
