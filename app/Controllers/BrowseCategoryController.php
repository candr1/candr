<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

abstract class BrowseCategoryController extends LoginController {

	protected $view;
	protected $upload_dir;
	protected $thumb_dir;
	protected $logger;

	public function __construct($logger, Twig $view, $upload_dir, $thumb_dir, $logintime) {
		parent::__construct($logger, $view, $logintime);
		$this->upload_dir = $upload_dir;
		$this->thumb_dir = $thumb_dir;
	}
	public function list(Request $request, Response $response, array $args) { }
	public function view(Request $request, Response $response, array $args) { }

	protected function add_transcribers($transcribers = [], $current = []) {
		$return_transcribers = $current;
		foreach($transcribers as $new_transcriber) {
			$add_this = true;
			foreach($return_transcribers as $cur_transcriber) {
				if($new_transcriber->is($cur_transcriber)) {
					$add_this = false;
					break;
				}
			}
			if($add_this) {
				$return_transcribers[] = $new_transcriber;
			}
		}
		return $return_transcribers;
	}
	protected function find_adjacent($haystack, $needle) {
		$stacklen = count($haystack);
		for($i = 0; $i < $stacklen; $i++) {
			$curr = $haystack[$i];
			if($curr->is($needle)) {
				if($i == 0) {
					$prev = null;
				} else {
					$prev = $haystack[$i - 1];
				}
				if($i == ($stacklen - 1)) {
					$next = null;
				} else {
					$next = $haystack[$i + 1];
				}
				return [
					"prev" => $prev,
					"next" => $next
				];
			}
		}
		return [
			"prev" => null,
			"next" => null
		];
	}
	protected function plain_text(Response $response, $text = '') {
		return $response->withHeader('Content-Type', 'text/plain')->write($text);
	}
	protected function xml_response(Response $response, $xml = '') {
		return $response->withHeader('Content-Type', 'text/xml')->write($xml);
	}
	protected function get_cache($record, $type = 'text') {
		$cache_type = \App\Models\TranscriptionCacheType::where('name', $type)->first();
		$cache = $record->caches()->whereHas('type', function($q) use ($cache_type) {
			$q->where('type_id', $cache_type->id);
		})->first();
		return $cache;
	}
	protected function make_cache($record, $cache_data, $type = 'text') {
		$cache = new \App\Models\TranscriptionCache();
		$cache->text = $cache_data;
		$cache_type = \App\Models\TranscriptionCacheType::firstOrCreate(['name' => $type]);
		$cache->type()->associate($cache_type);
		$cache->save();
		$record->caches()->save($cache);
	}
	protected function text_walking($record, $cache_type = 'text', $walker) {
		if(!$record) {
			return '';
		}
		// cache?
		$cache = $this->get_cache($record, $cache_type);
		if($cache) {
			return $cache->text;
		}
		// no cache
		$walker->walk($record);
		$transcription = $walker->get_transcription();
		$this->make_cache($record, $transcription, $cache_type);
		return $transcription;
	}
}
