<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class EditFacsimileSetController extends EditCategoryController {
	public function edit_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\FacsimileSet::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
		}
		return $this->view->render($response, 'edit/facsimileset/title.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_title(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$title = $this->get_post_or_get($request, 'value');
		if($title) {
			$record = \App\Models\FacsimileSet::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
			}
			$record->name = $title;
			$record->save();
			return $this->do_redirect($response, 'view_facsimileset', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_facsmimileset_title', ['id' => $args['id']]);
	}
	public function edit_copyright(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\FacsimileSet::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
		}
		return $this->view->render($response, 'edit/facsimileset/copyright.twig', [
			'record' => $record->toArray(),
			'loggedin' => true,
		]);
	}
	public function set_copyright(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$copyright_notice = $this->get_post_or_get($request, 'value_copyright_notice');
		$copyright_url = $this->get_post_or_get($request, 'value_copyright_url');
		if($copyright_notice || $copyright_url) {
			$record = \App\Models\FacsimileSet::find($args['id']);
			if(!$record) {
				return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
			}
			$record->copyright_notice = $copyright_notice;
			$record->copyright_url = $copyright_url;
			$record->save();
			return $this->do_redirect($response, 'view_facsimileset', ['id' => $args['id']]);
		}
		return $this->return_message($response, 'error', 'Submitted value not valid', 'edit_facsimileset_copyright', ['id' => $args['id']]);
	}
	public function delete(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\FacsimileSet::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
		}
		if(isset($args['confirm']) && $args['confirm'] == 'confirm') {
			$record->delete();
			return $this->return_message($response, 'success', 'Facsimile Set deleted', 'list_facsimilesets', []);
		}
		return $this->confirm_message($response, "Facsimile set will be deleted completely. Continue?", ['delete_facsimileset', ['id' => $args['id'], 'confirm' => 'confirm']], ['view_facsimileset', ['id' => $args['id']]]);
	}
	public function edit_facsimiles(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\FacsimileSet::with([
			'facsimiles' => function($q) {
				return $q->orderBy('name', 'ASC');
			}
		])->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
		}
		$facsimiles = $record->facsimiles;
		$others = \App\Models\Facsimile::doesntHave('set')->get();
		return $this->view->render($response, 'edit/facsimileset/facsimiles.twig', [
			'record' => $record->toArray(),
			'facsimiles' => $facsimiles->toArray(),
			'others' => $others->toArray(),
			'loggedin' => true,
		]);
	}
	public function add_facsimile(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\FacsimileSet::find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
		}
		$facsimile = \App\Models\Facsimile::find($args['tid']);
		if(!$facsimile) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'edit_facsimileset_facsimiles', ['id' => $args['id']]);
		}
		$facsimile->set()->associate($record);
		$facsimile->save();
		return $this->do_redirect($response, 'view_facsimileset', ['id' => $args['id']]);
	}
	public function remove_facsimile(Request $request, Response $response, array $args) {
		if(!$this->user_logged_in()) {
			return $response->withRedirect('/login');
		}
		$record = \App\Models\FacsimileSet::with('set')->find($args['id']);
		if(!$record) {
			return $this->return_message($response, 'error', 'Facsimile Set not found', 'list_facsimilesets', []);
		}
		$facsimile = $record->facsimiles()->find($args['tid']);
		if(!$facsimile) {
			return $this->return_message($response, 'error', 'Facsimile not found', 'edit_facsimileset_facsimiles', ['id' => $args['id']]);
		}
		$facsimile->set->dissociate();
		$facsimile->save();
		return $this->return_message($response, 'success', 'Facsimile removed', 'view_facsimileset', ['id' => $args['id']]);
	}
}
