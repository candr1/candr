<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLStaveController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'stave-' . $args['id'] . '.xml');
		$record = \App\Models\Stave::with(['items'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->xw->startElementNs($this->candrns, "stave", $this->candrns_endpoint);
		$this->add_id($this->xw, "stave", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
		{
			$warp = $record->warp()->first();
			if($warp) {
				$this->include_link($this->router->pathFor('warp_get_xml', ['id' => $warp->id]));
			}
		}
		{
			$this->xw->startElementNs($this->candrns, "items", $this->candrns_endpoint);
			$items = $record->items()->with(['type'])->get()->filter(function($item) {
				$type = $item->type()->first();
				return (get_class($type) !== \App\Models\Note::class) || !($type->ligature()->first());
			});
			$minx = $miny = INF;
			$maxx = $maxy = -INF;
			foreach($items as $item) {
				$type = $item->type()->first();
				$x = $type->comp_x;
				$y = $type->comp_y;
				if($x !== NULL) {
					if($x < $minx) {
						$minx = $x;
					}
					if($x > $maxx) {
						$maxx = $x;
					}
				}
				if($y !== NULL) {
					if($y < $miny) {
						$miny = $y;
					}
					if($y > $maxy) {
						$maxy = $y;
					}
				}
			}
			$sorted = $items->sortBy(function($item) use(&$minx, &$miny, &$maxx, &$maxy) {
				$type = $item->type()->first();
				$x = $type->comp_x - $minx;
				$y = $type->comp_y - $miny;
				$class = get_class($type);
				if($class === \App\Models\Staffline::class) {
					$fmt = "-" . str_pad(number_format($y, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($y, 2, '.', ''), 25, "0", STR_PAD_LEFT);
				} else {
					$addx = $x + $maxx;
					$addy = $y + $maxy;
					$fmt = str_pad(number_format($addx, 2, '.', ''), 25, "0", STR_PAD_LEFT) . "-" . str_pad(number_format($addy, 2, '.', ''), 25, "0", STR_PAD_LEFT);
				}
				return $fmt;
			});
			foreach($sorted as $item) {
				$this->include_link($this->router->pathFor('item_get_xml', ['id' => $item->id]));
			}
			$this->xw->endElement();
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
