<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use GuzzleHttp\Psr7;

class BrowseSystemSyncController extends BrowseCategoryController {
	public function view(Request $request, Response $response, array $args) {
		$record = \App\Models\System::with(['up', 'down', 'down.warp', 'down.items.type', 'transcribe.set', 'links.items', 'links.setting'])->find($args['id']);
		$transcribe = $record->transcribe;
		$facsimiles = $record->up;
		$links = $record->links;
		$fac = $folio = $source = null;
		if($facsimiles) {
			foreach($facsimiles as &$f) {
				if(!$f) {
					continue;
				}
				if($transcribe->is($f)) {
					$warp = $f->pivot->warp()->first();
					$transcribe['warp'] = $warp;
					$fac = $transcribe;
					break;
				}
			}
			if($fac) {
				$folio = $fac->up;
				if($folio) {
					$source = $folio->up;
				}
			}
		}
		$staves = $record->down;
		$output = [];
		foreach($staves as &$stave) {
			$staveout = [];
			$warp = $stave->warp;
			if(!$warp) {
				continue;
			}
			$staveout['warp'] = $warp->nodes;
			$items = $stave->items;
			if(!$items) {
				continue;
			}
			$staveout['items'] = [];
			foreach($items as $item) {
				$type = $item->type;
				if(!$type) {
					continue;
				}
				$nodes = $type->getNodes();
				if(empty($nodes)) {
					continue;
				}
				$staveout['items'][$item->id] = ['name' => $type->model_id, 'nodes' => $nodes];
			}

			$output[$stave->id] = $staveout;
		}
		$processed_links = [];
		foreach($links as $link) {
			$items = $link->items;
			$itemids = [];
			foreach($items as $item) {
				$itemids[] = $item->id;
			}
			$processed_links[$link->id] = [
				"ids" => $itemids,
				"setting" => ($link->setting ? true : false),
			];
		}
		return $this->view->render($response, 'browse/systemsynchronise.twig', [
			"system" => $record->toArray(),
			"transcribe" => $transcribe->makeVisible(['warp', 'set'])->toArray(),
			"items" => $output,
			"links" => $processed_links,
			'up' => [
				'view_source' => $source,
				'view_folio' => $folio,
				'view_facsimile' => $transcribe,
			],
			'down' => $staves->toArray(),
			'facsimiles' => $facsimiles,
			'loggedin' => $this->user_logged_in(),
		]);
	}
}
