<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLLinkController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'link-' . $args['id'] . '.xml');
		$record = \App\Models\Link::with(['items'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->xw->startElementNs($this->candrns, "link", $this->candrns_endpoint);
		$this->add_id($this->xw, "link", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
		{
			$items = $record->items()->get();
			foreach($items as $item) {
				$this->include_link($this->router->pathFor('item_get_xml', ['id' => $item->id]));
			}
			$this->xw->endElement();
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
