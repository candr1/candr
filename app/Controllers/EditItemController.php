<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class ParseMessageException extends \Exception { }

class EditItemController extends EditCategoryController {
	private $local_id_store = [];
	/* Generate stock error response */
	private function response_generate_error($user_message = null, $technical_message = null) {
		$err = ['result' => 'error'];
		if($user_message !== null) {
			$err['user_message'] = $user_message;
		}
		if($technical_message !== null) {
			$err['technical_message'] = $technical_message;
		}
		return $err;
	}
	/* Generate stock success response with updated items */
	private function response_generate_success($message = null, $update = null) {
		$succ = ['result' => 'success'];
		if($message !== null) {
			$succ['message'] = $message;
		}
		if($update !== null) {
			$succ['update'] = $update;
		}
		return $succ;
	}
	/* Validate array of messages */
	private function validate_input($in) {
		if(!is_array($in)) {
			return "Not an array of items";
		}
		return true;
	}
	/* Validate a single message */
	private function validate_message($msg) {
		/* There should only be one key => value */
		$keys = array_keys($msg);
		$vals = array_values($msg);
		if(count($keys) != 1 || count($vals) != 1) {
			return "Malformed message: more than one key-value pair";
		}
		$action = $keys[0];
		$props = $vals[0];
		$commands = ["delete", "edit"];
		if(!in_array($action, $commands)) {
			return "Command must be one of: " . implode(", ", $commands);
		}
		$rec = function($prps) use (&$rec) {
			foreach($prps as $setname => $setval) {
				if(!is_string($setname)) {
					return "Property set name is not a string";
				}
				foreach($setval as $name => $prop) {
					if(!is_string($name)) {
						return "Property name is not a string";
					}
					if(is_array($prop)) {
						foreach($prop as $p) {
							$ret = $rec($p);
							if($ret !== true) {
								return $ret;
							}
						}
					} else if(!(
						is_string($prop) ||
						is_numeric($prop) ||
						is_bool($prop)
					)) {
						return "Property type must be one of string, numeric or boolean";
					}
				}
			}
			return true;
		};
		return $rec($props);
	}
	/* Function for retrieving specific property by JSON diving */
	private function get_prop($props, $path) {
		$head = $props;
		foreach($path as $id) {
			if(!isset($head[$id])) {
				return null;
			}
			$head = $head[$id];
		}
		return $head;
	}
	/* Do a delete message */
	private function parse_delete($props, $stave) {
		$id = $this->get_prop($props, ["id", "id"]);
		if(!$id) {
			/* It never existed on the server anyway */
			return true;
		}
		$item = \App\Models\Item::with('up')->find($id);
		if(!$item) {
			/* Can't find it on the server anyway */
			return true;
		}
		if(!$item->up->is($stave)) {
			throw new ParseMessageException("Item with this ID does not belong to this stave");
		}
		$item->delete();
		return true;
	}
	private function local_id_lookup($local_id) {
		if(isset($this->local_id_store[$local_id])) {
			$id = $this->local_id_store[$local_id];
			return $id;
		}
		return false;
	}
	private function add_to_local_id_store($local_id, $id) {
		$this->local_id_store[$local_id] = $id;
		return true;
	}
	/* Use the name of an item to retrieve its model class */
	private function get_item_class($props) {
		$name = $this->get_prop($props, ["name", "name"]);
		$this->logger->info("Getting class for: " . $name);
		if(!$name) {
			throw new ParseMessageException("Cannot find name of props: " . json_encode($props));
			return false;
		}
		switch($name) {
			case "Staffline":
				return \App\Models\Staffline::class;
			case "Divisione":
				return \App\Models\Divisione::class;
			case "Clef":
				return \App\Models\Clef::class;
			case "Accidental":
				return \App\Models\Accidental::class;
			case "Syllable":
				return \App\Models\Syllable::class;
			case "Note":
				return \App\Models\Note::class;
			case "Ligature":
				return \App\Models\Ligature::class;
			case "EditorialCClef":
			case "EditorialFClef":
			case "EditorialDClef":
			case "EditorialAClef":
				return \App\Models\EditorialClef::class;
			case "EditorialSharp":
			case "EditorialFlat":
				return \App\Models\EditorialAccidental::class;
			case "EditorialSyllable":
				return \App\Models\EditorialSyllable::class;
			case "EditorialText":
				return \App\Models\EditorialText::class;
		}
		return null;
	}
	private function is_ligature_message($msg) {
		$keys = array_keys($msg);
		$vals = array_values($msg);
		if(count($keys) != 1 || count($vals) != 1) {
			throw new ParseMessageException("Malformed message: more than one key-value pair");
		}
		$action = $keys[0];
		$props = $vals[0];
		if($this->get_item_class($props) == \App\Models\Ligature::class) {
			return true;
		}
		return false;
	}
	/* Create a new item */
	private function edit_new($props, $stave) {
		$this->logger->info("edit_new");
		/* Create the 'item' wrapper */
		$item = new \App\Models\Item();
		/* Attach the editorial commment */
		$comment = $this->get_prop($props, ["editorial comment", "editorial comment"]);
		if($comment !== null) {
			$item->editorial_comment = $comment;
		}
		/* Attach the item to the stave */
		$item->up()->associate($stave);
		/* Attach the type to the item */
		[$type, $notes] = $this->create_new_type($props, $stave);
		$item->type()->associate($type);
		$item->save();
		$this->add_to_local_id_store($this->get_prop($props, ["id", "local_id"]), $item->id);
		return [$item->id, $notes];
	}
	private function notes_to_ligature($propnotearr, $ligature, $stave) {
		$notes_to_add = [];
		$ret_notes = [];
		foreach($propnotearr as $propnote) {
			/* Find the ID */
			$local_id = $this->get_prop($propnote, ["id", "local_id"]);
			$id = $this->get_prop($propnote, ["id", "id"]);
			if(!$id) {
				$id = $this->local_id_lookup($local_id);
			}
			if(!$id) {
				$this->logger->info("Cannot find ID in props: " . json_encode($propnote));
				$this->logger->info("local_id_store is: " . json_encode($this->local_id_store));
				/* No ID, make a new note */
				$args = \App\Models\Note::generate_args_from_props($propnote);
				if(!$args) {
					throw new ParseMessageException("Passed parameters for note were not valid");
				}
				$type = new \App\Models\Note($args);
				$type->save();
				$item = new \App\Models\Item();
				$item->up()->associate($stave);
				$item->type()->associate($type);
				$item->save();
				$this->logger->info('Made new note with ID: ' . $item->id);
				$propnote['id']['id'] = $item->id;
				$ret_notes[] = $propnote;
				$this->add_to_local_id_store($local_id, $item->id);
			} else {
				$this->logger->info("Found ID " . $id);
				/* ID found, retrieve item with this ID */
				$item = \App\Models\Item::find($id);
				if(!$item) {
					throw new ParseMessageException("Note with ID: " . $id . " does not exist");
				}
				/* Check it's part of the right stave */
				if(!$item->up()->first()->is($stave)) {
					throw new ParseMessageException("Item with this ID does not belong to this stave");
				}
				$type = $item->type()->first();
				if(get_class($type) != \App\Models\Note::class) {
					throw new ParseMessageException("Item with this ID is not a note");
				}
				$ret_notes[] = $propnote;
			}
			[$type, $notes] = $this->update_type($type, $propnote, $stave);
			array_push($ret_notes, ...$notes);
			$comment = $this->get_prop($propnote, ["editorial comment", "editorial comment"]);
			if($comment !== null) {
				$item->editorial_comment = $comment;
			}
			$notes_to_add[] = $type;
		}
		$this->logger->info("Saving notes: ". json_encode($notes_to_add) . " to ligature with ID: " . $ligature->id);
		$ligature->notes()->saveMany($notes_to_add);
		for($i = 1; $i < count($notes_to_add); $i++) {
			$notes_to_add[$i]->moveAfter($notes_to_add[$i - 1]);
		}
		$rids = [];
		$final_ret = [];
		foreach($ret_notes as $note) {
			if(in_array($note['id']['id'], $rids) || in_array($note['id']['local_id'], $rids)) {
				continue;
			}
			array_push($rids, $note['id']['id'], $note['id']['local_id']);
			$final_ret[] = $note;
		}
		return $final_ret;
	}
	private function create_new_type($props, $stave) {
		/* Get its class from the name */
		$item_class = $this->get_item_class($props);
		if(!$item_class) {
			throw new ParseMessageException("Item class not recognised");
		}
		/* Use supplied props to create args for new item */
		$args = $item_class::generate_args_from_props($props);
		if(!$args) {
			throw new ParseMessageException("Passed parameters were not valid");
		}
		/* Create the item */
		$this->logger->info('Creating new: ' . $item_class . ' with args: ' . json_encode($args));
		$type = new $item_class($args);
		$type->save();
		if($item_class == \App\Models\Ligature::class) {
			$noteprops = $this->get_prop($props, ["notes", "notes"]);
			$notes = $this->notes_to_ligature($noteprops, $type, $stave);
		}
		return [$type, $notes];
	}
	private function edit_preexisting($props, $id, $stave) {
		$this->logger->info("edit_preexisting");
		/* Find the item */
		$item = \App\Models\Item::find($id);
		if(!$item) {
			throw new ParseMessageException("Item does not exist");
		}
		if(!$item->up()->first()->is($stave)) {
			throw new ParseMessageException("Item with this ID does not belong to this stave");
		}
		$type = $item->type()->first();
		[$type, $notes] = $this->update_type($type, $props, $stave);
		/* Attach editorial comment */
		$comment = $this->get_prop($props, ["editorial comment", "editorial comment"]);
		if($comment !== null) {
			$item->editorial_comment = $comment;
		}
		/* Save! */
		$item->save();
		return [$item->id, $notes];
	}
	private function update_type($type, $props, $stave) {
		/* Get its class from the name */
		$item_class = $this->get_item_class($props);
		if(!$item_class) {
			throw new ParseMessageException("Item class not recognised");
		}
		/* The type needs to match the supplied type else something has
		 * gone wrong! */
		if(get_class($type) != $item_class) {
			throw new ParseMessageException("Item does not match type");
		}
		/* Use supplied props to create args */
		$args = $item_class::generate_args_from_props($props);
		if(!$args) {
			throw new ParseMessageException("Passed parameters were not valid");
		}
		$notes = [];
		if($item_class == \App\Models\Ligature::class) {
			$noteprops = $this->get_prop($props, ["notes", "notes"]);
			$notes = $this->notes_to_ligature($noteprops, $type, $stave);
		}
		/* Assign args to item */
		foreach($args as $prop => $val) {
			$type->$prop = $val;
		}
		$type->save();
		return [$type, $notes];
	}
	/* Do an edit */
	private function parse_edit($props, $stave) {
		/* If we have an ID prop then we think this item already
		 * exists */
		$id = $this->get_prop($props, ["id", "id"]);
		$this->logger->info("Parsing edit");
		if(!$id) {
			return $this->edit_new($props, $stave);
		}
		return $this->edit_preexisting($props, $id, $stave);
	}
	private function unique_and_filter_ligature_ids($ids) {
		$this->logger->info("Unique and filter IDs");
		$used_ids = [];
		$ids_in_ligatures = [];
		$ret = [];
		foreach($ids as $props) {
			if(!isset($props['notes'])) {
				continue;
			}
			foreach($props['notes'] as $note) {
				$id = $this->get_prop($note, ["id", "id"]);
				if($id) {
					$ids_in_ligatures[] = $id;
				}
			}
		}
		foreach(array_reverse($ids) as $props) {
			$id = $this->get_prop($props, ["id", "id"]);
			if($id) {
				if(in_array($id, $ids_in_ligatures)) {
					$this->logger->info($id . " is in ligature.");
					continue;
				}
				if(in_array($id, $used_ids)) {
					$this->logger->warning("Duplicate ID " . $id);
					continue;
				}
				$ret[] = $props;
				$used_ids[] = $id;
			}
		}
		return array_reverse($ret);
	}
	private function parse_message($msg, $stave) {
		$this->logger->info("Parsing message");
		/* There should only be one key => value */
		$keys = array_keys($msg);
		$vals = array_values($msg);
		if(count($keys) != 1 || count($vals) != 1) {
			throw new ParseMessageException("Malformed message: more than one key-value pair");
		}
		$action = $keys[0];
		$props = $vals[0];
		$local_id = $this->get_prop($props, ["id", "local_id"]);
		$id = $this->get_prop($props, ["id", "id"]);
		$name = $this->get_prop($props, ["name", "name"]);
		switch($action) {
			case "delete":
				$this->logger->info("Recognised as delete msg");
				$ret = $this->parse_delete($props, $stave);
				return [
					"id" => [
						"local_id" => $local_id,
						"id" => null
					],
					"name" => [
						"name" => $name,
					],
				];
			case "edit":
				$this->logger->info("Recognised as edit msg");
				[$id, $notes] = $this->parse_edit($props, $stave);
				$ret = [
					"id" => [
						"local_id" => $local_id,
						"id" => $id
					],
					"name" => [
						"name" => $name,
					],
				];
				if(count($notes) > 0) {
					$this->logger->info("This item has " . count($notes) . " notes");
					$ret['notes'] = $notes;
				}
				return $ret;
		}
		throw new ParseMessageException("Action '" . $action . "' not recognised");
		return false;
	}
	private function same_message($aa, $ab) {
		$reccmp = function($a, $b) use (&$reccmp) {
			foreach($a as $k => $av) {
				if(!array_key_exists($k, $b)) {
					return false;
				}
				$bv = $b[$k];
				if(is_array($av)) {
					if(is_array($bv)) {
						if(!$reccmp($av, $bv)) {
							return false;
						}
					} else {
						return false;
					}
				} else if($av !== $bv) {
					return false;
				}
			}
			return true;
		};
		return $reccmp($aa, $ab);
	}
	private function predict_stave($stave) {
		if($stave->transcribed) {
			throw new ParseMessageException("Cannot fetch predictions for transcribed stave");
			return false;
		}
		$predictions = $stave->predictions;
		$stafflines = $predictions['stafflines'] ?? [];
		$notes = $predictions['notes'] ?? [];
		$divisiones = $predictions['divisiones'] ?? [];
		$clefs = $predictions['clefs'] ?? [];
		$accidentals = $predictions['accidentals'] ?? [];
		foreach($stafflines as $staffline) {
			$props = [
				"editorial_comment" => [
					"editorial_comment" => "",
				],
				"name" => [
					"name" => "Staffline",
				],
				"nodes" => [
					"ax" => floatval($staffline[0][0]),
					"ay" => floatval($staffline[0][1]),
					"bx" => floatval($staffline[1][0]),
					"by" => floatval($staffline[1][1]),
				],
			];
			$this->edit_new($props, $stave);
		}
		foreach($notes as $note) {
			$props = [
				"editorial_comment" => [
					"editorial_comment" => "",
				],
				"name" => [
					"name" => "Note",
				],
				"node" => [
					"x" => floatval($note[0]),
					"y" => floatval($note[1]),
				],
				"shift" => [
					"shift" => 0
				],
				"plica" => [
					"plica" => false,
				],
			];
			$this->edit_new($props, $stave);
		}
		foreach($divisiones as $divisione) {
			$props = [
				"editorial_comment" => [
					"editorial_comment" => "",
				],
				"name" => [
					"name" => "Divisione",
				],
				"nodes" => [
					"ax" => floatval($divisione[0][0]),
					"ay" => floatval($divisione[0][1]),
					"bx" => floatval($divisione[1][0]),
					"by" => floatval($divisione[1][1]),
				],
			];
			$this->edit_new($props, $stave);
		}
		foreach($clefs as $clef) {
			$props = [
				"editorial_comment" => [
					"editorial_comment" => "",
				],
				"name" => [
					"name" => "Clef",
				],
				"position" => [
					"top" => floatval($clef['top']),
					"left" => floatval($clef['left']),
					"width" => floatval($clef['width']),
					"height" => floatval($clef['height']),
					"angle" => floatval($clef['angle']),
				],
				"centrepoint" => [
					"x" => floatval($clef['centrepoint_x']),
					"y" => floatval($clef['centrepoint_y']),
				],
				"type" => [
					"type" => (
						in_array($clef['type'], ['F', 'D', 'A']) 
						? $clef['type']
						: 'C'
					),
				],
			];
			$this->edit_new($props, $stave);
		}
		foreach($accidentals as $accidental) {
			$props = [
				"editorial_comment" => [
					"editorial_comment" => "",
				],
				"name" => [
					"name" => "Accidental",
				],
				"position" => [
					"top" => floatval($accidental['top']),
					"left" => floatval($accidental['left']),
					"width" => floatval($accidental['width']),
					"height" => floatval($accidental['height']),
					"angle" => floatval($accidental['angle']),
				],
				"centrepoint" => [
					"x" => floatval($accidental['centrepoint_x']),
					"y" => floatval($accidental['centrepoint_y']),
				],
				"type" => [
					"type" => (strtolower($accidental['type']) == 'sharp' ? 'Sharp' : 'Flat'),
				],
			];
			$this->edit_new($props, $stave);
		}
		return $this->request_all_data($stave);
	}
	public function receive_message(Request $request, Response $response, array $args) {
		$body = $request->getParsedBody();
		/* Clean up ligatures that have no notes */
		\App\Models\Ligature::doesntHave('notes')->delete();
		if(!isset($body['stave'])) {
			return $response->withStatus(400)->withJson($this->response_generate_error("Error saving", "No stave ID passed"));
		}
		$stave = \App\Models\Stave::find($body['stave']);
		if(!$stave) {
			return $response->withStatus(400)->withJson($this->response_generate_error("Error saving", "Stave does not exist"));
		}
		if(
			$this->same_message($body['data'], [["recall"]]) ||
			$this->same_message($body['data'], ["recall"])
		) {
			try {
				$dat = $this->request_all_data($stave);
				return $response->withStatus(200)->withJson($this->response_generate_success(null, $dat));
			} catch(ParseMessageException $e) {
				return $response->withStatus(400)->withJson($this->response_generate_error("Error requesting data", "Request data: " . $e->getMessage()));
			}
		}
		if(!$this->user_logged_in()) {
			return $response->withStatus(401)->withJson($this->response_generate_error("User not logged in"));
		}
		if($this->same_message($body['data'], [["predict"]])) {
			try {
				$dat = $this->predict_stave($stave);
				return $response->withStatus(200)->withJson($this->response_generate_success(null, $dat));
			} catch(ParseMessageException $e) {
				return $response->withStatus(400)->withJson($this->response_generate_error("Error fetching predictions", "Request data: ", $e->getMessage()));
			}
		}
		$ret = $this->validate_input($body['data']);
		if($ret !== true) {
			return $response->withStatus(400)->withJson($this->response_generate_error("Error saving", "Validate input: " . $ret));
		}
		$resp = null;
		$ids = [];
		//\Illuminate\Database\Capsule\Manager::beginTransaction();
		$ligature_msgs = [];
		foreach($body['data'] as $msg) {
			$ret = $this->validate_message($msg);
			if($ret !== true) {
				$resp = $response->withStatus(400)->withJson($this->response_generate_error("Error saving", "Validate message: " . $ret));
				continue;
			}
			try {
				if($this->is_ligature_message($msg)) {
					$ligature_msgs[] = $msg;
					continue;
				}
				$tids = $this->parse_message($msg, $stave);
				$ids[] = $tids;
			} catch(ParseMessageException $e) {
				$resp = $response->withStatus(400)->withJson($this->response_generate_error("Error saving", "Parse message: " . $e->getMessage()));
				continue;
			}
		}
		if($resp) {
			//\Illuminate\Database\Capsule\Manager::rollback();
			return $resp;
		}
		//\Illuminate\Database\Capsule\Manager::commit();
		//\Illuminate\Database\Capsule\Manager::beginTransaction();
		foreach($ligature_msgs as $msg) {
			try {
				$tids = $this->parse_message($msg, $stave);
				$ids[] = $tids;
			} catch(ParseMessageException $e) {
				$resp = $response->withStatus(400)->withJson($this->response_generate_error("Error saving", "Parse message: " . $e->getMessage()));
				continue;
			}
		}
		if($resp) {
			//\Illuminate\Database\Capsule\Manager::rollback();
			return $resp;
		}
		//\Illuminate\Database\Capsule\Manager::commit();
		$ids = $this->unique_and_filter_ligature_ids($ids);
		$json = $this->response_generate_success(null, $ids);
		return $response->withStatus(200)->withJson($json);
	}
}
