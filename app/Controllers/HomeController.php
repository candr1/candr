<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class HomeController extends LoginController {
	public function index(Request $request, Response $response) {
		$loggedin = $this->user_logged_in();
		$recent_staves = \App\Models\Stave::latest('updated_at')->with('up.transcribe.up')->take(10)->get();
		$proc_staves = [];
		foreach($recent_staves as $stave) {
			$first = $stave->up;
			if(!$first || $first->count() === 0) {
				continue;
			}
			$transcribe = $first->transcribe;
			if($transcribe->count() === 0) {
				continue;
			}
			$transcribe_first = $transcribe;
			if(!$transcribe_first) {
				continue;
			}
			$proc_staves[] = [
				'updated_at' => $stave->updated_at->timestamp,
				'folio' => $transcribe_first->toArray(),
				'id' => $stave->id,
			];
		}
		$data = [
			'loggedin' => $loggedin,
			'recent_staves' => $proc_staves,
		];
		return $this->view->render($response, 'home.twig', $data);
	}
	public function about(Request $request, Response $response) {
		$data['loggedin'] = $this->user_logged_in();
		return $this->view->render($response, 'home.twig', $data);
	}
	public function transcription(Request $request, Response $response) {
		$loggedin = $this->user_logged_in();
		$n_facsimiles = \App\Models\Facsimile::count();
		$n_transcribed_facsimiles = \App\Models\Facsimile::where('transcribed', true)->count();
		$n_facsimiles_with_systems = \App\Models\Facsimile::has('down')->count();
		$per_transcribed_facsimiles = $n_facsimiles > 0 ? round(
			$n_transcribed_facsimiles * 100 / (float)$n_facsimiles
		) : 0;
		$n_systems = \App\Models\System::count();
		$n_transcribed_systems = \App\Models\System::where('transcribed', true)->count();
		$per_transcribed_systems = $n_systems > 0 ? round(
			$n_transcribed_systems * 100 / (float)$n_systems
		) : 0;
		$n_systems_with_staves = \App\Models\System::has('down')->count();
		$n_staves = \App\Models\Stave::count();
		$n_transcribed_staves = \App\Models\Stave::where('transcribed', true)->count();
		$per_transcribed_staves = $n_staves > 0 ? round(
			$n_transcribed_staves * 100 / (float)$n_staves
		) : 0;
		$n_omr = \App\Models\Stave::where('transcribed', false)->where('last_predicted', '!=', '-infinity')->count();
		$n_untranscribed_staves = $n_staves - $n_transcribed_staves;
		$per_omr_staves = $n_untranscribed_staves > 0 ? round(
			$n_omr * 100 / (float)$n_untranscribed_staves
		) : 0;
		$data = [
			'loggedin' => $loggedin,
			'n_staves' => $n_staves,
			'per_transcribed_staves' => $per_transcribed_staves,
			'n_systems' => $n_systems,
			'per_transcribed_systems' => $per_transcribed_systems,
			'n_systems_no_staves' => ($n_systems - $n_systems_with_staves),
			'n_facsimiles' => $n_facsimiles,
			'per_transcribed_facsimiles' => $per_transcribed_facsimiles,
			'n_facsimiles_no_systems' => ($n_facsimiles - $n_facsimiles_with_systems),
			'n_omr' => $n_omr,
			'per_omr_staves' => $per_omr_staves
		];
		return $this->view->render($response, 'transcription.twig', $data);
	}
}
