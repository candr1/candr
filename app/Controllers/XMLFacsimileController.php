<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLFacsimileController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'facsimile-' . $args['id'] . '.xml');
		$record = \App\Models\Facsimile::with(['down'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->xw->startElementNs($this->candrns, "facsimile", $this->candrns_endpoint);
		$this->add_id($this->xw, "facsimile", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$this->add_dates($record);
		$this->xw->writeAttributeNs($this->candrns, "image", $this->candrns_endpoint, $this->router->pathFor('image', ['id' => $record->id]));
		{
			$this->xw->startElementNs($this->candrns, "systems", $this->candrns_endpoint);
			$systems = $record->down()->get();
			foreach($systems as $system) {
				$this->include_link($this->router->pathFor('system_get_xml', ['id' => $system->id]));
			}
			$this->xw->endElement();
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
