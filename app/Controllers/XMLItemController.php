<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLItemController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'item-' . $args['id'] . '.xml');
		$record = \App\Models\Item::with(['type'])->find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->new_item($record);
		{
			$record_type = $record->type()->first();
			if(!$record_type) {
				return $this->get_output_string($item);
			}
			$this->new_type($record_type);
			$this->xw->endElement();
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
	private function new_item(&$record) {
		$this->xw->startElementNs($this->candrns, "item", $this->candrns_endpoint);
		$this->add_id($this->xw, "item", $record->id);
		$this->add_generic_root_attributes($this->xw);
		if($record->editorial_comment) {
			$this->xw->writeAttributeNs($this->candrns, "editorialComment", $this->candrns_endpoint, $record->editorial_comment);
		}
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
	}
	private function new_type($record_type) {
		$ns = $this->candrns;
		$ep = $this->candrns_endpoint;
		$xw = $this->xw;
		$add_id = function(&$xml, &$it, &$id) { return $this->add_id($xml, $it, $id); };
		$generic_new = function($type, $name, $attrs) use (&$xw, &$ns, &$ep, &$add_id) {
			$xw->startElementNs($ns, $name, $ep);
			$id = $type->id;
			($add_id)($xw, $name, $id);
			foreach($attrs as $tattr => $dat) {
				$xattr = $dat[0];
				$fun = $dat[1];
				$required = $dat[2];
				$transformed = $type->$tattr;
				if($fun) {
					$transformed = $fun($transformed);
				}
				if($required || $transformed !== null) {
					$xw->writeAttributeNs($ns, $xattr, $ep, $transformed);
				}
			}
		};
		$rtwo = function($a) { return round($a, 2); };
		$rint = function($a) { return (int)round($a); };
		$datefmt = function($d) { return $d ? $d->format('c') : null; };
		$end_el = function() { return $this->xw->endElement(); };
		$class = get_class($record_type);
		$funcs = [
			\App\Models\Ligature::class => function($t) use(&$xw, &$ns, &$ep, &$add_id, &$datefmt, &$funcs, &$end_el) {
				$xw->startElementNs($ns, "ligature", $ep);
				$tagname = "ligature";
				$id = $t->id;
				($add_id)($xw, $tagname, $id);
				$xw->writeAttributeNs($ns, "ligatureType", $ep, $t->type);
				if($t->created_at) {
					$xw->writeAttributeNs($ns, "createdAt", $ep, $datefmt($t->created_at));
				}
				if($t->updated_at) {
					$xw->writeAttributeNs($ns, "updatedAt", $ep, $datefmt($t->updated_at));
				}
				if($t->deleted_at) {
					$xw->writeAttributeNs($ns, "deletedAt", $ep, $datefmt($t->deleted_at));
				}
				$notes = $t->notes()->get();
				foreach($notes as $note) {
					$funcs[\App\Models\Note::class]($note);
					$end_el();
				}
				$end_el();
			},
			\App\Models\Accidental::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'width' => ['width', $rtwo, true],
					'height' => ['height', $rtwo, true],
					'angle' => ['angle', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['accidentalType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "accidental", $attrs);
			},
			\App\Models\Clef::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'width' => ['width', $rtwo, true],
					'height' => ['height', $rtwo, true],
					'angle' => ['angle', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['clefType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "clef", $attrs);
			},
			\App\Models\Divisione::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'ax' => ['ax', $rtwo, true],
					'ay' => ['ay', $rtwo, true],
					'bx' => ['bx', $rtwo, true],
					'by' => ['by', $rtwo, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "divisione", $attrs);
			},
			\App\Models\EditorialAccidental::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['accidentalType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialAccidental", $attrs);
			},
			\App\Models\EditorialClef::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'type' => ['clefType', null, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialClef", $attrs);
			},
			\App\Models\EditorialSyllable::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'text' => ['text', null, false],
					'wordstart' => ['wordstart', function($a) { return $a ? "true" : "false"; }, false],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialSyllable", $attrs);
			},
			\App\Models\EditorialText::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'scale' => ['scale', $rtwo, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "editorialText", $attrs);
			},
			\App\Models\Note::class => function($t) use (&$generic_new, &$rtwo, &$rint, &$datefmt) {
				$attrs = [
					'x' => ['x', $rtwo, true],
					'y' => ['y', $rtwo, true],
					'shift' => ['shift', $rint, false],
					'plica' => ['plica', $rint, false],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "note", $attrs);
			},
			\App\Models\Staffline::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'ax' => ['ax', $rtwo, true],
					'ay' => ['ay', $rtwo, true],
					'bx' => ['bx', $rtwo, true],
					'by' => ['by', $rtwo, true],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "staffline", $attrs);
			},
			\App\Models\Syllable::class => function($t) use (&$generic_new, &$rtwo, &$datefmt) {
				$attrs = [
					'top' => ['top', $rtwo, true],
					'left' => ['left', $rtwo, true],
					'width' => ['width', $rtwo, true],
					'height' => ['height', $rtwo, true],
					'angle' => ['angle', $rtwo, true],
					'centrepoint_x' => ['centrepoint-x', $rtwo, true],
					'centrepoint_y' => ['centrepoint-y', $rtwo, true],
					'text' => ['text', null, false],
					'wordstart' => ['wordstart', function($a) { return $a ? "true" : "false"; }, false],
					'created_at' => ['createdAt', $datefmt, false],
					'updated_at' => ['updatedAt', $datefmt, false],
					'deleted_at' => ['deletedAt', $datefmt, false],
				];
				return $generic_new($t, "syllable", $attrs);
			},
		];
		if(!isset($funcs[$class])) {
			return null;
		}
		return ($funcs[$class])($record_type);
	}
}
