<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class SearchController {
	protected $view;
	protected $logger;
	public function __construct($logger, Twig $view) {
		$this->logger = $logger;
		$this->view = $view;
	}
	public function search(Request $request, Response $response) {
		if(!isset($_SERVER['HTTP_HOST'])) {
			error_log("cannot find HTTP host.");
			return $response->withJson(['message' => 'cannot find HTTP host']);
		}
		$dom = $_SERVER['HTTP_HOST'];
		$params = $request->getQueryParams();
		if(!isset($params['q'])) {
			$params['q'] = "";
		}
		$q = $params['q'];
		$search = urlencode("site: ". $dom . " " . $q);
		$search = "https://duckduckgo.com/?kj=%23353535&kx=l&k7=w&k8=g&kaa=p&ks=l&km=m&kt=Palatino&ka=Palatino&ku=-1&ko=s&k1=-1&ia=web&q=" . $search;
		return $response->withRedirect($search, 302);
	}
}
