<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class XMLWarpController extends XMLGenericController {
	public function generate(Request $request, Response $response, array $args) {
		$response = $this->set_headers($response, 'warp-' . $args['id'] . '.xml');
		$record = \App\Models\Warp::find($args['id']);
		if(!$record) {
			return $this->end_xml_writer($this->xw);
		}
		$this->xw->startElementNs($this->candrns, "warp", $this->candrns_endpoint);
		$this->add_id($this->xw, "warp", $record->id);
		$this->add_generic_root_attributes($this->xw);
		$dates = [
			'createdAt' => $record->created_at,
			'updatedAt' => $record->updated_at,
			'deletedAt' => $record->deleted_at,
		];
		foreach($dates as $attr => $val) {
			if(!$val) {
				continue;
			}
			$this->xw->writeAttributeNs($this->candrns, $attr, $this->candrns_endpoint, $val->format('c'));
		}
		{
			$nodes = $record->nodes;
			foreach($nodes as $node) {
				$this->xw->startElementNs($this->candrns, "node", $this->candrns_endpoint);
				$this->xw->writeAttributeNs($this->candrns, "x", $this->candrns_endpoint, $node[0]);
				$this->xw->writeAttributeNs($this->candrns, "y", $this->candrns_endpoint, $node[1]);
				$this->xw->endElement();
			}
			$this->xw->endElement();
		}
		$this->xw->endElement();
		return $response->write($this->end_xml_writer($this->xw));
	}
}
