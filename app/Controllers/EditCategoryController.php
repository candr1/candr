<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

abstract class EditCategoryController extends LoginController {
	protected $logger;
	protected $view;
	protected $upload_dir;
	protected $thumb_dir;
	protected $thumb_x;
	protected $thumb_y;
	protected $router;
	public function __construct($logger, Twig $view, $upload_dir, $thumb_dir, $thumb_x, $thumb_y, $router, $logintime) {
		parent::__construct($logger, $view, $logintime);
		$this->upload_dir = $upload_dir;
		$this->thumb_dir = $thumb_dir;
		$this->thumb_x = $thumb_x;
		$this->thumb_y = $thumb_y;
		$this->router = $router;
	}
	protected function thumbnailImage($fname) {
		$fullname = $this->upload_dir . DIRECTORY_SEPARATOR . $fname;
		$im = new \Imagick(realpath($fullname));
		$im->setbackgroundcolor('rgb(0, 0, 0)');
		$im->thumbnailImage($this->thumb_x, $this->thumb_y, true);
		$fullout = realpath($this->thumb_dir) . DIRECTORY_SEPARATOR . $fname;
		$im->writeImage($fullout);
	}
	protected function moveUploadedFile($dir, \Slim\Http\UploadedFile $uf) {
		$ext = pathinfo($uf->getClientFilename(), PATHINFO_EXTENSION);
		$bn = uniqid();
		$fn = sprintf('%s.%0.8s', $bn, $ext);
		$uf->moveTo($dir . DIRECTORY_SEPARATOR . $fn);
		return $fn;
	}
	protected function get_history($record) {
		$hist_field = $record::$history_field ?? 'history';
		$history = $record->$hist_field;
		ksort($history, SORT_NUMERIC);
		return $history;
	}
	private function get_filtered_history($record, $key) {
		$history = $this->get_history($record);
		$last = [];
		if($history) {
			$klast = array_key_last($history);
			$last = array_pop($history);
		}
		if(!$last) {
			return $last;
		}
		$new = [];
		$new[$klast] = $last;
		foreach($history as $time => $model) {
			if(isset($last[$key]) && isset($model[$key]) && $last[$key] == $model[$key]) {
				continue;
			}
			$new[$time] = $model;
			$last = $model;
		}
		return $new;
	}
	private function get_history_times_and_keys($record, $key) {
		$filthist = $this->get_filtered_history($record, $key);
		$new = [];
		foreach($filthist as $time => $model) {
			if(isset($model[$key])) {
				$new[$time] = $model[$key];
			}
		}
		return $new;
	}
	protected function get_history_classes($record, $key, $class) {
		$tandk = $this->get_history_times_and_keys($record, $key);
		$kandt = [];
		foreach($tandk as $time => $key) {
			$kandt[$key] = $time;
		}
		$models = $this->get_models_by_id($class, array_keys($kandt));
		$new = [];
		foreach($models as $model) {
			if(isset($kandt[$model['id']])) {
				$new[$kandt[$model['id']]] = $model;
			}
		}
		return $new;
	}
	private function get_models_by_id($class, $ids) {
		return $class::find($ids)->toArray();;
	}
	protected function swap($d, $a, $b) {
		error_log(print_r($d, true));
		$e = $d->find([$a, $b]);
		if(count($e) != 2) {
			$msg = "Could not find records with these IDs";
			error_log($msg);
			return $msg;
		}
		$e[0]->moveAfter($e[1]);
		return 'OK';
	}
	protected function get_post_or_get($request, $key) {
		return $request->getParsedBodyParam($key) ?? $request->getQueryParam($key);
	}
	protected function return_message($response, $type, $message, $routename, $options) {
		return $this->view->render($response, 'message.twig', [
			'type' => $type,
			'message' => $message,
			'routename' => $routename,
			'options' => $options,
			'loggedin' => true,
		]);
	}
	protected function do_redirect($response, $routename, $options) {
		$url = $this->router->pathFor($routename, $options);
		return $response->withRedirect($url);
	}

	protected function confirm_message($response, $message, $yes_data, $no_data) {
		return $this->view->render($response, 'confirm.twig', [
			'message' => $message,
			'yes' => [
				'route' => $yes_data[0],
				'args' => $yes_data[1],
			],
			'no' => [
				'route' => $no_data[0],
				'args' => $no_data[1],
			],
			'loggedin' => true,
		]);
	}
}
