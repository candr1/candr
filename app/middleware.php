<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\HttpBasicAuthentication;

require __DIR__ . '/Authenticators/DefaultAuthenticator.php';

$app->add(function (Request $request, Response $response, callable $next) {
	$uri = $request->getUri();
	$path = $uri->getPath();
	if(substr($path, -1) != "/") {
		$uri = $uri->withPath($path . "/");
		if($request->getMethod() == 'GET') {
			return $response->withRedirect((string)$uri, 301);
		} else {
			return $next($request->withUri($uri), $response);
		}
	}
	return $next($request, $response);
});

//$app->add(new RunTracy\Middlewares\TracyMiddleware($app));

$app->add(new HttpBasicAuthentication([
	'path' => '/login',
	'realm' => 'Login',
	'authenticator' => new App\Authenticators\DefaultAuthenticator,
	'error' => function($response, $args) {
		$_SESSION['login'] = 0;
		return $response->withRedirect('/login');
	},
	'after' => function($response, $args) {
		$_SESSION['login'] = time();
	},
]));

