<?php
namespace App\Walkers;

class WalkerIDStore {
	private $ids;
	private $logger;

	public function __construct($logger = null) {
		$this->logger = $logger;
		$this->ids = [];
	}
	public function add($item) {
		$class = get_class($item);
		if(!array_key_exists($class, $this->ids)) {
			$this->ids[$class] = new \Ds\Set();
		}
		$this->ids[$class]->add($item->id);
	}
	public function contains($item) {
		$class = get_class($item);
		if(!array_key_exists($class, $this->ids)) {
			return false;
		}
		return $this->ids[$class]->contains($item->id);
	}
	public function get_ids_of_class($class) {
		if(!array_key_exists($class, $this->ids)) {
			return [];
		}
		return $this->ids[$class]->toArray();
	}
}

abstract class AbstractWalker {
	private $walked_ids;
	protected $transcription;
	protected $logger;
	private $limit;
	private $limit_count;
	public function __construct($logger = null, $limit = ['App\Models\Facsimile' => 10]) {
		$this->walked_ids = new WalkerIDStore();
		$this->logger = $logger;
		$this->limit = $limit;
		$this->limit_count = [];
	}
	public function get_ids_of_class($class) {
		return $this->walked_ids->get_ids_of_class($class);
	}
	protected function is_walked($record) {
		return $this->walked_ids->contains($record);
	}
	protected function record_walk($record) {
		if($this->is_walked($record)) {
			return true;
		}
		$class = get_class($record);
		if(isset($this->limit[$class])) {
			if(!isset($this->limit_count[$class])) {
				$this->limit_count[$class] = 0;
			}
			if($this->limit_count[$class] > $this->limit[$class]) {
				$this->logger->error("Reached limit of walk for " . $class);
				return false;
			}
			$this->limit_count[$class]++;
		}
		$this->walked_ids->add($record);
		return true;
	}
	protected function walk($record, $walk_up = false, $start = null, $stop = null, $start_from = null, $transcribing_model = 'App\Models\Stave') {
		if($this->is_walked($record)) {
			return !$this->transcription->state->is_stop();
		}
		if(!$this->record_walk($record)) {
			return false;
		}
		$rec_class = get_class($record);
		if($rec_class === 'App\Models\Folio') {
			$this->transcription->page($record);
		}
		if($rec_class === $transcribing_model) {
			$ret = $this->transcription->transcribe($record, $start, $stop);
			if($this->transcription->state->is_stop()) {
				return false;
			}
		} else {
			if(method_exists($record, 'down')) {
				$children = $record->down();
				try {
					$children = $children->sorted();
				} catch (\BadMethodCallException $e) { }
				$children = $children->get();
				$sorted_children = $children;
				if(count($children) > 0) {
					switch(get_class($children[0])) {
						case 'App\Models\Stave':
							$sorted_children = $children->sort(function($a, $b) {
								$a_class = get_class($a);
								$b_class = get_class($b);
								if($a_class !== $b_class) {
									return 0;
								}
								$the_class = $a_class;
								$a_warp = null;
								$b_warp = null;
								switch($the_class) {
									case 'App\Models\Stave':
										$a_warp = $a->warp()->first();
										$b_warp = $b->warp()->first();
										break;
									case 'App\Models\System':
										$a_warp = $a->up()->first()->pivot->warp()->first();
										$b_warp = $b->up()->first()->pivot->warp()->first();
										break;
								}
								$a_centroid = $a_warp->centroid;
								$b_centroid = $b_warp->centroid;
								$c = $a_centroid[1] <=> $b_centroid[1];
								if($c) {
									return $c;
								}
								return ($a_centroid[0] <=> $b_centroid[0]);
							});
							break;
					}
					$transcribe_child = true;
					if(!is_null($start_from)) {
						$transcribe_child = false;
					}
					foreach($sorted_children as $child) {
						if($transcribe_child === false) {
							if($start_from->contains($child)) {
								$transcribe_child = true;
							}
							continue;
						} else if($this->is_walked($child)) {
							continue;
						}
						$ret = $this->walk($child, false, $start, $stop, null, $transcribing_model);
						if($ret !== true) {
							return $ret;
						}
					}
				} else {
				}
			}
		}
		if($walk_up && method_exists($record, 'up')) {
			$parent = $record->up()->first();
			if($parent && !$this->is_walked($parent)) {
				$start_from = new \Illuminate\Database\Eloquent\Collection([$record]);
				return $this->walk($parent, $walk_up, $start, $stop, $start_from, $transcribing_model);
			}
		}
		return true;
	}
	public function get_transcription() {
		return $this->transcription->get_transcription();
	}
}
