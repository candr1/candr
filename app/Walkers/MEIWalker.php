<?php

namespace App\Walkers;

class MEIWalker extends AbstractWalker {
	public function __construct($logger = null, $limit = ['App\Models\Facsimile' => 10]) {
		$this->transcription = new \App\Transcribers\MEITranscriber($logger);
		parent::__construct($logger, $limit);
	}
	public function walk($record, $walk_up = false, $start = null, $stop = null, $start_from = null, $transcribing_model = 'App\Models\System') {
		return parent::walk($record, $walk_up, $start, $stop, $start_from, $transcribing_model);
	}
}
