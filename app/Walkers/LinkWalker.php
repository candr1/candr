<?php

namespace App\Walkers;

class LinkWalker extends AbstractWalker {
	public function __construct($starting_system_id, $logger = null) {
		parent::__construct($logger);
		$this->transcription = new \App\Transcribers\LinkTranscriber($starting_system_id, $logger);
	}
	public function walk($record, $walk_up = false, $start = null, $stop = null, $start_from = null, $transcribing_model = 'App\Models\System') {
		switch(get_class($record)) {
			case 'App\Models\Setting':
				return $this->walk($record->link()->first(), $walk_up, $start, $stop, $start_from, $transcribing_model);
			case 'App\Models\Link':
				return $this->walk($record->system()->first(), $walk_up, $start, $stop, $start_from, $transcribing_model);
			case 'App\Models\System':
				// we won't be going any further down than this,
				// so we'd better add all the staves to the
				// walked_ids here.
				$staves = $record->down()->get();
				foreach($staves as $stave) {
					$this->record_walk($stave);
				}
				// fallthru...
			default:
				return parent::walk($record, $walk_up, $start, $stop, $start_from, $transcribing_model);
		}
	}
}
