<?php

namespace App\Walkers;

class TextWalker extends AbstractWalker {
	public function __construct($logger = null, $limit = ['App\Models\Facsimile' => 10]) {
		$this->transcription = new \App\Transcribers\TextTranscriber($logger);
		parent::__construct($logger, $limit);
	}
	public function walk($record, $walk_up = false, $start = null, $stop = null, $start_from = null, $transcribing_model = 'App\Models\Stave') {
		if(get_class($record) === 'App\Models\System') {
			if($this->is_walked($record)) {
				return;
			}
			if(!$this->record_walk($record)) {
				return false;
			}
			$staves = $record->down()->get();
			// backwards so we get the LOWEST stave
			$sorted_staves = $staves->sort(function($b, $a) {
				$a_centroid = $a->warp()->first()->centroid;
				$b_centroid = $b->warp()->first()->centroid;
				$c = $a_centroid[1] <=> $b_centroid[1];
				if($c) {
					return $c;
				}
				return ($a_centroid[0] <=> $b_centroid[0]);
			});
			// what is last is now first
			$last = $sorted_staves->first();
			$ret = $this->walk($last, $walk_up, $start, $stop, $start_from, $transcribing_model);
			if($ret === false) {
				return false;
			}
			if($walk_up && method_exists($record, 'up')) {
				$parent = $record->up()->first();
				$start_from = new \Illuminate\Database\Eloquent\Collection([$record]);
				if($parent && !$this->is_walked($parent)) {
					return $this->walk($parent, $walk_up, $start, $stop, $start_from, $transcribing_model);
				}
			}
		} else {
			return parent::walk($record, $walk_up, $start, $stop, $start_from, $transcribing_model);
		}
		return true;
	}
}
