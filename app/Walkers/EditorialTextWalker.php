<?php

namespace App\Walkers;

class EditorialTextWalker extends TextWalker {
	public function __construct($logger = null, $limit = ['App\Models\Facsimile' => 10]) {
		$this->transcription = new \App\Transcribers\EditorialTextTranscriber($logger);
		AbstractWalker::__construct($logger, $limit);
	}
}

