<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warp extends Model {
	use SoftDeletes;

	public $timestamps = true;

	protected $fillable = [];

	protected $dates = ['deleted_at'];

	protected $visible = ['id', 'nodes', 'created_at', 'updated_at', 'deleted_at'];

	protected static $history_field = 'history';

	protected $casts = [
		'nodes' => 'array',
	];

	public function getModelIdAttribute() {
		return 'warp';
	}
	public function getCentroidAttribute() {
		$poly = new \Geometry\Polygon($this->nodes);
		return $poly->centroid();
	}
}
