<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class MachineLearningStatus extends Model {
	public $timestamps = false;

	protected $fillable = ['status'];

	protected $visible = ['status', 'last_updated'];

	protected $dates = ['last_updated'];

	protected $dateFormat = 'Y-m-d H:i:s.u';

	protected $casts = ['status' => 'array'];
}
