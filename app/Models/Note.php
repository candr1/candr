<?php

namespace App\Models;

class Note extends ItemModel {
	use \Rutorika\Sortable\SortableTrait;

	protected $fillable = ['x', 'y', 'shift', 'plica'];

	protected $visible = ['id', 'ligature', 'created_at', 'updated_at', 'deleted_at', 'x', 'y', 'shift', 'plica'];

	protected static $sortableField = 'order_column';
	protected static $sortableGroupField = 'ligature_id';

	public function ligature() {
		return $this->belongsTo('App\Models\Ligature');
	}
	public function getModelIdAttribute() {
		return 'note';
	}
	public function getCompXAttribute() {
		return $this->x;
	}
	public function getCompYAttribute() {
		return $this->y;
	}
	public static function generate_args_from_props($props) {
		$plica = self::get_prop($props, ["plica", "plica"]);
		$note = self::get_prop($props, ["plica", "note"]);
		$pplica = 0;
		if($note) {
			$pplica = $note;
		} else if($plica) {
			$pplica = 1;
		}
		return self::validate_args([
			"x" => self::get_prop($props, ["node", "x"]),
			"y" => self::get_prop($props, ["node", "y"]),
			"shift" => self::get_prop($props, ["shift", "shift"]),
			"plica" => $pplica,
		], [
			"x" => 'is_numeric',
			"y" => 'is_numeric',
			"shift" => 'is_int',
			"plica" => 'is_int',
		]);
	}
	public function parsed_attributes() {
		$pl = $this->plica != 0;
		$s = ['plica' => $pl];
		if($pl) {
			$s['note'] = $this->plica;
		}
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'Note',
			],
			'node' => [
				'x' => $this->x,
				'y' => $this->y,
			],
			'shift' => [
				'shift' => $this->shift,
			],
			'plica' => $s,
		];
	}
	public function getNodes() {
		return [[(float)$this->x, (float)$this->y]];
	}
}
