<?php

namespace App\Models;

class Staffline extends ItemModel {
	protected $fillable = ['ax', 'ay', 'bx', 'by'];

	protected $visible = ['id', 'created_at', 'updated_at', 'deleted_at', 'ax', 'ay', 'bx', 'by'];

	public function getModelIdAttribute() {
		return 'staffline';
	}
	public function getCompXAttribute() {
		return min($this->ax, $this->bx);
	}
	public function getCompYAttribute() {
		return min($this->ay, $this->by);
	}
	public static function generate_args_from_props($props) {
		return self::validate_args([
			"ax" => self::get_prop($props, ["nodes", "ax"]),
			"ay" => self::get_prop($props, ["nodes", "ay"]),
			"bx" => self::get_prop($props, ["nodes", "bx"]),
			"by" => self::get_prop($props, ["nodes", "by"]),
		], [
			"ax" => 'is_numeric',
			"ay" => 'is_numeric',
			"bx" => 'is_numeric',
			"by" => 'is_numeric',
		]);
	}
	public function parsed_attributes() {
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'Staffline',
			],
			'nodes' => [
				'ax' => $this->ax,
				'ay' => $this->ay,
				'bx' => $this->bx,
				'by' => $this->by,
			]
		];
	}
	public function getNodes() {
		return [
			[(float)$this->ax, (float)$this->ay],
			[(float)$this->bx, (float)$this->by],
		];
	}
}
