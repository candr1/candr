<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class Setting extends CacheableModel {
	use SoftDeletes;
	use ModelHistory;

	public $timestamps = true;

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected $visible = ['id', 'link', 'name'];

	protected static $history_field = 'history';

	protected $casts = [
		'history' => 'array',
	];

	public function link() {
		return $this->belongsTo('App\Models\Link', 'link_id');
	}
	public function related_caches() {
		return $this->belongsToMany('App\Models\Stave', 'related_caches');
	}
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		if($propagate_down) {
			$staves = $this->related_caches()->with(['up.up.up.up'])->get();
			$systems = new \Illuminate\Database\Eloquent\Collection();
			$facsimiles = new \Illuminate\Database\Eloquent\Collection();
			$folios = new \Illuminate\Database\Eloquent\Collection();
			foreach($staves as $stave) {
				$stave->invalidate_caches(true, false);
				$systems = $systems->push($stave->up);
			}
			foreach($systems as $system) {
				$system->invalidate_caches(false, false);
				$facsimiles = $facsimiles->merge($system->up);
			}
			foreach($facsimiles as $facsimile) {
				$facsimile->invalidate_caches(false, false);
				$folios = $folios->push($facsimile->up);
			}
			foreach($folios as $folio) {
				$folio->invalidate_caches(false, false);
			}
		}
		parent::invalidate_caches($propagate_down, $propagate_up);
	}
	public function cached_settings() {
		return false;
	}
	public function getModelIdAttribute() {
		return 'setting';
	}
}
