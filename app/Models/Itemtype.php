<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itemtype extends Model {
	use SoftDeletes;

	public $timestamps = true;

	protected $fillable = ['type'];

	protected $dates = ['deleted_at'];

	public function getModelIdAttribute() {
		return 'itemtype';
	}
}
