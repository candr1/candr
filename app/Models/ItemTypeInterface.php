<?php

namespace App\Models;

interface ItemTypeInterface {
	public static function generate_args_from_props($props);
	public function parsed_attributes();
}
