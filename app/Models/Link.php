<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends CacheableModel {
	use SoftDeletes;

	public $timestamps = true;

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected $visible = ['id', 'items'];

	public function items() {
		return $this->belongsToMany('App\Models\Item');
	}
	public function system() {
		return $this->belongsTo('App\Models\System', 'system_id');
	}
	public function setting() {
		return $this->hasOne('App\Models\Setting');
	}
	public function getModelIdAttribute() {
		return 'link';
	}
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		if($propagate_up) {
			$system = $this->system()->first();
			$system->invalidate_caches(true, true);
			$setting = $this->setting()->first();
			if($setting) {
				$setting->invalidate_caches(true, true);
			}
		}
	}
}
