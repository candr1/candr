<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class Source extends Model {
	use SoftDeletes;
	use ModelHistory;

	public $timestamps = true;

	protected $fillable = [
		'name', 'archive'
	];
	protected $visible = ['id', 'name', 'archive', 'created_at', 'updated_at', 'deleted_at', 'view_path', 'transcribed'];

	protected static $history_field = 'history';

	protected $dates = ['deleted_at'];

	protected $casts = [
		'history' => 'array',
		'transcribed' => 'boolean',
	];

	public function down() {
		return $this->hasMany('App\Models\Folio');
	}
	public function current_transcribers() {
		return $this->morphMany('App\Models\User', 'transcribing');
	}
	public function all_transcribers() {
		return $this->morphToMany('App\Models\User', 'transcriberable');
	}
	public function getModelIdAttribute() {
		return 'source';
	}
	public function getViewPathAttribute() {
		return 'view_source';
	}
}
