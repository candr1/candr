<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class CacheableModel extends Model {
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		$this->caches()->delete();
		return true;
	}
	public function caches() {
		return $this->morphMany(TranscriptionCache::class, 'cacheable');
	}
	public function cached_settings() {
		return $this->morphMany(Setting::class, 'related_caches');
	}
	public static function boot() {
		parent::boot();
		self::saving(function($m) {
			$m->invalidate_caches();
		});
		self::deleting(function($m) {
			$m->invalidate_caches();
		});
	}
}
