<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class FacsimileSet extends Model {
	use SoftDeletes;
	use ModelHistory;

	public $timestamps = true;

	protected $fillable = [
		'name', 'copyright_notice', 'copyright_url', 'world_visible'
	];

	protected $dates = ['deleted_at'];

	protected $visible = ['id', 'name', 'copyright_notice', 'copyright_url', 'created_at', 'updated_at', 'deleted_at', 'zoom'];

	protected $casts = [
		'history' => 'array',
		'system_margins' => 'array',
		'world_visible' => 'boolean',
	];

	public function facsimiles() {
		return $this->hasMany('App\Models\Facsimile');
	}
	public function getModelIdAttribute() {
		return 'facsimileset';
	}
}
