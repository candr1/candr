<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class Folio extends CacheableModel {
	use SoftDeletes;
	use \Rutorika\Sortable\SortableTrait;
	use ModelHistory;

	public $timestamps = true;

	protected $fillable = [
		'name'
	];

	protected $visible = ['id', 'name', 'source_id', 'order_column', 'created_at', 'updated_at', 'deleted_at', 'view_path', 'transcribed', 'zoom'];

	protected static $history_field = 'history';

	protected $dates = ['deleted_at'];

	protected static $sortableField = 'order_column';
	protected static $sortableGroupField = 'source_id';

	protected $casts = [
		'history' => 'array',
		'transcribed' => 'boolean',
	];

	public function down() {
		return $this->hasMany('App\Models\Facsimile');
	}
	public function up() {
		return $this->belongsTo('App\Models\Source', 'source_id', 'id');
	}
	public function current_transcribers() {
		return $this->morphMany('App\Models\User', 'transcribing');
	}
	public function all_transcribers() {
		return $this->morphToMany('App\Models\User', 'transcriberable');
	}
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		if($propagate_down) {
			$this->down()->get()->map(function($m) {
				$m->invalidate_caches(true, false);
			});
		}
		parent::invalidate_caches($propagate_down, $propagate_up);
	}
	public function getModelIdAttribute() {
		return 'folio';
	}
	public function getViewPathAttribute() {
		return 'view_folio';
	}
}
