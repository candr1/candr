<?php

namespace App\Models;

class Clef extends ItemModel {
	protected $fillable = ['top', 'left', 'width', 'height', 'angle', 'centrepoint_x', 'centrepoint_y', 'type'];

	protected $visible = ['id', 'created_at', 'updated_at', 'deleted_at', 'top', 'left', 'width', 'height', 'angle', 'centrepoint_x', 'centrepoint_y', 'type'];

	public function getModelIdAttribute() {
		return 'clef';
	}
	public function getCompXAttribute() {
		return $this->centrepoint_x;
	}
	public function getCompYAttribute() {
		return $this->centrepoint_y;
	}
	public static function generate_args_from_props($props) {
		return self::validate_args([
			"top" => self::get_prop($props, ["position", "top"]),
			"left" => self::get_prop($props, ["position", "left"]),
			"width" => self::get_prop($props, ["position", "width"]),
			"height" => self::get_prop($props, ["position", "height"]),
			"angle" => self::get_prop($props, ["position", "angle"]),
			"centrepoint_x" => self::get_prop($props, ["centrepoint", "x"]),
			"centrepoint_y" => self::get_prop($props, ["centrepoint", "y"]),
			"type" => self::get_prop($props, ["type", "type"]),
		], [
			"top" => 'is_numeric',
			"left" => 'is_numeric',
			"width" => 'is_numeric',
			"height" => 'is_numeric',
			"angle" => 'is_numeric',
			"centrepoint_x" => 'is_numeric',
			"centrepoint_y" => 'is_numeric',
			"type" => 'is_string'
		]);
	}
	public function setTypeAttribute($value) {
		switch(strtolower($value)) {
			case "d":
				$value = "D";
				break;
			case "f":
				$value = "F";
				break;
			case "a":
				$value = "A";
				break;
			case "c":
			default:
				$value = "C";
				break;
		}
		$this->attributes['type'] = $value;
		return $value;
	}
	public function parsed_attributes() {
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'Clef',
			],
			'position' => [
				'top' => $this->top,
				'left' => $this->left,
				'width' => $this->width,
				'height' => $this->height,
				'angle' => $this->angle,
			],
			'centrepoint' => [
				'x' => $this->centrepoint_x,
				'y' => $this->centrepoint_y,
			],
			'type' => [
				'type' => $this->{'type'},
			],
		];
	}
	public function getNodes() {
		return $this->ltwha2nodes();
	}
}
