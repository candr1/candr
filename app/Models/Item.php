<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model {
	use SoftDeletes;

	public $timestamps = true;

	protected $fillable = ['serverid', 'editorial_comment'];

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected $visible = ['id', 'editorial_comment', 'type', 'created_at', 'updated_at', 'deleted_at'];

	protected $with = ['type'];

	public function up() {
		return $this->belongsTo('App\Models\Stave', 'stave_id', 'id');
	}
	public function type() {
		return $this->morphTo(__FUNCTION__, 'itemtype_type', 'itemtype_id');
	}
	public function links() {
		return $this->belongsToMany('App\Models\Link');
	}
	public function getModelIdAttribute() {
		return 'item';
	}
	public function getCompXAttribute() {
		$type = $this->type()->first();
		if($type) {
			return $type->comp_x;
		}
		return 0;
	}	
	public function getCompYAttribute() {
		$type = $this->type()->first();
		if($type) {
			return $type->comp_y;
		}
		return 0;
	}	
	public static function boot() {
		parent::boot();
		self::deleting(function($m) {
			$type = $m->type()->first();
			if($type) {
				$type->delete();
			}
			$m->up()->first()->invalidate_caches();
		});
		self::saving(function($m) {
			$m->up()->first()->invalidate_caches();
		});
	}
}
