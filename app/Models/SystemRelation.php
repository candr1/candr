<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class SystemRelation extends Pivot {
	use \Rutorika\Sortable\SortableTrait;

	public $timestamps = true;

	protected $fillable = [];

	protected $with = ['warp'];

	protected static $sortableGroupField = 'facsimile_id';

	protected static $sortableField = 'order_column';

	public function warp() {
		return $this->belongsTo('App\Models\Warp');
	}
	public function getModelIdAttribute() {
		return 'systemrelation';
	}
}
