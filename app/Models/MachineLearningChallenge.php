<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class MachineLearningChallenge extends Model {
	public $timestamps = false;

	protected $visible = ['challenge', 'expires'];

	protected $dates = ['expires'];

	protected $dateFormat = 'Y-m-d H:i:s.u';
}
