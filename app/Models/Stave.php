<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stave extends CacheableModel {
	use SoftDeletes;

	public $timestamps = true;

	protected $fillable = [];

	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'last_predicted'];

	protected $visible = ['id', 'warp', 'created_at', 'updated_at', 'deleted_at', 'view_path', 'model_id', 'transcribed', 'zoom'];

	protected $appends = ['zoom'];

	protected $with = ['warp'];

	protected $casts = [
		'transcribed' => 'boolean',
		'predictions' => 'array'
	];

	public function up() {
		return $this->belongsTo('App\Models\System', 'system_id', 'id');
	}
	public function warp() {
		return $this->belongsTo('App\Models\Warp');
	}
	public function items() {
		return $this->hasMany('App\Models\Item');
	}
	public function current_transcribers() {
		return $this->morphMany('App\Models\User', 'transcribing');
	}
	public function all_transcribers() {
		return $this->morphToMany('App\Models\User', 'transcriberable');
	}
	public function getModelIdAttribute() {
		return 'stave';
	}
	public function getZoomAttribute() {
		if($this->up()->exists()) {
			return $this->up()->first()->zoom;
		}
		return 1;
	}
	public function getViewPathAttribute() {
		return 'view_stave';
	}
	public function settings_caching_this() {
		return $this->belongsToMany('App\Models\Setting', 'related_caches');
	}
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		if($propagate_down) {
			$this->settings_caching_this()->get()->map(function($m) {
				$m->invalidate_caches(false, false);
			});
		}
		parent::invalidate_caches($propagate_down, $propagate_up);
	}
	public static function boot() {
		parent::boot();
		self::deleting(function($m) {
			$items = $m->items()->get();
			foreach($items as $item) {
				$item->delete();
			}
		});
	}
}
