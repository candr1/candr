<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class System extends CacheableModel {
	use SoftDeletes;
	use \AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;

	public $timestamps = true;

	protected $fillable = [];

	protected $visible = ['id', 'transcribe_id', 'created_at', 'updated_at', 'deleted_at', 'view_path', 'transcribed', 'links', 'zoom'];

	protected $appends = ['zoom'];

	protected $dates = ['deleted_at'];

	protected $with = ['up'];

	protected $casts = [
		'transcribed' => 'boolean',
	];

	public function up() {
		return $this->belongsToMany('App\Models\Facsimile', 'system_relations')->withPivot('warp_id')->using('App\Models\SystemRelation');
	}
	public function transcribe() {
		return $this->belongsTo('App\Models\Facsimile', 'transcribe_id');
	}
	public function down() {
		return $this->hasMany('App\Models\Stave');
	}
	public function links() {
		return $this->hasMany('App\Models\Link');
	}
	public function current_transcribers() {
		return $this->morphMany('App\Models\User', 'transcribing');
	}
	public function all_transcribers() {
		return $this->morphToMany('App\Models\User', 'transcriberable');
	}
	public function getModelIdAttribute() {
		return 'system';
	}
	public function getZoomAttribute() {
		if($this->transcribe()->exists()) {
			return $this->transcribe()->first()->zoom;
		} else if($this->up()->exists()) {
			return $this->up()->first()->zoom;
		}
		return 1;
	}
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		if($propagate_down) {
			$this->down()->get()->map(function($m) {
				$m->invalidate_caches(true, false);
			});
		}
		if($propagate_up) {
			$this->up()->get()->map(function($m) {
				$m->invalidate_caches(false, true);
			});
		}
		parent::invalidate_caches($propagate_down, $propagate_up);
	}
	public function getViewPathAttribute() {
		return 'view_system';
	}
	public static function boot() {
		parent::boot();
		self::deleting(function($m) {
			$staves = $m->down()->get();
			foreach($staves as $stave) {
				$stave->delete();
			}
		});
	}
}
