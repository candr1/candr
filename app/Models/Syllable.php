<?php

namespace App\Models;

class Syllable extends ItemModel {
	protected $fillable = ['text', 'wordstart', 'top', 'left', 'width', 'height', 'angle', 'centrepoint_x', 'centrepoint_y', 'stanza'];

	protected $visible = ['id', 'created_at', 'updated_at', 'deleted_at', 'text', 'wordstart', 'top', 'left', 'width', 'height', 'angle', 'centrepoint_x', 'centrepoint_y', 'stanza'];

	protected $casts = [
		'wordstart' => 'boolean'
	];

	public function getModelIdAttribute() {
		return 'syllable';
	}
	public function getCompXAttribute() {
		return $this->centrepoint_x;
	}
	public function getCompYAttribute() {
		return $this->centrepoint_y;
	}
	public static function generate_args_from_props($props) {
		return self::validate_args([
			"top" => self::get_prop($props, ["position", "top"]),
			"left" => self::get_prop($props, ["position", "left"]),
			"width" => self::get_prop($props, ["position", "width"]),
			"height" => self::get_prop($props, ["position", "height"]),
			"angle" => self::get_prop($props, ["position", "angle"]),
			"centrepoint_x" => self::get_prop($props, ["centrepoint", "x"]),
			"centrepoint_y" => self::get_prop($props, ["centrepoint", "y"]),
			"text" => self::get_prop($props, ["text", "text"]),
			"wordstart" => self::get_prop($props, ["text", "wordstart"]),
			"stanza" => self::get_prop($props, ["text", "stanza"]),
		], [
			"top" => 'is_numeric',
			"left" => 'is_numeric',
			"width" => 'is_numeric',
			"height" => 'is_numeric',
			"angle" => 'is_numeric',
			"centrepoint_x" => 'is_numeric',
			"centrepoint_y" => 'is_numeric',
			"text" => 'is_string',
			"wordstart" => (function() { return true; }),
			"stanza" => 'is_integer',
		]);
	}
	public function setWordstartAttribute($value) {
		return $this->attributes['wordstart'] = ($value ? true : false);
	}
	public function parsed_attributes() {
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'Syllable',
			],
			'position' => [
				'top' => $this->top,
				'left' => $this->left,
				'width' => $this->width,
				'height' => $this->height,
				'angle' => $this->angle,
			],
			'centrepoint' => [
				'x' => $this->centrepoint_x,
				'y' => $this->centrepoint_y,
			],
			'text' => [
				'text' => $this->text,
				'wordstart' => $this->wordstart,
				'stanza' => $this->stanza,
			],
		];
	}
	public function getNodes() {
		return $this->ltwha2nodes();
	}
}
