<?php

namespace App\Models;

class EditorialClef extends ItemModel {
	protected $fillable = ['top', 'left', 'scale', 'centrepoint_x', 'centrepoint_y', 'type'];

	protected $visible = ['id', 'created_at', 'updated_at', 'deleted_at', 'top', 'left', 'scale', 'centrepoint_x', 'centrepoint_y', 'type'];

	public function getModelIdAttribute() {
		return 'editorial_clef';
	}
	public function getCompXAttribute() {
		return $this->centrepoint_x;
	}
	public function getCompYAttribute() {
		return $this->centrepoint_y;
	}
	public static function generate_args_from_props($props) {
		return self::validate_args([
			"top" => self::get_prop($props, ["position", "top"]),
			"left" => self::get_prop($props, ["position", "left"]),
			"scale" => self::get_prop($props, ["position", "scale"]),
			"centrepoint_x" => self::get_prop($props, ["centrepoint", "x"]),
			"centrepoint_y" => self::get_prop($props, ["centrepoint", "y"]),
			"type" => self::get_prop($props, ["type", "type"]),
		], [
			"top" => 'is_numeric',
			"left" => 'is_numeric',
			"scale" => 'is_numeric',
			"centrepoint_x" => 'is_numeric',
			"centrepoint_y" => 'is_numeric',
			"type" => 'is_string'
		]);
	}
	public function setTypeAttribute($value) {
		switch(strtolower($value)) {
			case "d":
				$value = "D";
				break;
			case "f":
				$value = "F";
				break;
			case "a":
				$value = "A";
				break;
			case "c":
			default:
				$value = "C";
				break;
		}
		$this->attributes['type'] = $value;
		return $value;
	}
	public function parsed_attributes() {
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'Editorial' . strtoupper($this->{'type'}) . 'Clef',
			],
			'position' => [
				'top' => $this->top,
				'left' => $this->left,
				'scale' => $this->scale,
			],
			'centrepoint' => [
				'x' => $this->centrepoint_x,
				'y' => $this->centrepoint_y,
			],
			'type' => [
				'type' => $this->{'type'},
			],
		];
	}
	public function getNodes() {
		return [(float)$this->left, (float)$this->top];
	}
}
