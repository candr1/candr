<?php

namespace App\Models;

class Ligature extends ItemModel {
	protected $fillable = ['type'];

	protected $visible = ['id', 'type', 'notes'];

	public function notes() {
		return $this->hasMany('App\Models\Note');
	}
	public function getModelIdAttribute() {
		return 'ligature';
	}
	public function getCompXAttribute() {
		$notes = $this->notes()->get();
		$minx = null;
		foreach($notes as $note) {
			$x = $note->comp_x;
			if($minx === null || $x < $minx) {
				$minx = $x;
			}
		}
		return $minx;
	}
	public function getCompYAttribute() {
		$notes = $this->notes()->get();
		$miny = null;
		foreach($notes as $note) {
			$y = $note->comp_y;
			if($miny === null || $y < $miny) {
				$miny = $y;
			}
		}
		return $miny;
	}
	public static function generate_args_from_props($props) {
		return self::validate_args([
			"type" => self::get_prop($props, ["type", "type"]),
		], [
			"type" => 'is_string',
		]);
	}
	public function setTypeAttribute($value) {
		switch(strtolower($value)) {
			case "currentes":
				$value = "Currentes";
				break;
			case "square":
			default:
				$value = "Square";
				break;
		}
		$this->attributes['type'] = $value;
	}
	public function parsed_attributes() {
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'Ligature',
			],
			'type' => [
				'type' => $this->type,
			],
		];
	}
	public function getNodes() {
		return [];
	}
}
