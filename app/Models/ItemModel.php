<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class ItemModel extends Model implements ItemTypeInterface {
	use SoftDeletes;

	public $timestamps = true;

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function up() {
		return $this->morphOne('App\Models\Item', 'itemtype');
	}
	protected static function get_prop($props, $path) {
		$head = $props;
		foreach($path as $id) {
			if(!isset($head[$id])) {
				error_log("Can't find prop for " . get_called_class() . ": " . $id);
				return null;
			}
			$head = $head[$id];
		}
		return $head;
	}
	protected static function validate_args($input, $validators) {
		foreach($input as $attr => $val) {
			if(isset($validators[$attr]) && is_callable($validators[$attr])) {
				if(!(($validators[$attr])($val))) {
					return false;
				}
			}
		}
		return $input;
	}
	protected function ltwha2nodes() {
		$ax = (float)$this->left;
		$ay = (float)$this->top;
		$w = (float)$this->width;
		$h = (float)$this->height;
		$a = (float)$this->angle;
		$bx = $ax + $w;
		$by = $ay + $h;
		$cx = $ax + ($w / 2.0);
		$cy = $by + ($h / 2.0);
		$coord_rotate = function($x, $y, $cx, $cy, $sin, $cos) {
			$x -= $cx;
			$y -= $cy;
			$tx = $x * $cos - $y * $sin;
			$ty = $x * $sin + $y * $cos;
			$tx += $cx;
			$ty += $cy;
			return [$tx, $ty];
		};
		$coords = [
			[$ax, $ay], [$bx, $ay],
			[$ax, $by], [$bx, $by],
		];
		if($a != 0) {
			$rad = deg2rad($a);
			$sin = sin($rad);
			$cos = cos($rad);
			foreach($coords as &$coord) {
				$coord = $coord_rotate($coord[0], $coord[1], $cx, $cy, $sin, $cos);
			}
		}
		return $coords;
	}
}
