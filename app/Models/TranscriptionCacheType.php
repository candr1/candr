<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranscriptionCacheType extends Model {
	use SoftDeletes;

	public $timestamps = true;

	protected $dates = ['created_at', 'updated-at', 'deleted_at'];

	protected $visible = ['id', 'name', 'caches'];

	protected $fillable = ['name'];

	public function caches() {
		return $this->hasMany('App\Models\TranscriptionCache');
	}
	public function setNameAttribute($v) {
		$this->attributes['name'] = strtolower($v);
	}
	public function getModelIdAttribute() {
		return 'transcription-cache-type';
	}
}
