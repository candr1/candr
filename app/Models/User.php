<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model {
	use SoftDeletes;

	public $timestamps = true;

	protected $fillable = [
		'user', 'password',
	];

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected $dateFormat = 'Y-m-d H:i:s.u';

	public function transcribing() {
		return $this->morphTo();
	}
	public function transcribed_facsimiles() {
		return $this->morphedByMany('App\Models\Facsimile', 'transcriberable');
	}
	public function transcribed_folios() {
		return $this->morphedByMany('App\Models\Folio', 'transcriberable');
	}
	public function transcribed_sources() {
		return $this->morphedByMany('App\Models\Source', 'transcriberable');
	}
	public function transcribed_staves() {
		return $this->morphedByMany('App\Models\Stave', 'transcriberable');
	}
	public function transcribed_systems() {
		return $this->morphedByMany('App\Models\System', 'transcriberable');
	}
	public function transcriber_of() {
		return [
			$this->transcribed_facsimiles(),
			$this->transcribed_folios(),
			$this->transcribed_sources(),
			$this->transcribed_staves(),
			$this->transcribed_system(),
		];
	}
}
