<?php

namespace App\Models;

class EditorialText extends ItemModel {
	protected $fillable = ['top', 'left', 'scale'];

	protected $visible = ['id', 'created_at', 'updated_at', 'deleted_at', 'top', 'left', 'scale'];

	public function getModelIdAttribute() {
		return 'editorial_text';
	}
	public function getCompXAttribute() {
		return $this->left;
	}
	public function getCompYAttribute() {
		return $this->top;
	}
	public static function generate_args_from_props($props) {
		return self::validate_args([
			"top" => self::get_prop($props, ["position", "top"]),
			"left" => self::get_prop($props, ["position", "left"]),
			"scale" => self::get_prop($props, ["position", "scale"]),
		], [
			"top" => 'is_numeric',
			"left" => 'is_numeric',
			"scale" => 'is_numeric',
		]);
	}
	public function parsed_attributes() {
		return [
			'id' => [
				'id' => $this->id
			],
			'name' => [
				'name' => 'EditorialText',
			],
			'position' => [
				'top' => $this->top,
				'left' => $this->left,
				'scale' => $this->scale,
			],
		];
	}
	public function getNodes() {
		return [(float)$this->left, (float)$this->top];
	}
}
