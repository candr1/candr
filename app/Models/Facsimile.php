<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ModelTraits\ModelHistory;

class Facsimile extends CacheableModel {
	use SoftDeletes;
	use \Rutorika\Sortable\BelongsToSortedManyTrait;
	use \AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;
	use ModelHistory;

	public $timestamps = true;

	protected $fillable = [
		'name'
	];

	protected $visible = ['id', 'name', 'folio_id', 'image', 'created_at', 'updated_at', 'deleted_at', 'view_path', 'transcribed', 'zoom'];

	protected $appends = ['zoom'];

	protected static $history_field = 'history';

	protected $dates = ['deleted_at'];

	protected $casts = [
		'history' => 'array',
		'transcribed' => 'boolean',
	];

	public function up() {
		return $this->belongsTo('App\Models\Folio', 'folio_id', 'id');
	}
	public function set() {
		return $this->belongsTo('App\Models\FacsimileSet', 'facsimile_set_id', 'id');
	}
	public function down() {
		return $this->belongsToSortedMany('App\Models\System', 'order_column', 'system_relations')->withPivot('warp_id')->using('App\Models\SystemRelation');
	}
	public function current_transcribers() {
		return $this->morphMany('App\Models\User', 'transcribing');
	}
	public function all_transcribers() {
		return $this->morphToMany('App\Models\User', 'transcriberable');
	}

	public function getModelIdAttribute() {
		return 'facsimile';
	}
	public function getZoomAttribute() {
		if($this->set()->exists()) {
			return $this->set()->first()->zoom;
		}
		return 1;
	}
	public function invalidate_caches($propagate_down = true, $propagate_up = true) {
		if($propagate_down) {
			$this->down()->get()->map(function($m) {
				$m->invalidate_caches(true, false);
			});
		}
		if($propagate_up) {
			$this->up()->first()->invalidate_caches(false, true);
		}
		parent::invalidate_caches($propagate_down, $propagate_up);
	}
	public function getViewPathAttribute() {
		return 'view_facsimile';
	}
	public static function boot() {
		parent::boot();
		self::deleting(function($m) {
			$set = $m->set()->withCount('facsimiles')->first();
			if($set->facsimiles_count <= 1) {
				$set->delete();
			}
		});
	}
}
