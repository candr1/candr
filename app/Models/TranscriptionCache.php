<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranscriptionCache extends Model {

	public $timestamps = true;

	protected $dates = ['created_at', 'updated_at'];

	protected $visible = ['id', 'type', 'text'];

	public function type() {
		return $this->belongsTo('App\Models\TranscriptionCacheType', 'type_id');
	}
	public function cacheable() {
		return $this->morphTo();
	}
	public function getModelIdAttribute() {
		return 'transcription-cache';
	}
}
