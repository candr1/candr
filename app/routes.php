<?php

$app->get('/', 'HomeController:index')->setName('index');
$app->get('/about/', 'HomeController:about')->setName('about');
$app->any('/transcription/', 'HomeController:transcription')->setName('transcription');

$app->get('/browse/', 'BrowseRootController:root')->setName('root');

$app->get('/imagedl/{fname}/', 'ImageDownloadController:get_file')->setName('download_image');

$app->any('/search/', 'SearchController:search')->setName('search');
$app->any('/submit/{action}/{category}/[{id}/{attribute}/]', 'EditController:submit')->setName('submit');

$app->post('/console/', 'RunTracy\Controllers\RunTracyConsole:index');
$app->get('/login/', 'LoginController:login')->setName('login');
$app->post('/logout/', 'LoginController:give401')->setName('give401');
$app->get('/logout/', 'LoginController:logout')->setName('logout');
$app->get('/cleartranscribe/', 'LoginController:clear_transcribe')->setName('clear_transcribe');

$app->any('/machinelearning/', 'MachineLearningController:get_status')->setName('machine_learning_get_status');
$app->any('/machinelearning/challenge/', 'MachineLearningController:get_challenge')->setName('machine_learning_get_challenge');
$app->any('/machinelearning/status/', 'MachineLearningController:set_status')->setName('machine_learning_set_status');

$beginTranscribe_data = ['source', 'folio', 'facsimile', 'system', 'stave'];
foreach($beginTranscribe_data as $modelid) {
	$app->get('/transcribe/' . $modelid . '/{id}/begin/', 'TranscribeController:begin_transcribe_' . $modelid)->setName('begin_transcribe_' . $modelid);
	$app->get('/transcribe/' . $modelid . '/{id}/set/', 'TranscribeController:set_transcribe_' . $modelid)->setName('set_transcribe_' . $modelid);
	$app->get('/transcribe/' . $modelid . '/{id}/unset/', 'TranscribeController:unset_transcribe_' . $modelid)->setName('unset_transcribe_' . $modelid);
}

$app->any('/browse/image/{id}/[{thumb}/]', 'BrowseImageController:view')->setName('image');

$browse_data = [
	'source' => [
		'BrowseSourceController',
		[
			['view', 'view_source'],
			['list', 'list_sources'],
			null,
			null,
			['mei', 'mei_source'],
		]
	],
	'folio' => [
		'BrowseFolioController',
		[
			['view', 'view_folio'],
			null,
			null,
			['text', 'text_folio'],
			['mei', 'mei_folio'],
		]
	],
	'set' => [
		'BrowseFacsimileSetController',
		[
			['view', 'view_facsimileset'],
			['list', 'list_facsimilesets']
		]
	],
	'facsimile' => [
		'BrowseFacsimileController',
		[
			['view', 'view_facsimile'],
			null,
			null,
			['text', 'text_facsimile'],
			['mei', 'mei_facsimile']
		]
	],
	'system' => [
		'BrowseSystemController',
		[
			['view', 'view_system'],
			null,
			null,
			['text', 'text_system'],
			['mei', 'mei_system']
		]
	],
	'stave' => [
		'BrowseStaveController',
		[
			['view', 'view_stave'],
			null,
			null,
			['text', 'text_stave'],
			null,
			['view_rdf', 'view_stave_rdf'],
		]
	],
	'setting' => [
		'BrowseSettingController',
		[
			['view', 'view_setting'],
			['list', 'list_settings'],
			null,
			['text', 'text_setting'],
			['mei', 'mei_setting'],
		]
	]
];
$xml_data = [
	'item' => 'XMLItemController',
	'link' => 'XMLLinkController',
	'setting' => 'XMLSettingController',
	'stave' => 'XMLStaveController',
	'warp' => 'XMLWarpController',
	'system' => 'XMLSystemController',
	'chunk' => 'XMLChunkController',
	'context' => 'XMLContextController',
	'set' => 'XMLFacsimileSetController',
	'facsimile' => 'XMLFacsimileController',
];
$xslt_data = [
	[
		'name' => 'settingToText',
		'filename' => 'candrToSettingText.xsl',
	],
	[
		'name' => 'settingToMEI',
		'filename' => 'candrSettingToMei.xsl',
	],
	[
		'name' => 'MEIToABC',
		'filename' => 'MeiToABC.xsl',
	],
];
$edit_data = [
	'source' => ['EditSourceController', [
		'title/' => ['edit_title', 'edit_source_title'],
		'title/set/' => ['set_title', 'set_source_title'],
		'archive/' => ['edit_archive', 'edit_source_archive'],
		'archive/set/' => ['set_archive', 'set_source_archive'],
		'folios/' => ['edit_folios', 'edit_source_folios'],
		'folios/add/new/' => ['add_new_folio', 'add_source_new_folio'],
		'folios/add/{tid}/' => ['add_folio', 'add_source_folio'],
		'folios/remove/{tid}/' => ['remove_folio', 'remove_source_folio'],
		'delete/[{confirm}/]' => ['delete', 'delete_source'],
	]],
	'folio' => ['EditFolioController', [
		'title/' => ['edit_title', 'edit_folio_title'],
		'title/set/' => ['set_title', 'set_folio_title'],
		'source/' => ['edit_source', 'edit_folio_source'],
		'source/{tid}/' => ['set_source', 'set_folio_source'],
		'facsimiles/' => ['edit_facsimiles', 'edit_folio_facsimiles'],
		'facsimiles/add/new/' => ['add_new_facsimile', 'add_folio_new_facsimile'],
		'facsimiles/add/{tid}/' => ['add_facsimile', 'add_folio_facsimile'],
		'facsimiles/remove/{tid}/' => ['remove_facsimile', 'remove_folio_facsimile'],
		'delete/[{confirm}/]' => ['delete', 'delete_folio'],
	]],
	'set' => ['EditFacsimileSetController', [
		'title/' => ['edit_title', 'edit_facsimileset_title'],
		'title/set/' => ['set_title', 'set_facsimileset_title'],
		'copyright/' => ['edit_copyright', 'edit_facsimileset_copyright'],
		'copyright/set/' => ['set_copyright', 'set_facsimileset_copyright'],
		'facsimiles/' => ['edit_facsimiles', 'edit_facsimileset_facsimiles'],
		'facsimiles/add/{tid}/' => ['add_facsimile', 'add_facsimileset_facsimile'],
		'facsimiles/remove/{tid}/' => ['remove_facsimile', 'remove_facsimileset_facsimile'],
		'delete/[{confirm}/]' => ['delete', 'delete_facsimileset'],
	]],
	'facsimile' => ['EditFacsimileController', [
		'title/' => ['edit_title', 'edit_facsimile_title'],
		'title/set/' => ['set_title', 'set_facsimile_title'],
		'folio/' => ['edit_folio', 'edit_facsimile_folio'],
		'folio/{tid}/' => ['set_folio', 'set_facsimile_folio'],
		'image/' => ['edit_image', 'edit_facsimile_image'],
		'image/set/' => ['set_image', 'set_facsimile_image'],
		'set/' => ['edit_set', 'edit_facsimile_facsimileset'],
		'set/new/' => ['set_new', 'set_facsimile_new_facsimileset'],
		'set/{tid}/' => ['set_set', 'set_facsimile_facsimileset'],
		'systems/' => ['edit_systems', 'edit_facsimile_systems'],
		'systems/others/' => ['edit_systems_from_other_facsimile', 'edit_facsimile_systems_from_other_facsimile'],
		'systems/add/new/' => ['add_new_system', 'add_facsimile_new_system'],
		'systems/add/{tid}/' => ['add_system', 'add_facsimile_system'],
		'systems/{tid}/up/' => ['move_system_up', 'move_up_facsimile_system'],
		'systems/{tid}/down/' => ['move_system_down', 'move_down_facsimile_system'],
		'systems/nodes/set/' => ['set_system_nodes', 'set_facsimile_system_nodes'],
		'delete/[{confirm}/]' => ['delete', 'delete_facsimile'],
	]],
	'system' => ['EditSystemController', [
		'delete/[{confirm}/]' => ['delete', 'delete_system'],
		'transcribe/' => ['edit_transcribe', 'edit_system_transcribe'],
		'transcribe/{tid}/' => ['set_transcribe', 'set_system_transcribe'],
		'staves/' => ['edit_staves', 'edit_system_staves'],
		'staves/add/new/' => ['add_new_stave', 'add_system_new_stave'],
		'staves/remove/{tid}/' => ['remove_stave', 'remove_system_stave'],
		'staves/nodes/set/' => ['set_stave_nodes', 'set_system_stave_nodes'],
		'delete/[{confirm}/]' => ['delete', 'delete_system'],
	]],
	'setting' => ['EditSettingController', [
		'title/' => ['edit_title', 'edit_setting_title'],
		'title/set/' => ['set_title', 'set_setting_title'],
		'delete/[{confirm}/]' => ['delete', 'delete_setting'],
	]],
];

$app->any('/browse/system/{id}/sync/', 'BrowseSystemSyncController:view')->setName('system_view_sync');
$app->any('/edit/system/{id}/sync/', 'EditSystemSyncController:edit_sync')->setName('system_edit_sync');
$app->any('/edit/system/{id}/sync/set/', 'EditSystemSyncController:set_sync')->setName('system_set_sync');
$app->any('/edit/system/{id}/sync/setting/', 'EditSystemSyncController:add_setting')->setName('system_sync_setting_add');
$app->any('/edit/setting/new/link/{id}/', 'EditSettingController:new_setting_from_link')->setName('setting_add_new');
$app->any('/edit/setting/new/item/{id}/', 'EditSettingController:new_setting_from_item')->setName('setting_add_from_item');

$app->any('/util/addstavestoemptysystems/', 'UtilityController:add_staves_to_empty_systems')->setName('add_staves_to_empty_systems');

$app->any('/edit/stave/{id}/delete/[{confirm}/]', 'EditStaveController:delete')->setName('delete_stave');

$app->post('/edit/item/', 'EditItemController:receive_message')->setName('edit_item');

$app->any('/edit/source/new/', 'EditSourceController:new')->setName('new_source');
$app->any('/edit/folio/new/', 'EditFolioController:new')->setName('new_folio');
$app->any('/edit/facsimile/new/', 'EditFacsimileController:new')->setName('new_facsimile');

$app->any('/xslt/setting/{id}/text/', 'XMLSettingController:toText')->setName('setting_transform_xml_text');
$app->any('/xslt/setting/{id}/MEI/', 'XMLSettingController:toMEI')->setName('setting_transform_xml_MEI');
$app->any('/xslt/setting/{id}/ABC/', 'XMLSettingController:toABC')->setName('setting_transform_xml_ABC');
$app->any('/xslt/setting/{id}/SVG/', 'XMLSettingController:toSVG')->setName('setting_transform_xml_SVG');
foreach($browse_data as $key => $data) {
	$controller = $data[0];
	$d = $data[1];
	$len = count($d);
	$view = $len > 0 ? $d[0] : null;
	$list = $len > 1 ? $d[1] : null;
	$setting = $len > 2 ? $d[2] : null;
	$text = $len > 3 ? $d[3] : null;
	$mei = $len > 4 ? $d[4] : null;
	$rdf = $len > 5 ? $d[5] : null;
	if($rdf) {
		$function = $routename = $rdf[0];
		if(count($rdf) > 1 && $rdf[1]) {
			$routename = $rdf[1];
		}
		$uri = '/browse/' . $key . '/{id}/rdf/';
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
	}
	if($view) {
		$function = $routename = $view[0];
		if(count($view) > 1 && $view[1]) {
			$routename = $view[1];
		}
		$uri = '/browse/' . $key . '/{id}/';
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
	}
	if($list) {
		$function = $routename = $list[0];
		if(count($list) > 1 && $list[1]) {
			$routename = $list[1];
		}
		$uri = '/browse/' . $key . '/';
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
	}
	if($setting) {
		$function = $routename = $setting[0];
		if(count($setting) > 1 && $setting[1]) {
			$routename = $setting[1];
		}
		$uri = '/browse/' . $key . '/{id}/settings/';
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
	}
	if($text) {
		$function = $routename = $text[0];
		if(count($text) > 1 && $text[1]) {
			$routename = $text[1];
		}
		$uri = '/browse/' . $key . '/{id}/text/';
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
		$uri .= 'editorial/';
		$fun .= '_editorial';
		$app->any($uri, $fun)->setName($routename . '_editorial');
	}
	if($mei) {
		$function = $routename = $mei[0];
		if(count($mei) > 1 && $mei[1]) {
			$routename = $mei[1];
		}
		$uri = '/browse/' . $key . '/{id}/mei/';
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
	}
}
foreach($xml_data as $key => $controller) {
	$uri = '/xml/' . $key . '/{id}/[{xinclude}/]';
	$fun = $controller . ':generate';
	$routename = $key . "_get_xml";
	$app->any($uri, $fun)->setName($routename);
}
$xslt_root = $app->getContainer()->get('settings')['xsltstore'];
foreach($xslt_data as $dat) {
	$xsl_name = '/xsl/' . $dat['name'] . '/';
	$fname = $xslt_root . DS . $dat['filename'];
	$app->any($xsl_name, (function($fname) {
		return function($req, $res) use ($fname) {
			return mhndev\slimFileResponse\FileResponse::getResponse($res, $fname)->withHeader('Content-Type', 'text/xml');
		};
	})($fname))->setName('xsl_get_' . $dat['name']);
}
foreach($edit_data as $key => $data) {
	$controller = $data[0];
	foreach($data[1] as $route => $d) {
		$function = $routename = $d[0];
		if($d[1]) {
			$routename = $d[1];
		}
		$uri = '/edit/' . $key . '/{id}/' . $route;
		$fun = $controller . ':' . $function;
		$app->any($uri, $fun)->setName($routename);
	}
}
