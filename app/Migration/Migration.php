<?php

namespace App\Migration;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;


class Migration extends AbstractMigration {
	public $capsule;
	public $schema;

	public function init() {
		$settings = require __DIR__ . '/../../bootstrap/settings.php';
		$this->capsule = new Capsule;
		$this->capsule->addConnection($settings['connections'][$settings['default']]);
		$this->capsule->bootEloquent();
		$this->capsule->setAsGlobal();
		$this->schema = $this->capsule->schema();
	}
}
