<?php

namespace App\Helpers;

use Matrix\Matrix;

/* Ported from Javascript of jlouthan/perspective-transform */
class PerspectiveTransform {
	private $grid;
	function __construct($srcpts, $dstpts) {
		/* Input like this:
		 * [
		 * 	[x, y], [x, y], [x, y], [x, y]
		 * ] 	
		 */
		$r1 = [ $srcpts[0][0], $srcpts[0][1], 1, 0, 0, 0, -1*$dstpts[0][0]*$srcpts[0][0], -1*$dstpts[0][0]*$srcpts[0][1]];
		$r2 = [ 0, 0, 0, $srcpts[0][0], $srcpts[0][1], 1, -1*$dstpts[0][1]*$srcpts[0][0], -1*$dstpts[0][1]*$srcpts[0][1]];
		$r3 = [ $srcpts[1][0], $srcpts[1][1], 1, 0, 0, 0, -1*$dstpts[1][0]*$srcpts[1][0], -1*$dstpts[1][0]*$srcpts[1][1]];
		$r4 = [ 0, 0, 0, $srcpts[1][0], $srcpts[1][1], 1, -1*$dstpts[1][1]*$srcpts[1][0], -1*$dstpts[1][1]*$srcpts[1][1]];
		$r5 = [ $srcpts[2][0], $srcpts[2][1], 1, 0, 0, 0, -1*$dstpts[2][0]*$srcpts[2][0], -1*$dstpts[2][0]*$srcpts[2][1]];
		$r6 = [ 0, 0, 0, $srcpts[2][0], $srcpts[2][1], 1, -1*$dstpts[2][1]*$srcpts[2][0], -1*$dstpts[2][1]*$srcpts[2][1]];
		$r7 = [ $srcpts[3][0], $srcpts[3][1], 1, 0, 0, 0, -1*$dstpts[3][0]*$srcpts[3][0], -1*$dstpts[3][0]*$srcpts[3][1]];
		$r8 = [ 0, 0, 0, $srcpts[3][0], $srcpts[3][1], 1, -1*$dstpts[3][1]*$srcpts[3][0], -1*$dstpts[3][1]*$srcpts[3][1]];
		$r9 = [ $dstpts[0][0], $dstpts[0][1], $dstpts[1][0], $dstpts[1][1], $dstpts[2][0], $dstpts[2][1], $dstpts[3][0], $dstpts[3][1]];
		$matA = new Matrix([$r1, $r2, $r3, $r4, $r5, $r6, $r7, $r8]);
		$matB = new Matrix($r9);
		$matATranspose = $matA->transpose();
		$matC = $matATranspose->multiply($matA)->inverse();
		$matD = $matC->multiply($matATranspose);
		$matX = $matD->multiply($matB);
		$out = $matX->toArray();
		$grid = array();
		array_walk_recursive($out, function($a) use (&$grid) { $grid[] = $a; });
		$this->grid = $grid;
		foreach($this->grid as &$coord) {
			$coord = round($coord);
		}
		$this->grid[7] = 1;
	}
	public function getCoefficients() {
		return $this->grid;
	}
	public function transform($x, $y) {
		$d = $this->grid[6]*$x + $this->grid[7]*$y + 1;
		if($d == 0) {
			return [$x, $y];
		}
		$coordinates = [];
		$coordinates[0] = ($this->grid[0]*$x + $this->grid[1]*$y + $this->grid[2]) / $d;
		$coordinates[1] = ($this->grid[3]*$x + $this->grid[4]*$y + $this->grid[5]) / $d;
		return $coordinates;
	}
}
