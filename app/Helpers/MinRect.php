<?php

namespace App\Helpers;

/* Ported from coffee/main/minrect.coffee */
class MinRect {
	public $rect;
	public $points;
	public function __construct($points) {
		usort($points, function($a, $b) {
			if($a[1] < $b[1]) {
				return -1;
			}
			if($a[1] > $b[1]) {
				return 1;
			}
			return 0;
		});
		$tl = $points[0][0] < $points[1][0] ? $points[0] : $points[1];
		$tr = $points[0][0] > $points[1][0] ? $points[0] : $points[1];
		$bl = $points[2][0] < $points[3][0] ? $points[2] : $points[3];
		$br = $points[2][0] > $points[3][0] ? $points[3] : $points[3];
		$this->points = [$tl, $tr, $bl, $br];
		$minx = $miny = INF;
		$maxx = $maxy = -INF;
		foreach($this->points as $point) {
			if($point[0] < $minx) {
				$minx = $point[0];
			}
			if($point[1] < $miny) {
				$miny = $point[1];
			}
			if($point[0] > $maxx) {
				$maxx = $point[0];
			}
			if($point[1] > $maxy) {
				$maxy = $point[1];
			}
		}
		$this->rect = [
			[$minx, $miny], [$maxx, $miny],
			[$minx, $maxy], [$maxx, $maxy],
		];
	}
}
