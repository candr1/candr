<?php

namespace App\Authenticators;

class DefaultAuthenticator implements \Tuupola\Middleware\HttpBasicAuthentication\AuthenticatorInterface {
	private $algo = PASSWORD_DEFAULT;
	private $options = [
		'cost' => 10,
	];
	public function __invoke(array $args): bool {
		if($args['user'] == null || $args['password'] == null) {
			error_log('params not set, we have: ' . json_encode($args));
			$_SESSION['login_time'] = 0;
			unset($_SESSION['login_user']);
			return false;
		}
		$u = $args['user'];
		$p = $args['password'];
		$user = \App\Models\User::where('user', $u)->first();
		if($user == null) {
			error_log('can\'t find user: ' . $u);
			$_SESSION['login_time'] = 0;
			unset($_SESSION['login_user']);
			return false;
		}
		$hash = $user['password'];
		$verified = password_verify($p, $hash);
		if($verified != true) {
			error_log('password incorrect');
			$_SESSION['login_time'] = 0;
			unset($_SESSION['login_user']);
			return false;
		}
		if(password_needs_rehash($hash, $this->algo, $this->options)) {
			$hash = password_hash($p, $this->algo, $this->options);
			$user->password = $hash;
			$user->save();
		}
		$_SESSION['login_time'] = time();
		$_SESSION['login_user'] = $user->id;
		return true;
	}
}
