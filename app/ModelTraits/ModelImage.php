<?php

namespace App\ModelTraits;

trait ModelImage {
	public function get_image($im, $thumb) {
		$imagefn = "";
		if($thumb) {
			$imagefn .= $this->thumb_dir;
		} else {
			$imagefn .= $this->upload_dir;
		}
		$imagefn .= DIRECTORY_SEPARATOR . $im['image'];
		if(!file_exists($imagefn)) {
			error_log($imagefn . ' does not exist');
			return null;
		}
		return $imagefn;
	}
	public function image_dimensions($im) {
		$imagefn = $this->get_image($im, false);
		if(!$imagefn) {
			return null;
		}
		$info = getimagesize($imagefn);
		$width = $info[0];
		$height = $info[1];
		return [$width, $height];
	}
}
