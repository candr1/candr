<?php

namespace App\ModelTraits;

trait ModelHistory {
	public static function boot() {
		parent::boot();
		self::creating(function($m) {
			$m->history = [];
		});
		self::saving(function($m) {
			$historyf = self::$history_field ?? 'history';
			$data = $m->makeHidden($historyf)->toArray();
			$hist = $m->$historyf;
			$hist[time()] = $data;
			$m->$historyf = $hist;
		});
	}
}
