<?php

namespace App\ModelTraits;

trait ModelNodes {
	public function bounding_box($nodes) {
		$min_x = PHP_INT_MAX;
		$min_y = PHP_INT_MAX;
		$max_x = PHP_INT_MIN;
		$max_y = PHP_INT_MIN;
		foreach($nodes as $node) {
			if($node[0] > $max_x) {
				$max_x = $node[0];
			}
			if($node[0] < $min_x) {
				$min_x = $node[0];
			}
			if($node[1] > $max_y) {
				$max_y = $node[1];
			}
			if($node[1] < $min_y) {
				$min_y = $node[1];
			}
		}
		return [
			[$min_x, $min_y],
			[$max_x, $min_y],
			[$max_x, $max_y],
			[$min_x, $max_y],
		];
	}
}
