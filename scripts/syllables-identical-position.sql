SELECT items.stave_id, COUNT(*) FROM syllables
	INNER JOIN items ON items.itemtype_id = syllables.id AND items.itemtype_type = 'App\Models\Syllable'
	GROUP BY items.stave_id, syllables.top, syllables.left
	HAVING COUNT(*) > 1;
