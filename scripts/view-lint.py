#!/usr/bin/env python3
import json, sys
data = json.load(sys.stdin)
for stave_id, lint in data.items():
    print(json.dumps({stave_id: lint}))
