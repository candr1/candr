#!/bin/bash
read -p "Start jpeg (no zero pad): " start
read -p "Start folio: " folstart
read -p "End folio: " folend
read -p "Filename width: " filewidth
read -p "Filename extension (no dot): " fileext
for i in $(seq $folstart $folend); do
	printf '%sr\t%0'"${filewidth}"'d.'"${fileext}"'\n%sv\t%0'"${filewidth}"'d.'"${fileext}"'\n' "$i" "$start" "$i" "$((start + 1))"
	let "start=start+2"
done
