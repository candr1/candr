#!/bin/bash
read -p "Foliation file: " infile
read -p "Base directory of files: " basedir
read -p "Base URL of site: " baseurl
read -p "Username: " un
read -p "Password: " pw
read -p "Source ID: " sid
echo "Logging in..."
curl --user "$un":"$pw" -sLi --cookie-jar uploader-cookies "${baseurl}/login"
while IFS=$'\t' read -r -a inArray
do
	folio="${inArray[0]}"
	filename="${inArray[1]}"
	echo "$folio $filename"
	url="${baseurl}/edit/source/${sid}/folios/add/new/"
	echo "Fetching ${url}"
	newfol=$(curl --cookie uploader-cookies -sLi "$url" | pup 'form[class="inline"] attr{action}' | grep -Eo '[0-9]+') 
	url="${baseurl}/edit/folio/${newfol}/title/set/"
	echo "Posting '${folio}' to ${url}"
	curl -sLi --cookie uploader-cookies -X POST --data-urlencode "value=${folio}" "${url}"
	url="${baseurl}/edit/folio/${newfol}/facsimiles/add/new/"
	echo "Fetching ${url}"
	newfac=$(curl --cookie uploader-cookies -sLi "$url" | pup 'form[class="inline"] attr{action}' | grep -Eo '[0-9]+') 
	abspath=$(realpath "${basedir}/${filename}")
	url="${baseurl}/edit/facsimile/${newfac}/image/set/"
	echo "Uploading '${abspath}' to ${url}"
	curl -sLi --cookie uploader-cookies -F "value=@${abspath};filename=${filename}" "${url}"
done < "$infile"
rm uploader-cookies
