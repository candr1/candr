#!/usr/bin/env python3
import psycopg2
import json
import itertools
from tqdm import tqdm
from collections import namedtuple
from mergedeep import merge

Rectangle = namedtuple('Rectangle', ['xmin', 'ymin', 'xmax', 'ymax'])
Point = namedtuple('Point', ['x', 'y'])

with open('db-password', 'r') as password_file:
    password_string = ''.join(password_file.read().splitlines())
    conn = psycopg2.connect(host='localhost', database='candr', user='candr', password=password_string)
conn.autocommit = True
cur = conn.cursor()


def merge_results(a, b):
    return merge(a, b)
def lint_syllables_empty(cur):
    cur.execute('''SELECT items.stave_id, COUNT(*) FROM syllables
        INNER JOIN items ON
            items.itemtype_id = syllables.id AND
            items.itemtype_type = 'App\Models\Syllable' AND
            syllables.text = '' AND
            items.deleted_at IS NULL AND
            syllables.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    ret = {}
    empty_syllables = cur.fetchall()
    for stave_id, count in empty_syllables:
        ret[stave_id] = {"syllables_empty": count}
    return ret
def overlapping_rects(a, b):
    def area(r):
        return (r.xmax - r.xmin) * (r.ymax - r.ymin)
    a_rea = area(a)
    b_rea = area(b)
    min_rea = min(a_rea, b_rea)
    dx = min(a.xmax, b.xmax) - max(a.xmin, b.xmin)
    dy = min(a.ymax, b.ymax) - max(a.ymin, b.ymin)
    if (dx >= 0) and (dy >= 0):
        return (dx * dy) / min_rea
    return 0
def point_distance(a, b):
    return sum((p-q)**2 for p, q in zip(a, b)) ** .5
def lint_syllables_duplicate(cur):
    cur.execute('''SELECT items.stave_id FROM syllables
        INNER JOIN items ON
            items.itemtype_id = syllables.id AND
            items.itemtype_type = 'App\Models\Syllable' AND
            items.deleted_at IS NULL AND
            syllables.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    staves = cur.fetchall()
    ret = {}
    for stave in staves:
        stave_id = stave[0]
        cur.execute('''SELECT syllables.id, syllables.top, syllables.left, syllables.width, syllables.height, syllables.angle, syllables.centrepoint_x, syllables.centrepoint_y, syllables.text, syllables.wordstart FROM syllables
            INNER JOIN items ON
                items.itemtype_id = syllables.id
            WHERE
                items.itemtype_type = 'App\Models\Syllable' AND
                items.deleted_at IS NULL AND
                syllables.deleted_at IS NULL AND
                items.stave_id = %s;''', (stave_id,))
        syllables = cur.fetchall()
        def syllable_eq(a, b):
            for i in range(1, 8):
                if a[i] != b[i]:
                    return False
            if a[8] == '':
                return a[0]
            if b[8] == '':
                return b[0]
            if a[9] != b[9]:
                return False
            return True
        for comb in itertools.combinations(syllables, 2):
            a = comb[0]
            b = comb[1]
            r = syllable_eq(a, b)
            if r is False:
                continue
            to_delete = None
            if r is True:
                to_delete = comb[0][0]
            else:
                to_delete = r
            print("Syllables " + str(comb[0][0]) + " and " + str(comb[1][0]) + " are identical")
            cur.execute('''UPDATE syllables SET deleted_at = NOW() WHERE syllables.id = %s;''', (to_delete,))
            cur.execute('''UPDATE items SET deleted_at = NOW() WHERE items.itemtype_type = 'App\Models\Syllable' AND items.itemtype_id = %s;''', (to_delete,))
    return {}
def lint_accidentals_duplicate(cur):
    cur.execute('''SELECT items.stave_id FROM accidentals
        INNER JOIN items ON
            items.itemtype_id = accidentals.id AND
            items.itemtype_type = 'App\Models\Accidental' AND
            items.deleted_at IS NULL AND
            accidentals.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    staves = cur.fetchall()
    ret = {}
    for stave in staves:
        stave_id = stave[0]
        cur.execute('''SELECT accidentals.id, accidentals.top, accidentals.left, accidentals.width, accidentals.height, accidentals.angle, accidentals.centrepoint_x, accidentals.centrepoint_y, accidentals.type FROM accidentals
            INNER JOIN items ON
                items.itemtype_id = accidentals.id
            WHERE
                items.itemtype_type = 'App\Models\Accidental' AND
                items.deleted_at IS NULL AND
                accidentals.deleted_at IS NULL AND
                items.stave_id = %s;''', (stave_id,))
        accidentals = cur.fetchall()
        def accidental_eq(a, b):
            for i in range(1, 9):
                if a[i] != b[i]:
                    return False
            return True
        for comb in itertools.combinations(accidentals, 2):
            a = comb[0]
            b = comb[1]
            r = accidental_eq(a, b)
            if r is False:
                continue
            to_delete = None
            if r is True:
                to_delete = comb[0][0]
            else:
                to_delete = r
            print("Accidentals " + str(comb[0][0]) + " and " + str(comb[1][0]) + " are identical")
            cur.execute('''UPDATE accidentals SET deleted_at = NOW() WHERE accidentals.id = %s;''', (to_delete,))
            cur.execute('''UPDATE items SET deleted_at = NOW() WHERE items.itemtype_type = 'App\Models\Accidental' AND items.itemtype_id = %s;''', (to_delete,))
    return {}
def lint_clefs_duplicate(cur):
    cur.execute('''SELECT items.stave_id FROM clefs
        INNER JOIN items ON
            items.itemtype_id = clefs.id AND
            items.itemtype_type = 'App\Models\Clef' AND
            items.deleted_at IS NULL AND
            clefs.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    staves = cur.fetchall()
    ret = {}
    for stave in staves:
        stave_id = stave[0]
        cur.execute('''SELECT clefs.id, clefs.top, clefs.left, clefs.width, clefs.height, clefs.angle, clefs.centrepoint_x, clefs.centrepoint_y, clefs.type FROM clefs
            INNER JOIN items ON
                items.itemtype_id = clefs.id
            WHERE
                items.itemtype_type = 'App\Models\Clef' AND
                items.deleted_at IS NULL AND
                clefs.deleted_at IS NULL AND
                items.stave_id = %s;''', (stave_id,))
        clefs = cur.fetchall()
        def clef_eq(a, b):
            for i in range(1, 9):
                if a[i] != b[i]:
                    return False
            return True
        for comb in itertools.combinations(clefs, 2):
            a = comb[0]
            b = comb[1]
            r = clef_eq(a, b)
            if r is False:
                continue
            to_delete = None
            if r is True:
                to_delete = comb[0][0]
            else:
                to_delete = r
            print("Clefs " + str(comb[0][0]) + " and " + str(comb[1][0]) + " are identical")
            cur.execute('''UPDATE clefs SET deleted_at = NOW() WHERE clefs.id = %s;''', (to_delete,))
            cur.execute('''UPDATE items SET deleted_at = NOW() WHERE items.itemtype_type = 'App\Models\Clef' AND items.itemtype_id = %s;''', (to_delete,))
    return {}
def lint_notes_duplicate(cur):
    cur.execute("SELECT items.stave_id FROM notes INNER JOIN items ON items.itemtype_id = notes.id AND items.itemtype_type = 'App\Models\\Note' AND items.deleted_at IS NULL AND notes.deleted_at IS NULL GROUP BY items.stave_id ORDER BY items.stave_id;")
    staves = cur.fetchall()
    ret = {}
    for stave in staves:
        stave_id = stave[0]
        cur.execute("SELECT notes.id, notes.x, notes.y, notes.ligature_id FROM notes INNER JOIN items ON items.itemtype_id = notes.id WHERE items.itemtype_type = 'App\Models\\Note' AND items.deleted_at IS NULL AND notes.deleted_at IS NULL AND items.stave_id = %s;", (stave_id,))
        notes = cur.fetchall()
        def note_eq(a, b):
            for i in range(1, 2):
                if a[i] != b[i]:
                    return False
            if a[2] != None and b[2] == None:
                return a[0]
            if b[2] != None and a[2] == None:
                return b[0]
            return True
        for comb in itertools.combinations(notes, 2):
            a = comb[0]
            b = comb[1]
            r = note_eq(a, b)
            if r is False:
                continue
            to_delete = None
            if r is True:
                to_delete = comb[0][0]
            else:
                to_delete = r
            print("Notes " + str(comb[0][0]) + " and " + str(comb[1][0]) + " are identical")
            cur.execute('''UPDATE notes SET deleted_at = NOW() WHERE notes.id = %s;''', (to_delete,))
            cur.execute("UPDATE items SET deleted_at = NOW() WHERE items.itemtype_type = 'App\Models\\Note' AND items.itemtype_id = %s;", (to_delete,))
    return {}
def lint_syllables_overlapping(cur, overlap_threshold=0.5):
    cur.execute('''SELECT items.stave_id FROM syllables
        INNER JOIN items ON
            items.itemtype_id = syllables.id AND
            items.itemtype_type = 'App\Models\Syllable' AND
            items.deleted_at IS NULL AND
            syllables.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    staves = cur.fetchall()
    ret = {}
    whitelist = [2223, 4892, 5349, 6573, 6589, 8402, 8660]
    for stave in staves:
        stave_id = stave[0]
        if stave_id in whitelist:
            continue
        cur.execute('''SELECT syllables.top, syllables.left, syllables.width, syllables.height FROM syllables
            INNER JOIN items ON
                items.itemtype_id = syllables.id
            WHERE
                items.itemtype_type = 'App\Models\Syllable' AND
                items.deleted_at IS NULL AND
                syllables.deleted_at IS NULL AND
                items.stave_id = %s;''', (stave_id,))
        syllables = cur.fetchall()
        rects = []
        for syllable in syllables:
            top = syllable[0]
            left = syllable[1]
            right = left + syllable[2]
            bottom = top + syllable[3]
            rects.append(Rectangle(left, top, right, bottom))
        overlaps = 0
        for comb in itertools.combinations(rects, 2):
            overlap = overlapping_rects(comb[0], comb[1])
            if overlap > overlap_threshold:
                overlaps += 1
        if overlaps > 0:
            ret[stave_id] = {"syllables_overlapping": overlaps}
    return ret
def lint_clefs_overlapping(cur, overlap_threshold=0.5):
    cur.execute('''SELECT items.stave_id FROM clefs
        INNER JOIN items ON
            items.itemtype_id = clefs.id AND
            items.itemtype_type = 'App\Models\Clef' AND
            items.deleted_at IS NULL AND
            clefs.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    staves = cur.fetchall()
    ret = {}
    for stave in staves:
        stave_id = stave[0]
        cur.execute('''SELECT clefs.top, clefs.left, clefs.width, clefs.height FROM clefs
            INNER JOIN items ON
                items.itemtype_id = clefs.id
            WHERE
                items.itemtype_type = 'App\Models\Clef' AND
                items.deleted_at IS NULL AND
                clefs.deleted_at IS NULL AND
                items.stave_id = %s;''', (stave_id,))
        clefs = cur.fetchall()
        rects = []
        for clef in clefs:
            top = clef[0]
            left = clef[1]
            right = left + clef[2]
            bottom = top + clef[3]
            rects.append(Rectangle(left, top, right, bottom))
        overlaps = 0
        for comb in itertools.combinations(rects, 2):
            overlap = overlapping_rects(comb[0], comb[1])
            if overlap > overlap_threshold:
                overlaps += 1
        if overlaps > 0:
            ret[stave_id] = {"clefs_overlapping": overlaps}
    return ret
def lint_accidentals_overlapping(cur, overlap_threshold=0.5):
    cur.execute('''SELECT items.stave_id FROM accidentals
        INNER JOIN items ON
            items.itemtype_id = accidentals.id AND
            items.itemtype_type = 'App\Models\Accidental' AND
            items.deleted_at IS NULL AND
            accidentals.deleted_at IS NULL
        GROUP BY items.stave_id
        ORDER BY items.stave_id;''')
    staves = cur.fetchall()
    ret = {}
    for stave in staves:
        stave_id = stave[0]
        cur.execute('''SELECT accidentals.top, accidentals.left, accidentals.width, accidentals.height FROM accidentals
            INNER JOIN items ON
                items.itemtype_id = accidentals.id
            WHERE
                items.itemtype_type = 'App\Models\Accidental' AND
                items.deleted_at IS NULL AND
                accidentals.deleted_at IS NULL AND
                items.stave_id = %s;''', (stave_id,))
        accidentals = cur.fetchall()
        rects = []
        for accidental in accidentals:
            top = accidental[0]
            left = accidental[1]
            right = left + accidental[2]
            bottom = top + accidental[3]
            rects.append(Rectangle(left, top, right, bottom))
        overlaps = 0
        for comb in itertools.combinations(rects, 2):
            overlap = overlapping_rects(comb[0], comb[1])
            if overlap > overlap_threshold:
                overlaps += 1
        if overlaps > 0:
            ret[stave_id] = {"accidentals_overlapping": overlaps}
    return ret
def lint_notes_proximity(cur, proximity_threshold=3):
    cur.execute("SELECT items.stave_id FROM notes INNER JOIN items ON items.itemtype_id = notes.id AND items.itemtype_type = 'App\Models\\Note' AND items.deleted_at IS NULL AND notes.deleted_at IS NULL GROUP BY items.stave_id ORDER BY items.stave_id;")
    staves = cur.fetchall()
    ret = {}
    whitelist = [7383]
    for stave in staves:
        stave_id = stave[0]
        if stave_id in whitelist:
            continue
        cur.execute("SELECT notes.x, notes.y FROM notes INNER JOIN items ON items.itemtype_id = notes.id WHERE items.itemtype_type = 'App\Models\\Note' AND items.deleted_at IS NULL AND notes.deleted_at IS NULL AND items.stave_id = %s;", (stave_id,))
        notes = cur.fetchall()
        points = []
        for note in notes:
            x = note[0]
            y = note[1]
            points.append(Point(x, y))
        proximities = 0
        for comb in itertools.combinations(points, 2):
            proximity = point_distance(comb[0], comb[1])
            if proximity < proximity_threshold:
                proximities += 1
        if proximities > 0:
            ret[stave_id] = {"notes_proximity": proximities}
    return ret
linters = [lint_notes_duplicate, lint_syllables_duplicate, lint_accidentals_duplicate, lint_clefs_duplicate, lint_syllables_empty, lint_syllables_overlapping, lint_accidentals_overlapping, lint_clefs_overlapping, lint_notes_proximity]
final = {}
for linter in tqdm(linters):
    ret = linter(cur)
    merge_results(final, ret)
print(json.dumps(final))
