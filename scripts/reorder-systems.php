<?php

require __DIR__ . '/../vendor/autoload.php';

$settings = require __DIR__ . '/../bootstrap/settings.php';

$capsule = new \Illuminate\Database\Capsule\Manager();
$capsule->addConnection($settings['connections'][$settings['default']]);
$capsule->setAsGlobal();
$capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher(new \Illuminate\Container\Container));
$capsule->connection()->enableQueryLog();
$capsule->bootEloquent();

$pretend = false;

\App\Models\Facsimile::with(['down'])->get()->each(function($fac) use ($pretend) {
	$systems = $fac->down()->get()->sort(function($a, $b) {
		$a_warp = $a->pivot->warp()->first();
		$b_warp = $b->pivot->warp()->first();
		$a_centroid = $a_warp->centroid;
		$b_centroid = $b_warp->centroid;
		$c = $a_centroid[1] <=> $b_centroid[1];
		if($c) {
			return $c;
		}
		return ($a_centroid[0] <=> $b_centroid[0]);
	});
	$pos = 1;
	foreach($systems as $system) {
		echo "System " . strval($system->id) . " gets pos " . strval($pos) . "\n";
		if($pretend === false) {
			$system->pivot->order_column = $pos;
			$system->pivot->save();
		}
		$pos++;
	}
});

