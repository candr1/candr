<?php

use \App\Migration\Migration;

class SourceTranscribers extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $table = $this->table('transcriberables');
	    $table->addColumn('user_id', 'integer')
	    ->addForeignKey('user_id', 'users', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	    $table->addColumn('transcriberable_id', 'integer');
	    $table->addColumn('transcriberable_type', 'string');
	    $table->create();
	    $table->addIndex(['user_id']);
	    $table->addIndex(['transcriberable_id', 'transcriberable_type']);
	    $table->update();
    }
}
