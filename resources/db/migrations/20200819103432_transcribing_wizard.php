<?php

use \App\Migration\Migration;

class TranscribingWizard extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $table = $this->table('users');
	    $table->addColumn('transcribing_id', 'integer', ['null' => true]);
	    $table->addColumn('transcribing_type', 'string', ['null' => true]);
	    $table->update();

	    $table = $this->table('facsimiles');
	    $table->addColumn('transcribed', 'boolean', ['default' => false]);
	    $table->update();

	    $table = $this->table('folios');
	    $table->addColumn('transcribed', 'boolean', ['default' => false]);
	    $table->update();

	    $table = $this->table('sources');
	    $table->addColumn('transcribed', 'boolean', ['default' => false]);
	    $table->update();

	    $table = $this->table('staves');
	    $table->addColumn('transcribed', 'boolean', ['default' => false]);
	    $table->update();

	    $table = $this->table('systems');
	    $table->addColumn('transcribed', 'boolean', ['default' => false]);
	    $table->update();
    }
}
