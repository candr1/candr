<?php

use \App\Migration\Migration;

class AddTranscriptionCaches extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	// related caches table
	$table = $this->table('related_caches');
	$table->addColumn('setting_id', 'integer', ['null' => true])
	->addForeignKey('setting_id', 'settings', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	$table->addColumn('stave_id', 'integer', ['null' => true])
	->addForeignKey('stave_id', 'staves', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	$table->create();
	$table->addIndex(['setting_id']);
	$table->addIndex(['stave_id']);
	$table->update();

	// transcription cache type table
	$table = $this->table('transcription_cache_types');
	$table->addColumn('name', 'string', ['null' => false]);
	$table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	$table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	$table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	$table->create();
	$table->addIndex(['name']);
	$table->update();

	// transcription cache table
	$table = $this->table('transcription_caches');
	$table->addColumn('type_id', 'integer', ['null' => true])
	->addForeignKey('type_id', 'transcription_cache_types', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	$table->addColumn('text', 'string', ['null' => false]);
	$table->addColumn('cacheable_id', 'integer', ['null' => true]);
	$table->addColumn('cacheable_type', 'string', ['null' => true]);
	$table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	$table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	$table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	$table->create();
	$table->addIndex(['cacheable_id']);
	$table->update();
    }
}
