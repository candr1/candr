<?php

use \App\Migration\Migration;

class InitialMigration extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $table = $this->table('sources');
	    $table->addColumn('name', 'string');
	    $table->addColumn('archive', 'string');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();

	    $table = $this->table('folios');
	    $table->addColumn('name', 'string');
	    $table->addColumn('source_id', 'integer', ['null' => true])
	    ->addForeignKey('source_id', 'sources', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	    $table->addColumn('order_column', 'integer');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();
	    $table->addIndex(['source_id']);
	    $table->update();

	    $table = $this->table('facsimiles');
	    $table->addColumn('name', 'string');
	    $table->addColumn('folio_id', 'integer', ['null' => true])
	    ->addForeignKey('folio_id', 'folios', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	    $table->addColumn('image', 'string');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();
	    $table->addIndex(['folio_id']);
	    $table->update();

	    $table = $this->table('warps');
	    $table->addColumn('nodes', 'string', ['default' => '[]']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
    	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();

	    $table = $this->table('system_relations');
	    $table->addColumn('facsimile_id', 'integer', ['null' => true])
	    ->addForeignKey('facsimile_id', 'facsimiles', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('system_id', 'integer', ['null' => true])
	    ->addForeignKey('system_id', 'systems', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('warp_id', 'integer', ['null' => true])
	    ->addForeignKey('warp_id', 'warps', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('order_column', 'integer');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
    	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();
	    $table->addIndex(['facsimile_id', 'system_id']);
	    $table->addIndex(['warp_id']);
	    $table->update();

	    $table = $this->table('systems');
	    $table->addColumn('transcribe_id', 'integer', ['null' => true])
	    ->addForeignKey('transcribe_id', 'facsimiles', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();
	    $table->addIndex(['transcribe_id']);
	    $table->update();

	    $table = $this->table('staves');
    	    $table->addColumn('system_id', 'integer', ['null' => true])
	    ->addForeignKey('system_id', 'systems', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('warp_id', 'integer', ['null' => true])
	    ->addForeignKey('warp_id', 'warps', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('order_column', 'integer');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->addColumn('history', 'string', ['default' => '{}']);
	    $table->create();
	    $table->addIndex(['system_id']);
	    $table->addIndex(['warp_id']);
	    $table->update();

	    $table = $this->table('items');
	    $table->addColumn('editorial_comment', 'string', ['default' => '']);
	    $table->addColumn('stave_id', 'integer', ['null' => true])
	    ->addForeignKey('stave_id', 'staves', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('itemtype_id', 'integer', ['null' => true]);
	    $table->addColumn('itemtype_type', 'string', ['null' => true]);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();
	    $table->addIndex(['stave_id']);
	    $table->addIndex(['itemtype_id']);
	    $table->update(); 

	    $table = $this->table('stafflines');
	    $table->addColumn('ax', 'float');
	    $table->addColumn('ay', 'float');
	    $table->addColumn('bx', 'float');
	    $table->addColumn('by', 'float');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('divisiones');
	    $table->addColumn('ax', 'float');
	    $table->addColumn('ay', 'float');
	    $table->addColumn('bx', 'float');
	    $table->addColumn('by', 'float');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('clefs');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('width', 'float');
	    $table->addColumn('height', 'float');
	    $table->addColumn('angle', 'float');
	    $table->addColumn('centrepoint_x', 'float');
	    $table->addColumn('centrepoint_y', 'float');
	    $table->addColumn('type', 'string', ['default' => 'C']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('accidentals');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('width', 'float');
	    $table->addColumn('height', 'float');
	    $table->addColumn('angle', 'float');
	    $table->addColumn('centrepoint_x', 'float');
	    $table->addColumn('centrepoint_y', 'float');
	    $table->addColumn('type', 'string', ['default' => 'Flat']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('syllables');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('width', 'float');
	    $table->addColumn('height', 'float');
	    $table->addColumn('angle', 'float');
	    $table->addColumn('centrepoint_x', 'float');
	    $table->addColumn('centrepoint_y', 'float');
	    $table->addColumn('text', 'string', ['default' => '']);
	    $table->addColumn('wordstart', 'integer', ['default' => 0]);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('notes');
	    $table->addColumn('x', 'float');
	    $table->addColumn('y', 'float');
	    $table->addColumn('shift', 'integer', ['default' => 0]);
	    $table->addColumn('plica', 'integer', ['default' => 0]);
	    $table->addColumn('ligature_id', 'integer', ['null' => true])
	    ->addForeignKey('ligature_id', 'ligatures', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->addColumn('order_column', 'integer', ['null' => true]);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();
	    $table->addIndex(['ligature_id']);
	    $table->update();

	    $table = $this->table('ligatures');
	    $table->addColumn('type', 'string', ['default' => 'Square']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('editorial_clefs');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('scale', 'float');
	    $table->addColumn('centrepoint_x', 'float');
	    $table->addColumn('centrepoint_y', 'float');
	    $table->addColumn('type', 'string', ['default' => 'C']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('editorial_accidentals');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('scale', 'float');
	    $table->addColumn('centrepoint_x', 'float');
	    $table->addColumn('centrepoint_y', 'float');
	    $table->addColumn('type', 'string', ['default' => 'Flat']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('editorial_syllables');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('width', 'float');
	    $table->addColumn('height', 'float');
	    $table->addColumn('angle', 'float');
	    $table->addColumn('centrepoint_x', 'float');
	    $table->addColumn('centrepoint_y', 'float');
	    $table->addColumn('text', 'string', ['default' => '']);
	    $table->addColumn('wordstart', 'integer', ['default' => 0]);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('editorial_texts');
	    $table->addColumn('top', 'float');
	    $table->addColumn('left', 'float');
	    $table->addColumn('scale', 'float');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();

	    $table = $this->table('users');
	    $table->addColumn('user', 'string');
	    $table->addColumn('password', 'string');
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();
	    $table->addIndex(['user'], ['unique' => true]);
	    $table->update();

    }
}
