<?php

use \App\Migration\Migration;

class AddMachineLearningStatusAndChallenge extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $table = $this->table('machine_learning_challenges');
	    $table->addColumn('challenge', 'text', ['null' => false])
		->addColumn('answer', 'text', ['null' => false])
	    	->addColumn('expires', 'datetime')
		->addIndex(['challenge'], ['unique' => true])
	    	->create();

	    $table = $this->table('machine_learning_statuses');
	    $table->addColumn('status', 'text', ['default' => '{}', 'null' => false])
	    	->addColumn('last_updated', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
		->create();
    }
}
