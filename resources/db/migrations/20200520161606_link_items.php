<?php

use \App\Migration\Migration;

class LinkItems extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $table = $this->table('item_link');
	    $table->addColumn('item_id', 'integer', ['null' => true])
	    ->addForeignKey('item_id', 'items', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	    $table->addColumn('link_id', 'integer', ['null' => true])
	    ->addForeignKey('link_id', 'links', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
    	    $table->create();
	    $table->addIndex(['item_id', 'link_id']);
	    $table->update();

	    $table = $this->table('links');
	    $table->addColumn('system_id', 'integer', ['null' => true])
	    ->addForeignKey('system_id', 'systems', 'id', ['delete' => 'SET_NULL', 'update' => 'NO_ACTION']);
	    $table->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
	    $table->addColumn('deleted_at', 'timestamp', ['null' => true]);
	    $table->create();
	    $table->addIndex(['system_id']);
	    $table->update();
    	    

    }
}
