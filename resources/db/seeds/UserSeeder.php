<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
	    $faker = Faker\Factory::create();
	    $file = __DIR__ . '/users.txt';
	    $fp = fopen($file, 'w');
	    $data = [];
	    $n = $faker->numberBetween(2, 10);
	    for($i = 0; $i < $n; $i++) {
		    $user = $faker->userName;
		    $pw = $faker->userName;
		    fwrite($fp, $user . ' ' . $pw . PHP_EOL);
		    $data[] = [
			    'user' => $user,
			    'password' => password_hash($pw, PASSWORD_DEFAULT),
		    ];
	    }
	    fclose($fp);
	    $table = $this->table('users');
	    $table->truncate();
	    $table->insert($data)->save();
    }
}
