gulp = require 'gulp'
gutil = require 'gulp-util'
sass = require 'gulp-sass'
cssimport = require 'gulp-cssimport'
prefix = require 'gulp-autoprefixer'
postcss = require 'gulp-postcss'
purgecss = require 'gulp-purgecss'
coffee = require 'gulp-coffee'
coffeelint = require 'gulp-coffeelint'
browserify = require 'browserify'
babel = require 'gulp-babel'
fs = require 'fs'
concat = require 'gulp-concat'
plumber = require 'gulp-plumber'
changed = require 'gulp-changed'
terser = require 'gulp-terser'
watch = require 'gulp-watch'
sourcemaps = require 'gulp-sourcemaps'
stripCssComments = require 'gulp-strip-css-comments'
uglifycss = require 'gulp-uglifycss'
livereload = require 'gulp-livereload'
mainBowerFiles = require 'gulp-main-bower-files'
htmlmin = require 'gulp-htmlmin'
lr = require 'tiny-lr'
filter = require 'gulp-filter'
server = lr()
notify = require 'gulp-notify'
count = require 'gulp-count'
flatten = require 'gulp-flatten'

options = {
	SASS_SOURCE: "sass/**/*.scss",
	SASS_DEST: "build/css/",
	COFFEE_MAIN_SOURCE: "coffee/main/**/*.coffee",
	COFFEE_WORKER_SOURCE: "coffee/worker/**/*.coffee",
	COFFEE_DEST: "build/js/",
	MAPS_DEST: "../maps",
	HTML_SOURCE: "html/**/*.php",
	HTACCESS: "html/.htaccess",
	HTML_DEST: "build/",
	TWIG_SOURCE: "resources/views/**/*.twig",
	IMG_SOURCE: "img/**/*",
	IMG_DEST: "build/img/",
	FAVICON_SOURCE: "favicon/**/*",
	FAVICON_DEST: "build/",
	FONT_SOURCE: "fonts/**/*",
	FONT_DEST: "build/fonts/",
	LIVE_RELOAD_PORT: 35729,
	LINT_OPTS: {
	"arrow_spacing": {"level": "error"},
	"braces_spacing": {
		"level": "error",
		"spaces": 0,
		"empty_object_spaces": 0
	},
	"camel_case_classes": {"level": "error"},
	"coffeescript_error": {"level": "error"},
	"colon_assignment_spacing": {
		"level": "error",
		"spacing": {"left": 0, "right": 1}
	},
	"cyclomatic_complexity": {"level": "ignore", "value": 10},
	"duplicate_key": {"level": "error"},
	"empty_constructor_needs_parens": {"level": "error"},
	"ensure_comprehensions": {"level": "error"},
	"eol_last": {"level": "error"},
	"indentation": {"value": 1, "level": "error"},
	"line_endings": {"level": "error",	"value": "unix"},
	"max_line_length": {"value": 80, "level": "error", "limitComments": true},
	"missing_fat_arrows": {"level": "ignore", "is_strict": true},
	"newlines_after_classes": {"value": 3,	"level": "ignore"},
	"no_backticks": {"level": "error"},
	"no_debugger": {"level": "error", "console": false},
	"no_empty_functions": {"level": "ignore"},
	"no_empty_param_list": {"level": "error"},
	"no_implicit_braces": {"level": "error", "strict": true},
	"no_implicit_parens": {"level": "ignore", "strict": true},
	"no_interpolation_in_single_quotes": {"level": "error"},
	"no_nested_string_interpolation": {"level": "error"},
	"no_plusplus": {"level": "ignore"},
	"no_private_function_fat_arrows": {"level": "error"},
	"no_stand_alone_at": {"level": "error"},
	"no_tabs": {"level": "ignore"},
	"no_this": {"level": "error"},
	"no_throwing_strings": {"level": "error"},
	"no_trailing_semicolons": {"level": "error"},
	"no_trailing_whitespace": {
		"level": "error",
		"allowed_in_comments": false,
		"allowed_in_empty_lines": false
	},
	"no_unnecessary_double_quotes": {"level": "ignore"},
	"no_unnecessary_fat_arrows": {"level": "error"},
	"non_empty_constructor_needs_parens": {"level": "ignore"},
	"prefer_english_operator": {"level": "error", "doubleNotLevel": "ignore"},
	"space_operators": {"level": "error"},
	"spacing_after_comma": {"level": "error"},
	"transform_messes_up_line_numbers": {"level": "error"}
	}
	TERSER_OPTS: {
		compress: {
			drop_console: true
		}
	}
}

gulp.task 'sass', ->
	gulp.src options.SASS_SOURCE
		.pipe plumber()
		.pipe sass {
			outputStyle: 'compressed',
		}
		.on "error", notify.onError()
		.on "error", (err) ->
			console.log "Error:", err
		.pipe cssimport {}
		.pipe postcss [
			(require 'tailwindcss'),
			(require 'autoprefixer')
		]
		.pipe prefix "last 2 versions", "> 10%"
		.pipe count '## css files selected'
		.pipe purgecss {
			content: [options.TWIG_SOURCE],
			whitelistPatterns: [/dial-.*-percent$/]
		}
		.pipe concat 'main.css'
		.pipe gulp.dest options.SASS_DEST
		.pipe livereload server
gulp.task 'coffee-main', ->
	gulp.src options.COFFEE_MAIN_SOURCE
		.pipe sourcemaps.init()
		.pipe changed options.COFFEE_MAIN_SOURCE
		.pipe count '## coffee files selected'
		.pipe concat 'main.coffee'
		.pipe coffee()
		.on 'error', gutil.log
		.pipe concat 'main.js'
		.pipe babel()
		#.pipe terser options.TERSER_OPTS
		.pipe sourcemaps.write options.MAPS_DEST
		.pipe gulp.dest options.COFFEE_DEST
		.pipe livereload server
gulp.task 'coffee-worker', ->
	gulp.src options.COFFEE_WORKER_SOURCE
		.pipe sourcemaps.init()
		.pipe changed options.COFFEE_WORKER_SOURCE
		.pipe count '## coffee files selected'
		.pipe coffee()
		.on 'error', gutil.log
		.pipe babel()
		#.pipe terser options.TERSER_OPTS
		.pipe sourcemaps.write options.MAPS_DEST
		.pipe gulp.dest options.COFFEE_DEST
		.pipe livereload server
gulp.task 'html', ->
	gulp.src options.HTML_SOURCE
		.pipe htmlmin {
			collapseWhitespace: true,
			minifyCSS: true,
			minifyJS: true,
			minifyURLS: true,
			removeComments: true,
			removeEmptyAttributes: true,
			sortAttributes: true,
			sortClassName: true
		}
		.pipe gulp.dest options.HTML_DEST
		.pipe livereload server
	gulp.src options.HTACCESS
		.pipe gulp.dest options.HTML_DEST
		.pipe livereload server
gulp.task 'imgfont', ->
	gulp.src options.IMG_SOURCE
		.pipe gulp.dest options.IMG_DEST
		.pipe livereload server
	gulp.src options.FAVICON_SOURCE
		.pipe gulp.dest options.FAVICON_DEST
		.pipe livereload server
	gulp.src options.FONT_SOURCE
		.pipe gulp.dest options.FONT_DEST
		.pipe livereload server
gulp.task 'refreshgulpfile', ->
	gulp.src 'gulpfile.coffee'
gulp.task 'lint', ->
	gulp.src [options.COFFEE_MAIN_SOURCE, options.COFFEE_WORKER_SOURCE]
		.pipe coffeelint options.LINT_OPTS
		.pipe coffeelint.reporter()
gulp.task 'coffee', gulp.series 'coffee-main', 'coffee-worker'
gulp.task 'espresso', gulp.series 'lint', 'coffee'
gulp.task 'project', gulp.series 'espresso', 'sass', 'html', 'imgfont'
gulp.task 'gulpfilelint', ->
	gulp.src 'gulpfile.coffee'
		.pipe coffeelint options.LINT_OPTS
		.pipe coffeelint.reporter()
gulp.task 'gulpfilecoffee', ->
	gulp.src './gulpfile.coffee'
		.pipe coffee()
		.on 'error', gutil.log
		.pipe count '## coffee files selected'
		.pipe babel()
		.pipe concat 'gulpfile.js'
		.pipe sourcemaps.write()
		.pipe gulp.dest '.'
		.pipe livereload server
gulp.task 'gulpfile', gulp.series 'gulpfilelint', 'gulpfilecoffee'
gulp.task 'bowerjs', ->
	gulp.src './bower/main/bower.json'
		.pipe mainBowerFiles()
		.pipe filter '**/*.js'
		.pipe sourcemaps.init()
		.pipe count '## js files selected'
		.pipe babel()
		.pipe concat 'plugins.js'
		#.pipe terser options.TERSER_OPTS
		.pipe sourcemaps.write()
		.pipe gulp.dest options.COFFEE_DEST
gulp.task 'bowerworkerjs', ->
	gulp.src './bower/worker/bower.json'
		.pipe mainBowerFiles()
		.pipe filter '**/*.js'
		.pipe sourcemaps.init()
		.pipe count '## js files selected'
		.pipe babel()
		.pipe concat 'worker-plugins.js'
		#.pipe terser options.TERSER_OPTS
		.pipe sourcemaps.write()
		.pipe gulp.dest options.COFFEE_DEST
gulp.task 'bowercss', ->
	gulp.src './bower/main/bower.json'
		.pipe mainBowerFiles()
		.pipe filter '**/*.css'
		.pipe cssimport {}
		.pipe count '## css files selected'
		.pipe concat 'plugins.css'
		.pipe stripCssComments {
			preserve: false
		}
		.pipe uglifycss()
		.pipe gulp.dest options.SASS_DEST
gulp.task 'bowerscss', ->
	gulp.src './bower/main/bower.json'
		.pipe mainBowerFiles()
		.pipe filter '**/*.scss'
		.pipe plumber()
		.pipe sass {
			outputStyle: 'compressed',
		}
		.on 'error', notify.onError()
		.on 'error', (err) ->
			console.log "Error:", err
		.pipe cssimport {}
		.pipe prefix 'last 2 versions', '> 10%'
		.pipe count '## scss files selected'
		.pipe flatten()
		.pipe gulp.dest options.SASS_DEST
		.pipe livereload server
gulp.task 'bower', gulp.series 'bowerjs', 'bowerworkerjs', 'bowerscss', 'bowercss'
gulp.task 'default', gulp.series('sass', 'espresso', 'html', 'imgfont', 'bower')
gulp.task 'watch', ->
	server.listen options.LIVE_RELOAD_PORT, (err) ->
		if err
			return console.log err
	(watch [options.COFFEE_MAIN_SOURCE, options.COFFEE_WORKER_SOURCE]).on 'change', gulp.series('espresso')
	(watch options.SASS_SOURCE).on 'change', gulp.series('sass')
	(watch options.HTML_SOURCE).on 'change',  gulp.series('html', 'sass')
	(watch options.HTACCESS).on 'change', gulp.series('html')
	(watch options.TWIG_SOURCE).on 'change', gulp.series('html', 'sass')
	(watch options.IMG_SOURCE).on 'change', gulp.series('imgfont')
	(watch options.FONT_SOURCE).on 'change', gulp.series('imgfont')
