<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" xmlns:math="http://exslt.org/math" xmlns:func="http://exslt.org/functions" xmlns:str="http://exslt.org/strings" xmlns:mei="http://www.music-encoding.org/ns/mei" xmlns:candr="http://candr.ac.uk/ns/candr/" extension-element-prefixes="exsl math func str">
	<xsl:output method="text" media-type="text/vnd.abc"/>
	<xsl:strip-space elements="*"/>
	<!-- Start -->
	<xsl:template match="/">
		<xsl:apply-templates select="mei:mei"/>
	</xsl:template>
	<!-- Function to escape special characters in ABC headers -->
	<func:function name="candr:abcEscape">
		<xsl:param name="string" select="''"/>
		<xsl:variable name="repl">
			<in>
				<v>\</v>
				<v>%</v>
				<v>&amp;</v>
				<v>&quot;</v>
			</in>
			<out>
				<v>\\</v>
				<v>\%</v>
				<v>\&amp;</v>
				<v>&amp;quot;</v>
			</out>
		</xsl:variable>
		<func:result select="str:replace($string, exsl:node-set($repl)/in, exsl:node-set($repl)/out)"/>
	</func:function>
	<!-- Polyfilling EXSLT function as not available in xsltproc (http://exslt.org/str/functions/replace/str.replace.function.xsl) -->
	<func:function name="str:replace">
		<xsl:param name="string" select="''"/>
		<xsl:param name="search" select="/.."/>
		<xsl:param name="replace" select="/.."/>
		<xsl:choose>
			<xsl:when test="not($string)">
			  <func:result select="/.."/>
			</xsl:when>
			<xsl:when test="function-available('exsl:node-set')">
				<!-- this converts the search and replace arguments to node sets if they are one of the other XPath types -->
				<xsl:variable name="search-nodes-rtf">
				  <xsl:copy-of select="$search"/>
				</xsl:variable>
				<xsl:variable name="replace-nodes-rtf">
				  <xsl:copy-of select="$replace"/>
				</xsl:variable>
				<xsl:variable name="replacements-rtf">
					<xsl:for-each select="exsl:node-set($search-nodes-rtf)/node()">
						<xsl:variable name="pos" select="position()"/>
						<replace search="{.}">
							<xsl:copy-of select="exsl:node-set($replace-nodes-rtf)/node()[$pos]"/>
						</replace>
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="sorted-replacements-rtf">
					<xsl:for-each select="exsl:node-set($replacements-rtf)/replace">
						<xsl:sort select="string-length(@search)" data-type="number" order="descending"/>
						<xsl:copy-of select="."/>
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="result">
				  <xsl:choose>
					  <xsl:when test="not($search)">
						 <xsl:value-of select="$string"/>
					  </xsl:when>
					 <xsl:otherwise>
						<xsl:call-template name="str:_replace">
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="replacements" select="exsl:node-set($sorted-replacements-rtf)/replace"/>
						</xsl:call-template>
					 </xsl:otherwise>
				  </xsl:choose>
				</xsl:variable>
				<func:result select="exsl:node-set($result)/node()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message terminate="yes">
					ERROR: function implementation of str:replace() relies on exsl:node-set().
				</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>
	<xsl:template name="str:_replace">
	  <xsl:param name="string" select="''"/>
	  <xsl:param name="replacements" select="/.."/>
	  <xsl:choose>
		 <xsl:when test="not($string)"/>
		 <xsl:when test="not($replacements)">
			<xsl:value-of select="$string"/>
		 </xsl:when>
		 <xsl:otherwise>
			<xsl:variable name="replacement" select="$replacements[1]"/>
			<xsl:variable name="search" select="$replacement/@search"/>
			<xsl:choose>
			  <xsl:when test="not(string($search))">
				 <xsl:value-of select="substring($string, 1, 1)"/>
				 <xsl:copy-of select="$replacement/node()"/>
				 <xsl:call-template name="str:_replace">
					<xsl:with-param name="string" select="substring($string, 2)"/>
					<xsl:with-param name="replacements" select="$replacements"/>
				 </xsl:call-template>
			  </xsl:when>
			  <xsl:when test="contains($string, $search)">
				 <xsl:call-template name="str:_replace">
					<xsl:with-param name="string" select="substring-before($string, $search)"/>
					<xsl:with-param name="replacements" select="$replacements[position() > 1]"/>
				 </xsl:call-template>		
				 <xsl:copy-of select="$replacement/node()"/>
				 <xsl:call-template name="str:_replace">
					<xsl:with-param name="string" select="substring-after($string, $search)"/>
					<xsl:with-param name="replacements" select="$replacements"/>
				 </xsl:call-template>
			  </xsl:when>
			  <xsl:otherwise>
				 <xsl:call-template name="str:_replace">
					<xsl:with-param name="string" select="$string"/>
					<xsl:with-param name="replacements" select="$replacements[position() > 1]"/>
				 </xsl:call-template>
			  </xsl:otherwise>
			</xsl:choose>
		 </xsl:otherwise>
	  </xsl:choose>
	</xsl:template>
	<xsl:template match="mei:mei">
		<!-- Generate ABC boilerplate, including ABC custom decorations -->
		<xsl:text>%abc-2.1
%%beginps
/divisione{1.2 SLW M 0 -5 RM 0 -18 RL stroke}!
/ligature{
	1.2 SLW
	M -2 0 RM
	0 6 RL
	16 add 0 RL
	currentpoint stroke M
	0 -6 RL stroke}!
/plicaup {3 SLW M 2.3 -1 RM 0 14 RL stroke}!
/plicadn {3 SLW M 2.3 -1 RM 0 -14 RL stroke}!
%%endps
%%deco div 3 divisione 0 1 1
%%deco l( 5 - 24 0 0
%%deco l) 5 ligature 24 0 0
%%deco pu 0 plicaup 14 0 0 0
%%deco pd 0 plicadn 14 0 0 0
X:1
I:abc-charset utf-8
I:abc-version 2.1
I:linebreak $
I:staffnonote 2
I:maxshrink 1
</xsl:text>
		<!-- Generate header -->
		<xsl:apply-templates select="mei:meiHead"/>
		<!-- Start music -->
		<xsl:text>L:1/4
M:none
K:none
</xsl:text>
		<!-- Make music -->
		<xsl:apply-templates select="mei:music/mei:body"/>
	</xsl:template>
	<xsl:template match="mei:meiHead">
		<!-- Generate each header section in turn -->
		<xsl:apply-templates select=".//mei:encodingDesc"/>
		<xsl:apply-templates select=".//mei:titleStmt"/>
		<xsl:apply-templates select=".//mei:sourceDesc"/>
		<xsl:apply-templates select=".//mei:respStmt"/>
	</xsl:template>
	<xsl:template match="mei:encodingDesc">
		<!-- mei:name becomes abc-creator -->
		<xsl:for-each select=".//mei:name">
			<xsl:text>I:abc-creator </xsl:text>
			<xsl:value-of select="candr:abcEscape(.)"/>
			<xsl:text>&#xa;</xsl:text>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="mei:titleStmt">
		<!-- Title becomes T: -->
		<xsl:for-each select=".//mei:title">
			<xsl:text>T:</xsl:text>
			<xsl:value-of select="candr:abcEscape(.)"/>
			<xsl:text>&#xa;</xsl:text>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="mei:sourceDesc">
		<xsl:for-each select=".//mei:source">
			<xsl:text>C:</xsl:text>
			<!-- Create string identifying the source and folios -->
			<xsl:variable name="CStr">
				<xsl:value-of select="mei:biblStruct/mei:identifier"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="mei:locus"/>
				<xsl:text>)</xsl:text>
			</xsl:variable>
			<xsl:value-of select="candr:abcEscape($CStr)"/>
			<!-- Also add source header -->
			<xsl:text>&#xa;S:</xsl:text>
			<xsl:value-of select="candr:abcEscape(mei:biblStruct/mei:identifier/text())"/>
			<xsl:text>&#xa;</xsl:text>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="mei:respStmt">
		<!-- respStmt => Z: -->
		<xsl:text>Z:</xsl:text>
		<xsl:variable name="ZStr">
			<xsl:for-each select=".//mei:name">
				<xsl:if test="position() != 1">
					<xsl:text>, </xsl:text>
				</xsl:if>
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="candr:abcEscape($ZStr)"/>
		<xsl:text>&#xa;</xsl:text>
	</xsl:template>
	<!-- Recursive template to make a list of numbered voices for score definitions -->
	<xsl:template name="makeNVoices">
		<xsl:param name="n" select="1"/>
		<xsl:param name="i" select="1"/>
		<xsl:text>v</xsl:text>
		<xsl:value-of select="$i"/>
		<xsl:if test="$i &lt; $n">
			<xsl:text> </xsl:text>
			<xsl:call-template name="makeNVoices">
				<xsl:with-param name="n" select="$n"/>
				<xsl:with-param name="i" select="$i + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- Body is comprised of sections -->
	<xsl:template match="mei:body">
		<xsl:apply-templates select=".//mei:section"/>
	</xsl:template>
	<xsl:template match="mei:section">
		<!-- Each section redefines the score -->
		<xsl:text>I:score [</xsl:text>
		<xsl:call-template name="makeNVoices">
			<xsl:with-param name="n" select="count(./mei:scoreDef/mei:staffGrp/*)"/>
		</xsl:call-template>
		<xsl:text>]&#xa;</xsl:text>
		<!-- Variable that contains each staff's length in units of number of notes -->
		<xsl:variable name="staffLens">
			<xsl:for-each select="mei:staff">
				<value>
					<xsl:value-of select="count(.//mei:note) + count(.//mei:divisione)"/>
				</value>
			</xsl:for-each>
		</xsl:variable>
		<!-- Use max function on the previous to calculate the maximum length -->
		<xsl:variable name="maxLen" select="math:max(exsl:node-set($staffLens)/*)"/>
		<!-- Generate staves -->
		<xsl:for-each select="mei:staff">
			<xsl:variable name="thisN" select="@n"/>
			<xsl:variable name="stafflines">
				<xsl:choose>
					<!-- Lookup the staff definition to find how many stafflines we should have -->
					<xsl:when test="preceding-sibling::mei:scoreDef/mei:staffGrp/mei:staffDef[@n = $thisN]/@lines">
						<xsl:value-of select="preceding-sibling::mei:scoreDef/mei:staffGrp/mei:staffDef[@n = $thisN]/@lines"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>5</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- n.b. there should only be one layer -->
			<xsl:apply-templates select="./mei:layer">
				<xsl:with-param name="staffNum" select="position()"/>
				<xsl:with-param name="len" select="$maxLen"/>
				<xsl:with-param name="stafflines" select="$stafflines"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="mei:layer">
		<xsl:param name="staffNum" select="1"/>
		<xsl:param name="len" select="0"/>
		<xsl:param name="stafflines" select="5"/>
		<!-- Length of this layer in notes -->
		<xsl:variable name="thisLen" select="count(.//mei:note) + count(.//mei:divisione)"/>
		<xsl:if test="count(./*) &gt; 0">
			<xsl:text>I:stafflines </xsl:text>
			<xsl:value-of select="$stafflines"/>
			<xsl:if test="name(./*[1]) != 'clef'">
				<xsl:text>&#xa;</xsl:text>
				<xsl:text>V:v</xsl:text>
				<xsl:value-of select="$staffNum"/>
				<xsl:text>&#xa;</xsl:text>
			</xsl:if>
			<!-- How much rest we have to add -->
			<xsl:variable name="makeup">
				<xsl:choose>
					<xsl:when test="$len &gt; $thisLen">
						<xsl:value-of select="$len - $thisLen"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Begin the recursive iterate template on the first item, it will iterate along siblings -->
			<xsl:apply-templates select="./*[1]" mode="iterateItems">
				<xsl:with-param name="staffNum" select="$staffNum"/>
				<xsl:with-param name="makeup" select="$makeup"/>
			</xsl:apply-templates>
		</xsl:if>
		<xsl:if test="$thisLen = 0">
			<xsl:variable name="makeup">
				<xsl:call-template name="makeMakeup">
					<xsl:with-param name="makeup" select="$len"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:value-of select="exsl:node-set($makeup)/rest"/>
		</xsl:if>
		<xsl:text>[|]&#xa;</xsl:text>
	</xsl:template>
	<!-- Recursive template for outputting items -->
	<xsl:template match="*" mode="iterateItems">
		<!-- Staffnum (V:) to output on -->
		<xsl:param name="staffNum" select="1"/>
		<!-- Extra invisible rests to add -->
		<xsl:param name="makeup" select="0"/>
		<!-- Parameter for ending recta ligature -->
		<xsl:param name="beforeLast" select="''"/>
		<!-- Number of notes remaining (including this one?) -->
		<xsl:variable name="numNotes" select="count(following-sibling::*[name() = 'note']) + count(following-sibling::*[name() = 'ligature']/mei:note)"/>
		<!-- If last note, end the ligature -->
		<xsl:if test="$numNotes = 0">
			<xsl:value-of select="$beforeLast"/>
		</xsl:if>
		<!-- The exact fraction of rest we want to add -->
		<xsl:variable name="exact">
			<xsl:choose>
				<xsl:when test="$numNotes = 0">
					<xsl:text>0</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="($makeup div $numNotes) * (count(self::*[name() = 'note']) + count(./mei:note))"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Output the item (no template for ligature, it's dealt with below -->
		<xsl:variable name="templateCall">
			<xsl:apply-templates select=".">
				<xsl:with-param name="staffNum" select="$staffNum"/>
				<xsl:with-param name="makeup" select="$exact"/>
			</xsl:apply-templates>
		</xsl:variable>
		<!-- The amount of rest the template did -->
		<xsl:variable name="done" select="$exact - exsl:node-set($templateCall)/rem"/>
		<!-- Output the string value -->
		<xsl:value-of select="exsl:node-set($templateCall)/output"/>
		<!-- Call the next item -->
		<xsl:if test="count(following-sibling::*) &gt; 0">
			<xsl:apply-templates select="following-sibling::*[1]" mode="iterateItems">
				<xsl:with-param name="staffNum" select="$staffNum"/>
				<xsl:with-param name="beforeLast" select="$beforeLast"/>
				<xsl:with-param name="makeup" select="$makeup - $done"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>
	<xsl:template name="makeMakeup">
		<!-- How much to make up -->
		<xsl:param name="makeup" select="0"/>
		<!-- Granularity limit for powers -->
		<xsl:param name="limit" select="4"/>
		<!-- Current granularity -->
		<xsl:param name="curr" select="1"/>
		<!-- Numerator -->
		<xsl:param name="num" select="1"/>
		<!-- Granularity as a power of 2 -->
		<xsl:variable name="pow" select="math:power(2, ($curr - 1))"/>
		<!-- How that is going to alter the makeup -->
		<xsl:variable name="mul" select="1 div $pow"/>
		<!-- How much is going to come off the makeup as a result -->
		<xsl:variable name="tot" select="$num * $mul"/>
		<!-- What is left -->
		<xsl:variable name="rem" select="$makeup - $tot"/>
		<xsl:choose>
			<xsl:when test="$curr &gt; $limit or $makeup &lt;= 0">
				<!-- No rest -->
				<rest/>
				<rem>
					<!-- What remains is what we started with -->
					<xsl:value-of select="$makeup"/>
				</rem>
			</xsl:when>
			<!-- This level didn't work, try a level lower -->
			<xsl:when test="$rem &lt; 0">
				<xsl:call-template name="makeMakeup">
					<xsl:with-param name="makeup" select="$makeup"/>
					<xsl:with-param name="limit" select="$limit"/>
					<xsl:with-param name="curr" select="$curr + 1"/>
				</xsl:call-template>
			</xsl:when>
			<!-- We haven't yet reached granularity limit -->
			<xsl:otherwise>
				<!-- Variable that holds the result of recursion -->
				<xsl:variable name="theRest">
					<xsl:choose>
						<!-- We've got more than what we just took off remaining, take more off of the same -->
						<xsl:when test="$rem &gt;= $tot">
							<xsl:call-template name="makeMakeup">
								<xsl:with-param name="makeup" select="$rem"/>
								<xsl:with-param name="limit" select="$limit"/>
								<xsl:with-param name="curr" select="$curr"/>
							</xsl:call-template>
						</xsl:when>
						<!-- We've got less to take off, recurse one level lower -->
						<xsl:otherwise>
							<xsl:call-template name="makeMakeup">
								<xsl:with-param name="makeup" select="$rem"/>
								<xsl:with-param name="limit" select="$limit"/>
								<xsl:with-param name="curr" select="$curr + 1"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- The string value of the rest -->
				<rest>
					<!-- The rest itself -->
					<xsl:text>x</xsl:text>
					<xsl:if test="$curr &gt; 1">
						<xsl:text>/</xsl:text>
						<xsl:value-of select="$pow"/>
					</xsl:if>
					<!-- The result of recursions -->
					<xsl:value-of select="exsl:node-set($theRest)/rest"/>
				</rest>
				<!-- What remains -->
				<rem>
					<xsl:value-of select="exsl:node-set($theRest)/rem"/>
				</rem>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="mei:clef">
		<xsl:param name="staffNum" select="1"/>
		<xsl:param name="makeup" select="0"/>
		<output>
			<xsl:text>&#xa;V:v</xsl:text>
			<xsl:value-of select="$staffNum"/>
			<xsl:text> clef=</xsl:text>
			<!-- I believe this is the extent of the clefs possible in ABC -->
			<xsl:choose>
				<xsl:when test="@shape = 'F'">
					<xsl:text>F</xsl:text>
				</xsl:when>
				<xsl:when test="@shape = 'G'">
					<xsl:text>G</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>C</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="@line"/>
			<xsl:text> octave=-1&#xa;</xsl:text>
		</output>
		<rem>
			<xsl:value-of select="$makeup"/>
		</rem>
	</xsl:template>
	<xsl:template match="mei:ligature">
		<xsl:param name="makeup" select="0"/>
		<xsl:param name="staffNum" select="1"/>
		<output>
			<!-- Make ligature then iterate on items contained within -->
			<xsl:choose>
				<xsl:when test="@form = 'obliqua'">
					<xsl:text>(</xsl:text>
					<xsl:apply-templates select="./*[1]" mode="iterateItems">
						<xsl:with-param name="staffNum" select="$staffNum"/>
						<xsl:with-param name="makeup" select="$makeup"/>
					</xsl:apply-templates>
					<xsl:text>)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>!l(!</xsl:text>
					<xsl:apply-templates select="./*[1]" mode="iterateItems">
						<xsl:with-param name="staffNum" select="$staffNum"/>
						<xsl:with-param name="makeup" select="$makeup"/>
						<xsl:with-param name="beforeLast" select="'!l)!'"/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</output>
		<rem>0</rem>
	</xsl:template>
	<xsl:template match="mei:note">
		<xsl:param name="makeup" select="0"/>
		<xsl:variable name="makeupOutput">
			<xsl:call-template name="makeMakeup">
				<xsl:with-param name="makeup" select="$makeup"/>
			</xsl:call-template>
		</xsl:variable>
		<output>
			<!-- Deal with plicae using a deco function -->
			<xsl:choose>
				<xsl:when test="@plica = 'asc'">
					<xsl:text>!pu!</xsl:text>
				</xsl:when>
				<xsl:when test="@plica = 'desc'">
					<xsl:text>!pd!</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:call-template name="pname2abc">
				<xsl:with-param name="pname" select="@pname"/>
				<xsl:with-param name="oct" select="@oct"/>
			</xsl:call-template>
			<!-- Duration of 0 makes no stem (this might be a bug in abcm2ps, but it works for me) -->
			<xsl:text>0</xsl:text>
			<xsl:value-of select="exsl:node-set($makeupOutput)/rest"/>
		</output>
		<rem>
			<xsl:value-of select="exsl:node-set($makeupOutput)/rem"/>
		</rem>
	</xsl:template>
	<xsl:template match="mei:divisione">
		<xsl:param name="makeup" select="0"/>
		<output>
			<!-- Can't trust div attached to other items, so attach it to a spacer rest -->
			<xsl:text>!div!x</xsl:text>
		</output>
		<rem>
			<xsl:value-of select="$makeup"/>
		</rem>
	</xsl:template>
	<xsl:template match="mei:accid">
		<xsl:param name="makeup" select="0"/>
		<output>
			<!-- Define each as a custom key -->
			<xsl:text>[K:C exp </xsl:text>
			<xsl:call-template name="accid2abc">
				<xsl:with-param name="accid" select="@accid"/>
			</xsl:call-template>
			<xsl:call-template name="pname2abc">
				<xsl:with-param name="pname" select="@ploc"/>
				<!-- Shift octave down as everything is shifted down anyway -->
				<xsl:with-param name="oct" select="@oloc - 1"/>
			</xsl:call-template>
			<xsl:text>]</xsl:text>
		</output>
		<rem>
			<xsl:value-of select="$makeup"/>
		</rem>
	</xsl:template>
	<!-- Convert an MEI accidental to an ABC output -->
	<xsl:template name="accid2abc">
		<xsl:param name="accid"/>
		<xsl:choose>
			<xsl:when test="$accid = 'f'">
				<xsl:text>_</xsl:text>
			</xsl:when>
			<xsl:when test="$accid = 's'">
				<xsl:text>^</xsl:text>
			</xsl:when>
			<xsl:when test="$accid = 'n'">
				<xsl:text>=</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>
					<xsl:text>WARNING: Undefined accidental '</xsl:text>
					<xsl:value-of select="$accid"/>
					<xsl:text>'</xsl:text>
				</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Convert an MEI pname + oct to an ABC output -->
	<xsl:template name="pname2abc">
		<xsl:param name="pname"/>
		<xsl:param name="oct"/>
		<!-- If valid note -->
		<xsl:if test="contains('abcdefg', $pname)">
			<xsl:choose>
				<!-- Lower octaves are made with lowercase letters -->
				<xsl:when test="$oct &lt; 4">
					<xsl:value-of select="translate($pname, 'abcdefg', 'ABCDEFG')"/>
				</xsl:when>
				<!-- Upper octaves are made with uppercase letters -->
				<xsl:otherwise>
					<xsl:value-of select="$pname"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<!-- Even higher octaves have apostrophes denoting extra octaves -->
				<xsl:when test="$oct &gt; 4">
					<xsl:variable name="apos">'</xsl:variable>
					<xsl:call-template name="generateNChars">
						<xsl:with-param name="char" select="$apos"/>
						<xsl:with-param name="n" select="$oct - 4"/>
					</xsl:call-template>
				</xsl:when>
				<!-- Even lower octaves have commas denoting extra octaves -->
				<xsl:when test="$oct &lt; 3">
					<xsl:call-template name="generateNChars">
						<xsl:with-param name="char" select="','"/>
						<xsl:with-param name="n" select="3 - $oct"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="generateNChars">
		<xsl:param name="char" select="''"/>
		<xsl:param name="n" select="0"/>
		<xsl:if test="$n &gt; 0">
			<xsl:value-of select="$char"/>
			<xsl:call-template name="generateNChars">
				<xsl:with-param name="char" select="$char"/>
				<xsl:with-param name="n" select="$n - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- Ignore unmatched elements -->
	<xsl:template match="*">
	</xsl:template>
</xsl:stylesheet>
