<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:candr="http://candr.ac.uk/ns/candr/">
	<xsl:output method="text" omit-xml-declaration="yes" indent="no" media-type="text/plain"/>
	<xsl:template match="/">
		<xsl:apply-templates select="//candr:syllable|//candr:editorialSyllable"/>
	</xsl:template>
	<xsl:template match="candr:syllable">
		<xsl:call-template name="syllable-output"/>
	</xsl:template>
	<xsl:template match="candr:editorialSyllable">
		<xsl:text>[</xsl:text>
		<xsl:call-template name="syllable-output"/>
		<xsl:text>]</xsl:text>
	</xsl:template>
	<xsl:template name="syllable-output">
		<xsl:choose>
			<xsl:when test="@candr:wordstart = 'true'">
				<xsl:text> </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:value-of select="@candr:text"/>
	</xsl:template>
</xsl:stylesheet>
