<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:candr="http://candr.ac.uk/ns/candr/">
	<xsl:output method="html" omit-xml-declaration="yes" indent="yes" media-type="text/html"/>
	<xsl:template match="/">
		<html>
			<head>
				<style>
					<xsl:text>body {
						margin:0;
						background: #272822;
						color: #f8f8f2;
						font-family: monospace;
					}
					span {
						display: inline-block;
					}
					pre {
						display: block;
						margin: 0;
					}
					.node {
						display: inline-block;
					}	
					.element-node-open, .element-node-close, .element-selfclosing {
						color: #f92672;
					}
					.attribute-name {
						color: #a6e22e;
					}
					.attribute-quote, .attribute-value {
						color: #e6db74;
					}</xsl:text>
				</style>
			</head>
			<body>
				<xsl:apply-templates select="*"/>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="*">
		<span class="node" style="margin-left: 1em;">
			<xsl:choose>
				<xsl:when test="node()">
					<span class="element-node-open">
						<span class="bracket open-node">
							<xsl:text>&lt;</xsl:text>
						</span>
						<span class="node-name">
							<xsl:value-of select="name()"/>
						</span>
						<xsl:if test="count(@*) &gt; 0">
							<xsl:text> </xsl:text>
							<span class="attributes">
								<xsl:apply-templates select="@*"/>
							</span>
						</xsl:if>
						<span class="bracket close-node">
							<xsl:text>&gt;</xsl:text>
						</span>
					</span>
					<xsl:apply-templates/>
					<span class="element-node-close">
						<span class="bracket open-node">
							<xsl:text>&lt;/</xsl:text>
						</span>
						<span class="node-name">
							<xsl:value-of select="name()"/>
						</span>
						<span class="bracket close-node">
							<xsl:text>&gt;</xsl:text>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="element-selfclosing">
						<span class="bracket open-node">
							<xsl:text>&lt;</xsl:text>
						</span>
						<span class="node-name">
							<xsl:value-of select="name()"/>
						</span>
						<xsl:if test="count(@*) &gt; 0">
							<xsl:text> </xsl:text>
							<span class="attributes">
								<xsl:apply-templates select="@*"/>
							</span>
						</xsl:if>
						<span class="bracket close-node">
							<xsl:text>/&gt;</xsl:text>
						</span>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>
	<xsl:template match="@*">
		<xsl:text> </xsl:text>
		<span class="attribute-name">
			<xsl:value-of select="name()"/>
		</span>
		<span class="attribute-equals">
			<xsl:text>=</xsl:text>
		</span>
		<span class="attribute-quote">
			<xsl:text>"</xsl:text>
		</span>
		<span class="attribute-value">
			<xsl:value-of select="."/>
		</span>
		<span class="attribute-quote">
			<xsl:text>"</xsl:text>
		</span>
	</xsl:template>
	<xsl:template match="text()">
		<xsl:if test="normalize-space(.) != ''">
			<xsl:call-template name="insertBreaks"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="text()" name="insertBreaks">
		<xsl:param name="txt" select="."/>
		<xsl:choose>
			<xsl:when test="not(contains($txt, '&#xA;'))">
				<xsl:copy-of select="$txt"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring-before($txt, '&#xA;')"/>
				<br/>
				<xsl:call-template name="insertBreaks">
					<xsl:with-param name="txt" select="substring-after($txt, '&#xA;')"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>


