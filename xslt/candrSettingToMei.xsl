<?xml version="1.0"?>
<!-- This stylesheet started out with the best of intentions, I promise -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" xmlns:math="http://exslt.org/math" xmlns:set="http://exslt.org/sets" xmlns:candr="http://candr.ac.uk/ns/candr/" xmlns="http://www.music-encoding.org/ns/mei">
	<xsl:output method="xml" indent="yes"/>
	<xsl:param name="facsimile" select="'true'"/>
	
	<!-- This key holds the names of all sources -->
	<xsl:key name="kSourceName" match="//candr:source[@candr:name]" use="@candr:name"/>
	<!-- This key holds the names of all folios -->
	<xsl:key name="kFolioName" match="//candr:folio[@candr:name]" use="@candr:name"/>
	<!-- This key holds the URLs of all facsimiles -->
	<xsl:key name="kFacsimileURL" match="//candr:facsimile[@candr:image]" use="@candr:image"/>
	<!-- This key holds the IDs of all warps -->
	<xsl:key name="kWarpID" match="//candr:warp[@candr:id]" use="@candr:id"/>

	<!-- Start -->
	<xsl:template match="/">
		<xsl:apply-templates select="/candr:setting"/>
	</xsl:template>
	<!-- Small helper that converts folios to a comma-separated list of folio names -->
	<xsl:template name="foliosToText">
		<xsl:param name="folios"/>
		<!-- Make folio names unique -->
		<xsl:for-each select="$folios[generate-id()=generate-id(key('kFolioName', @candr:name)[1])]">
			<xsl:value-of select="@candr:name"/>
			<xsl:if test="position() != last()">
				<xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- Generate source bibliographic identifier (from name) -->
	<xsl:template name="sourceIdentifiers">
		<xsl:param name="sources"/>
		<!-- Make source name unique -->
		<xsl:for-each select="$sources[generate-id()=generate-id(key('kSourceName', @candr:name)[1])]">
			<identifier>
				<xsl:attribute name="xml:id">
					<xsl:value-of select="@candr:id"/>
				</xsl:attribute>
				<xsl:value-of select="@candr:name"/>
			</identifier>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="candr:stave">
		<xsl:param name="staffNum" select="1"/>
		<xsl:param name="outputContext" select="'true'"/>
		<!-- Calculate mean y coordinate of each staffline -->
		<xsl:variable name="stafflinesWithMeanY">
			<xsl:for-each select="./candr:context/candr:items/candr:item/candr:staffline">
				<xsl:copy>
					<xsl:attribute name="ax">
						<xsl:value-of select="@candr:ax"/>
					</xsl:attribute>
					<xsl:attribute name="ay">
						<xsl:value-of select="@candr:ay"/>
					</xsl:attribute>
					<xsl:attribute name="bx">
						<xsl:value-of select="@candr:bx"/>
					</xsl:attribute>
					<xsl:attribute name="by">
						<xsl:value-of select="@candr:by"/>
					</xsl:attribute>
					<xsl:attribute name="meany">
						<xsl:value-of select="(@candr:ay + @candr:by) div 2"/>
					</xsl:attribute>
				</xsl:copy>
			</xsl:for-each>
		</xsl:variable>
		<!-- Sort stafflines by mean y-coordinate -->
		<xsl:variable name="stafflinesSorted">
			<xsl:for-each select="exsl:node-set($stafflinesWithMeanY)/*">
				<xsl:sort select="@meany" data-type="number"/>
				<xsl:copy>
					<!-- Sort the points ascending -->
					<xsl:choose>
						<xsl:when test="@ay &lt;= @by">
							<xsl:copy-of select="@*"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="ax">
								<xsl:value-of select="@bx"/>
							</xsl:attribute>
							<xsl:attribute name="ay">
								<xsl:value-of select="@by"/>
							</xsl:attribute>
							<xsl:attribute name="bx">
								<xsl:value-of select="@ax"/>
							</xsl:attribute>
							<xsl:attribute name="by">
								<xsl:value-of select="@ay"/>
							</xsl:attribute>
							<xsl:attribute name="meany">
								<xsl:value-of select="@meany"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="position() != 1">
						<!-- Add an extra attribute of the difference in y between this and the previous -->
						<xsl:attribute name="dy">
							<xsl:value-of select="math:abs(@ay - preceding-sibling::*[1]/@ay) + math:abs(@by - preceding-sibling::*[1]/@by)"/>
						</xsl:attribute>
					</xsl:if>
				</xsl:copy>
			</xsl:for-each>
		</xsl:variable>
		<!-- Create guidelines -->
		<xsl:variable name="guidelines">
			<xsl:variable name="plusdy" select="sum(exsl:node-set($stafflinesSorted)/*/@dy) div ((count(exsl:node-set($stafflinesSorted)/*) - 1) * 4)"/>
			<xsl:for-each select="exsl:node-set($stafflinesSorted)/*">
				<xsl:choose>
					<xsl:when test="position() = 1">
						<!-- Add a centreline before the first line -->
						<centreline>
							<xsl:attribute name="ax">
								<xsl:value-of select="@ax"/>
							</xsl:attribute>
							<xsl:attribute name="bx">
								<xsl:value-of select="@bx"/>
							</xsl:attribute>
							<xsl:attribute name="ay">
								<xsl:value-of select="@ay - $plusdy"/>
							</xsl:attribute>
							<xsl:attribute name="by">
								<xsl:value-of select="@by - $plusdy"/>
							</xsl:attribute>
						</centreline>
					</xsl:when>
					<xsl:otherwise>
						<!-- Add a centreline halfway between the last line and this line -->
						<centreline>
							<xsl:attribute name="ax">
								<xsl:value-of select="(@ax + preceding-sibling::*[1]/@ax) div 2"/>
							</xsl:attribute>
							<xsl:attribute name="bx">
								<xsl:value-of select="(@bx + preceding-sibling::*[1]/@bx) div 2"/>
							</xsl:attribute>
							<xsl:attribute name="ay">
								<xsl:value-of select="(@ay + preceding-sibling::*[1]/@ay) div 2"/>
							</xsl:attribute>
							<xsl:attribute name="by">
								<xsl:value-of select="(@by + preceding-sibling::*[1]/@by) div 2"/>
							</xsl:attribute>
						</centreline>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Staffline -->
				<staffline>
					<xsl:attribute name="ax">
						<xsl:value-of select="@ax"/>
					</xsl:attribute>
					<xsl:attribute name="bx">
						<xsl:value-of select="@bx"/>
					</xsl:attribute>
					<xsl:attribute name="ay">
						<xsl:value-of select="@ay"/>
					</xsl:attribute>
					<xsl:attribute name="by">
						<xsl:value-of select="@by"/>
					</xsl:attribute>
				</staffline>
				<xsl:if test="position() = last()">
					<!-- Add a centreline after the last line -->
					<centreline>
						<xsl:attribute name="ax">
							<xsl:value-of select="@ax"/>
						</xsl:attribute>
						<xsl:attribute name="bx">
							<xsl:value-of select="@bx"/>
						</xsl:attribute>
						<xsl:attribute name="ay">
							<xsl:value-of select="@ay + $plusdy"/>
						</xsl:attribute>
						<xsl:attribute name="by">
							<xsl:value-of select="@by + $plusdy"/>
						</xsl:attribute>
					</centreline>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<!-- Lookup table for syllables to attach to notes -->
		<xsl:variable name="noteSyllableLookup">
			<xsl:call-template name="makeNoteSyllableLookup">
				<!-- All notes in staff -->
				<xsl:with-param name="notes" select=".//candr:note"/>
				<!-- All syllables or editorial syllables in staff -->
				<xsl:with-param name="syllables" select=".//candr:syllable|.//candr:editorialSyllable"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Finally, make staff -->
		<staff>
			<xsl:attribute name="xml:id">
				<xsl:text>staff-</xsl:text>
				<xsl:value-of select="$staffNum"/>
			</xsl:attribute>
			<xsl:attribute name="n">
				<xsl:number/>
			</xsl:attribute>
			<layer>
				<xsl:attribute name="xml:id">
					<xsl:text>layer-</xsl:text>
					<xsl:value-of select="$staffNum"/>
					<xsl:text>-1</xsl:text>
				</xsl:attribute>
				<!-- Process context items first -->
				<xsl:choose>
					<xsl:when test="$outputContext = 'true'">
						<xsl:apply-templates select="./candr:context//candr:item">
							<!-- Pitchlines to pitch items -->
							<xsl:with-param name="pitchlines" select="$guidelines"/>
							<!-- Syllable lookup table to attach syllables to notes -->
							<xsl:with-param name="noteSyllableLookup" select="$noteSyllableLookup"/>
							<!-- All syllables to find following syllables -->
							<xsl:with-param name="allSyllables" select="//candr:syllable|//candr:editorialSyllable"/>
						</xsl:apply-templates>
						<!-- Then process everything else, excluding what we just processed -->
						<xsl:apply-templates select="./candr:items/candr:item">
							<!-- Pitchlines to pitch items -->
							<xsl:with-param name="pitchlines" select="$guidelines"/>
							<!-- Syllable lookup table to attach syllables to notes -->
							<xsl:with-param name="noteSyllableLookup" select="$noteSyllableLookup"/>
							<!-- All syllables to find following syllables -->
							<xsl:with-param name="allSyllables" select="//candr:syllable|//candr:editorialSyllable"/>
							<!-- Exclude what was already in context -->
							<xsl:with-param name="exclude" select="./candr:context//candr:item"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="./candr:items/candr:item">
							<!-- Pitchlines to pitch items -->
							<xsl:with-param name="pitchlines" select="$guidelines"/>
							<!-- Syllable lookup table to attach syllables to notes -->
							<xsl:with-param name="noteSyllableLookup" select="$noteSyllableLookup"/>
							<!-- All syllables to find following syllables -->
							<xsl:with-param name="allSyllables" select="//candr:syllable|//candr:editorialSyllable"/>
						</xsl:apply-templates>
					</xsl:otherwise>
				</xsl:choose>
			</layer>
		</staff>
	</xsl:template>
	<!-- Apply warps on staff level -->
	<xsl:template match="*" mode="applyStaffWarps">
		<xsl:param name="warps"/>
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:choose>
				<xsl:when test="name() = 'candr:stave'">
					<xsl:variable name="matchingWarps">
						<xsl:for-each select="./candr:context/candr:warp">
							<xsl:variable name="thisID" select="@candr:id"/>
							<xsl:copy-of select="exsl:node-set($warps)/*[@candr:id = $thisID]"/>
						</xsl:for-each>
					</xsl:variable>
					<xsl:for-each select="./*">
						<xsl:copy>
							<xsl:copy-of select="@*"/>
							<xsl:apply-templates match="." mode="applyWarps">
								<xsl:with-param name="warps" select="$matchingWarps"/>
							</xsl:apply-templates>
						</xsl:copy>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates match="./*" mode="applyStaffWarps">
						<xsl:with-param name="warps" select="$warps"/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>
	<!-- Apply warps on system level -->
	<xsl:template match="*" mode="applySystemWarps">
		<xsl:param name="warps"/>
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:choose>
				<xsl:when test="name() = 'candr:system'">
					<xsl:variable name="matchingWarps">
						<xsl:for-each select="./candr:warp">
							<xsl:variable name="thisID" select="@candr:id"/>
							<xsl:copy-of select="exsl:node-set($warps)/*[@candr:id = $thisID]"/>
						</xsl:for-each>
					</xsl:variable>
					<xsl:for-each select="./*">
						<xsl:copy>
							<xsl:copy-of select="@*"/>
							<xsl:apply-templates match="." mode="applyWarps">
								<xsl:with-param name="warps" select="$matchingWarps"/>
							</xsl:apply-templates>
						</xsl:copy>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates match="./*" mode="applySystemWarps">
						<xsl:with-param name="warps" select="$warps"/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>
	<!-- Recursive template to apply warps one by one in order -->
	<xsl:template match="*" mode="applyWarps">
		<xsl:param name="warps"/>
		<xsl:choose>
			<xsl:when test="count(exsl:node-set($warps)/*) = 0">
				<xsl:copy-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="warped">
					<xsl:copy>
						<xsl:copy-of select="@*"/>
						<xsl:apply-templates select="./*" mode="applyWarp">
							<xsl:with-param name="warp" select="exsl:node-set($warps)/*[1]/*[1]"/>
						</xsl:apply-templates>
					</xsl:copy>
				</xsl:variable>
				<xsl:apply-templates select="exsl:node-set($warped)" mode="applyWarps">
					<xsl:with-param name="warps" select="exsl:node-set($warps)/*[position() &gt; 1]"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Template to apply transform to point -->
	<xsl:template name="transformPoint">
		<xsl:param name="x"/>
		<xsl:param name="y"/>
		<xsl:param name="warp"/>
		<xsl:variable name="w0" select="exsl:node-set($warp)/*[1]/text()"/>
		<xsl:variable name="w1" select="exsl:node-set($warp)/*[2]/text()"/>
		<xsl:variable name="w2" select="exsl:node-set($warp)/*[3]/text()"/>
		<xsl:variable name="w3" select="exsl:node-set($warp)/*[4]/text()"/>
		<xsl:variable name="w4" select="exsl:node-set($warp)/*[5]/text()"/>
		<xsl:variable name="w5" select="exsl:node-set($warp)/*[6]/text()"/>
		<xsl:variable name="w6" select="exsl:node-set($warp)/*[7]/text()"/>
		<xsl:variable name="w7" select="exsl:node-set($warp)/*[8]/text()"/>
		<point>
			<xsl:variable name="d" select="$w6 * $x + $w7 * $y + 1"/>
			<xsl:attribute name="x">
				<xsl:value-of select="($w0 * $x + $w1 * $y + $w2) div $d"/>
			</xsl:attribute>
			<xsl:attribute name="y">
				<xsl:value-of select="($w3 * $x + $w4 * $y + $w5) div $d"/>
			</xsl:attribute>
		</point>
	</xsl:template>
	<!-- Template to apply warp to items -->
	<xsl:template match="*" mode="applyWarp">
		<xsl:param name="warp"/>
		<xsl:copy>
			<xsl:if test="@candr:x and @candr:y">
				<xsl:variable name="xandy">
					<xsl:call-template name="transformPoint">
						<xsl:with-param name="x" select="@candr:x"/>
						<xsl:with-param name="y" select="@candr:y"/>
						<xsl:with-param name="warp" select="$warp"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:attribute name="candr:x">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@x"/>
				</xsl:attribute>
				<xsl:attribute name="candr:y">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@y"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@candr:ax and @candr:ay">
				<xsl:variable name="xandy">
					<xsl:call-template name="transformPoint">
						<xsl:with-param name="x" select="@candr:ax"/>
						<xsl:with-param name="y" select="@candr:ay"/>
						<xsl:with-param name="warp" select="$warp"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:attribute name="candr:ax">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@x"/>
				</xsl:attribute>
				<xsl:attribute name="candr:ay">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@y"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@candr:bx and @candr:by">
				<xsl:variable name="xandy">
					<xsl:call-template name="transformPoint">
						<xsl:with-param name="x" select="@candr:bx"/>
						<xsl:with-param name="y" select="@candr:by"/>
						<xsl:with-param name="warp" select="$warp"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:attribute name="candr:bx">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@x"/>
				</xsl:attribute>
				<xsl:attribute name="candr:by">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@y"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@candr:left and @candr:top">
				<xsl:variable name="xandy">
					<xsl:call-template name="transformPoint">
						<xsl:with-param name="x" select="@candr:left"/>
						<xsl:with-param name="y" select="@candr:top"/>
						<xsl:with-param name="warp" select="$warp"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:attribute name="candr:left">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@x"/>
				</xsl:attribute>
				<xsl:attribute name="candr:top">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@y"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@candr:width and @candr:height">
				<xsl:variable name="xandy">
					<xsl:call-template name="transformPoint">
						<xsl:with-param name="x" select="@candr:width"/>
						<xsl:with-param name="y" select="@candr:height"/>
						<xsl:with-param name="warp" select="$warp"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:attribute name="candr:width">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@x"/>
				</xsl:attribute>
				<xsl:attribute name="candr:height">
					<xsl:value-of select="exsl:node-set($xandy)/*[1]/@y"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:for-each select="@*">
				<xsl:if test="name() != 'candr:x' and name() != 'candr:y' and name() != 'candr:top' and name() != 'candr:left' and name() != 'candr:width' and name() != 'candr:height'">
					<xsl:copy-of select="."/>
				</xsl:if>
			</xsl:for-each>
			<xsl:apply-templates select="./*" mode="applyWarp">
				<xsl:with-param name="warp" select="$warp"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!-- Make syllable lookup table for notes -->
	<xsl:template name="makeNoteSyllableLookup">
		<xsl:param name="notes"/>
		<xsl:param name="syllables"/>
		<!-- For each syllable -->
		<xsl:for-each select="$syllables">
			<!-- Copy that syllable -->
			<xsl:copy>
				<xsl:copy-of select="@*"/>
				<!-- But also include the closest note to that syllable -->
				<xsl:call-template name="findClosestNote">
					<xsl:with-param name="notes" select="$notes"/>
					<xsl:with-param name="x" select="@centrepoint-x"/>
					<xsl:with-param name="y" select="@centrepoint-y"/>
				</xsl:call-template>
			</xsl:copy>
		</xsl:for-each>
	</xsl:template>
	<!-- Add pitch info to lines -->
	<xsl:template name="pitchlinesApplyClefs">
		<xsl:param name="pitchlines"/>
		<xsl:param name="clefs"/>
		<xsl:choose>
			<xsl:when test="count(exsl:node-set($clefs)) &gt; 0">
				<xsl:call-template name="pitchlinesApplyClef">
					<xsl:with-param name="pitchlines" select="$pitchlines"/>
					<xsl:with-param name="clef" select="exsl:node-set($clefs)[last()]"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="exsl:node-set($pitchlines)/*">
					<xsl:copy-of select="exsl:node-set($pitchlines)/*/@*"/>
				</xsl:copy-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="pitchlinesApplyAccidentals">
		<xsl:param name="pitchlines"/>
		<xsl:param name="accidentals"/>
		<!-- Parameters for recursive template -->
		<!--
			We want to iterate through the accidentals backwards as
			the preceding-sibling axis is given to us backwards
			anyway... so backwards is actually forwards? So start at
			count() node then -1 until you reach node 1.
		-->
		<xsl:param name="idx" select="-1"/>
		<xsl:variable name="realIdx">
			<xsl:choose>
				<xsl:when test="$idx &lt; 0">
					<xsl:value-of select="count(exsl:node-set($accidentals))"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$idx"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$realIdx &gt; 0">
				<xsl:variable name="recursivePitchlines">
					<xsl:call-template name="pitchlinesApplyAccidental">
						<xsl:with-param name="pitchlines" select="$pitchlines"/>
						<xsl:with-param name="accidental" select="exsl:node-set($accidentals)[$realIdx]"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:call-template name="pitchlinesApplyAccidentals">
					<xsl:with-param name="pitchlines" select="exsl:node-set($recursivePitchlines)"/>
					<xsl:with-param name="accidentals" select="$accidentals"/>
					<xsl:with-param name="idx" select="$realIdx - 1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="exsl:node-set($pitchlines)/*">
					<xsl:copy-of select="exsl:node-set($pitchlines)/*/@*"/>
				</xsl:copy-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Return note that is closest to coord -->
	<xsl:template name="findClosestNote">
		<xsl:param name="notes"/>
		<xsl:param name="x"/>
		<xsl:param name="y"/>
		<!-- Parameters for recursive template -->
		<xsl:param name="idx" select="1"/>
		<xsl:param name="closest" select="-1"/>
		<xsl:param name="closestIdx" select="-1"/>
		<!-- n.b. this is basically just a simpler version of findClosestLine but with a different dist function (see below) -->
		<xsl:choose>
			<!-- End condition: idx > number of notes -->
			<xsl:when test="$idx &gt; count(exsl:node-set($notes))">
					<xsl:attribute name="count">
						<xsl:value-of select="count(exsl:node-set($notes))"/>
					</xsl:attribute>
					<xsl:attribute name="idx">
						<xsl:value-of select="$idx"/>
					</xsl:attribute>
				<!-- Return copy of closest note using closestIdx -->
				<xsl:copy-of select="exsl:node-set($notes)[$closestIdx]">
					<xsl:copy-of select="exsl:node-set($notes)[$closestIdx]/@*"/>
				</xsl:copy-of>
			</xsl:when>
			<xsl:otherwise>
				<!-- Note x -->
				<xsl:variable name="nx">
					<xsl:value-of select="exsl:node-set($notes)[$idx]/@candr:x"/>
				</xsl:variable>
				<!-- Note y -->
				<xsl:variable name="ny">
					<xsl:value-of select="exsl:node-set($notes)[$idx]/@candr:y"/>
				</xsl:variable>
				<!-- Pythagoras distance -->
				<xsl:variable name="dist">
					<xsl:value-of select="math:sqrt(math:power(($nx - $x),2) + math:power(($ny - $y),2))"/>
				</xsl:variable>
				<xsl:choose>
					<!-- We have a previous "closest" result -->
					<xsl:when test="$closest &gt;= 0">
						<xsl:choose>
							<!-- this dist < closest dist, recurse using this new closest parameter -->
							<xsl:when test="$dist &lt; $closest">
								<xsl:call-template name="findClosestNote">
									<xsl:with-param name="notes" select="$notes"/>
									<xsl:with-param name="x" select="$x"/>
									<xsl:with-param name="y" select="$y"/>
									<xsl:with-param name="idx" select="$idx + 1"/>
									<xsl:with-param name="closest" select="$dist"/>
									<xsl:with-param name="closestIdx" select="$idx"/>
								</xsl:call-template>
							</xsl:when>
							<!-- this dist > closest dist, recurse with the same parameters on the next line -->
							<xsl:otherwise>
								<xsl:call-template name="findClosestNote">
									<xsl:with-param name="notes" select="$notes"/>
									<xsl:with-param name="x" select="$x"/>
									<xsl:with-param name="y" select="$y"/>
									<xsl:with-param name="idx" select="$idx + 1"/>
									<xsl:with-param name="closest" select="$closest"/>
									<xsl:with-param name="closestIdx" select="$closestIdx"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!-- No previous "closest" result (first iteration), no contest! -->
					<xsl:otherwise>
						<xsl:call-template name="findClosestNote">
							<xsl:with-param name="notes" select="$notes"/>
							<xsl:with-param name="x" select="$x"/>
							<xsl:with-param name="y" select="$y"/>
							<xsl:with-param name="idx" select="$idx + 1"/>
							<xsl:with-param name="closest" select="$dist"/>
							<xsl:with-param name="closestIdx" select="$idx"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Return line that is closest to coord -->
	<xsl:template name="findClosestLine">
		<xsl:param name="lines"/>
		<xsl:param name="x"/>
		<xsl:param name="y"/>
		<xsl:param name="shift" select="0"/>
		<!-- Parameters for recursive template -->
		<xsl:param name="idx" select="1"/>
		<xsl:param name="closest" select="-1"/>
		<xsl:param name="closestIdx" select="-1"/>
		<xsl:choose>
			<!-- End condition: idx > number of lines -->
			<xsl:when test="$idx &gt; count(exsl:node-set($lines)/*)">
				<!-- Return copy of closest line using closestIdx -->
				<xsl:choose>
					<!-- Shift is shifting it too low, return first -->
					<xsl:when test="$closestIdx + $shift &lt; 1">
						<xsl:copy-of select="exsl:node-set($lines)/*[1]">
							<xsl:copy-of select="exsl:node-set($lines)/*[1]/@*"/>
						</xsl:copy-of>
					</xsl:when>
					<!-- Shift is shifting it too high, return last -->
					<xsl:when test="$closestIdx + $shift &gt; count(exsl:node-set($lines)/*)">
						<xsl:copy-of select="exsl:node-set($lines)/*[count(exsl:node-set($lines)/*)]">
							<xsl:copy-of select="exsl:node-set($lines)/*[count(exsl:node-set($lines)/*)]/@*"/>
						</xsl:copy-of>
					</xsl:when>
					<!-- Shift isn't messing things up -->
					<xsl:otherwise>
						<xsl:copy-of select="exsl:node-set($lines)/*[$closestIdx]">
							<xsl:copy-of select="exsl:node-set($lines)/*[$closestIdx]/@*"/>
						</xsl:copy-of>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Recurse condition -->
			<xsl:otherwise>
				<!-- Simplify some difficult XPaths with variables -->
				<xsl:variable name="ax">
					<xsl:value-of select="exsl:node-set($lines)/*[$idx]/@ax"/>
				</xsl:variable>
				<xsl:variable name="ay">
					<xsl:value-of select="exsl:node-set($lines)/*[$idx]/@ay"/>
				</xsl:variable>
				<xsl:variable name="bx">
					<xsl:value-of select="exsl:node-set($lines)/*[$idx]/@bx"/>
				</xsl:variable>
				<xsl:variable name="by">
					<xsl:value-of select="exsl:node-set($lines)/*[$idx]/@by"/>
				</xsl:variable>
				<!-- Precompute some differences -->
				<xsl:variable name="byminay">
					<xsl:value-of select="$by - $ay"/>
				</xsl:variable>
				<xsl:variable name="bxminax">
					<xsl:value-of select="$bx - $ax"/>
				</xsl:variable>
				<!-- Calculate distance between line and point -->
				<xsl:variable name="dist">
					<xsl:value-of select="(math:abs($byminay * $x - $bxminax * $y + $bx * $ay - $by * $ax)) div (math:sqrt(math:power($byminay,2) + math:power($bxminax,2)))"/>
				</xsl:variable>
				<xsl:choose>
					<!-- We have a previous "closest" result -->
					<xsl:when test="$closest &gt;= 0">
						<xsl:choose>
							<!-- this dist < closest dist, recurse using this new closest parameter -->
							<xsl:when test="$dist &lt; $closest">
								<xsl:call-template name="findClosestLine">
									<xsl:with-param name="lines" select="$lines"/>
									<xsl:with-param name="x" select="$x"/>
									<xsl:with-param name="y" select="$y"/>
									<xsl:with-param name="idx" select="$idx + 1"/>
									<xsl:with-param name="closest" select="$dist"/>
									<xsl:with-param name="closestIdx" select="$idx"/>
								</xsl:call-template>
							</xsl:when>
							<!-- this dist > closest dist, recurse with the same parameters on the next line -->
							<xsl:otherwise>
								<xsl:call-template name="findClosestLine">
									<xsl:with-param name="lines" select="$lines"/>
									<xsl:with-param name="x" select="$x"/>
									<xsl:with-param name="y" select="$y"/>
									<xsl:with-param name="idx" select="$idx + 1"/>
									<xsl:with-param name="closest" select="$closest"/>
									<xsl:with-param name="closestIdx" select="$closestIdx"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!-- No previous "closest" result (first iteration), no contest! -->
					<xsl:otherwise>
						<xsl:call-template name="findClosestLine">
							<xsl:with-param name="lines" select="$lines"/>
							<xsl:with-param name="x" select="$x"/>
							<xsl:with-param name="y" select="$y"/>
							<xsl:with-param name="idx" select="$idx + 1"/>
							<xsl:with-param name="closest" select="$dist"/>
							<xsl:with-param name="closestIdx" select="$idx"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Repitch lines using clef -->
	<xsl:template name="pitchlinesApplyClef">
		<xsl:param name="pitchlines"/>
		<xsl:param name="clef"/>
		<!-- Find the line that is closet to the clef's centrepoint. -->
		<xsl:variable name="closestLine">
			<xsl:call-template name="findClosestLine">
				<xsl:with-param name="lines" select="$pitchlines"/>
				<xsl:with-param name="x" select="exsl:node-set($clef)[1]/@candr:centrepoint-x"/>
				<xsl:with-param name="y" select="exsl:node-set($clef)[1]/@candr:centrepoint-y"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Store some variables -->
		<xsl:variable name="closestAx">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@ax"/>
		</xsl:variable>
		<xsl:variable name="closestAy">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@ay"/>
		</xsl:variable>
		<xsl:variable name="closestBx">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@bx"/>
		</xsl:variable>
		<xsl:variable name="closestBy">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@by"/>
		</xsl:variable>
		<!-- Calculate mean y-coordinate of the closest line -->
		<xsl:variable name="closestMeany">
			<xsl:value-of select="($closestAy + $closestBy) div 2"/>
		</xsl:variable>
		<!-- Find all the lines with y-coordinates larger than this line -->
		<xsl:variable name="linesAbove">
			<xsl:for-each select="exsl:node-set($pitchlines)/*">
				<!-- Test we're not accidentally including the closest line itself -->
				<xsl:if test="@ax != $closestAx or @ay != $closestAy or @bx != $closestBx or @by != $closestBy">
					<xsl:variable name="meany">
						<xsl:value-of select="(@ay + @by) div 2"/>
					</xsl:variable>
					<xsl:if test="$meany &gt;= $closestMeany">
						<xsl:copy>
							<xsl:copy-of select="@*"/>
						</xsl:copy>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<!-- Very much the same as directly above, but for the lines below -->
		<xsl:variable name="linesBelow">
			<xsl:for-each select="exsl:node-set($pitchlines)/*">
				<xsl:sort select="position()" data-type="number" order="descending"/>
				<xsl:if test="@ax != $closestAx or @ay != $closestAy or @bx != $closestBx or @by != $closestBy">
					<xsl:variable name="meany">
						<xsl:value-of select="(@ay + @by) div 2"/>
					</xsl:variable>
					<xsl:if test="$meany &lt; $closestMeany">
						<xsl:copy>
							<xsl:copy-of select="@*"/>
						</xsl:copy>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<!-- Apply pitch steps to lines above -->
		<xsl:variable name="steppedLinesAbove">
			<xsl:for-each select="exsl:node-set($linesAbove)/*">
				<xsl:copy>
					<xsl:copy-of select="@*"/>
					<!-- Add negative pitch step: -1, -2, -3, -4 etc -->
					<xsl:attribute name="pitchStep">
						<xsl:text>-</xsl:text>
						<xsl:number value="position()"/>
					</xsl:attribute>
				</xsl:copy>
			</xsl:for-each>
		</xsl:variable>
		<!-- Apply pitch steps to lines below -->
		<xsl:variable name="steppedLinesBelow">
			<xsl:for-each select="exsl:node-set($linesBelow)/*">
				<xsl:copy>
					<xsl:copy-of select="@*"/>
					<!-- Add positive pitch step: 1, 2, 3, 4 etc -->
					<xsl:attribute name="pitchStep">
						<xsl:number value="position()"/>
					</xsl:attribute>
				</xsl:copy>
			</xsl:for-each>
		</xsl:variable>
		<!-- All the lines together again -->
		<xsl:variable name="allLines">
			<!-- First the upper lines -->
			<xsl:for-each select="exsl:node-set($steppedLinesAbove)/*">
				<xsl:sort select="@pitchStep" data-type="number"/>
				<xsl:copy-of select=".">
					<xsl:copy-of select="@*"/>
				</xsl:copy-of>
			</xsl:for-each>
			<!-- Then the clef line -->
			<xsl:for-each select="exsl:node-set($closestLine)/*">
				<xsl:sort select="@pitchStep" data-type="number"/>
				<xsl:copy>
					<xsl:copy-of select="@*"/>
					<xsl:attribute name="pitchStep">0</xsl:attribute>
				</xsl:copy>
			</xsl:for-each>
			<!-- Then the lowest line -->
			<xsl:for-each select="exsl:node-set($steppedLinesBelow)/*">
				<xsl:sort select="@pitchStep" data-type="number"/>
				<xsl:copy-of select=".">
					<xsl:copy-of select="@*"/>
				</xsl:copy-of>
			</xsl:for-each>
		</xsl:variable>
		<!-- Convert pitchSteps to pname -->
		<xsl:for-each select="exsl:node-set($allLines)/*">
			<xsl:variable name="pnameAndOct">
				<xsl:call-template name="pitchStepToPnameAndOct">
					<xsl:with-param name="pitchStep" select="./@pitchStep"/>
					<xsl:with-param name="clefType" select="exsl:node-set($clef)[1]/@candr:clefType"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:copy>
				<xsl:copy-of select="@*"/>
				<!-- Add pname attribute -->
				<xsl:attribute name="pname">
					<xsl:value-of select="exsl:node-set($pnameAndOct)/*/@pname"/>
				</xsl:attribute>
				<!-- Add oct attribute -->
				<xsl:attribute name="oct">
					<xsl:value-of select="exsl:node-set($pnameAndOct)/*/@oct"/>
				</xsl:attribute>
			</xsl:copy>
		</xsl:for-each>
	</xsl:template>
	<!-- Convert a pitch step to an absolute pitch name using clef type -->
	<xsl:template name="pitchStepToPnameAndOct">
		<xsl:param name="pitchStep" select="0"/>
		<xsl:param name="clefType" select="C"/>
		<!-- Step should be in the range 0 - 6 (inclusive) -->
		<xsl:variable name="corrStep">
			<xsl:choose>
				<xsl:when test="$pitchStep &lt; 0">
					<xsl:value-of select="($pitchStep mod 7) + 7"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$pitchStep mod 7"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- The alphabet of the clef is [note][octave][note][octave] etc -->
		<xsl:variable name="alphabet">
			<xsl:choose>
				<xsl:when test="$clefType = 'F'">
					<xsl:text>f3g3a3b3c4d4e4</xsl:text>
				</xsl:when>
				<xsl:when test="$clefType = 'D'">
					<xsl:text>d3e3f3g3a3b3c4</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>c4d4e4f4g4a4b4</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<pitch>
			<!-- Pname is the odd-numbered characters of alphabet -->
			<xsl:attribute name="pname">
				<xsl:value-of select="substring($alphabet, (($corrStep + 1) * 2) - 1, 1)"/>
			</xsl:attribute>
			<!-- Oct is the even-numbered characters of alphabet -->
			<xsl:attribute name="oct">
				<xsl:value-of select="floor($pitchStep div 7) + substring($alphabet, ($corrStep + 1) * 2, 1)"/>
			</xsl:attribute>
		</pitch>
	</xsl:template>
	<!-- Repitch lines using accidental -->
	<xsl:template name="pitchlinesApplyAccidental">
		<xsl:param name="pitchlines"/>
		<xsl:param name="accidental"/>
		<!-- Find the line that is closet to the accidentals's centrepoint. -->
		<xsl:variable name="closestLine">
			<xsl:call-template name="findClosestLine">
				<xsl:with-param name="lines" select="$pitchlines"/>
				<xsl:with-param name="x" select="exsl:node-set($accidental)[1]/@candr:centrepoint-x"/>
				<xsl:with-param name="y" select="exsl:node-set($accidental)[1]/@candr:centrepoint-y"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Store some variables -->
		<xsl:variable name="closestAx">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@ax"/>
		</xsl:variable>
		<xsl:variable name="closestAy">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@ay"/>
		</xsl:variable>
		<xsl:variable name="closestBx">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@bx"/>
		</xsl:variable>
		<xsl:variable name="closestBy">
			<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@by"/>
		</xsl:variable>
		<!-- Calculate mean y-coordinate of the closest line -->
		<xsl:variable name="closestMeany">
			<xsl:value-of select="($closestAy + $closestBy) div 2"/>
		</xsl:variable>
		<xsl:variable name="accidentalType">
			<xsl:value-of select="exsl:node-set($accidental)[1]/@candr:accidentalType"/>
		</xsl:variable>
		<xsl:variable name="allLines">
			<!-- other lines -->
			<xsl:for-each select="exsl:node-set($pitchlines)/*">
				<!-- Test we're not accidentally including the closest line itself -->
				<xsl:if test="@ax != $closestAx or @ay != $closestAy or @bx != $closestBx or @by != $closestBy">
					<xsl:copy>
						<xsl:copy-of select="@*"/>
					</xsl:copy>
				</xsl:if>
			</xsl:for-each>
			<xsl:for-each select="exsl:node-set($closestLine)/*">
				<xsl:copy>
					<xsl:copy-of select="@*"/>
					<xsl:choose>
						<xsl:when test="$accidentalType = 'Flat'">
							<xsl:attribute name="pitchAlteration">-1</xsl:attribute>
							<xsl:attribute name="accid.ges">f</xsl:attribute>
						</xsl:when>
						<xsl:when test="$accidentalType = 'Sharp'">
							<xsl:attribute name="pitchAlteration">1</xsl:attribute>
							<xsl:attribute name="accid.ges">s</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="pitchAlteration">0</xsl:attribute>
							<xsl:attribute name="accid.ges">n</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:copy>
			</xsl:for-each>
		</xsl:variable>
		<xsl:for-each select="exsl:node-set($allLines)/*">
			<xsl:sort select="@pitchStep" data-type="number"/>
			<xsl:copy-of select=".">
				<xsl:copy-of select="@*"/>
			</xsl:copy-of>
		</xsl:for-each>
	</xsl:template>
	<!-- Process single item -->
	<xsl:template match="candr:item">
		<xsl:param name="pitchlines"/>
		<xsl:param name="noteSyllableLookup"/>
		<xsl:param name="allSyllables"/>
		<xsl:param name="exclude"/>
		<xsl:variable name="thisID" select="@candr:id"/>
		<xsl:if test="count(exsl:node-set($exclude)[@candr:id = $thisID]) = 0 and count(preceding::*[@candr:id = $thisID][1]) = 0">
			<!-- Apply clefs to pitchlines -->
			<xsl:variable name="cleffedPitchlines">
				<xsl:call-template name="pitchlinesApplyClefs">
					<xsl:with-param name="pitchlines" select="$pitchlines"/>
						<!-- 
					1) All preceding sibling items that have (editorial) clefs
					2) Parent of item is candr:items, preceding sibling items that also have such
					3) Parent of item is candr:items, preceding sibling contexts that have items that also have such
					-->
					<xsl:with-param name="clefs" select="(
					(preceding-sibling::candr:item/candr:clef)
					|
					(preceding-sibling::candr:item/candr:editorialClef)
					|
					(parent::node()/preceding-sibling::candr:items/candr:item/candr:clef)
					|
					(parent::node()/preceding-sibling::candr:items/candr:item/candr:editorialClef)
					|
					(parent::node()/preceding-sibling::candr:context/candr:items/candr:item/candr:clef)
					|
					(parent::node()/preceding-sibling::candr:context/candr:items/candr:item/candr:editorialClef)
					)[1]"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Apply accidentals to pitchlines -->
			<xsl:variable name="accidentalledPitchlines">
				<xsl:call-template name="pitchlinesApplyAccidentals">
					<xsl:with-param name="pitchlines" select="exsl:node-set($cleffedPitchlines)"/>
					<xsl:with-param name="accidentals" select="(
					(preceding-sibling::candr:item/candr:accidental)
					|
					(preceding-sibling::candr:editorialAccidental)
					|
					(parent::node()/preceding-sibling::candr:items/candr:item/candr:accidental)
					|
					(parent::node()/preceding-sibling::candr:items/candr:item/candr:editorialAccidental)
					|
					(parent::node()/preceding-sibling::candr:context/candr:items/candr:item/candr:accidental)
					|
					(parent::node()/preceding-sibling::candr:context/candr:items/candr:item/candr:editorialAccidental)
					)[1]"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- Apply particular element template, i.e. accidental, ligature, note, etc, etc -->
			<xsl:apply-templates match="*[1]">
				<!-- Pitchlines to find pitch of item -->
				<xsl:with-param name="pitchlines" select="exsl:node-set($accidentalledPitchlines)"/>
				<xsl:with-param name="noteSyllableLookup" select="$noteSyllableLookup"/>
				<xsl:with-param name="allSyllables" select="$allSyllables"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>
	<!-- Transform a coordinate into a MEI staffloc. Works only down to how many pitchlines we have -->
	<xsl:template name="coordToPlace">
		<xsl:param name="pitchlines"/>
		<xsl:param name="x" select="0"/>
		<xsl:param name="y" select="0"/>
		<xsl:variable name="closestLine">
			<xsl:call-template name="findClosestLine">
				<xsl:with-param name="lines" select="$pitchlines"/>
				<xsl:with-param name="x" select="$x"/>
				<xsl:with-param name="y" select="$y"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Find closest line in pitchlines using pitchStep, count preceding siblings, minus one to account for the added centreline -->
		<xsl:value-of select="count(exsl:node-set($pitchlines)/*[@pitchStep = exsl:node-set($closestLine)/*[1]/@pitchStep]/preceding-sibling::*) - 1"/>
	</xsl:template>
	<xsl:template name="makeRectZone">
		<zone>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-zone-</xsl:text>
				<xsl:value-of select="generate-id()"/>
			</xsl:attribute>
			<xsl:attribute name="ulx">
				<xsl:value-of select="round(@candr:left)"/>
			</xsl:attribute>
			<xsl:attribute name="uly">
				<xsl:value-of select="round(@candr:top)"/>
			</xsl:attribute>
			<xsl:attribute name="lrx">
				<xsl:value-of select="round(@candr:left + @candr:width)"/>
			</xsl:attribute>
			<xsl:attribute name="lry">
				<xsl:value-of select="round(@candr:top + @candr:height)"/>
			</xsl:attribute>
		</zone>
	</xsl:template>
	<xsl:template name="makeCentrepointZone">
		<zone>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-zone-</xsl:text>
				<xsl:value-of select="generate-id()"/>
			</xsl:attribute>
			<xsl:attribute name="ulx">
				<xsl:value-of select="round(@candr:centrepoint-x)"/>
			</xsl:attribute>
			<xsl:attribute name="uly">
				<xsl:value-of select="round(@candr:centrepoint-y)"/>
			</xsl:attribute>
			<xsl:attribute name="lrx">
				<xsl:value-of select="round(@candr:centrepoint-x)"/>
			</xsl:attribute>
			<xsl:attribute name="lry">
				<xsl:value-of select="round(@candr:centrepoint-y)"/>
			</xsl:attribute>
		</zone>
	</xsl:template>
	<xsl:template name="makeLineZone">
		<zone>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-zone-</xsl:text>
				<xsl:value-of select="generate-id()"/>
			</xsl:attribute>
			<xsl:attribute name="ulx">
				<xsl:choose>
					<xsl:when test="@candr:ax &lt; @candr:bx">
						<xsl:value-of select="round(@candr:ax)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="round(@candr:bx)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="uly">
				<xsl:choose>
					<xsl:when test="@candr:ay &lt; @candr:by">
						<xsl:value-of select="round(@candr:ay)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="round(@candr:by)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="lrx">
				<xsl:choose>
					<xsl:when test="@candr:ax &gt; @candr:bx">
						<xsl:value-of select="round(@candr:ax)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="round(@candr:bx)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="lry">
				<xsl:choose>
					<xsl:when test="@candr:ay &gt; @candr:by">
						<xsl:value-of select="round(@candr:ay)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="round(@candr:by)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</zone>
	</xsl:template>
	<!-- Generate MEI accidental -->
	<xsl:template match="candr:accidental" name="processAccidental">
		<xsl:param name="pitchlines"/>
		<!-- Find closest line to this accidental -->
		<xsl:variable name="closestLine">
			<xsl:call-template name="findClosestLine">
				<xsl:with-param name="lines" select="$pitchlines"/>
				<xsl:with-param name="x" select="@candr:centrepoint-x"/>
				<xsl:with-param name="y" select="@candr:centrepoint-y"/>
			</xsl:call-template>
		</xsl:variable>
		<accid>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
			</xsl:attribute>
			<xsl:if test="$facsimile = 'true'">
				<xsl:attribute name="facs">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@candr:id"/>
					<xsl:text>-zone-</xsl:text>
					<xsl:value-of select="generate-id()"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="accid">
				<xsl:choose>
					<xsl:when test="@candr:accidentalType = 'Flat'">
						<xsl:text>f</xsl:text>
					</xsl:when>
					<xsl:when test="@candr:accidentalType = 'Sharp'">
						<xsl:text>s</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>n</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<!-- Convert oct and pname to accidental-specific ones -->
			<xsl:attribute name="oloc">
				<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@oct"/>
			</xsl:attribute>
			<xsl:attribute name="ploc">
				<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@pname"/>
			</xsl:attribute>
		</accid>
	</xsl:template>
	<xsl:template match="candr:accidental" mode="makeZone">
		<xsl:call-template name="makeRectZone"/>
	</xsl:template>
	<!-- Generate editorial MEI accidental -->
	<xsl:template match="candr:editorialAccidental">
		<xsl:param name="pitchlines"/>
		<corr>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-corr</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="processAccidental">
				<xsl:with-param name="pitchlines" select="$pitchlines"/>
			</xsl:call-template>
		</corr>
	</xsl:template>
	<xsl:template match="candr:editorialAccidental" mode="makeZone">
		<xsl:call-template name="makeCentrepointZone"/>
	</xsl:template>
	<!-- Helper to find pitchline in pitchlines and return its index in the node set -->
	<xsl:template name="pitchlineItemToIdx">
		<xsl:param name="pitchlines"/>
		<xsl:param name="item"/>
		<!-- Parameters for recursive template -->
		<xsl:param name="idx" select="1"/>
		<xsl:choose>
			<xsl:when test="$idx &gt; count(exsl:node-set($pitchlines)/*)">
				<xsl:text>-1</xsl:text>
			</xsl:when>
			<xsl:when test="
				name(exsl:node-set($item)/*) = name(exsl:node-set($pitchlines)/*[$idx])
				and
				exsl:node-set($item)/*/@ax = exsl:node-set($pitchlines)/*[$idx]/@ax
				and
				exsl:node-set($item)/*/@ay = exsl:node-set($pitchlines)/*[$idx]/@ay
				and
				exsl:node-set($item)/*/@bx = exsl:node-set($pitchlines)/*[$idx]/@bx
				and
				exsl:node-set($item)/*/@by = exsl:node-set($pitchlines)/*[$idx]/@by
			">
				<xsl:value-of select="$idx"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="pitchlineItemToIdx">
					<xsl:with-param name="pitchlines" select="$pitchlines"/>
					<xsl:with-param name="item" select="$item"/>
					<xsl:with-param name="idx" select="$idx + 1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Generate MEI clef -->
	<xsl:template match="candr:clef" name="processClef">
		<xsl:param name="pitchlines"/>
		<xsl:variable name="closestLine">
			<xsl:call-template name="findClosestLine">
				<xsl:with-param name="lines" select="$pitchlines"/>
				<xsl:with-param name="x" select="@candr:centrepoint-x"/>
				<xsl:with-param name="y" select="@candr:centrepoint-y"/>
			</xsl:call-template>
		</xsl:variable>
		<clef>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
			</xsl:attribute>
			<xsl:if test="$facsimile = 'true'">
				<xsl:attribute name="facs">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@candr:id"/>
					<xsl:text>-zone-</xsl:text>
					<xsl:value-of select="generate-id()"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="line">
				<!-- Find closest line in pitchlines and get its index -->
				<xsl:variable name="pitchlineIdx">
					<xsl:call-template name="pitchlineItemToIdx">
						<xsl:with-param name="pitchlines" select="$pitchlines"/>
						<xsl:with-param name="item" select="$closestLine"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Line is actually stafflines only, not centrelines -->
				<xsl:value-of select="ceiling($pitchlineIdx div 2)"/>
			</xsl:attribute>
			<xsl:attribute name="shape">
				<xsl:choose>
					<xsl:when test="@candr:clefType = 'F'">
						<xsl:text>F</xsl:text>
					</xsl:when>
					<xsl:when test="@candr:clefType = 'D'">
						<xsl:text>D</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>C</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</clef>
	</xsl:template>
	<xsl:template match="candr:clef" mode="makeZone">
		<xsl:call-template name="makeRectZone"/>
	</xsl:template>
	<!-- Generate editorial MEI clef -->
	<xsl:template match="candr:editorialClef">
		<xsl:param name="pitchlines"/>
		<corr>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-corr</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="processClef">
				<xsl:with-param name="pitchlines" select="$pitchlines"/>
			</xsl:call-template>
		</corr>
	</xsl:template>
	<xsl:template match="candr:editorialClef" mode="makeZone">
		<xsl:call-template name="makeCentrepointZone"/>
	</xsl:template>
	<!-- Generate MEI divisione -->
	<xsl:template match="candr:divisione">
		<xsl:param name="pitchlines"/>
		<!-- Find the minimum y-coordinate -->
		<xsl:variable name="minCoord">
			<coord>
				<xsl:choose>
					<xsl:when test="@candr:ay &lt; @candr:by">
						<xsl:attribute name="x">
							<xsl:value-of select="@candr:ax"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="@candr:ay"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="x">
							<xsl:value-of select="@candr:bx"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="@candr:by"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</coord>
		</xsl:variable>
		<!-- Find the max y-coordinate -->
		<xsl:variable name="maxCoord">
			<coord>
				<xsl:choose>
					<xsl:when test="@candr:ay &gt;= @candr:by">
						<xsl:attribute name="x">
							<xsl:value-of select="@candr:ax"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="@candr:ay"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="x">
							<xsl:value-of select="@candr:bx"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="@candr:by"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</coord>
		</xsl:variable>
		<xsl:variable name="minPlace">
			<xsl:call-template name="coordToPlace">
				<xsl:with-param name="pitchlines" select="$pitchlines"/>
				<xsl:with-param name="x" select="exsl:node-set($minCoord)/*[1]/@x"/>
				<xsl:with-param name="y" select="exsl:node-set($minCoord)/*[1]/@y"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="maxPlace">
			<xsl:call-template name="coordToPlace">
				<xsl:with-param name="pitchlines" select="$pitchlines"/>
				<xsl:with-param name="x" select="exsl:node-set($maxCoord)/*[1]/@x"/>
				<xsl:with-param name="y" select="exsl:node-set($maxCoord)/*[1]/@y"/>
			</xsl:call-template>
		</xsl:variable>
		<divisione>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
			</xsl:attribute>
			<xsl:if test="$facsimile = 'true'">
				<xsl:attribute name="facs">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@candr:id"/>
					<xsl:text>-zone-</xsl:text>
					<xsl:value-of select="generate-id()"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="place">
				<xsl:value-of select="$minPlace"/>
			</xsl:attribute>
			<xsl:attribute name="len">
				<xsl:value-of select="math:abs($maxPlace - $minPlace)"/>
			</xsl:attribute>
		</divisione>
	</xsl:template>
	<xsl:template match="candr:divisione" mode="makeZone">
		<xsl:call-template name="makeLineZone"/>
	</xsl:template>
	<!-- Generate MEI ligature -->
	<xsl:template match="candr:ligature">
		<xsl:param name="pitchlines"/>
		<xsl:param name="noteSyllableLookup"/>
		<xsl:param name="allSyllables"/>
		<ligature>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
			</xsl:attribute>
			<xsl:if test="$facsimile = 'true'">
				<xsl:attribute name="facs">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@candr:id"/>
					<xsl:text>-zone-</xsl:text>
					<xsl:value-of select="generate-id()"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="form">
				<xsl:choose>
					<xsl:when test="@candr:ligatureType = 'Currentes'">
						<xsl:text>obliqua</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>recta</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates select="candr:note">
				<xsl:with-param name="pitchlines" select="$pitchlines"/>
				<xsl:with-param name="noteSyllableLookup" select="$noteSyllableLookup"/>
				<xsl:with-param name="allSyllables" select="$allSyllables"/>
			</xsl:apply-templates>
		</ligature>
	</xsl:template>
	<xsl:template match="candr:ligature" mode="makeZone">
		<xsl:variable name="noteXValues">
			<xsl:for-each select="./candr:note">
				<value>
					<xsl:value-of select="@candr:x"/>
				</value>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="noteYValues">
			<xsl:for-each select="./candr:note">
				<value>
					<xsl:value-of select="@candr:y"/>
				</value>
			</xsl:for-each>
		</xsl:variable>
		<zone>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-zone-</xsl:text>
				<xsl:value-of select="generate-id()"/>
			</xsl:attribute>
			<xsl:attribute name="ulx">
				<xsl:value-of select="round(math:min(exsl:node-set($noteXValues)/*))"/>
			</xsl:attribute>
			<xsl:attribute name="uly">
				<xsl:value-of select="round(math:min(exsl:node-set($noteYValues)/*))"/>
			</xsl:attribute>
			<xsl:attribute name="lrx">
				<xsl:value-of select="round(math:max(exsl:node-set($noteXValues)/*))"/>
			</xsl:attribute>
			<xsl:attribute name="lry">
				<xsl:value-of select="round(math:max(exsl:node-set($noteYValues)/*))"/>
			</xsl:attribute>
		</zone>
	</xsl:template>
	<!-- Generate MEI note -->
	<xsl:template match="candr:note">
		<xsl:param name="pitchlines"/>
		<xsl:param name="noteSyllableLookup"/>
		<xsl:param name="allSyllables"/>
		<xsl:variable name="closestLine">
			<xsl:call-template name="findClosestLine">
				<xsl:with-param name="lines" select="$pitchlines"/>
				<xsl:with-param name="x" select="@candr:x"/>
				<xsl:with-param name="y" select="@candr:y"/>
				<xsl:with-param name="shift" select="@candr:shift"/>
			</xsl:call-template>
		</xsl:variable>
		<note>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
			</xsl:attribute>
			<xsl:if test="$facsimile = 'true'">
				<xsl:attribute name="facs">
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@candr:id"/>
					<xsl:text>-zone-</xsl:text>
					<xsl:value-of select="generate-id()"/>
				</xsl:attribute>
			</xsl:if>
			<!-- Copy pitch info from closestLine to the note -->
			<xsl:if test="exsl:node-set($closestLine)/*[1]/@accid.ges">
				<xsl:attribute name="accid.ges">
					<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@accid.ges"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="exsl:node-set($closestLine)/*[1]/@oct">
				<xsl:attribute name="oct">
					<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@oct"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="exsl:node-set($closestLine)/*[1]/@pname">
				<xsl:attribute name="pname">
					<xsl:value-of select="exsl:node-set($closestLine)/*[1]/@pname"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@plica and @plica != 0">
				<xsl:attribute name="plica">
					<xsl:choose>
						<xsl:when test="@plica &gt; 0">
							<xsl:text>asc</xsl:text>
						</xsl:when>
						<xsl:when test="@plica &lt; 0">
							<xsl:text>desc</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
			<!-- We'll be referencing this -->
			<xsl:variable name="thisNoteID">
				<xsl:value-of select="@candr:id"/>
			</xsl:variable>
			<!-- All the syllables that follow this note in the staff -->
			<xsl:variable name="followingSyllables">
				<xsl:value-of select="(./following::candr:syllable|./following::candr:editorialSyllable)[1]"/>
			</xsl:variable>
			<!-- noteSyllableLookup is an array of syllables each with their closest note contained within, made at stave level and passed down to this level -->
			<!-- Find note in lookup table -->
			<xsl:for-each select="(exsl:node-set($noteSyllableLookup)/candr:syllable[candr:note/@candr:id = $thisNoteID])|(exsl:node-set($noteSyllableLookup)/candr:editorialSyllable[candr:note/@candr:id = $thisNoteID])">
				<xsl:variable name="syllableID">
					<xsl:value-of select="@candr:id"/>
				</xsl:variable>
				<syl>
					<xsl:attribute name="xml:id">
						<xsl:value-of select="@candr:id"/>
					</xsl:attribute>
					<xsl:attribute name="wordpos">
						<xsl:choose>
							<!-- Syllable says it's a wordstart, so pass that to MEI -->
							<xsl:when test="@candr:wordstart = 'true'">
								<xsl:text>i</xsl:text>
							</xsl:when>
							<!-- Find following syllables after this note that are not this syllable. Get the first one, if it is a wordstart, then this must be the end of a word -->
							<xsl:when test="set:trailing(exsl:node-set($allSyllables), exsl:node-set($allSyllables)[$syllableID = @candr:id])[1]/@candr:wordstart = 'true'">
								<xsl:text>t</xsl:text>
							</xsl:when>
							<!-- Else, we're in the middle of a word -->
							<xsl:otherwise>
								<xsl:text>m</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="name() = 'candr:syllable'">
							<xsl:value-of select="@candr:text"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- editorial syllables are wrapped in <corr> -->
							<corr>
								<xsl:attribute name="xml:id">
									<xsl:value-of select="@candr:id"/>
									<xsl:text>-corr</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="@candr:text"/>
							</corr>
						</xsl:otherwise>
					</xsl:choose>
				</syl>
			</xsl:for-each>
		</note>
	</xsl:template>
	<xsl:template match="candr:note" mode="makeZone">
		<zone>
			<xsl:attribute name="xml:id">
				<xsl:value-of select="@candr:id"/>
				<xsl:text>-zone-</xsl:text>
				<xsl:value-of select="generate-id()"/>
			</xsl:attribute>
			<xsl:attribute name="ulx">
				<xsl:value-of select="round(@candr:x)"/>
			</xsl:attribute>
			<xsl:attribute name="uly">
				<xsl:value-of select="round(@candr:y)"/>
			</xsl:attribute>
			<xsl:attribute name="lrx">
				<xsl:value-of select="round(@candr:x)"/>
			</xsl:attribute>
			<xsl:attribute name="lry">
				<xsl:value-of select="round(@candr:y)"/>
			</xsl:attribute>
		</zone>
	</xsl:template>
	<xsl:template match="candr:warp" mode="makePoints">
		<xsl:variable name="sortedPoints">
			<xsl:for-each select="./candr:node">
				<xsl:sort select="@candr:y" data-type="number"/>
				<xsl:copy-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<points>
			<point>
				<xsl:choose>
					<xsl:when test="exsl:node-set($sortedPoints)/*[1]/@candr:x &lt; exsl:node-set($sortedPoints)/*[2]/@candr:x">
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[1]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[1]/@candr:y"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[2]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[2]/@candr:y"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</point>
			<point>
				<xsl:choose>
					<xsl:when test="exsl:node-set($sortedPoints)/*[1]/@candr:x &lt; exsl:node-set($sortedPoints)/*[2]/@candr:x">
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[2]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[2]/@candr:y"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[1]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[1]/@candr:y"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</point>
			<point>
				<xsl:choose>
					<xsl:when test="exsl:node-set($sortedPoints)/*[3]/@candr:x &lt; exsl:node-set($sortedPoints)/*[4]/@candr:x">
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[3]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[3]/@candr:y"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[4]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[4]/@candr:y"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</point>
			<point>
				<xsl:choose>
					<xsl:when test="exsl:node-set($sortedPoints)/*[3]/@candr:x &lt; exsl:node-set($sortedPoints)/*[4]/@candr:x">
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[4]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[4]/@candr:y"/>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="x">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[3]/@candr:x"/>
						</xsl:attribute>
						<xsl:attribute name="y">
							<xsl:value-of select="exsl:node-set($sortedPoints)/*[3]/@candr:y"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</point>
		</points>
	</xsl:template>
	<xsl:template match="candr:warp" mode="makeMinRect">
		<xsl:variable name="pointXs">
			<xsl:for-each select="./candr:node">
				<value>
					<xsl:value-of select="./@candr:x"/>
				</value>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="pointYs">
			<xsl:for-each select="./candr:node">
				<value>
					<xsl:value-of select="./@candr:y"/>
				</value>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="minx">
			<xsl:value-of select="math:min(exsl:node-set($pointXs)/*)"/>
		</xsl:variable>
		<xsl:variable name="miny">
			<xsl:value-of select="math:min(exsl:node-set($pointYs)/*)"/>
		</xsl:variable>
		<xsl:variable name="maxx">
			<xsl:value-of select="math:max(exsl:node-set($pointXs)/*)"/>
		</xsl:variable>
		<xsl:variable name="maxy">
			<xsl:value-of select="math:max(exsl:node-set($pointYs)/*)"/>
		</xsl:variable>
		<minRect>
			<point>
				<xsl:attribute name="x">
					<xsl:value-of select="$minx"/>
				</xsl:attribute>
				<xsl:attribute name="y">
					<xsl:value-of select="$miny"/>
				</xsl:attribute>
			</point>
			<point>
				<xsl:attribute name="x">
					<xsl:value-of select="$maxx"/>
				</xsl:attribute>
				<xsl:attribute name="y">
					<xsl:value-of select="$miny"/>
				</xsl:attribute>
			</point>
			<point>
				<xsl:attribute name="x">
					<xsl:value-of select="$minx"/>
				</xsl:attribute>
				<xsl:attribute name="y">
					<xsl:value-of select="$maxy"/>
				</xsl:attribute>
			</point>
			<point>
				<xsl:attribute name="x">
					<xsl:value-of select="$maxx"/>
				</xsl:attribute>
				<xsl:attribute name="y">
					<xsl:value-of select="$maxy"/>
				</xsl:attribute>
			</point>
		</minRect>
	</xsl:template>
	<xsl:template match="candr:system">
		<xsl:param name="systemNum" select="1"/>
		<scoreDef>
			<xsl:attribute name="xml:id">
				<xsl:text>scoreDef-</xsl:text>
				<xsl:value-of select="$systemNum"/>
			</xsl:attribute>
			<staffGrp>
				<xsl:attribute name="xml:id">
					<xsl:text>staffGrp-</xsl:text>
					<xsl:value-of select="$systemNum"/>
				</xsl:attribute>
				<xsl:for-each select="./candr:staves/candr:stave">
					<staffDef notationtype="mensural">
						<xsl:attribute name="xml:id">
							<xsl:text>staffDef-</xsl:text>
							<xsl:value-of select="$systemNum"/>
							<xsl:text>-</xsl:text>
							<xsl:number/>
						</xsl:attribute>
						<xsl:attribute name="n">
							<xsl:number/>
						</xsl:attribute>
						<xsl:choose>
							<xsl:when test="./candr:context">
								<xsl:attribute name="lines">
									<xsl:value-of select="count(./candr:context/candr:items/candr:item/candr:staffline)"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="lines">
									<xsl:value-of select="count(./candr:items/candr:item/candr:staffline)"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</staffDef>
				</xsl:for-each>
			</staffGrp>
		</scoreDef>
		<xsl:for-each select="./candr:staves/candr:stave">
			<xsl:apply-templates select=".">
				<xsl:with-param name="staffNum">
					<xsl:value-of select="$systemNum"/>
					<xsl:text>-</xsl:text>
					<xsl:number/>
				</xsl:with-param>
				<xsl:with-param name="outputContext">
					<xsl:choose>
						<xsl:when test="position() = 1">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="candr:chunk">
		<xsl:param name="chunkNum" select="1"/>
		<section>
			<xsl:attribute name="xml:id">
				<xsl:text>section-</xsl:text>
				<xsl:value-of select="$chunkNum"/>
			</xsl:attribute>
			<xsl:for-each select=".//candr:system">
				<xsl:apply-templates select=".">
					<xsl:with-param name="systemNum">
						<xsl:value-of select="$chunkNum"/>
						<xsl:text>-</xsl:text>
						<xsl:number/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:for-each>
		</section>
	</xsl:template>
	<xsl:template match="candr:setting">
		<xsl:processing-instruction name="xml-model">href="http://music-encoding.org/schema/4.0.1/mei-all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:processing-instruction>
		<xsl:text>&#xa;</xsl:text>
		<xsl:processing-instruction name="xml-model">href="http://music-encoding.org/schema/4.0.1/mei-all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
		<xsl:text>&#xa;</xsl:text>
		<mei xmlns:xlink="http://www.w3.org/1999/xlink" meiversion="4.0.1">
			<meiHead>
				<fileDesc>
					<titleStmt>
						<title>
							<xsl:value-of select="@candr:name"/>
						</title>
					</titleStmt>
					<respStmt>
						<corpName>
							<name>
								<xsl:text>CANDR (Clausula Archive of the Notre Dame Repertory)</xsl:text>
							</name>
						</corpName>
					</respStmt>
					<sourceDesc>
						<source xml:id="settingSource">
							<locus>
								<xsl:call-template name="foliosToText">
									<xsl:with-param name="folios" select=".//candr:folios/candr:folio"/>
								</xsl:call-template>
							</locus>
							<biblStruct>
								<xsl:call-template name="sourceIdentifiers">
									<xsl:with-param name="sources" select="./candr:chunk/candr:source"/>
								</xsl:call-template>
								<xsl:if test="./candr:chunk/candr:source/@candr:archive">
									<repository>
										<ptr>
											<xsl:attribute name="target">
												<xsl:value-of select="./candr:chunk/candr:source/@candr:archive"/>
											</xsl:attribute>
										</ptr>
									</repository>
								</xsl:if>
							</biblStruct>
						</source>
					</sourceDesc>
				</fileDesc>
				<encodingDesc>
					<appInfo>
						<application>
							<ref target="http://candr.ac.uk/">
								<name>
									<xsl:text>CANDR setting-to-MEI transform</xsl:text>
								</name>
							</ref>
						</application>
					</appInfo>
				</encodingDesc>
			</meiHead>
			<xsl:variable name="warps">
				<xsl:if test="$facsimile = 'true'">
					<xsl:variable name="numWarps" select="count(//candr:warp[generate-id()=generate-id(key('kWarpID', @candr:id)[1])])"/>
					<xsl:for-each select="//candr:warp[generate-id()=generate-id(key('kWarpID', @candr:id)[1])]">
						<xsl:message>
							<xsl:text>Calculating warp </xsl:text>
							<xsl:value-of select="position()"/>
							<xsl:text>/</xsl:text>
							<xsl:value-of select="$numWarps"/>
						</xsl:message>
						<xsl:variable name="dstPts">
							<xsl:apply-templates select="." mode="makePoints"/>
						</xsl:variable>
						<xsl:variable name="srcPts">
							<xsl:apply-templates select="." mode="makeMinRect"/>
						</xsl:variable>
						<warp>
							<xsl:attribute name="candr:id">
								<xsl:value-of select="@candr:id"/>
							</xsl:attribute>
							<xsl:call-template name="perspectiveTransform">
								<xsl:with-param name="srcPts" select="exsl:node-set($srcPts)/*[1]"/>
								<xsl:with-param name="dstPts" select="exsl:node-set($dstPts)/*[1]"/>
							</xsl:call-template>
						</warp>
					</xsl:for-each>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="staffWarped">
				<xsl:apply-templates match="." mode="applyStaffWarps">
					<xsl:with-param name="warps" select="$warps"/>
				</xsl:apply-templates>
			</xsl:variable>
			<xsl:variable name="warped">
				<xsl:apply-templates match="exsl:node-set($staffWarped)" mode="applySystemWarps">
					<xsl:with-param name="warps" select="$warps"/>
				</xsl:apply-templates>
			</xsl:variable>
			<music>
				<xsl:if test="$facsimile = 'true'">
					<facsimile decls="#settingSource">
						<xsl:variable name="allFacsimileIDs">
							<xsl:for-each select=".//candr:facsimile[generate-id()=generate-id(key('kFacsimileURL', @candr:image)[1])]">
								<xsl:copy>
									<xsl:copy-of select="@*"/>
								</xsl:copy>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="rootFacs" select="exsl:node-set($warped)//candr:facsimile"/>
						<xsl:for-each select="exsl:node-set($allFacsimileIDs)/*">
							<surface>
								<xsl:if test="@candr:name">
									<xsl:attribute name="label">
										<xsl:value-of select="@candr:name"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:attribute name="xml:id">
									<xsl:value-of select="@candr:id"/>
								</xsl:attribute>
								<graphic>
									<xsl:attribute name="target">
										<xsl:value-of select="@candr:image"/>
									</xsl:attribute>
									<xsl:variable name="thisID">
										<xsl:value-of select="@candr:id"/>
									</xsl:variable>
									<xsl:variable name="matchingFac" select="exsl:node-set($rootFacs)[@candr:id = $thisID]"/>
									<xsl:apply-templates select="exsl:node-set($matchingFac)//candr:item//*" mode="makeZone"/>
								</graphic>
							</surface>
						</xsl:for-each>
					</facsimile>
				</xsl:if>
				<body>
					<mdiv>
						<score>
							<xsl:for-each select="exsl:node-set($warped)/candr:chunk">
								<xsl:apply-templates select=".">
									<xsl:with-param name="chunkNum">
										<xsl:number/>
									</xsl:with-param>
								</xsl:apply-templates>
							</xsl:for-each>
						</score>
					</mdiv>
				</body>
			</music>
		</mei>
	</xsl:template>
	<!-- Matrix things -->
	<xsl:template name="MMul8x8">
		<xsl:param name="a"/>
		<xsl:param name="b"/>
		<xsl:variable name="ap11" select="exsl:node-set($a)/*[1]/*[1]/*[1]/text()"/>
		<xsl:variable name="ap12" select="exsl:node-set($a)/*[1]/*[1]/*[2]/text()"/>
		<xsl:variable name="ap13" select="exsl:node-set($a)/*[1]/*[1]/*[3]/text()"/>
		<xsl:variable name="ap14" select="exsl:node-set($a)/*[1]/*[1]/*[4]/text()"/>
		<xsl:variable name="ap15" select="exsl:node-set($a)/*[1]/*[1]/*[5]/text()"/>
		<xsl:variable name="ap16" select="exsl:node-set($a)/*[1]/*[1]/*[6]/text()"/>
		<xsl:variable name="ap17" select="exsl:node-set($a)/*[1]/*[1]/*[7]/text()"/>
		<xsl:variable name="ap18" select="exsl:node-set($a)/*[1]/*[1]/*[8]/text()"/>
		<xsl:variable name="ap21" select="exsl:node-set($a)/*[1]/*[2]/*[1]/text()"/>
		<xsl:variable name="ap22" select="exsl:node-set($a)/*[1]/*[2]/*[2]/text()"/>
		<xsl:variable name="ap23" select="exsl:node-set($a)/*[1]/*[2]/*[3]/text()"/>
		<xsl:variable name="ap24" select="exsl:node-set($a)/*[1]/*[2]/*[4]/text()"/>
		<xsl:variable name="ap25" select="exsl:node-set($a)/*[1]/*[2]/*[5]/text()"/>
		<xsl:variable name="ap26" select="exsl:node-set($a)/*[1]/*[2]/*[6]/text()"/>
		<xsl:variable name="ap27" select="exsl:node-set($a)/*[1]/*[2]/*[7]/text()"/>
		<xsl:variable name="ap28" select="exsl:node-set($a)/*[1]/*[2]/*[8]/text()"/>
		<xsl:variable name="ap31" select="exsl:node-set($a)/*[1]/*[3]/*[1]/text()"/>
		<xsl:variable name="ap32" select="exsl:node-set($a)/*[1]/*[3]/*[2]/text()"/>
		<xsl:variable name="ap33" select="exsl:node-set($a)/*[1]/*[3]/*[3]/text()"/>
		<xsl:variable name="ap34" select="exsl:node-set($a)/*[1]/*[3]/*[4]/text()"/>
		<xsl:variable name="ap35" select="exsl:node-set($a)/*[1]/*[3]/*[5]/text()"/>
		<xsl:variable name="ap36" select="exsl:node-set($a)/*[1]/*[3]/*[6]/text()"/>
		<xsl:variable name="ap37" select="exsl:node-set($a)/*[1]/*[3]/*[7]/text()"/>
		<xsl:variable name="ap38" select="exsl:node-set($a)/*[1]/*[3]/*[8]/text()"/>
		<xsl:variable name="ap41" select="exsl:node-set($a)/*[1]/*[4]/*[1]/text()"/>
		<xsl:variable name="ap42" select="exsl:node-set($a)/*[1]/*[4]/*[2]/text()"/>
		<xsl:variable name="ap43" select="exsl:node-set($a)/*[1]/*[4]/*[3]/text()"/>
		<xsl:variable name="ap44" select="exsl:node-set($a)/*[1]/*[4]/*[4]/text()"/>
		<xsl:variable name="ap45" select="exsl:node-set($a)/*[1]/*[4]/*[5]/text()"/>
		<xsl:variable name="ap46" select="exsl:node-set($a)/*[1]/*[4]/*[6]/text()"/>
		<xsl:variable name="ap47" select="exsl:node-set($a)/*[1]/*[4]/*[7]/text()"/>
		<xsl:variable name="ap48" select="exsl:node-set($a)/*[1]/*[4]/*[8]/text()"/>
		<xsl:variable name="ap51" select="exsl:node-set($a)/*[1]/*[5]/*[1]/text()"/>
		<xsl:variable name="ap52" select="exsl:node-set($a)/*[1]/*[5]/*[2]/text()"/>
		<xsl:variable name="ap53" select="exsl:node-set($a)/*[1]/*[5]/*[3]/text()"/>
		<xsl:variable name="ap54" select="exsl:node-set($a)/*[1]/*[5]/*[4]/text()"/>
		<xsl:variable name="ap55" select="exsl:node-set($a)/*[1]/*[5]/*[5]/text()"/>
		<xsl:variable name="ap56" select="exsl:node-set($a)/*[1]/*[5]/*[6]/text()"/>
		<xsl:variable name="ap57" select="exsl:node-set($a)/*[1]/*[5]/*[7]/text()"/>
		<xsl:variable name="ap58" select="exsl:node-set($a)/*[1]/*[5]/*[8]/text()"/>
		<xsl:variable name="ap61" select="exsl:node-set($a)/*[1]/*[6]/*[1]/text()"/>
		<xsl:variable name="ap62" select="exsl:node-set($a)/*[1]/*[6]/*[2]/text()"/>
		<xsl:variable name="ap63" select="exsl:node-set($a)/*[1]/*[6]/*[3]/text()"/>
		<xsl:variable name="ap64" select="exsl:node-set($a)/*[1]/*[6]/*[4]/text()"/>
		<xsl:variable name="ap65" select="exsl:node-set($a)/*[1]/*[6]/*[5]/text()"/>
		<xsl:variable name="ap66" select="exsl:node-set($a)/*[1]/*[6]/*[6]/text()"/>
		<xsl:variable name="ap67" select="exsl:node-set($a)/*[1]/*[6]/*[7]/text()"/>
		<xsl:variable name="ap68" select="exsl:node-set($a)/*[1]/*[6]/*[8]/text()"/>
		<xsl:variable name="ap71" select="exsl:node-set($a)/*[1]/*[7]/*[1]/text()"/>
		<xsl:variable name="ap72" select="exsl:node-set($a)/*[1]/*[7]/*[2]/text()"/>
		<xsl:variable name="ap73" select="exsl:node-set($a)/*[1]/*[7]/*[3]/text()"/>
		<xsl:variable name="ap74" select="exsl:node-set($a)/*[1]/*[7]/*[4]/text()"/>
		<xsl:variable name="ap75" select="exsl:node-set($a)/*[1]/*[7]/*[5]/text()"/>
		<xsl:variable name="ap76" select="exsl:node-set($a)/*[1]/*[7]/*[6]/text()"/>
		<xsl:variable name="ap77" select="exsl:node-set($a)/*[1]/*[7]/*[7]/text()"/>
		<xsl:variable name="ap78" select="exsl:node-set($a)/*[1]/*[7]/*[8]/text()"/>
		<xsl:variable name="ap81" select="exsl:node-set($a)/*[1]/*[8]/*[1]/text()"/>
		<xsl:variable name="ap82" select="exsl:node-set($a)/*[1]/*[8]/*[2]/text()"/>
		<xsl:variable name="ap83" select="exsl:node-set($a)/*[1]/*[8]/*[3]/text()"/>
		<xsl:variable name="ap84" select="exsl:node-set($a)/*[1]/*[8]/*[4]/text()"/>
		<xsl:variable name="ap85" select="exsl:node-set($a)/*[1]/*[8]/*[5]/text()"/>
		<xsl:variable name="ap86" select="exsl:node-set($a)/*[1]/*[8]/*[6]/text()"/>
		<xsl:variable name="ap87" select="exsl:node-set($a)/*[1]/*[8]/*[7]/text()"/>
		<xsl:variable name="ap88" select="exsl:node-set($a)/*[1]/*[8]/*[8]/text()"/>
		<xsl:variable name="bp11" select="exsl:node-set($b)/*[1]/*[1]/*[1]/text()"/>
		<xsl:variable name="bp12" select="exsl:node-set($b)/*[1]/*[1]/*[2]/text()"/>
		<xsl:variable name="bp13" select="exsl:node-set($b)/*[1]/*[1]/*[3]/text()"/>
		<xsl:variable name="bp14" select="exsl:node-set($b)/*[1]/*[1]/*[4]/text()"/>
		<xsl:variable name="bp15" select="exsl:node-set($b)/*[1]/*[1]/*[5]/text()"/>
		<xsl:variable name="bp16" select="exsl:node-set($b)/*[1]/*[1]/*[6]/text()"/>
		<xsl:variable name="bp17" select="exsl:node-set($b)/*[1]/*[1]/*[7]/text()"/>
		<xsl:variable name="bp18" select="exsl:node-set($b)/*[1]/*[1]/*[8]/text()"/>
		<xsl:variable name="bp21" select="exsl:node-set($b)/*[1]/*[2]/*[1]/text()"/>
		<xsl:variable name="bp22" select="exsl:node-set($b)/*[1]/*[2]/*[2]/text()"/>
		<xsl:variable name="bp23" select="exsl:node-set($b)/*[1]/*[2]/*[3]/text()"/>
		<xsl:variable name="bp24" select="exsl:node-set($b)/*[1]/*[2]/*[4]/text()"/>
		<xsl:variable name="bp25" select="exsl:node-set($b)/*[1]/*[2]/*[5]/text()"/>
		<xsl:variable name="bp26" select="exsl:node-set($b)/*[1]/*[2]/*[6]/text()"/>
		<xsl:variable name="bp27" select="exsl:node-set($b)/*[1]/*[2]/*[7]/text()"/>
		<xsl:variable name="bp28" select="exsl:node-set($b)/*[1]/*[2]/*[8]/text()"/>
		<xsl:variable name="bp31" select="exsl:node-set($b)/*[1]/*[3]/*[1]/text()"/>
		<xsl:variable name="bp32" select="exsl:node-set($b)/*[1]/*[3]/*[2]/text()"/>
		<xsl:variable name="bp33" select="exsl:node-set($b)/*[1]/*[3]/*[3]/text()"/>
		<xsl:variable name="bp34" select="exsl:node-set($b)/*[1]/*[3]/*[4]/text()"/>
		<xsl:variable name="bp35" select="exsl:node-set($b)/*[1]/*[3]/*[5]/text()"/>
		<xsl:variable name="bp36" select="exsl:node-set($b)/*[1]/*[3]/*[6]/text()"/>
		<xsl:variable name="bp37" select="exsl:node-set($b)/*[1]/*[3]/*[7]/text()"/>
		<xsl:variable name="bp38" select="exsl:node-set($b)/*[1]/*[3]/*[8]/text()"/>
		<xsl:variable name="bp41" select="exsl:node-set($b)/*[1]/*[4]/*[1]/text()"/>
		<xsl:variable name="bp42" select="exsl:node-set($b)/*[1]/*[4]/*[2]/text()"/>
		<xsl:variable name="bp43" select="exsl:node-set($b)/*[1]/*[4]/*[3]/text()"/>
		<xsl:variable name="bp44" select="exsl:node-set($b)/*[1]/*[4]/*[4]/text()"/>
		<xsl:variable name="bp45" select="exsl:node-set($b)/*[1]/*[4]/*[5]/text()"/>
		<xsl:variable name="bp46" select="exsl:node-set($b)/*[1]/*[4]/*[6]/text()"/>
		<xsl:variable name="bp47" select="exsl:node-set($b)/*[1]/*[4]/*[7]/text()"/>
		<xsl:variable name="bp48" select="exsl:node-set($b)/*[1]/*[4]/*[8]/text()"/>
		<xsl:variable name="bp51" select="exsl:node-set($b)/*[1]/*[5]/*[1]/text()"/>
		<xsl:variable name="bp52" select="exsl:node-set($b)/*[1]/*[5]/*[2]/text()"/>
		<xsl:variable name="bp53" select="exsl:node-set($b)/*[1]/*[5]/*[3]/text()"/>
		<xsl:variable name="bp54" select="exsl:node-set($b)/*[1]/*[5]/*[4]/text()"/>
		<xsl:variable name="bp55" select="exsl:node-set($b)/*[1]/*[5]/*[5]/text()"/>
		<xsl:variable name="bp56" select="exsl:node-set($b)/*[1]/*[5]/*[6]/text()"/>
		<xsl:variable name="bp57" select="exsl:node-set($b)/*[1]/*[5]/*[7]/text()"/>
		<xsl:variable name="bp58" select="exsl:node-set($b)/*[1]/*[5]/*[8]/text()"/>
		<xsl:variable name="bp61" select="exsl:node-set($b)/*[1]/*[6]/*[1]/text()"/>
		<xsl:variable name="bp62" select="exsl:node-set($b)/*[1]/*[6]/*[2]/text()"/>
		<xsl:variable name="bp63" select="exsl:node-set($b)/*[1]/*[6]/*[3]/text()"/>
		<xsl:variable name="bp64" select="exsl:node-set($b)/*[1]/*[6]/*[4]/text()"/>
		<xsl:variable name="bp65" select="exsl:node-set($b)/*[1]/*[6]/*[5]/text()"/>
		<xsl:variable name="bp66" select="exsl:node-set($b)/*[1]/*[6]/*[6]/text()"/>
		<xsl:variable name="bp67" select="exsl:node-set($b)/*[1]/*[6]/*[7]/text()"/>
		<xsl:variable name="bp68" select="exsl:node-set($b)/*[1]/*[6]/*[8]/text()"/>
		<xsl:variable name="bp71" select="exsl:node-set($b)/*[1]/*[7]/*[1]/text()"/>
		<xsl:variable name="bp72" select="exsl:node-set($b)/*[1]/*[7]/*[2]/text()"/>
		<xsl:variable name="bp73" select="exsl:node-set($b)/*[1]/*[7]/*[3]/text()"/>
		<xsl:variable name="bp74" select="exsl:node-set($b)/*[1]/*[7]/*[4]/text()"/>
		<xsl:variable name="bp75" select="exsl:node-set($b)/*[1]/*[7]/*[5]/text()"/>
		<xsl:variable name="bp76" select="exsl:node-set($b)/*[1]/*[7]/*[6]/text()"/>
		<xsl:variable name="bp77" select="exsl:node-set($b)/*[1]/*[7]/*[7]/text()"/>
		<xsl:variable name="bp78" select="exsl:node-set($b)/*[1]/*[7]/*[8]/text()"/>
		<xsl:variable name="bp81" select="exsl:node-set($b)/*[1]/*[8]/*[1]/text()"/>
		<xsl:variable name="bp82" select="exsl:node-set($b)/*[1]/*[8]/*[2]/text()"/>
		<xsl:variable name="bp83" select="exsl:node-set($b)/*[1]/*[8]/*[3]/text()"/>
		<xsl:variable name="bp84" select="exsl:node-set($b)/*[1]/*[8]/*[4]/text()"/>
		<xsl:variable name="bp85" select="exsl:node-set($b)/*[1]/*[8]/*[5]/text()"/>
		<xsl:variable name="bp86" select="exsl:node-set($b)/*[1]/*[8]/*[6]/text()"/>
		<xsl:variable name="bp87" select="exsl:node-set($b)/*[1]/*[8]/*[7]/text()"/>
		<xsl:variable name="bp88" select="exsl:node-set($b)/*[1]/*[8]/*[8]/text()"/>
		<matrix>
			<row>
				<el><xsl:value-of select="$ap11 * $bp11 + $ap12 * $bp21 + $ap13 * $bp31 + $ap14 * $bp41 + $ap15 * $bp51 + $ap16 * $bp61 + $ap17 * $bp71 + $ap18 * $bp81"/></el>
				<el><xsl:value-of select="$ap11 * $bp12 + $ap12 * $bp22 + $ap13 * $bp32 + $ap14 * $bp42 + $ap15 * $bp52 + $ap16 * $bp62 + $ap17 * $bp72 + $ap18 * $bp82"/></el>
				<el><xsl:value-of select="$ap11 * $bp13 + $ap12 * $bp23 + $ap13 * $bp33 + $ap14 * $bp43 + $ap15 * $bp53 + $ap16 * $bp63 + $ap17 * $bp73 + $ap18 * $bp83"/></el>
				<el><xsl:value-of select="$ap11 * $bp14 + $ap12 * $bp24 + $ap13 * $bp34 + $ap14 * $bp44 + $ap15 * $bp54 + $ap16 * $bp64 + $ap17 * $bp74 + $ap18 * $bp84"/></el>
				<el><xsl:value-of select="$ap11 * $bp15 + $ap12 * $bp25 + $ap13 * $bp35 + $ap14 * $bp45 + $ap15 * $bp55 + $ap16 * $bp65 + $ap17 * $bp75 + $ap18 * $bp85"/></el>
				<el><xsl:value-of select="$ap11 * $bp16 + $ap12 * $bp26 + $ap13 * $bp36 + $ap14 * $bp46 + $ap15 * $bp56 + $ap16 * $bp66 + $ap17 * $bp76 + $ap18 * $bp86"/></el>
				<el><xsl:value-of select="$ap11 * $bp17 + $ap12 * $bp27 + $ap13 * $bp37 + $ap14 * $bp47 + $ap15 * $bp57 + $ap16 * $bp67 + $ap17 * $bp77 + $ap18 * $bp87"/></el>
				<el><xsl:value-of select="$ap11 * $bp18 + $ap12 * $bp28 + $ap13 * $bp38 + $ap14 * $bp48 + $ap15 * $bp58 + $ap16 * $bp68 + $ap17 * $bp78 + $ap18 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap21 * $bp11 + $ap22 * $bp21 + $ap23 * $bp31 + $ap24 * $bp41 + $ap25 * $bp51 + $ap26 * $bp61 + $ap27 * $bp71 + $ap28 * $bp81"/></el>
				<el><xsl:value-of select="$ap21 * $bp12 + $ap22 * $bp22 + $ap23 * $bp32 + $ap24 * $bp42 + $ap25 * $bp52 + $ap26 * $bp62 + $ap27 * $bp72 + $ap28 * $bp82"/></el>
				<el><xsl:value-of select="$ap21 * $bp13 + $ap22 * $bp23 + $ap23 * $bp33 + $ap24 * $bp43 + $ap25 * $bp53 + $ap26 * $bp63 + $ap27 * $bp73 + $ap28 * $bp83"/></el>
				<el><xsl:value-of select="$ap21 * $bp14 + $ap22 * $bp24 + $ap23 * $bp34 + $ap24 * $bp44 + $ap25 * $bp54 + $ap26 * $bp64 + $ap27 * $bp74 + $ap28 * $bp84"/></el>
				<el><xsl:value-of select="$ap21 * $bp15 + $ap22 * $bp25 + $ap23 * $bp35 + $ap24 * $bp45 + $ap25 * $bp55 + $ap26 * $bp65 + $ap27 * $bp75 + $ap28 * $bp85"/></el>
				<el><xsl:value-of select="$ap21 * $bp16 + $ap22 * $bp26 + $ap23 * $bp36 + $ap24 * $bp46 + $ap25 * $bp56 + $ap26 * $bp66 + $ap27 * $bp76 + $ap28 * $bp86"/></el>
				<el><xsl:value-of select="$ap21 * $bp17 + $ap22 * $bp27 + $ap23 * $bp37 + $ap24 * $bp47 + $ap25 * $bp57 + $ap26 * $bp67 + $ap27 * $bp77 + $ap28 * $bp87"/></el>
				<el><xsl:value-of select="$ap21 * $bp18 + $ap22 * $bp28 + $ap23 * $bp38 + $ap24 * $bp48 + $ap25 * $bp58 + $ap26 * $bp68 + $ap27 * $bp78 + $ap28 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap31 * $bp11 + $ap32 * $bp21 + $ap33 * $bp31 + $ap34 * $bp41 + $ap35 * $bp51 + $ap36 * $bp61 + $ap37 * $bp71 + $ap38 * $bp81"/></el>
				<el><xsl:value-of select="$ap31 * $bp12 + $ap32 * $bp22 + $ap33 * $bp32 + $ap34 * $bp42 + $ap35 * $bp52 + $ap36 * $bp62 + $ap37 * $bp72 + $ap38 * $bp82"/></el>
				<el><xsl:value-of select="$ap31 * $bp13 + $ap32 * $bp23 + $ap33 * $bp33 + $ap34 * $bp43 + $ap35 * $bp53 + $ap36 * $bp63 + $ap37 * $bp73 + $ap38 * $bp83"/></el>
				<el><xsl:value-of select="$ap31 * $bp14 + $ap32 * $bp24 + $ap33 * $bp34 + $ap34 * $bp44 + $ap35 * $bp54 + $ap36 * $bp64 + $ap37 * $bp74 + $ap38 * $bp84"/></el>
				<el><xsl:value-of select="$ap31 * $bp15 + $ap32 * $bp25 + $ap33 * $bp35 + $ap34 * $bp45 + $ap35 * $bp55 + $ap36 * $bp65 + $ap37 * $bp75 + $ap38 * $bp85"/></el>
				<el><xsl:value-of select="$ap31 * $bp16 + $ap32 * $bp26 + $ap33 * $bp36 + $ap34 * $bp46 + $ap35 * $bp56 + $ap36 * $bp66 + $ap37 * $bp76 + $ap38 * $bp86"/></el>
				<el><xsl:value-of select="$ap31 * $bp17 + $ap32 * $bp27 + $ap33 * $bp37 + $ap34 * $bp47 + $ap35 * $bp57 + $ap36 * $bp67 + $ap37 * $bp77 + $ap38 * $bp87"/></el>
				<el><xsl:value-of select="$ap31 * $bp18 + $ap32 * $bp28 + $ap33 * $bp38 + $ap34 * $bp48 + $ap35 * $bp58 + $ap36 * $bp68 + $ap37 * $bp78 + $ap38 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap41 * $bp11 + $ap42 * $bp21 + $ap43 * $bp31 + $ap44 * $bp41 + $ap45 * $bp51 + $ap46 * $bp61 + $ap47 * $bp71 + $ap48 * $bp81"/></el>
				<el><xsl:value-of select="$ap41 * $bp12 + $ap42 * $bp22 + $ap43 * $bp32 + $ap44 * $bp42 + $ap45 * $bp52 + $ap46 * $bp62 + $ap47 * $bp72 + $ap48 * $bp82"/></el>
				<el><xsl:value-of select="$ap41 * $bp13 + $ap42 * $bp23 + $ap43 * $bp33 + $ap44 * $bp43 + $ap45 * $bp53 + $ap46 * $bp63 + $ap47 * $bp73 + $ap48 * $bp83"/></el>
				<el><xsl:value-of select="$ap41 * $bp14 + $ap42 * $bp24 + $ap43 * $bp34 + $ap44 * $bp44 + $ap45 * $bp54 + $ap46 * $bp64 + $ap47 * $bp74 + $ap48 * $bp84"/></el>
				<el><xsl:value-of select="$ap41 * $bp15 + $ap42 * $bp25 + $ap43 * $bp35 + $ap44 * $bp45 + $ap45 * $bp55 + $ap46 * $bp65 + $ap47 * $bp75 + $ap48 * $bp85"/></el>
				<el><xsl:value-of select="$ap41 * $bp16 + $ap42 * $bp26 + $ap43 * $bp36 + $ap44 * $bp46 + $ap45 * $bp56 + $ap46 * $bp66 + $ap47 * $bp76 + $ap48 * $bp86"/></el>
				<el><xsl:value-of select="$ap41 * $bp17 + $ap42 * $bp27 + $ap43 * $bp37 + $ap44 * $bp47 + $ap45 * $bp57 + $ap46 * $bp67 + $ap47 * $bp77 + $ap48 * $bp87"/></el>
				<el><xsl:value-of select="$ap41 * $bp18 + $ap42 * $bp28 + $ap43 * $bp38 + $ap44 * $bp48 + $ap45 * $bp58 + $ap46 * $bp68 + $ap47 * $bp78 + $ap48 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap51 * $bp11 + $ap52 * $bp21 + $ap53 * $bp31 + $ap54 * $bp41 + $ap55 * $bp51 + $ap56 * $bp61 + $ap57 * $bp71 + $ap58 * $bp81"/></el>
				<el><xsl:value-of select="$ap51 * $bp12 + $ap52 * $bp22 + $ap53 * $bp32 + $ap54 * $bp42 + $ap55 * $bp52 + $ap56 * $bp62 + $ap57 * $bp72 + $ap58 * $bp82"/></el>
				<el><xsl:value-of select="$ap51 * $bp13 + $ap52 * $bp23 + $ap53 * $bp33 + $ap54 * $bp43 + $ap55 * $bp53 + $ap56 * $bp63 + $ap57 * $bp73 + $ap58 * $bp83"/></el>
				<el><xsl:value-of select="$ap51 * $bp14 + $ap52 * $bp24 + $ap53 * $bp34 + $ap54 * $bp44 + $ap55 * $bp54 + $ap56 * $bp64 + $ap57 * $bp74 + $ap58 * $bp84"/></el>
				<el><xsl:value-of select="$ap51 * $bp15 + $ap52 * $bp25 + $ap53 * $bp35 + $ap54 * $bp45 + $ap55 * $bp55 + $ap56 * $bp65 + $ap57 * $bp75 + $ap58 * $bp85"/></el>
				<el><xsl:value-of select="$ap51 * $bp16 + $ap52 * $bp26 + $ap53 * $bp36 + $ap54 * $bp46 + $ap55 * $bp56 + $ap56 * $bp66 + $ap57 * $bp76 + $ap58 * $bp86"/></el>
				<el><xsl:value-of select="$ap51 * $bp17 + $ap52 * $bp27 + $ap53 * $bp37 + $ap54 * $bp47 + $ap55 * $bp57 + $ap56 * $bp67 + $ap57 * $bp77 + $ap58 * $bp87"/></el>
				<el><xsl:value-of select="$ap51 * $bp18 + $ap52 * $bp28 + $ap53 * $bp38 + $ap54 * $bp48 + $ap55 * $bp58 + $ap56 * $bp68 + $ap57 * $bp78 + $ap58 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap61 * $bp11 + $ap62 * $bp21 + $ap63 * $bp31 + $ap64 * $bp41 + $ap65 * $bp51 + $ap66 * $bp61 + $ap67 * $bp71 + $ap68 * $bp81"/></el>
				<el><xsl:value-of select="$ap61 * $bp12 + $ap62 * $bp22 + $ap63 * $bp32 + $ap64 * $bp42 + $ap65 * $bp52 + $ap66 * $bp62 + $ap67 * $bp72 + $ap68 * $bp82"/></el>
				<el><xsl:value-of select="$ap61 * $bp13 + $ap62 * $bp23 + $ap63 * $bp33 + $ap64 * $bp43 + $ap65 * $bp53 + $ap66 * $bp63 + $ap67 * $bp73 + $ap68 * $bp83"/></el>
				<el><xsl:value-of select="$ap61 * $bp14 + $ap62 * $bp24 + $ap63 * $bp34 + $ap64 * $bp44 + $ap65 * $bp54 + $ap66 * $bp64 + $ap67 * $bp74 + $ap68 * $bp84"/></el>
				<el><xsl:value-of select="$ap61 * $bp15 + $ap62 * $bp25 + $ap63 * $bp35 + $ap64 * $bp45 + $ap65 * $bp55 + $ap66 * $bp65 + $ap67 * $bp75 + $ap68 * $bp85"/></el>
				<el><xsl:value-of select="$ap61 * $bp16 + $ap62 * $bp26 + $ap63 * $bp36 + $ap64 * $bp46 + $ap65 * $bp56 + $ap66 * $bp66 + $ap67 * $bp76 + $ap68 * $bp86"/></el>
				<el><xsl:value-of select="$ap61 * $bp17 + $ap62 * $bp27 + $ap63 * $bp37 + $ap64 * $bp47 + $ap65 * $bp57 + $ap66 * $bp67 + $ap67 * $bp77 + $ap68 * $bp87"/></el>
				<el><xsl:value-of select="$ap61 * $bp18 + $ap62 * $bp28 + $ap63 * $bp38 + $ap64 * $bp48 + $ap65 * $bp58 + $ap66 * $bp68 + $ap67 * $bp78 + $ap68 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap71 * $bp11 + $ap72 * $bp21 + $ap73 * $bp31 + $ap74 * $bp41 + $ap75 * $bp51 + $ap76 * $bp61 + $ap77 * $bp71 + $ap78 * $bp81"/></el>
				<el><xsl:value-of select="$ap71 * $bp12 + $ap72 * $bp22 + $ap73 * $bp32 + $ap74 * $bp42 + $ap75 * $bp52 + $ap76 * $bp62 + $ap77 * $bp72 + $ap78 * $bp82"/></el>
				<el><xsl:value-of select="$ap71 * $bp13 + $ap72 * $bp23 + $ap73 * $bp33 + $ap74 * $bp43 + $ap75 * $bp53 + $ap76 * $bp63 + $ap77 * $bp73 + $ap78 * $bp83"/></el>
				<el><xsl:value-of select="$ap71 * $bp14 + $ap72 * $bp24 + $ap73 * $bp34 + $ap74 * $bp44 + $ap75 * $bp54 + $ap76 * $bp64 + $ap77 * $bp74 + $ap78 * $bp84"/></el>
				<el><xsl:value-of select="$ap71 * $bp15 + $ap72 * $bp25 + $ap73 * $bp35 + $ap74 * $bp45 + $ap75 * $bp55 + $ap76 * $bp65 + $ap77 * $bp75 + $ap78 * $bp85"/></el>
				<el><xsl:value-of select="$ap71 * $bp16 + $ap72 * $bp26 + $ap73 * $bp36 + $ap74 * $bp46 + $ap75 * $bp56 + $ap76 * $bp66 + $ap77 * $bp76 + $ap78 * $bp86"/></el>
				<el><xsl:value-of select="$ap71 * $bp17 + $ap72 * $bp27 + $ap73 * $bp37 + $ap74 * $bp47 + $ap75 * $bp57 + $ap76 * $bp67 + $ap77 * $bp77 + $ap78 * $bp87"/></el>
				<el><xsl:value-of select="$ap71 * $bp18 + $ap72 * $bp28 + $ap73 * $bp38 + $ap74 * $bp48 + $ap75 * $bp58 + $ap76 * $bp68 + $ap77 * $bp78 + $ap78 * $bp88"/></el>
			</row>
			<row>
				<el><xsl:value-of select="$ap81 * $bp11 + $ap82 * $bp21 + $ap83 * $bp31 + $ap84 * $bp41 + $ap85 * $bp51 + $ap86 * $bp61 + $ap87 * $bp71 + $ap88 * $bp81"/></el>
				<el><xsl:value-of select="$ap81 * $bp12 + $ap82 * $bp22 + $ap83 * $bp32 + $ap84 * $bp42 + $ap85 * $bp52 + $ap86 * $bp62 + $ap87 * $bp72 + $ap88 * $bp82"/></el>
				<el><xsl:value-of select="$ap81 * $bp13 + $ap82 * $bp23 + $ap83 * $bp33 + $ap84 * $bp43 + $ap85 * $bp53 + $ap86 * $bp63 + $ap87 * $bp73 + $ap88 * $bp83"/></el>
				<el><xsl:value-of select="$ap81 * $bp14 + $ap82 * $bp24 + $ap83 * $bp34 + $ap84 * $bp44 + $ap85 * $bp54 + $ap86 * $bp64 + $ap87 * $bp74 + $ap88 * $bp84"/></el>
				<el><xsl:value-of select="$ap81 * $bp15 + $ap82 * $bp25 + $ap83 * $bp35 + $ap84 * $bp45 + $ap85 * $bp55 + $ap86 * $bp65 + $ap87 * $bp75 + $ap88 * $bp85"/></el>
				<el><xsl:value-of select="$ap81 * $bp16 + $ap82 * $bp26 + $ap83 * $bp36 + $ap84 * $bp46 + $ap85 * $bp56 + $ap86 * $bp66 + $ap87 * $bp76 + $ap88 * $bp86"/></el>
				<el><xsl:value-of select="$ap81 * $bp17 + $ap82 * $bp27 + $ap83 * $bp37 + $ap84 * $bp47 + $ap85 * $bp57 + $ap86 * $bp67 + $ap87 * $bp77 + $ap88 * $bp87"/></el>
				<el><xsl:value-of select="$ap81 * $bp18 + $ap82 * $bp28 + $ap83 * $bp38 + $ap84 * $bp48 + $ap85 * $bp58 + $ap86 * $bp68 + $ap87 * $bp78 + $ap88 * $bp88"/></el>
			</row>
		</matrix>
	</xsl:template>
	<xsl:template name="perspectiveTransform">
		<xsl:param name="srcPts"/>
		<xsl:param name="dstPts"/>
		<xsl:variable name="sp1x" select="exsl:node-set($srcPts)/*[1]/@x"/>
		<xsl:variable name="sp1y" select="exsl:node-set($srcPts)/*[1]/@y"/>
		<xsl:variable name="sp2x" select="exsl:node-set($srcPts)/*[2]/@x"/>
		<xsl:variable name="sp2y" select="exsl:node-set($srcPts)/*[2]/@y"/>
		<xsl:variable name="sp3x" select="exsl:node-set($srcPts)/*[3]/@x"/>
		<xsl:variable name="sp3y" select="exsl:node-set($srcPts)/*[3]/@y"/>
		<xsl:variable name="sp4x" select="exsl:node-set($srcPts)/*[4]/@x"/>
		<xsl:variable name="sp4y" select="exsl:node-set($srcPts)/*[4]/@y"/>
		<xsl:variable name="dp1x" select="exsl:node-set($dstPts)/*[1]/@x"/>
		<xsl:variable name="dp1y" select="exsl:node-set($dstPts)/*[1]/@y"/>
		<xsl:variable name="dp2x" select="exsl:node-set($dstPts)/*[2]/@x"/>
		<xsl:variable name="dp2y" select="exsl:node-set($dstPts)/*[2]/@y"/>
		<xsl:variable name="dp3x" select="exsl:node-set($dstPts)/*[3]/@x"/>
		<xsl:variable name="dp3y" select="exsl:node-set($dstPts)/*[3]/@y"/>
		<xsl:variable name="dp4x" select="exsl:node-set($dstPts)/*[4]/@x"/>
		<xsl:variable name="dp4y" select="exsl:node-set($dstPts)/*[4]/@y"/>
		<xsl:variable name="matA">
			<matrix>
				<row>
					<el><xsl:value-of select="$sp1x"/></el>
					<el><xsl:value-of select="$sp1y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp1x * $sp1x"/></el>
					<el><xsl:value-of select="-1 * $dp1x * $sp1y"/></el>
				</row>
				<row>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="$sp1x"/></el>
					<el><xsl:value-of select="$sp1y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp1y * $sp1x"/></el>
					<el><xsl:value-of select="-1 * $dp1y * $sp1y"/></el>
				</row>
				<row>
					<el><xsl:value-of select="$sp2x"/></el>
					<el><xsl:value-of select="$sp2y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp2x * $sp2x"/></el>
					<el><xsl:value-of select="-1 * $dp2x * $sp2y"/></el>
				</row>
				<row>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="$sp2x"/></el>
					<el><xsl:value-of select="$sp2y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp2y * $sp2x"/></el>
					<el><xsl:value-of select="-1 * $dp2y * $sp2y"/></el>
				</row>
				<row>
					<el><xsl:value-of select="$sp3x"/></el>
					<el><xsl:value-of select="$sp3y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp3x * $sp3x"/></el>
					<el><xsl:value-of select="-1 * $dp3x * $sp3y"/></el>
				</row>
				<row>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="$sp3x"/></el>
					<el><xsl:value-of select="$sp3y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp3y * $sp3x"/></el>
					<el><xsl:value-of select="-1 * $dp3y * $sp3y"/></el>
				</row>
				<row>
					<el><xsl:value-of select="$sp4x"/></el>
					<el><xsl:value-of select="$sp4y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp4x * $sp4x"/></el>
					<el><xsl:value-of select="-1 * $dp4x * $sp4y"/></el>
				</row>
				<row>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:text>0</xsl:text></el>
					<el><xsl:value-of select="$sp4x"/></el>
					<el><xsl:value-of select="$sp4y"/></el>
					<el><xsl:text>1</xsl:text></el>
					<el><xsl:value-of select="-1 * $dp4y * $sp4x"/></el>
					<el><xsl:value-of select="-1 * $dp4y * $sp4y"/></el>
				</row>
			</matrix>
		</xsl:variable>
		<xsl:variable name="matB">
			<matrix>
				<row><el><xsl:value-of select="$dp1x"/></el></row>
				<row><el><xsl:value-of select="$dp1y"/></el></row>
				<row><el><xsl:value-of select="$dp2x"/></el></row>
				<row><el><xsl:value-of select="$dp2y"/></el></row>
				<row><el><xsl:value-of select="$dp3x"/></el></row>
				<row><el><xsl:value-of select="$dp3y"/></el></row>
				<row><el><xsl:value-of select="$dp4x"/></el></row>
				<row><el><xsl:value-of select="$dp4y"/></el></row>
			</matrix>
		</xsl:variable>
		<xsl:variable name="matATranspose">
			<xsl:call-template name="MTranspose8x8">
				<xsl:with-param name="matrix" select="$matA"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="matATMulA">
			<xsl:call-template name="MMul8x8">
				<xsl:with-param name="a" select="$matATranspose"/>
				<xsl:with-param name="b" select="$matA"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="matC">
			<xsl:call-template name="MInverse8x8">
				<xsl:with-param name="matrix" select="$matATMulA"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="matD">
			<xsl:call-template name="MMul8x8">
				<xsl:with-param name="a" select="$matC"/>
				<xsl:with-param name="b" select="$matATranspose"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="matX">
			<xsl:call-template name="MMulV">
				<xsl:with-param name="matrix" select="$matD"/>
				<xsl:with-param name="vector" select="$matB"/>
			</xsl:call-template>
		</xsl:variable>
		<transform>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[1]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[2]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[3]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[4]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[5]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[6]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[7]/*[1]"/>
			</el>
			<el>
				<xsl:value-of select="exsl:node-set($matX)/*[1]/*[8]/*[1]"/>
			</el>
		</transform>
	</xsl:template>
	<!-- Template to multiply an 8-dimension matrix by a vector -->
	<xsl:template name="MMulV">
		<xsl:param name="matrix"/>
		<xsl:param name="vector"/>
		<matrix>
			<xsl:for-each select="exsl:node-set($matrix)/*[1]/*">
				<row>
					<xsl:variable name="products">
						<xsl:for-each select="./*">
							<xsl:variable name="pos" select="position()"/>
							<el>
								<xsl:value-of select="text() * exsl:node-set($vector)/*[1]/*[$pos]/*[1]/text()"/>
							</el>
						</xsl:for-each>
					</xsl:variable>
					<el>
						<xsl:value-of select="sum(exsl:node-set($products)/*)"/>
					</el>
				</row>
			</xsl:for-each>
		</matrix>
	</xsl:template>
	<xsl:template name="MTranspose4x4">
		<xsl:param name="matrix"/>
		<matrix>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[1]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[2]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[3]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[4]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
		</matrix>
	</xsl:template>
	<xsl:template name="MTranspose8x8">
		<xsl:param name="matrix"/>
		<matrix>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[1]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[2]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[3]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[4]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[5]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[6]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[7]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
			<row>
				<xsl:for-each select="exsl:node-set($matrix)/*[1]/*/*[8]">
					<xsl:copy>
						<xsl:value-of select="."/>
					</xsl:copy>
				</xsl:for-each>
			</row>
		</matrix>
	</xsl:template>
	<!-- Template for calculating the inverse of a matrix -->
	<xsl:template name="MInverse8x8">
		<xsl:param name="matrix"/>
		<xsl:variable name="rows" select="count(exsl:node-set($matrix)/*[1]/*)"/>
		<xsl:choose>
			<xsl:when test="$rows = 1">
				<matrix>
					<row>
						<el>
							<xsl:value-of select="exsl:node-set($matrix)/*[1]/*[1]/*[1]"/>
						</el>
					</row>
				</matrix>
			</xsl:when>
			<xsl:otherwise>
				<!-- Find the matrix's determinant -->
				<xsl:variable name="determinant">
					<xsl:call-template name="MDeterminant">
						<xsl:with-param name="matrix" select="$matrix"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Find the matrix's adjoint -->
				<xsl:variable name="adjoint">
					<xsl:call-template name="MAdjoint8x8">
						<xsl:with-param name="matrix" select="$matrix"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Scale the adjoint by 1 / det -->
				<xsl:call-template name="MScale">
					<xsl:with-param name="matrix" select="exsl:node-set($adjoint)"/>
					<xsl:with-param name="scale" select="1 div $determinant"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Template to scale the values of a matrix by a single value -->
	<xsl:template name="MScale">
		<xsl:param name="matrix"/>
		<xsl:param name="scale" select="1"/>
		<matrix>
			<xsl:for-each select="exsl:node-set($matrix)/*[1]/*">
				<row>
					<xsl:for-each select="./*">
						<el>
							<xsl:value-of select="text() * $scale"/>
						</el>
					</xsl:for-each>
				</row>
			</xsl:for-each>
		</matrix>
	</xsl:template>
	<!-- Template to calculate the adjoint of a matrix -->
	<xsl:template name="MAdjoint8x8">
		<xsl:param name="matrix"/>
		<!-- Calculate the cofactors of this matrix -->
		<xsl:variable name="cofactors">
			<xsl:call-template name="MCofactors">
				<xsl:with-param name="matrix" select="$matrix"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- Transpose the matrix -->
		<xsl:call-template name="MTranspose8x8">
			<xsl:with-param name="matrix" select="exsl:node-set($cofactors)"/>
		</xsl:call-template>
	</xsl:template>
	<!-- Template to calculate the cofactors of a matrix -->
	<xsl:template name="MCofactors">
		<xsl:param name="matrix"/>
		<xsl:variable name="minors">
			<xsl:call-template name="MMinors">
				<xsl:with-param name="matrix" select="$matrix"/>
			</xsl:call-template>
		</xsl:variable>
		<matrix>
			<xsl:for-each select="exsl:node-set($minors)/*[1]/*">
				<row>
					<xsl:variable name="ipos" select="(position() - 1)"/>
					<xsl:for-each select="./*">
						<xsl:variable name="pos" select="(position() - 1) + $ipos"/>
						<el>
							<xsl:value-of select="text() * math:power(-1, $pos)"/>
						</el>
					</xsl:for-each>
				</row>
			</xsl:for-each>
		</matrix>
	</xsl:template>
	<!-- Template to calculate the minors of a matrix -->
	<xsl:template name="MMinors">
		<xsl:param name="matrix"/>
		<matrix>
			<!-- Start recursion of rows on row 1 -->
			<xsl:call-template name="MMinorsRow">
				<xsl:with-param name="matrix" select="$matrix"/>
			</xsl:call-template>
		</matrix>
	</xsl:template>
	<xsl:template name="MMinorsRow">
		<xsl:param name="matrix"/>
		<xsl:param name="i" select="1"/>
		<xsl:variable name="rows" select="count(exsl:node-set($matrix)/*[1]/*)"/>
		<!-- Only recurse if we haven't reached the end condition -->
		<xsl:if test="$i &lt;= $rows">
			<!-- Make a row -->
			<row>
				<!-- Fill the row with elements -->
				<xsl:call-template name="MMinorsEl">
					<xsl:with-param name="matrix" select="$matrix"/>
					<xsl:with-param name="i" select="$i"/>
					<xsl:with-param name="j" select="1"/>
				</xsl:call-template>
			</row>
			<!-- Make the next rows -->
			<xsl:call-template name="MMinorsRow">
				<xsl:with-param name="matrix" select="$matrix"/>
				<xsl:with-param name="i" select="$i + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="MMinorsEl">
		<xsl:param name="matrix"/>
		<xsl:param name="i" select="1"/>
		<xsl:param name="j" select="1"/>
		<xsl:variable name="rows" select="count(exsl:node-set($matrix)/*[1]/*)"/>
		<!-- Only recurse if we haven't reached the end condition -->
		<xsl:if test="$i &lt;= $rows and $j &lt;= $rows">
			<!-- Make a segment of elements with pos > [i, j] -->
			<xsl:variable name="seg">
				<matrix>
					<xsl:for-each select="exsl:node-set($matrix)/*[1]/*[position() != $i]">
						<row>
							<xsl:for-each select="./*[position() != $j]">
								<el><xsl:value-of select="."/></el>
							</xsl:for-each>
						</row>
					</xsl:for-each>
				</matrix>
			</xsl:variable>
			<!-- Find the determinant of this segment -->
			<el>
				<xsl:call-template name="MDeterminant">
					<xsl:with-param name="matrix" select="exsl:node-set($seg)"/>
				</xsl:call-template>
			</el>
			<!-- Make the next elements -->
			<xsl:call-template name="MMinorsEl">
				<xsl:with-param name="matrix" select="$matrix"/>
				<xsl:with-param name="i" select="$i"/>
				<xsl:with-param name="j" select="$j + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- Template for calculating the determinant of a matrix using Laplace expansion along the top row recursively -->
	<xsl:template name="MDeterminant">
		<!-- Assumes as square matrix. Non-square matrices determinants are undefined behaviour -->
		<xsl:param name="matrix"/>
		<xsl:variable name="rows" select="count(exsl:node-set($matrix)/*[1]/*)"/>
		<xsl:choose>
			<xsl:when test="$rows = 1">
				<!-- One row, just take the value of that row -->
				<xsl:value-of select="exsl:node-set($matrix)/*[1]/*[1]/*[1]/text()"/>
			</xsl:when>
			<xsl:when test="$rows = 2">
				<!-- Two rows, ab - cd -->
				<xsl:value-of select="exsl:node-set($matrix)/*[1]/*[1]/*[1]/text() * exsl:node-set($matrix)/*[1]/*[2]/*[2]/text() - exsl:node-set($matrix)/*[1]/*[1]/*[2]/text() * exsl:node-set($matrix)/*[1]/*[2]/*[1]/text()"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- More than 2 rows, expand -->
				<xsl:variable name="rawRecDet">
					<!-- Call the recursive template to iterate expansion -->
					<xsl:call-template name="MDeterminantRecursive">
						<xsl:with-param name="matrix" select="$matrix"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Because of the way we add and subtract backwards, we have to change the sign on even-numbered matrices -->
				<xsl:variable name="inverter">
					<xsl:choose>
						<xsl:when test="($rows mod 2) = 0">
							<xsl:text>-1</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>1</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Add the sign -->
				<xsl:value-of select="$rawRecDet * $inverter"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Recursive template, only designed to be called by MDeterminant, that iterates the Laplace expansion -->
	<xsl:template name="MDeterminantRecursive">
		<xsl:param name="matrix"/>
		<!-- Parameter for recursion -->
		<xsl:param name="column" select="1"/>
		<xsl:variable name="rows" select="count(exsl:node-set($matrix)/*[1]/*)"/>
		<xsl:variable name="mul" select="exsl:node-set($matrix)/*[1]/*[1]/*[$column]"/>
		<!-- Calculate the determinant of this segment -->
		<xsl:variable name="segDetCalc">
			<xsl:choose>
				<!-- No point iterating when mul is 0, as we'll just get 0 anyway -->
				<xsl:when test="$mul = 0">
					<xsl:text>0</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<!-- Create a segment of the matrix leaving out this column -->
					<xsl:variable name="seg">
						<matrix>
							<xsl:for-each select="exsl:node-set($matrix)/*[1]/*[position() &gt; 1]">
								<row>
									<xsl:for-each select="./*[position() != $column]">
										<el><xsl:value-of select="."/></el>
									</xsl:for-each>
								</row>
							</xsl:for-each>
						</matrix>
					</xsl:variable>
					<xsl:call-template name="MDeterminant">
						<xsl:with-param name="matrix" select="$seg"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Multiply it by the top row column -->
		<xsl:variable name="segDet" select="$segDetCalc * $mul"/>
		<xsl:choose>
			<xsl:when test="$column &gt;= $rows">
				<!-- Recurse end condition, just this -->
				<xsl:value-of select="$segDet"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- There are more columns to calculate, recursively calculate these -->
				<xsl:variable name="theRest">
					<xsl:call-template name="MDeterminantRecursive">
						<xsl:with-param name="matrix" select="$matrix"/>
						<xsl:with-param name="column" select="$column + 1"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- Alternately add and subtract the current value from the rest based on the column -->
				<xsl:choose>
					<xsl:when test="(($column + $rows) mod 2) = 0">
						<xsl:value-of select="$theRest + $segDet"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$theRest - $segDet"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Identity transform -->
	<xsl:template match="@* | node()" mode="identity">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- Useful serialising function to output a matrix -->
	<xsl:template name="matrix2text">
		<xsl:param name="matrix"/>
		<xsl:text>--- MATRIX ---&#xa;</xsl:text>
		<xsl:for-each select="exsl:node-set($matrix)/*[1]/*">
			<xsl:for-each select="./*">
				<xsl:value-of select="."/>
				<xsl:text> </xsl:text>
			</xsl:for-each>
			<xsl:text>&#xa;</xsl:text>
		</xsl:for-each>
		<xsl:text>--- END MATRIX ---</xsl:text>
	</xsl:template>
	<xsl:template name="matrix2wa">
		<xsl:param name="matrix"/>
		<xsl:text>--- MATRIX ---&#xa;[</xsl:text>
		<xsl:for-each select="exsl:node-set($matrix)/*[1]/*">
			<xsl:text>[</xsl:text>
			<xsl:for-each select="./*">
				<xsl:choose>
					<xsl:when test="text() &lt; 0">
						<xsl:text>-</xsl:text>
						<xsl:number value="0 - text()" format="1"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:number value="text()" format="1"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="position() != last()">
					<xsl:text>,</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>]</xsl:text>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]&#xa;--- END MATRIX ---</xsl:text>
	</xsl:template>
	<!-- Serialise template -->
	<xsl:template match="*" mode="serialise">
		<xsl:text>&lt;</xsl:text>
		<xsl:value-of select="name()"/>
		<xsl:apply-templates select="@*" mode="serialise"/>
		<xsl:choose>
			<xsl:when test="node()">
				<xsl:text>&gt;</xsl:text>
				<xsl:apply-templates mode="serialise"/>
				<xsl:text>&lt;/</xsl:text>
				<xsl:value-of select="name()"/>
				<xsl:text>&gt;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>/&gt;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*" mode="serialise">
		<xsl:text> </xsl:text>
		<xsl:value-of select="name()"/>
		<xsl:text>="</xsl:text>
		<xsl:value-of select="."/>
		<xsl:text>"</xsl:text>
	</xsl:template>
	<xsl:template match="text()" mode="serialise">
		<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>
