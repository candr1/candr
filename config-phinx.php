<?php

$settings = require __DIR__ . '/bootstrap/settings.php';
$conn = $settings['connections'][$settings['default']];
return [
	'paths' => $conn['paths'],
	'migration_base_class' => $conn['migration_base_class'],
	'environments' => [
		'default_database' => 'default',
		'default' => [
			'adapter' => $conn['driver'],
			'name' => $conn['database'],
			'host' => $conn['host'],
			'user' => $conn['username'],
			'pass' => $conn['password'],
			'suffix' => '',
		],
	],
];
