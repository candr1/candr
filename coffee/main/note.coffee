class Note extends window.Candr.ClefLike
	@node = null
	@shift_amount = null
	@plica = null
	@plica_amount = null
	@colour = null
	@after = null
	constructor: (canvas, after, editor, colour = 'rgba(102,153,255,1)') ->
		super canvas, editor, colour
		@in_progress = true
		@plica = null
		@plica_amount = 0
		@shift_amount = 0
		@after = after
		@canvas.fabric.remove @centrepoint.fabric
		@canvas.fabric.remove @rect
		@rect = null
		@colour = colour
		@centrepoint = null
		@node = new window.Candr.Node @colour
		@node.setpos 0, 0
		nodeselectfun = =>
			console.debug 'Note selected'
			for line in @guidelines
				@canvas.fabric.remove line
			@guidelines = []
			ao = @canvas.fabric.getActiveObject()
			if ao and ao.type is 'activeSelection'
				return
			@editor.view_stafflines false
			stafflines = @editor.get_staffline_points()
			midlines = @get_centrelines stafflines
			console.debug 'Staffline points:', lines, midlines
			lines = stafflines.concat midlines
			for line in lines
				for point in line
					point[0] += @editor.offset[0]
					point[1] += @editor.offset[1]
				gl = @make_infinite_guideline line[0], line[1]
				@canvas.fabric.add gl
				@guidelines.push gl
			@guidelines.sort (a, b) ->
				pa = a.originalSegment
				pb = b.originalSegment
				cavy = (p) ->
					avy = 0
					for i in [1...p.length] by 2
						avy += p[i]
					return avy / p.length
				pavy = cavy pa
				pbvy = cavy pb
				return pbvy - pavy
			console.debug 'Made lines:', @guidelines
			@node.fabric.bringToFront()
			@canvas.fabric.renderAll()
			@do_highlight()
		@node.fabric.on 'selected', nodeselectfun
		@node.fabric.on 'deselected', =>
			for line in @guidelines
				@canvas.fabric.remove line
			@guidelines = []
			if @editor.checkboxes.hasOwnProperty 'stafflines'
				vis = @editor.checkboxes['stafflines'].checked
				@editor.view_stafflines vis
		@node.fabric.on 'moving', @do_higlight
		@node.set_dir 0
		@canvas.add @node
		@canvas.fabric.on {
			'mouse:move': @nodemove
			'mouse:down': @nodeset
		}
		@draw_centrepoint_line = undefined
		@centrepoint_visibility = undefined
	do_highlight: =>
		nx = @node.getx() + @offset[0]
		ny = @node.gety() + @offset[1]
		@highlight_closest_guideline nx, ny, 'rgba(0,255,0,1)', @shift_amount
	nodemove: (e) =>
		@node.setpos (e.pointer.x - @offset[0]), (e.pointer.y - @offset[1])
		@do_highlight()
		@canvas.fabric.renderAll()
	nodeset: (e) =>
		@node.setpos (e.pointer.x - @offset[0]), (e.pointer.y - @offset[1])
		@canvas.fabric.off 'mouse:move', @nodemove
		@canvas.fabric.off 'mouse:down', @nodeset
		@node.fabric.on 'moving', =>
			if @plica
				@plica.set {
					left: @node.fabric.left
					top: @node.fabric.top
				}
				@plica.setCoords()
		@in_progress = false
		@canvas.fabric.trigger 'object:modified', {
			target: @node.fabric
		}
		@after()
	cancel_create: ->
		super()
		@canvas.fabric.off 'mouse:move', @nodemove
		@canvas.fabric.off 'mouse:down', @nodeset
		@node.fabric.on 'moving', =>
			if @plica
				@plica.set {
					left: @node.fabric.left
					top: @node.fabric.top
				}
				@plica.setCoords()
		@deselect_fun()
		@in_progress = false
	shift_up: ->
		@shift 1
	shift_down: ->
		@shift -1
	shift: (alter) ->
		if not alter
			return
		console.debug 'Shifting note by: ', alter
		@shift_amount += alter
		dir = @node.get_dir()
		if @shift_amount > 0 and dir isnt 1
			@node.set_dir 1
		else if @shift_amount < 0 and dir isnt -1
			@node.set_dir -1
		else if @shift_amount is 0 and dir isnt 0
			@node.set_dir 0
		nx = @node.getx() + @offset[0]
		ny = @node.gety() + @offset[1]
		if @plica
			@plica.set {
				top: @node.getx()
				left: @note.gety()
			}
			@plica.setCoords()
		@highlight_closest_guideline nx, ny, 'rgba(0,255,0,1)', @shift_amount
		@canvas.fabric.trigger 'object:modified', {target: @node.fabric}
		@canvas.fabric.renderAll()
	plica_up: ->
		@plica_shift 1
	plica_down: ->
		@plica_shift -1
	plica_shift: (alter) ->
		if not alter
			return
		console.debug 'Shifting plica by: ', alter
		@plica_amount += alter
		@canvas.fabric.remove @plica
		colour = 'rgba(0,0,255,0.5)'
		if @plica
			colour = @plica.stroke
		if @plica_amount isnt 0
			@plica = new fabric.Line [0, 0, 0, 20], {
				stroke: colour
				hasBorders: false
				hasControls: false
				selectable: false
				lockMovementX: true
				lockMovementY: true
				lockScalingX: true
				lockScalingY: true
				lockRotation: true
				originX: 'center'
				originY: 'top'
				strokeWidth: 3
			}
		if @plica_amount > 0
			@plica.set 'originY', 'bottom'
		if @plica_amount is 0
			@plica = null
		else
			@plica.set {
				top: (@node.gety() + @offset[1])
				left: (@node.getx() + @offset[0])
			}
			@plica.setCoords()
			@canvas.fabric.add @plica
		@canvas.fabric.trigger 'object:modified', {target: @plica}
		@canvas.fabric.renderAll()
	get_bounds: ->
		@node.get_bounds()
	is_component: (obj) ->
		(@node.is_component obj) or (obj is @plica)
	remove: ->
		console.debug 'Removing note'
		for line in @guidelines
			@canvas.fabric.remove line
		@canvas.fabric.remove @node.fabric
		if @plica
			@canvas.fabric.remove @plica
		return null
	set_coords: ->
		for line in @guidelines
			line.setCoords()
		@node.set_coords()
		if @plica
			@plica.setCoords()
	bringToFront: ->
		@node.fabric.bringToFront()
	visibility: (visible) ->
		@node.visibility visible
		if @plica
			@plica.visible = visible
	set_offset: (x, y) ->
		@offset[0] = x
		@offset[1] = y
		@node.set_offset x, y
		if @plica
			@plica.set {
				top: (@node.gety() + @offset[1])
				left: (@node.getx() + @offset[0])
			}
			@plica.setCoords()
	set_colour: (colour, force) ->
		if colour is @colour and not force
			return false
		console.debug 'Note setting colour:', colour
		@colour = colour
		@node.set_fill @colour
		return true
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: ->
					{
						name: 'Note'
					}
				set: (dat) ->
				type: {
					name: 'label'
				}
			}
			node: {
				get: =>
					{
						x: (@node.getx() * zoom)
						y: (@node.gety() * zoom)
					}
				set: (dat) =>
					@node.setpos (dat.x / zoom), (dat.y / zoom)
				type: {
					x: 'int'
					y: 'int'
				}
			}
			shift: {
				get: =>
					{
						shift: @shift_amount
					}
				set: (dat) =>
					ds = dat.shift - @shift_amount
					@shift ds
				type: {
					shift: 'int'
				}
			}
			plica: {
				get: =>
					pl = @plica_amount isnt 0
					s = {plica: pl}
					if pl
						s.note = @plica_amount
					return s
				set: (dat) =>
					if dat.plica is false
						dat.note = 0
					else if dat.plica is true and dat.note is undefined
						dat.note = 1
					ds = dat.note - @plica_amount
					@plica_shift ds
				type: (=>
					s = {plica: 'bool'}
					if @plica_amount isnt 0
						s.note = 'int'
					return s
				)()
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.Note = Note
