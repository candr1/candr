class Image
	constructor: (canvas, url, w, h) ->
		console.debug 'Creating new image'
		@nodesets = []
		@left = 0
		@lastleft = 0
		@lastangle = 0
		@angle = 0
		@top = 0
		@lasttop = 0
		@width = 0
		@height = 0
		@canvas = canvas
		@fabric = new fabric.Image.fromURL url, (i) =>
			console.debug 'Image loaded callback'
			orderitems = =>
				console.debug 'Moving image to back of canvas'
				@canvas.fabric.sendToBack i
			imtranslateupdate = =>
				@left = i.left
				@top = i.top
				dl = @left - @lastleft
				dt = @top - @lasttop
				@lastleft = @left
				@lasttop = @top
				for nodeset in @nodesets
					nodeset.translate dl, dt
					console.debug 'imtranslateupdate'
					@canvas.fabric.remove nodeset.polygon
					@canvas.fabric.add nodeset.connect_polygon()
			ri = i.width / i.height
			rs = w / h
			@width = w
			@height = h
			scale = 1
			if rs < ri
				scale = w / i.width
			else
				scale = h / i.height
			i = i.set {
				lockUniScaling: true
				angle: 0
				originX: 'left'
				originY: 'top'
				hasRotatingPoint: false
				hoverCursor: 'pointer'
				scaleX: scale
				scaleY: scale
			}
			i.on 'selected', orderitems
			i.on 'moving', orderitems
			i.on 'moving', imtranslateupdate
			console.debug this
			@lastleft = i.left
			@lasttop = i.top
			@lastangle = i.angle
			@fabric = i
			@canvas.fabric.add i
			@canvas.fabric.sendToBack i
			imtranslateupdate()
		return this
	originalsize: ->
		os = @fabric.getOriginalSize()
		console.debug 'Original size: ', os
		return os
	add_nodeset: (nodeset) ->
		for node in nodeset.nodes
			node.fabric.on 'moving', =>
				console.debug 'node move'
				@canvas.fabric.remove nodeset.polygon
				@canvas.fabric.add nodeset.connect_polygon()
		@nodesets.push nodeset
		return this
	nodetranslateupdate: =>
		console.debug 'Updating node positions: ', @nodes
	add_nodeset_polygons: =>
		for nodeset in @nodesets
			console.debug 'add_nodeset_polygons'
			@canvas.fabric.remove nodeset.polygon
			@canvas.fabric.add nodeset.connect_polygon()
window.Candr.Image = Image
