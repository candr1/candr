# Base class that line-based items (staffline, divisio) derive from
class Line extends window.Candr.EditorItem
	@canvas = null
	@editor = null
	@nodes = null
	@lines = null
	@offset = null
	@colour = null
	@mmoveev = null
	@mdownev = null
	@width = null
	@in_progress = null
	@nodestoset = null
	@setnode = null
	@visible = null
	@originalcur = null
	constructor: (canvas, editor, after, colour = 'rgba(102,153,255,1)',
	offset = [0, 0], width = 5) ->
		super()
		@canvas = canvas
		@editor = editor
		@width = width
		@nodes = []
		@lines = []
		@visible = true
		@colour = colour
		@offset = offset
		@in_progress = true
		# save the original cursor
		@originalcur = @canvas.fabric.hoverCursor
		@canvas.fabric.hoverCursor = 'all-scroll'
		# a line has a start node and an end node
		sn = new window.Candr.Node @colour
		en = new window.Candr.Node @colour
		_this = this
		# we pop nodes off the end of nodestoset and set them one at a
		# time
		@nodestoset = [en, sn]
		for node in @nodestoset
			node.set_offset @offset[0], @offset[1]
			node.fabric.set 'selectable', false
			node.fabric.on 'deselected', =>
				@editor.save_optionspane()
		@setnode = @nodestoset.pop()
		@canvas.fabric.add @setnode.fabric
		# function for mouse move
		@mmoveev = (e) =>
			# ensure we have a node to set and we are in progress
			if not (@setnode and @in_progress)
				return
			# set node position = mouse position
			@setnode.setpos (e.pointer.x - @offset[0]), (e.pointer.y - @offset[1])
			@canvas.fabric.renderAll()
			if @nodes.length < 1
				return
			# connect the nodes, including this node that has not
			# yet been completed
			@drawlines @nodes.concat [@setnode]
		# function for mouse down
		@mdownev = (e) =>
			# ensure we have a node to set and we are in progress
			if not (@setnode and @in_progress)
				return
			# add this node to the completed nodes
			@nodes.push @setnode
			# get a new node
			@setnode = @nodestoset.pop()
			if @setnode
				# we have a new node, begin editing it
				@canvas.fabric.add @setnode.fabric
			else
				# no more nodes, we are done
				console.debug 'Unbinding create events'
				@canvas.fabric.off 'mouse:move', @mmoveev
				@canvas.fabric.off 'mouse:down', @mdownev
				# reset cursor
				@canvas.fabric.hoverCursor = @originalcur
				_this = this
				for node in @nodes
					# give each node a new, 'editable'
					# callback, basically updating all the
					# properties on move
					node.fabric.on 'moving', ->
						@setCoords()
						for node in _this.nodes
							node.set_fill _this.colour
						_this.drawlines _this.nodes
						_this.canvas.fabric.renderAll()
					node.fabric.set 'selectable', true
				# no longer in progress
				@in_progress = false
				# trigger modified callbacks
				@canvas.fabric.trigger 'object:modified', {
					target: @nodes[0].fabric
				}
				# call the 'after' callback function
				after()
		# bind the above functions
		@canvas.fabric.on {
			'mouse:move': @mmoveev
			'mouse:down': @mdownev
		}
	cancel_create: ->
		loop
			if @setnode?
				@canvas.fabric.add @setnode.fabric
				@nodes.push @setnode
			@setnode = @nodestoset.pop()
			break unless @setnode?
		@canvas.fabric.off 'mouse:move', @mmoveev
		@canvas.fabric.off 'mouse:down', @mdownev
		@canvas.fabric.hoverCursor = @originalcur
		_this = this
		for node in @nodes
			node.fabric.on 'moving', ->
				@setCoords()
				for node in _this.nodes
					node.set_fill _this.colour
				_this.drawlines _this.nodes
				_this.canvas.fabric.renderAll()
			node.fabric.set 'selectable', true
		@in_progress = false
	get_bounds: ->
		for node in @nodes
			[node.getx(), node.gety()]
	remove: ->
		console.debug 'Removing', this
		@cancel_create()
		if @setnode
			@setnode.fabric.evented = false
			console.debug 'Removing setnode component', @setnode
			@canvas.fabric.remove @setnode.fabric
		for node in @nodes
			node.fabric.evented = false
			console.debug 'Removing node component', node
			@canvas.fabric.remove node.fabric
		for node in @nodestoset
			node.fabric.evented = false
			console.debug 'Removing nodestoset component', node
			@canvas.fabric.remove node.fabric
		for line in @lines
			line.evented = false
			console.debug 'Removing line component', line
			@canvas.fabric.remove line
		return null
	set_coords: ->
		if @setnode
			@setnode.set_coords()
		for node in @nodes
			node.set_coords()
		for line in @lines
			line.setCoords()
		return true
	is_component: (obj) ->
		if @lines.includes obj
			return true
		for node in @nodes
			if node.is_component obj
				return true
		return false
	visibility: (visible) ->
		console.debug 'Altering Line visibility to: ', visible
		@visible = visible
		for node in @nodes
			node.visibility visible
		for line in @lines
			line.visible = visible
		@drawlines @nodes
	set_colour: (colour, force = false) ->
		if colour is @colour and not force
			return false
		@colour = colour
		console.debug 'Setting Line colour to:', @colour
		nnodes = [@nodes, @nodestoset]
		if @setnode?
			nnodes.push [@setnode]
		for nodes in nnodes
			for node in nodes
				node.set_fill @colour
		for line in @lines
			line.set 'stroke', @colour
		return true
	set_offset: (x, y) ->
		console.debug 'Line setting offset'
		@offset[0] = x
		@offset[1] = y
		for node in @nodes
			node.set_offset x, y
		@drawlines @nodes
	drawlines: (nodes) ->
		if not @visible
			return
		# Got rid of this because even for debug it was spam
		# console.debug 'Line drawing lines'
		# Remove previous lines
		for line in @lines
			@canvas.fabric.remove line
		@lines = []
		lastnode = nodes[0]
		if nodes.length < 2
			return
		# Create lines joining nodes in order. Really this loop will
		# only execute once because there never should be more than 2
		# nodes, but it doesn't hurt to cover all bases
		for i in [1...nodes.length]
			thisnode = nodes[i]
			linenodes = [lastnode, thisnode]
			tnx = thisnode.getx()
			tny = thisnode.gety()
			lnx = lastnode.getx()
			lny = lastnode.gety()
			coords = [tnx, tny, lnx, lny]
			tnc = [tnx, tny]
			lnc = [lnx, lny]
			quadc = [tnc, tnc, lnc, lnc]
			quad = new window.Candr.Quad quadc
			dl = new fabric.Line coords, {
				stroke: @colour
				hasBorders: false
				hasControls: false
				selectable: true
				lockMovementX: true
				lockMovementY: true
				lockScalingX: true
				lockScalingY: true
				lockRotation: true
				originX: 'left'
				originY: 'top'
				top: (quad.y + @offset[1] - @width / 2)
				left: (quad.x + @offset[0])
				strokeWidth: @width
			}
			dl.on 'deselected', =>
				@editor.save_optionspane()
			@lines.push dl
			lastnode = thisnode
		for line in @lines
			@canvas.fabric.add line
		for node in nodes
			node.fabric.bringToFront()
window.Candr.Line = Line
