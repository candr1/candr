class SystemSynchroniser
	@canvas = null
	@items = null
	@im = null
	@selected = null
	@links = null
	@offset = null
	@n_staves = null
	@link_urls = null
	constructor: (canvasid, editable, url, warpnodes) ->
		@canvas = new window.Candr.Canvas canvasid
		@offset = [0, 0]
		@items = []
		@n_staves = 0
		@link_urls = []
		@selected = null
		@links = []
		console.debug 'Adding image'
		@canvas.set_info_text 'Loading image...'
		if warpnodes
			iw = new window.Candr.ImageWarp url, warpnodes, (i, src, rect) =>
				@im = new window.Candr.BackgroundImage @canvas, src,
					((i) =>
						@canvas.clear_info_text()
						oldjson = JSON.stringify @offset
						offset = JSON.parse(window.Candr.Cookie.get 'offset', oldjson)
						if window.Candr.correctOffsetCookie offset
							@alter_border offset[0]
						else
							window.Candr.Cookie.set 'offset', oldjson
						for item in @items
							item.set 'visible', true
						for link in @links
							link.set 'visible', true
						@canvas.fabric.renderAll()
					), rect
		else
			@im = new window.Candr.BackgroundImage @canvas, url, (i) =>
				@canvas.clear_info_text()
				for item in @items
					item.set 'visible', true
				for link in @links
					link.set 'visible', true
				@canvas.fabric.renderAll()
		@canvas.fabric.on 'mouse:down', (e) ->
			console.debug e.target?.SynchroniserData
			url = e.target?.SynchroniserData?.url
			if url
				console.debug 'Going to href: ' + url
				window.location.assign url
		@canvas.fabric.on 'mouse:over', (e) =>
			sd = e.target?.SynchroniserData?
			if sd
				@canvas.fabric.hoverCursor = 'pointer'
			else
				@canvas.fabric.hoverCursor = 'default'
		if editable
			@canvas.fabric.on 'mouse:down', (e) =>
				idx = @links.indexOf e.target
				if idx is -1
					return
				if not e.target?.SynchroniserData?.editable
					alert "Cannot delete link: it likely ha\
s a setting attached to it. Delete the setting first"
					return
				console.debug 'Deleting link'
				links = e.target?.SynchroniserData?.links
				a = links[0]
				b = links[1]
				rem = (a, b) -> a?.SynchroniserData?.links?.remove b
				rem a, b
				rem b, a
				@canvas.fabric.remove e.target
				@links.remove e.target
				@canvas.fabric.renderAll()
			@canvas.fabric.on 'mouse:down', (e) =>
				recolour_item = (i, col = 'rgba(0,255,0,1)') ->
					if i.stroke?
						i.set {
							'oldStroke': i.stroke
							'stroke': col
						}
					if i.fill?
						i.set {
							'oldFill': i.fill
							'fill': col
						}
				reset_colour = (i) ->
					if i.oldStroke?
						i.set {
							'stroke': i.oldStroke
							'oldStroke': undefined
						}
					if i.oldFill?
						i.set {
							'fill': i.oldFill
							'oldFill': undefined
						}
				item = e.target
				if not (item? and @items.includes item)
					return
				if not @selected?
					recolour_item item
					@selected = item
				else
					@connect_items @selected, item
					reset_colour @selected
					@selected = null
				@canvas.fabric.renderAll()
				console.warn item
	connect_items: (a, b) ->
		if a is b
			return
		console.debug 'Connecting:', a, b
		# find staffids in the links of an object
		find_in_link = (needle_staffids, haystack, visited = []) ->
			if visited.includes haystack
				return false
			visited.push haystack
			if not haystack?.SynchroniserData?.staffid?
				return false
			# this staffid
			hsid = haystack.SynchroniserData.staffid
			# if the staffid is in the staffids to be found, then
			# yes, it's found
			if needle_staffids.includes hsid
				return true
			# no links to check, nothing found!
			if not haystack?.SynchroniserData?.links?
				return false
			# clone the original array, but add this staffid to
			# check
			new_needle_staffids = (needle_staffids.slice 0).concat hsid
			# for every link
			for link in haystack?.SynchroniserData?.links
				# try to find these staffids
				if find_in_link new_needle_staffids, link, visited
					return true
			# we didn't find it
			return false
		find_mutual = (a, b) ->
			# check both a in b and b in a
			if not (a?.SynchroniserData?.staffid? and b?.SynchroniserData?.staffid?)
				return false
			fil = (a, b) -> find_in_link [a.SynchroniserData.staffid], b
			if (fil a, b) or (fil b, a)
				return true
			return false
		if find_mutual a, b
			alert "Cannot link items of the same staff"
			return
		@add_link a, b, null, true
		@draw_links()
	add_link_by_id: (link, url = null, editable = false) ->
		console.debug 'Adding link'
		find_by_id = (id, items = @items) =>
			for item in items
				if not item?.SynchroniserData?.id?
					continue
				intid = parseInt (item.SynchroniserData.id)
				if intid is id
					return item
			return null
		ids = if Array.isArray(link) then link else []
		items = (ids.map (id) -> (find_by_id (parseInt id))).filter ((i) -> i?)
		items.sort (a, b) ->
			cmp = b.top - a.top
			if cmp isnt 0
				return cmp
			cmp = b.left - a.left
			return cmp
		if url
			if not @link_urls?
				@link_urls = []
			for id in ids
				@link_urls[id] = url
			console.debug @link_urls
		for i in [1...items.length]
			@add_link items[i - 1], items[i], url, editable
		@draw_links()
	add_link: (a, b, url = null, editable = false) ->
		if a is b
			# you can't link to itself
			return
		console.debug 'Linking:', a, b
		init = (i) ->
			if not i.SynchroniserData?
				i.SynchroniserData = {}
			if not i.SynchroniserData.links?
				i.SynchroniserData.links = []
			i.SynchroniserData.editable = editable
		init a
		init b
		if not a.SynchroniserData.links.includes b
			a.SynchroniserData.links.push b
		if not b.SynchroniserData.links.includes a
			b.SynchroniserData.links.push a
	get_links: ->
		console.debug 'Getting links'
		set_union = (a, b) ->
			u = new Set a
			b.forEach (x) ->
				u.add x
			return u
		has_links = (i) -> i.SynchroniserData?.links?
		console.debug 'Creating link objects'
		# Map of item -> link record
		ls = new Map()
		for item in @items
			# no links
			if not has_links item
				continue
			# get the link record for this item
			lr = ls.get item
			# initialise it if empty
			if not lr?
				lr = new Set([item])
			# foreach link
			for link in item.SynchroniserData.links
				# link linkrecord
				llr = ls.get link
				if not llr?
					llr = new Set([link])
				# they're the same object, fine
				if llr is lr
					continue
				# make a union of the two
				lr = set_union lr, llr
				# for each item in that link record, update the
				# link record to be this link record!
				lr.forEach (x) ->
					ls.set x, lr
		# Create a set of link records (i.e. unique)
		# So we now have a set of sets
		uniq = new Set (ls.values())
		console.debug 'Sorting into IDs'
		ids = []
		uniq.forEach (lr) ->
			ids.push (((Array.from lr).map (x) ->
				x?.SynchroniserData?.id
			).filter (x) -> x?)
		console.debug ids
		return ids
	draw_links: ->
		console.debug 'Drawing links'
		linked = new Set()
		@canvas.fabric.remove @links...
		@links = []
		has_links = (i) -> i.SynchroniserData?.links?
		canonical_links = []
		for item in @items
			if not has_links item
				continue
			url = @link_urls[item.SynchroniserData.id] ? null
			for link in item.SynchroniserData.links
				if linked.has link
					continue
				url = @link_urls[link.SynchroniserData.id] ? url
				canonical_links.push {
					url: url
					link: [item, link]
				}
			linked.add item
		linked = null
		get_midpoint = (i) ->
			ocoords = i.SynchroniserData.originalCoords
			if not ocoords?
				return [0, 0]
			xcum = 0
			ycum = 0
			for point in ocoords
				xcum += point[0]
				ycum += point[1]
			xcum /= ocoords.length
			ycum /= ocoords.length
			return [xcum, ycum]
		for linkdat in canonical_links
			link = linkdat.link
			pa = get_midpoint link[0]
			pa[0] += link[0].left
			pa[1] += link[0].top
			pb = get_midpoint link[1]
			pb[0] += link[1].left
			pb[1] += link[1].top
			minx = if pa[0] < pb[0] then pa[0] else pb[0]
			miny = if pa[1] < pb[1] then pa[1] else pb[1]
			coords = [pa, pb].flat()
			console.debug 'Drawing link between:', link[0], link[1]
			console.debug "Coords are:", coords
			editable = not link.some ((a) -> not (a?.SynchroniserData?.editable))
			colour = if editable then 'rgba(0,255,0,1)' else 'rgba(255,0,0,1)'
			l = new fabric.Line coords, {
				stroke: colour
				strokeWidth: 2
				originX: 'left'
				originY: 'top'
				hasBorders: false
				hasControls: false
				lockMovementX: true
				lockMovementY: true
				lockScalingX: true
				lockScalingY: true
				lockRotation: true
				top: miny
				left: minx
				SynchroniserData: {
					links: link
					url: linkdat.url
					editable: editable
				}
			}

			@links.push l
		@canvas.fabric.add @links...
		for item in @items
			item.bringToFront()
		@canvas.fabric.renderAll()
	increase_border: (px = 10) ->
		@alter_border px
	decrease_border: (px = 10) ->
		@alter_border -px
	alter_border: (amount) ->
		dim = [@canvas.getwidth(), @canvas.getheight()]
		dim[0] += amount * 2
		dim[1] += amount * 2
		@canvas.setdimensions dim[0], dim[1]
		xy = @canvas.getbackgroundimagepos()
		xy[0] += amount
		xy[1] += amount
		@offset[0] += amount
		@offset[1] += amount
		window.Candr.Cookie.set 'offset', (JSON.stringify @offset)
		@canvas.setbackgroundimagepos xy[0], xy[1]
		for item in @items
			item.left += amount
			item.top += amount
			item.setCoords()
		for link in @links
			link.left += amount
			link.top += amount
			link.setCoords()
		@canvas.fabric.renderAll()
	add_stave: (staffid, warp, items) ->
		r = new window.Candr.MinRect warp
		rtlx = r.rect[0][0]
		rtly = r.rect[0][1]
		b = r.rect.flat()
		a = r.points.flat()
		pspt = new window.Candr.PerspectiveTransform b, a
		item_sort_order = ['staffline', 'editorial_text', 'syllable',
			'editorial_syllable', 'clef', 'editorial_clef',
			'accidental', 'editorial_accidental', 'ligature',
			'note']
		item_dict = {}
		for itemid, item of items
			if not item_dict[item.name]?
				item_dict[item.name] = {}
			if item.name not in item_sort_order
				item_sort_order.push (item.name)
			item_dict[item.name][itemid] = item
		for order in item_sort_order
			if order is 'staffline'
				console.debug 'Ignoring stafflines'
				continue
			for itemid, item of item_dict[order]
				if not item.nodes?
					console.debug 'Item has no nodes'
					continue
				nodes = for node in item.nodes
					pspt.transform (node[0] + rtlx), (node[1] + rtly)
				dat = {
					id: itemid
					staffid: staffid
					name: item.name
				}
				if item?.url?
					dat.url = item.url
				@add_item nodes, dat, 'rgba(102,153,255,0.3)'
		for item in @items
			item.set 'visible', false
		for link in @links
			link.set 'visible', false
		@n_staves++
		@canvas.fabric.renderAll()
	add_item: (nodes, data, fill = 'rgba(102,153,255,1)') ->
		console.debug 'Adding item with nodes and data:', nodes, data
		quadnodes = []
		quadnodes.push nodes...
		while quadnodes.length < 4
			quadnodes.push nodes[0]
		quad = new window.Candr.Quad quadnodes
		for node in nodes
			node[0] -= quad.x
			node[1] -= quad.y
		regularise_nodes = (ns) ->
			ns.sort (b, a) ->
				cmp = b[1] - a[1]
				if cmp isnt 0
					return cmp
				cmp = b[0] - a[0]
				return cmp
			fn = ns.pop()
			objn = []
			for n in ns
				ax = n[0] - fn[0]
				ay = n[1] - fn[1]
				a = Math.atan2 ay, ax
				if a < 0
					a += Math.PI * 2
				s = ax * ax + ay * ay
				objn.push {
					x: n[0]
					y: n[1]
					a: a
					s: s
				}
			objn.sort (a, b) ->
				if a.a > b.a
					return -1
				if a.a < b.a
					return 1
				if a.s > b.s
					return -1
				if a.s < b.s
					return 1
				return 0
			objn.push {
				x: fn[0]
				y: fn[1]
				a: 0
				s: 0
			}
			out = []
			for n in objn
				out.push [n.x, n.y]
			return out
		nodes = regularise_nodes nodes
		item = switch nodes.length
			when 1 then new fabric.Circle {
				fill: fill
				radius: 6
				originX: 'center'
				originY: 'center'
			}
			when 2 then new fabric.Line nodes.flat(), {
				stroke: fill
				strokeWidth: 2
				originX: 'left'
				originY: 'top'
			}
			else new fabric.Polygon (nodes.map (n) -> {x: n[0], y: n[1]}), {
				fill: fill
				originX: 'left'
				originY: 'top'
			}
		data.originalCoords = nodes
		item.set {
			hasBorders: false
			hasControls: false
			selectable: false
			lockMovementX: true
			lockMovementY: true
			lockScalingX: true
			lockScalingY: true
			lockRotation: true
			top: (quad.y + @offset[1])
			left: (quad.x + @offset[0])
			SynchroniserData: data
		}
		@canvas.fabric.add item
		if name is 'staffline' or name is 'divisione'
			item.sendToBack()
		@items.push item
window.Candr.SystemSynchroniser = SystemSynchroniser
