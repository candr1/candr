# Set to true to enable debug output in console
debug = true

window.Candr = {}
if debug isnt true
	# Make debug a null function
	console.debug = ->
# Polyfill for includes
if not Array::includes
	Array::includes = (search, start) ->
		if search instanceof RegExp
			throw TypeError 'first argument must not be a RegExp'
		if start is undefined
			start = 0
		return (@indexOf search, start) isnt -1
# Polyfill for Object.assign
if typeof Object.assign isnt 'function'
	Object.defineProperty Object, 'assign', {
		value: (target, varArgs) ->
			if not target?
				throw new TypeError 'Cannot convert undefined or null to object'
			to = Object target
			for index in [1...arguments.length]
				nextSource = arguments[index]
				if nextSource?
					for nextKey, nextVal of nextSource
						if Object::hasOwnProperty.call nextSource, nextKey
							to[nextKey] = nextVal
			return to
		writable: true
		configurable: true
	}
# Polyfill for isInteger
if not Number::isInteger
	Number::isInteger = (value) ->
		ta = (i) -> (typeof i) is 'number'
		tb = (i) -> isFinite i
		tc = (i) -> (Math.floor i) is i
		return (ta i) and (tb i) and (tc i)
# Polyfill components
for key, val of exports
	if typeof window[key] is 'undefined'
		window[key] = val
# I know, you're not supposed to extend native objects. Shoot me.
Array::remove = (val) ->
	idx = @indexOf val
	if idx > -1
		@splice idx, 1
Array::shuffle ?= ->
	if @length > 1 then for i in [@length - 1..1]
		j = Math.floor Math.random() * (i + 1)
		[@[i], @[j]] = [@[j], @[i]]
	this
# Convert CSS color to RGBA
window.parseColor = (color) ->
	x = document.createElement 'div'
	document.body.appendChild x
	red = green = blue = alpha = 0
	try
		x.style = 'color: ' + color + '!important'
		color = (window.getComputedStyle x).color
		rgba = ((color.match /rgba?\((.*)\)/)[1].split ',').map Number
		red = rgba[0]
		green = rgba[1]
		blue = rgba[2]
		alpha = if '3' in rgba then rgba[3] else 1
	catch e
	x.parentNode.removeChild x
	return {red: red, green: green, blue: blue, alpha: alpha}
# Make UUID
window.uuidv4 = ->
	'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, (c) ->
		r = Math.random() * 16 | 0
		v = if c is 'x' then r else (r & 0x3 | 0x8)
		v.toString 16
