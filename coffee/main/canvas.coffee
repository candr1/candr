class Canvas
	constructor: (id = 'view', height = 720) ->
		console.debug 'Creating new canvas'
		@fabric = new fabric.Canvas(id, {preserveObjectStacking: true})
		@fabric.setHeight height
		@fabric.setWidth (u('body').size().width)
		@ptr = null
		@fabric.on 'mouse:move', (e) =>
			ptr = @fabric.getPointer e
			if ptr.x and ptr.y
				@ptr = ptr
		oldjson = JSON.stringify 1
		zoom = JSON.parse(window.Candr.Cookie.get 'zoom', oldjson)
		@set_zoom zoom
		return this
	add: (obj) ->
		console.debug 'Canvas adding: ', obj
		@fabric.add obj.fabric
		if @info_block
			@info_block.bringToFront()
		if @info_text
			@info_text.bringToFront()
		return this
	zoom: (increment) ->
		zoom = @fabric.getZoom() + increment
		return @set_zoom zoom
	set_zoom: (zoom) ->
		if zoom > 9.99
			zoom = 9.99
		if zoom < 0.01
			zoom = 0.01
		@fabric.setWidth (@getwidth() * zoom)
		@fabric.setHeight (@getheight() * zoom)
		@fabric.setZoom zoom
		window.Candr.Cookie.set 'zoom', (JSON.stringify zoom)
		return zoom
	getwidth: ->
		@fabric.getWidth()
	getheight: ->
		@fabric.getHeight()
	setdimensions: (x, y) ->
		console.log 'Canvas setDimensions', x, y
		@fabric.setDimensions {
			width: x,
			height: y
		}
	setbackgroundimage: (i, x = 0, y = 0, zoom = 1) ->
		scale = 1 / zoom
		@fabric.setBackgroundImage i, (@fabric.renderAll.bind @fabric), {
			originX: 'left'
			originY: 'top'
			left: x
			top: y
			scaleX: scale
			scaleY: scale
		}
		@fabric.sendToBack i
	getbackgroundimagepos: ->
		[@fabric.backgroundImage.left, @fabric.backgroundImage.top]
	setbackgroundimagepos: (x, y) ->
		@fabric.backgroundImage.set 'left', x
		@fabric.backgroundImage.set 'top', y
	set_info_text: (txt) ->
		@clear_info_text()
		@info_block = new fabric.Rect {
			left: 0
			top: 0
			fill: 'white'
			selectable: false
			evented: false
		}
		@info_text = new fabric.Text txt, {
			left: 0
			top: 0
			fill: 'black'
			fontSize: 12
			selectable: false
			evented: false
		}
		@fabric.add @info_text
		@fabric.renderAll()
		@info_block.set {
			width: @info_text.width
			height: @info_text.height
		}
		@fabric.add @info_block
		@info_block.bringToFront()
		@info_text.bringToFront()
		@fabric.renderAll()
	clear_info_text: ->
		@fabric.remove @info_text
		@fabric.remove @info_block
		@info_text = null
		@info_block = null
		@fabric.renderAll()
window.Candr.Canvas = Canvas
