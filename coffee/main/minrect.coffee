# class to find the minimum rectangle for warping.
# Currently just expands the points until rectangular
# This is copied in PHP (app/Helpers/MinRect.php) so changing anything here
# means you should change it there too!
class MinRect
	@points = null
	@rect = null
	constructor: (points) ->
		if points.length isnt 4
			console.error '4 points are required'
			return null
		console.debug 'Sorting points into corners'
		points.sort (a, b) ->
			if a[1] < b[1]
				return -1
			if a[1] > b[1]
				return 1
			return 0
		tl = if points[0][0] < points[1][0] then points[0] else points[1]
		tr = if points[0][0] > points[1][0] then points[0] else points[1]
		bl = if points[2][0] < points[3][0] then points[2] else points[3]
		br = if points[2][0] > points[3][0] then points[2] else points[3]
		console.debug 'Finding bounding rectangle'
		@points = [tl, tr, bl, br]
		minx = miny = Infinity
		maxx = maxy = -Infinity
		for point in @points
			if point[0] < minx
				minx = point[0]
			if point[1] < miny
				miny = point[1]
			if point[0] > maxx
				maxx = point[0]
			if point[1] > maxy
				maxy = point[1]
		@rect = [[minx, miny], [maxx, miny], [minx, maxy], [maxx, maxy]]
		return this
window.Candr.MinRect = MinRect
