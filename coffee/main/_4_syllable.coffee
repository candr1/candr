class Syllable extends window.Candr.RectWithCentrepoint
	@guidepoints = null
	@syllable_text = null
	@wordstart = null
	@stanza = null
	constructor: (canvas, editor, colour = 'rgba(102,153,255,1)', stroke = 5) ->
		super canvas, editor, colour, stroke
		@syllable_text = ''
		@wordstart = false
		@stanza = 1
		@guidepoints = []
		@centrepoint.fabric.on 'deselected', =>
			console.debug 'Syllable removing guidepoints'
			@remove_guidepoints()
			if @editor.checkboxes.hasOwnProperty 'notes'
				vis = @editor.checkboxes['notes'].checked
				@editor.view_notes vis
			@canvas.fabric.renderAll()
		@centrepoint.fabric.on 'moving', =>
			console.debug 'Syllable recalculating nearest guidepoint'
			dist = (pax, pay, pbx, pby) ->
				dx = pbx - pax
				dy = pby - pay
				return (dx * dx + dy * dy)
			mindist_node = null
			mindist = Infinity
			cpx = @centrepoint.getx() + @centrepoint.offset[0]
			cpy = @centrepoint.gety() + @centrepoint.offset[1]
			for node in @guidepoints
				nx = node.getx() + node.offset[0]
				ny = node.gety() + node.offset[1]
				d = dist cpx, cpy, nx, ny
				if d < mindist
					mindist = d
					mindist_node = node
				node.set_fill 'rgba(0,255,0,0.5)'
			if mindist_node
				console.debug 'Nearest guidepoint is:', mindist_node
				mindist_node.set_fill 'rgba(0,255,0,1)'
			@canvas.fabric.renderAll()
	remove_guidepoints: =>
		console.debug 'Removing guidepoints'
		for point in @guidepoints
			@canvas.fabric.remove point.fabric
		@guidepoints = []
	deselect_fun: ->
		@remove_guidepoints()
	centrepointselfun: =>
		super()
		console.debug 'Syllable adding guidepoints'
		@remove_guidepoints()
		@editor.view_notes false
		note_points = @editor.get_note_points()
		for point in note_points
			n = new window.Candr.Node()
			n.set_offset @offset[0], @offset[1]
			n.setpos point[0], point[1]
			n.set_fill 'rgba(0,255,0,0.5)'
			@canvas.add n
			@guidepoints.push n
		@canvas.fabric.renderAll()
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: ->
					{
						name: 'Syllable'
					}
				set: (dat) ->
				type: {
					name: 'label'
				}
			}
			position: {
				get: =>
					{
						top: ((@rect.top - @offset[1]) * zoom)
						left: ((@rect.left - @offset[0]) * zoom)
						width: (@rect.getScaledWidth() * zoom)
						height: (@rect.getScaledHeight() * zoom)
						angle: @rect.angle
					}
				set: (dat) =>
					border = -@rect.strokeWidth
					s = {
						top: ((dat.top / zoom) + @offset[1])
						left: ((dat.left / zoom) + @offset[0])
						angle: dat.angle
						width: ((dat.width / zoom) + border)
						height: ((dat.height / zoom) + border)
						scaleX: 1
						scaleY: 1
					}
					@rect.set s
					@rect.setCoords()
				type: {
					top: 'int'
					left: 'int'
					width: 'int'
					height: 'int'
					angle: 'int'
				}
			}
			centrepoint: {
				get: =>
					{
						x: (@centrepoint.getx() * zoom)
						y: (@centrepoint.gety() * zoom)
					}
				set: (dat) =>
					@centrepoint.setpos (dat.x / zoom), (dat.y / zoom)
				type: {
					x: 'int'
					y: 'int'
				}
			}
			text: {
				get: =>
					{
						text: @syllable_text
						wordstart: @wordstart
						stanza: @stanza
					}
				set: (dat) =>
					@syllable_text = dat.text
					@wordstart = dat.wordstart
					@stanza = dat.stanza
				type: {
					text: 'string'
					wordstart: 'bool'
					stanza: 'int'
				}
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.Syllable = Syllable
