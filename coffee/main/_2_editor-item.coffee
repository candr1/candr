class EditorItem
	@id = null
	@local_id = null
	@editorial_comment = null
	constructor: ->
		@local_id = null
		@id = null
		@editorial_comment = ''
window.Candr.EditorItem = EditorItem
