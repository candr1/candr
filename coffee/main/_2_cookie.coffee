class Cookie
	@utils = {
		isArray: (Array.isArray or (value) ->
			return (Object.prototype.toString.call value) is '[object Array]'
		)
		isPlainObject: (value) ->
			a = not not value
			b = (Object.prototype.toString.call value) is '[object Object]'
			return a and b
		toArray: (value) ->
			return Array.prototype.slice.call value
		getKeys: (Object.keys or (obj) ->
			keys = []
			key = ''
			for key of obj
				if obj.hasOwnProperty key
					keys.push key
			return keys
		)
		encode: (value) ->
			return (String value).replace /[,;"\\=\s%]/g, (character) ->
				return encodeURIComponent character
		decode: (value) ->
			return decodeURIComponent value
		retrieve: (value, fallback) ->
			return (if (value is null) or (value is undefined) then fallback else value)
	}
	@defaults = {}
	@expiresMultiplier = 60 * 60 * 24
	@set = (key, value, options) ->
		if @utils.isPlainObject key
			for k of key
				if key.hasOwnProperty k
					@set k, key[k], value
		else
			ipo = @utils.isPlainObject options
			options = if ipo then options else {expires: options}
			oeun = options.expires isnt undefined
			expires = if oeun then options.expires else (@defaults.expires or '')
			expiresType = typeof expires
			if expiresType is 'string' and expires isnt ''
				expires = new Date expires
			else if expiresType is 'number'
				expires = new Date (+new Date() + 1000 * @expiresMultiplier * expires)
			if expires isnt '' and 'toUTCString' in expires
				expires = ';expires=' + expires.toUTCString()
			path = options.path or @defaults.path
			path = if path then ';path=' + path else ''
			domain = options.domain or @defaults.domain
			domain = if domain then ';domain=' + domain else ''
			secure = if options.secure or @defaults.secure then ';secure' else ''
			if options.secure is false
				secure = ''
			sameSite = options.sameSite or @defaults.sameSite
			sameSite = if sameSite then ';SameSite=' + sameSite else ''
			if options.sameSite is null
				sameSite = ''
			document.cookie = (@utils.encode key) + '=' + (@utils.encode value)
			document.cookie += expires + path + domain + secure + sameSite
		return this
	@setDefault = (key, value, options) ->
		if @utils.isPlainObject key
			for k of key
				if @get k is undefined
					@set k, key[k], value
			return this
		else
			if @get key is undefined
				return @set.apply this, arguments
	@remove = (keys) ->
		keys = if @utils.isArray keys then keys else @utils.toArray arguments
		for i in [0...keys.length]
			@set keys[i], '', -1
		return this
	@removeSpecific = (keys, options) ->
		if not options
			return @remove keys
		keys = if @utils.isArray keys then keys else [keys]
		options.expires = -1
		for i in [0...keys.length]
			@set keys[i], '', options
		return this
	@empty = ->
		return @remove (@utils.getKeys @all())
	@get = (keys, fallback) ->
		cookies = @all()
		if @utils.isArray keys
			result = {}
			for value in keys
				result[value] = @utils.retrieve cookies[value], fallback
			return result
		else
			return @utils.retrieve cookies[keys], fallback
	@all = ->
		if document.cookie is ''
			return {}
		cookies = document.cookie.split '; '
		result = {}
		for item in cookies
			item = item.split '='
			key = @utils.decode item.shift()
			value = @utils.decode (item.join '=')
			result[key] = value
		return result
	@enabled = ->
		if navigator.cookieEnabled
			return true
		ret = ((cookie.set '_', '_').get '_') is '_'
		cookie.remove '_'
		return ret
window.Candr.Cookie = Cookie
window.Candr.correctOffsetCookie = (test) ->
	if not (Array.isArray test)
		return false
	for i in [0..1]
		if not (Number.isInteger test[i])
			return false
	return true
