class Divisione extends window.Candr.Line
	constructor: (canvas, editor, after, colour = 'rgba(102,153,255,1)',
	offset = [0, 0], width = 5) ->
		super canvas, editor, after, colour, offset, width
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: ->
					{
						name: 'Divisione'
					}
				set: (dat) ->
				type: {
					name: 'label'
				}
			}
			nodes: {
				get: =>
					n0 = @nodes[0]
					n1 = @nodes[1]
					return {
						ax: if n0 then (n0.getx() * zoom) else 0
						ay: if n0 then (n0.gety() * zoom) else 0
						bx: if n1 then (n1.getx() * zoom) else 0
						by: if n1 then (n1.gety() * zoom) else 0
					}
				set: (dat) =>
					ta = (i) -> i?
					tb = (i) -> i.length > 1
					tc = (i) -> typeof i.getx is 'function'
					td = (i) -> typeof i.gety is 'function'
					te = (i, e) -> i.getx() isnt e
					tf = (i, e) -> i.gety() isnt e
					tg = (i) -> (ta i) and (tb i)
					th = (i) -> (tc i[0]) and (tc i[1])
					ti = (i) -> (td i[0]) and (td i[1])
					tj = (i) -> (th i) and (ti i)
					tk = (i, x, y) -> (te i, x) or (tf i, y)
					tl = (i, ax, ay, bx, _by) -> (tk i[0], ax, ay) or (tk i[1], bx, _by)
					test = (i, ax, ay, bx, _by) ->
						(tg i) and (tj i) and (tl i, ax, ay, bx, _by)
					if test @nodes, dat.ax, dat.ay, dat.bx, dat.by
						@nodes[0].setpos (dat.ax / zoom), (dat.ay / zoom)
						@nodes[1].setpos (dat.bx / zoom), (dat.by / zoom)
						@drawlines @nodes
				type: {
					ax: 'int'
					ay: 'int'
					bx: 'int'
					by: 'int'
				}
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.Divisione = Divisione
