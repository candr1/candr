class Ligature extends window.Candr.EditorItem
	@notes = null
	@lines = null
	@canvas = null
	@offset = null
	@colour = null
	@width = null
	@in_progress = null
	@magic_ligature_object = null
	@visible = null
	@ligature_type = null
	constructor: (canvas, colour = 'rgba(102,153,255,1)', width = 2, notes = []) ->
		super()
		@notes = notes
		@canvas = canvas
		@colour = colour
		@width = width
		@magic_ligature_object = uuidv4()
		@offset = [0, 0]
		@lines = []
		@set_ligature_type 'default'
		@in_progress = false
		@visible = true
		@drawlines()
		if notes.length > 0
			@canvas.fabric.trigger 'object:modified', {
				target: notes[0].node.fabric
			}
	cancel_create: ->
	set_ligature_type: (type) ->
		if type.toLowerCase() is 'currentes'
			@ligature_type = 'Currentes'
		else
			@ligature_type = 'Square'
	set_colour: (colour, force = false) ->
		if @colour is colour and not force
			return false
		console.debug 'Setting ligature colour'
		@colour = colour
		ret = false
		for note in @notes
			if note.set_colour @colour, force
				ret = true
		if ret
			@drawlines()
		return ret
	visibility: (visible) ->
		@visible = visible
		for line in @lines
			line.visible = visible
		for note in @notes
			note.visibility visible
		return visible
	set_offset: (x, y) ->
		@offset[0] = x
		@offset[1] = y
		for note in @notes
			note.set_offset x, y
		@drawlines()
	add_note: (note) ->
		if not note instanceof window.Candr.Note
			alert 'Not a note'
			return
		console.debug 'Ligature adding note'
		note.node.fabric.on 'moving', @drawlines
		console.debug 'Ligature colour is:', @colour
		note.set_colour @colour, true
		@notes.push note
		@drawlines()
		@canvas.fabric.trigger 'object:modified', {
			target: note.node.fabric
		}
	remove_note: (note) ->
		@notes.remove note
		note.node.fabric.off 'moving', @drawlines
		@drawlines()
		tgt = @magic_ligature_object
		if @notes
			tgt = @notes[0].node.fabric
			@canvas.fabric.trigger 'object:modified', {
				target: tgt
			}
			return null
		return @remove()
	remove: ->
		for note in @notes
			note.node.fabric.off 'moving', @drawlines
		@canvas.fabric.remove @lines...
		for note in @notes
			note.set_coords()
		@canvas.fabric.renderAll()
		return @notes
	set_coords: ->
		for note in @notes
			note.set_coords()
		for line in @lines
			line.setCoords()
		return true
	empty: ->
		return @notes.length is 0
	is_component: (obj) ->
		if obj is @magic_ligature_object
			return true
		for note in @notes
			if note.is_component obj
				return true
		for line in @lines
			if line is obj
				return true
		return false
	get_notes: ->
		return @notes
	drawlines: =>
		@canvas.fabric.remove @lines...
		@lines = []
		if @notes.length < 2
			return @lines
		arrow_width = @width * 3
		# height of equilateral triangle is root 3 over 2
		arrow_height = arrow_width * (Math.sqrt 3) / 2.0
		for i in [1...@notes.length]
			a = @notes[i - 1]
			b = @notes[i]
			ax = a.node.getx() + @offset[0]
			ay = a.node.gety() + @offset[1]
			bx = b.node.getx() + @offset[0]
			_by = b.node.gety() + @offset[1]
			if ax < bx
				minx = ax
				maxx = bx
			else
				minx = bx
				maxx = ax
			if ay < _by
				miny = ay
				maxy = _by
			else
				miny = _by
				maxy = ay
			l = new fabric.Line [ax, ay, bx, _by], {
				stroke: @colour
				hasBorders: false
				hasControls: false
				evented: false
				selectable: false
				originX: 'left'
				originY: 'top'
				top: miny
				left: minx
				strokeWidth: @width
				visible: @visible
			}
			dy = ay - _by
			dx = ax - bx
			a = (Math.atan2 dy, dx) * 180 / Math.PI
			t = new fabric.Triangle {
				width: arrow_width
				height: arrow_height
				angle: (a - 90)
				originX: 'center'
				originY: 'top'
				fill: @colour
				top: _by
				left: bx
				evented: false
				selectable: false
			}
			g = new fabric.Group [l, t], {
				top: miny
				left: minx
				evented: false
				selectable: false
			}
			@lines.push g
		@canvas.fabric.add @lines...
		@canvas.fabric.renderAll()
		return @lines
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: ->
					{
						name: 'Ligature'
					}
				set: ->
				type: {
					name: 'label'
				}
			}
			notes: {
				get: =>
					res_props = (e) ->
						i = {}
						if not e?
							return null
						if typeof e.properties isnt 'function'
							return i
						p = e.properties zoom
						for frame, props of p
							if typeof props.get is 'function'
								i[frame] = props.get()
						return i
					{
						notes: ((res_props note) for note in @get_notes())
					}
				set: (dat) ->
				type: {}
			}
			type: {
				get: =>
					{
						type: @ligature_type
					}
				set: (dat) =>
					@set_ligature_type dat.type
				type: {
					type: ['Square', 'Currentes']
				}
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.Ligature = Ligature
