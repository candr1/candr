class EditorUpdater
	@editor = null
	@items = null
	@zoom = 1
	constructor: (editor, items, zoom) ->
		@editor = editor
		@items = items
		@zoom = zoom
	get_update_id: (update) ->
		if not update.id?
			return null
		if update.id.id?
			return update.id.id
		return null
	get_update_local_id: (update) ->
		if not update.id?
			return null
		if update.id.local_id?
			return update.id.local_id
		return null
	get_update_ids: (update) ->
		id = @get_update_id update
		local_id = @get_update_local_id update
		ids = []
		if id?
			ids.push id
		if local_id?
			ids.push local_id
		return ids
	get_item_id: (item) ->
		if not (item.properties? and typeof item.properties is 'function')
			return null
		ip = item.properties @zoom
		if not (ip.id? and ip.id.get? and typeof ip.id.get is 'function')
			return null
		ids = ip.id.get()
		if not ids?
			return null
		if ids.id?
			return ids.id
		if not ids.local_id?
			return null
		return ids.local_id
	find_item_by_id: (update, items = @items) ->
		uids = @get_update_ids update
		for item in items
			id = @get_item_id item
			if uids.includes id
				return item
		return null
	get_item_name: (item) ->
		if not (item.properties? and typeof item.properties is 'function')
			return null
		ip = item.properties @zoom
		if not (ip.name? and ip.name.get? and typeof ip.name.get is 'function')
			return null
		name = ip.name.get()
		if not name.name?
			return null
		return name.name
	get_update_name: (update) ->
		if not (update.name? and update.name.name?)
			return null
		return update.name.name
	create_item_from_update: (update) ->
		name = @get_update_name update
		return switch name
			when 'Staffline' then @editor.create_staffline()
			when 'Divisione' then @editor.create_divisione()
			when 'Clef' then @editor.create_clef()
			when 'Accidental' then @editor.create_accidental()
			when 'Syllable' then @editor.create_syllable()
			when 'Note' then @editor.create_note()
			when 'Ligature' then @editor.create_ligature()
			when 'EditorialCClef' then @editor.create_editorial_c_clef()
			when 'EditorialFClef' then @editor.create_editorial_f_clef()
			when 'EditorialDClef' then @editor.create_editorial_d_clef()
			when 'EditorialAClef' then @editor.create_editorial_a_clef()
			when 'EditorialEClef' then @editor.create_editorial_e_clef()
			when 'EditorialSharp' then @editor.create_editorial_sharp()
			when 'EditorialFlat' then @editor.create_editorial_flat()
			when 'EditorialSyllable' then @editor.create_editorial_syllable()
			when 'EditorialText' then @editor.create_editorial_text()
			else null
	apply_update: (update) ->
		console.debug 'Applying update:', update
		item = @find_item_by_id update
		if not item?
			id = @get_update_id update
			if not id?
				console.debug 'No ID from server, not creating item'
				return null
			console.debug 'Can\'t find item, creating...'
			item = @create_item_from_update update
			if not item?
				console.error 'Could not recognise update name'
				return null
		console.debug 'Concerns item:', item
		item_name = @get_item_name item
		update_name = @get_update_name update
		if not item_name? or not update_name? or item_name isnt update_name
			console.error ('Update cannot be applied: names do not' +
				' match. Item name is "' + item_name + '" but' +
				' update name is "' + update_name + '"')
			return null
		console.debug 'Name is:', update_name
		switch update_name
			when 'Staffline'
				@staffline_apply_update item, update
			when 'Divisione'
				@divisione_apply_update item, update
			when 'Clef'
				@clef_apply_update item, update
			when 'Accidental'
				@accidental_apply_update item, update
			when 'Syllable'
				@syllable_apply_update item, update
			when 'Note'
				@note_apply_update item, update
			when 'Ligature'
				@ligature_apply_update item, update
			when 'EditorialCClef'
				@editorial_clef_apply_update item, update
			when 'EditorialFClef'
				@editorial_clef_apply_update item, update
			when 'EditorialDClef'
				@editorial_clef_apply_update item, update
			when 'EditorialAClef'
				@editorial_clef_apply_update item, update
			when 'EditorialEClef'
				@editorial_clef_apply_update item, update
			when 'EditorialSharp'
				@editorial_accidental_apply_update item, update
			when 'EditorialFlat'
				@editorial_accidental_apply_update item, update
			when 'EditorialSyllable'
				@editorial_syllable_apply_update item, update
			when 'EditorialText'
				@editorial_text_apply_update item, update
			else
				console.error 'Name was not recognised'
				return null
		return true
	staffline_apply_update: (item, update) ->
		console.debug 'Applying staffline update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_nodes props, update
		@update_prop_editorial_comment props, update
	divisione_apply_update: (item, update) ->
		console.debug 'Applying divisione update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_nodes props, update
		@update_prop_editorial_comment props, update
	clef_apply_update: (item, update) ->
		console.debug 'Applying clef update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_centrepoint props, update
		@update_prop_type props, update
		@update_prop_editorial_comment props, update
	accidental_apply_update: (item, update) ->
		console.debug 'Applying accidental update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_centrepoint props, update
		@update_prop_type props, update
		@update_prop_editorial_comment props, update
	syllable_apply_update: (item, update) ->
		console.debug 'Applying syllable update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_centrepoint props, update
		@update_prop_text props, update
		@update_prop_editorial_comment props, update
	note_apply_update: (item, update) ->
		console.debug 'Applying note update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_node props, update
		@update_prop_shift props, update
		@update_prop_plica props, update
		@update_prop_editorial_comment props, update
	editorial_clef_apply_update: (item, update) ->
		console.debug 'Applying editorial clef update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_centrepoint props, update
		@update_prop_type props, update
		@update_prop_editorial_comment props, update
	editorial_accidental_apply_update: (item, update) ->
		console.debug 'Applying editorial accidental update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_centrepoint props, update
		@update_prop_type props, update
		@update_prop_editorial_comment props, update
	editorial_syllable_apply_update: (item, update) ->
		console.debug 'Applying editorial syllable update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_centrepoint props, update
		@update_prop_text props, update
		@update_prop_editorial_comment props, update
	editorial_text_apply_update: (item, update) ->
		console.debug 'Applying editorial text update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_position props, update
		@update_prop_editorial_comment props, update
	ligature_apply_update: (item, update) ->
		console.debug 'Applying ligature update'
		props = item.properties @zoom
		@update_prop_id props, update
		@update_prop_type props, update
		@update_prop_editorial_comment props, update
		# Update notes
		console.debug 'Applying update to property: notes'
		# not as clean as the others lol
		if update.notes?
			# remove all notes from ligature
			for note in item.notes
				@editor.canvas.fabric.remove note.fabric
			item.notes = []
			# make new notes
			for note in update.notes
				# make the note
				n = new window.Candr.Note @editor.canvas, (->), @editor
				# apply offset
				n.set_offset @editor.offset[0], @editor.offset[1]
				# cancel the usual creation callbacks
				n.cancel_create()
				# copied from ligature adding routine
				n.node.fabric.on 'moving', item.drawlines
				n.set_colour item.colour, true
				@note_apply_update n, note
				item.notes.push n
			item.drawlines()
	typeify: (update, type) ->
		ret = {}
		safe_parseInt = (e) ->
			ei = parseInt e
			if ei is NaN
				console.error 'Got NaN when parsing int, substituting 0'
				ei = 0
			return ei
		safe_parseFloat = (e) ->
			ef = parseFloat e
			if ef is NaN
				console.error 'Got NaN when parsing float, substituting 0'
				ef = 0
			return ef
		safe_parseBool = (e) ->
			if e is true or e is "true"
				return true
			return false
		safe_parseString = (e) ->
			if typeof e is 'string'
				return e
			return ''
		for id, prop of update
			if not type[id]?
				ret[id] = prop
				continue
			ret[id] = switch type[id]
				when 'int' then safe_parseInt prop
				when 'float' then safe_parseFloat prop
				when 'bool' then safe_parseBool prop
				when 'string', 'label' then safe_parseString prop
				else prop
		return ret
	update_prop_id: (props, update) ->
		console.debug 'Applying update to property: id'
		if update.id?
			iid = props.id.get()
			uid = @typeify update.id, props.id.type
			props.id.set (Object.assign iid, uid)
	update_prop_type: (props, update) ->
		console.debug 'Applying update to property: type'
		if update.type?
			itype = props.type.get()
			utype = @typeify update.type, props.type.type
			props.type.set (Object.assign itype, utype)
	update_prop_position: (props, update) ->
		console.debug 'Applying update to property: position'
		if update.position?
			iposition = props.position.get()
			uposition = @typeify update.position, props.position.type
			props.position.set (Object.assign iposition, uposition)
	update_prop_node: (props, update) ->
		console.debug 'Applying update to property: node'
		if update.node?
			inode = props.node.get()
			unode = @typeify update.node, props.node.type
			props.node.set (Object.assign inode, unode)
	update_prop_shift: (props, update) ->
		console.debug 'Applying update to property: shift'
		if update.shift?
			ishift = props.shift.get()
			ushift = @typeify update.shift, props.shift.type
			props.shift.set (Object.assign ishift, ushift)
	update_prop_plica: (props, update) ->
		console.debug 'Applying update to property: plica'
		if update.plica?
			iplica = props.plica.get()
			uplica = @typeify update.plica, props.plica.type
			props.plica.set (Object.assign iplica, uplica)
	update_prop_nodes: (props, update) ->
		console.debug 'Applying update to property: nodes'
		if update.nodes?
			inodes = props.nodes.get()
			unodes = @typeify update.nodes, props.nodes.type
			props.nodes.set (Object.assign inodes, unodes)
	update_prop_centrepoint: (props, update) ->
		console.debug 'Applying update to property: centrepoint'
		if update.centrepoint?
			icentrepoint = props.centrepoint.get()
			ucentrepoint = @typeify update.centrepoint, props.centrepoint.type
			props.centrepoint.set (Object.assign icentrepoint, ucentrepoint)
	update_prop_text: (props, update) ->
		console.debug 'Applying update to property: text'
		if update.text?
			itext = props.text.get()
			utext = @typeify update.text, props.text.type
			props.text.set (Object.assign itext, utext)
	update_prop_editorial_comment: (props, update) ->
		console.debug 'Applying update to property: editorial comment'
		if update['editorial comment']?
			iec = props['editorial comment'].get()
			uec = @typeify update['editorial comment'], props['editorial comment'].type
			props['editorial comment'].set (Object.assign iec, uec)
window.Candr.EditorUpdater = EditorUpdater
