class EditorialClefLike extends window.Candr.ClefLike
	constructor: (path, canvas, editor, colour = 'rgba(0,0,0,1)') ->
		super canvas, editor, 'rgba(0,0,0,0)', 0
		@set_path path
		@set_colour colour
	set_path: (path) ->
		@canvas.fabric.remove @rect
		@rect = new fabric.Path path
		@rect.set {
			left: 0
			top: 0
			lockUniScaling: true
			lockSkewingX: true
			lockSkewingY: true
			lockRotation: true
			noScaleCache: false
		}
		@rect.scaleToHeight Math.max (@canvas.fabric.height / 4), 32
		# Rebind all events
		bmove = @rectmovefun.bind this
		@rect.on 'moving', bmove
		@rect.on 'scaling', bmove
		@rect.on 'rotating', bmove
		@rect.on 'selected', (@rectselfun.bind this)
		@rect.on 'deselected', (@rectdeselfun.bind this)
		@centrepoint.fabric.on 'selected', (@centrepointselfun.bind this)
		@canvas.fabric.add @rect
	set_colour: (colour, force = false) ->
		if not (super colour, force)
			return false
		console.debug 'Setting EditorialClefLike colour to: ', colour
		@rect.set 'fill', colour
		@rect.set 'stroke', 'rgba(0,0,0,0)'
		@rect.setControlsVisibility {
			bl: true
			br: true
			mb: false
			ml: false
			mr: false
			mt: false
			tl: true
			tr: true
			mtr: false
		}
		return true
window.Candr.EditorialClefLike = EditorialClefLike
