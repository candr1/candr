class RectWithCentrepoint extends window.Candr.EditorItem
	@canvas = null
	@rect = null
	@offset = null
	@centrepoint = null
	@line = null
	@editor = null
	@in_progress = null
	@colour = null
	constructor: (canvas, editor, colour = 'rgba(102,153,255,1)', stroke = 5) ->
		super()
		console.debug 'RectWithCentrepoint constructor'
		@canvas = canvas
		@offset = [0, 0]
		@editor = editor
		@colour = colour
		@in_progress = false
		@rect = new fabric.Rect {
			left: 0
			top: 0
			fill: 'rgba(0,0,0,0)'
			stroke: @colour
			strokeWidth: stroke
			strokeUniform: true
			noScaleCache: false
			width: 100
			height: 100
		}

		@centrepoint = new window.Candr.Node @colour
		@centrepoint.setpos 50, 50
		console.debug 'Creating RectWithCentrepoint:', @rect
		@canvas.fabric.add @rect
		@canvas.add @centrepoint
		console.debug @centrepoint
		@draw_centrepoint_line @colour
		@centrepoint_visibility false
		@rect.on 'selected', @rectselfun
		@rect.on 'deselected', @rectdeselfun
		@rect.on 'moving', @rectmovefun
		@rect.on 'scaling', @rectmovefun
		@rect.on 'rotating', @rectmovefun
		@centrepoint.fabric.on 'selected', @centrepointselfun
		@centrepoint.fabric.on 'modified', =>
			@canvas.fabric.trigger 'object:modified', {target: @rect}
			@canvas.fabric.setActiveObject @rect
	cancel_create: ->
	set_to_mouse: ->
		ptr = @canvas.ptr
		if not (@rect and ptr and ptr.x and ptr.y)
			return
		halfw = @rect.width / 2
		halfh = @rect.height / 2
		setx = ptr.x - halfw
		sety = ptr.y - halfh
		maxx = @canvas.getwidth() - @rect.width
		maxy = @canvas.getheight() - @rect.height
		if setx < 0
			setx = 0
		else if setx > maxx
			setx = maxx
		if sety < 0
			sety = 0
		else if sety > maxy
			sety = maxy
		@rect.set {
			left: setx
			top: sety
		}
		@rect.setCoords()
		cx = ptr.x - @offset[0]
		cy = ptr.y - @offset[1]
		@centrepoint.setpos cx, cy
		@draw_centrepoint_line @colour
		@centrepoint_visibility false
	get_bounds: ->
		ax = @rect.left
		ay = @rect.top
		bx = ax + @rect.width
		_by = ay + @rect.height
		[
			[ax, ay],
			[bx, ay],
			[bx, _by],
			[ax, _by]
		]
	rectmovefun: =>
		console.debug 'RectWithCentrepoint moving'
		@draw_centrepoint_line()
	rectselfun: =>
		console.debug 'RectWithCentrepoint selected'
		# Centrepoint is hidden unless rect is selected
		@centrepoint_visibility true
		@draw_centrepoint_line()
	centrepointselfun: =>
		console.debug 'Centrepoint selected'
		# Function for when the centrepoint is deselected
		dfun = =>
			console.debug 'Centrepoint deselected. \
				Reselecting RectWithCentrepoint'
			@canvas.fabric.setActiveObject @rect
			@canvas.fabric.trigger 'object:selected', {target: @rect}
			@canvas.fabric.trigger 'object:modified', {target: @rect}
			@canvas.fabric.renderAll()
			@centrepoint.fabric.off 'deselected', dfun
		@centrepoint.fabric.on 'deselected', dfun
	rectdeselfun: =>
		console.debug 'RectWithCentrepoint deselected'
		# What is the new selection?
		newsel = @canvas.fabric.getActiveObject()
		if newsel isnt @centrepoint.fabric
			@centrepoint_visibility false
	is_component: (obj) ->
		return obj is @rect or @centrepoint.is_component obj
	remove: ->
		console.debug 'Removing RectWithCentrepoint'
		@canvas.fabric.remove @rect
		@canvas.fabric.remove @centrepoint.fabric
		@canvas.fabric.remove @line
		return null
	set_coords: ->
		@rect.setCoords()
		@centrepoint.set_coords()
		@line.setCoords()
		return true
	bringToFront: ->
		@rect.bringToFront()
		@centrepoint.fabric.bringToFront()
		if @line
			@line.bringToFront()
	draw_centrepoint_line: (colour = undefined, width = 2) ->
		console.debug 'Drawing line from centrepoint to rect'
		visible = false
		if not colour
			if not @line
				return null
			colour = @line.stroke
		if @line
			visible = @line.visible
			@canvas.fabric.remove @line
		console.debug this, @centrepoint
		cpx = @centrepoint.getx() + @centrepoint.offset[0]
		cpy = @centrepoint.gety() + @centrepoint.offset[1]
		rx = @rect.left + @rect.getScaledWidth() / 2
		ry = @rect.top + @rect.getScaledHeight() / 2
		coords = [cpx, cpy, rx, ry]
		minx = if cpx < rx then cpx else rx
		miny = if cpy < ry then cpy else ry
		@line = new fabric.Line [cpx, cpy, rx, ry], {
			stroke: colour
			hasBorders: false
			hasControls: false
			evented: false
			selectable: false
			originX: 'left'
			originY: 'top'
			top: miny
			left: minx
			strokeWidth: width
			visible: visible
		}
		console.debug 'Drawing line of coords: ', coords, 'at', [minx, miny]
		@canvas.fabric.add @line
	centrepoint_visibility: (visible) ->
		console.debug 'Setting centrepoint visibility to: ', visible
		@centrepoint.visibility visible
		if @line
			@line.visible = visible
		movefn = =>
			console.debug 'Rect centrepoint moving'
			@draw_centrepoint_line()
		if visible
			@centrepoint.fabric.on 'moving', movefn
			@draw_centrepoint_line()
		else
			@centrepoint.fabric.off 'moving', movefn
	visibility: (visible) ->
		@rect.visible = visible
		@centrepoint.visibility false
		@line.visible = false
	set_offset: (x, y) ->
		oldx = @offset[0]
		oldy = @offset[1]
		@offset[0] = x
		@offset[1] = y
		dx = x - oldx
		dy = y - oldy
		@rect.left += dx
		@rect.top += dy
		@centrepoint.set_offset x, y
		@rect.setCoords()
		console.debug 'Setting RectWithCentrepoint offsets by ', [dx, dy]
	set_colour: (colour, force = false) ->
		if colour is @colour and not force
			return false
		@colour = colour
		console.debug 'Setting RectWithCentrepoint colour to: ', colour
		@rect.set 'stroke', @colour
		@centrepoint.set_fill @colour
		@draw_centrepoint_line @colour
		return true
window.Candr.RectWithCentrepoint = RectWithCentrepoint
