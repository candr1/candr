class EditorialAccidental extends window.Candr.EditorialClefLike
	@accidental_type = null
	@flat_path = null
	@sharp_path = null
	constructor: (accidental_type, canvas, editor, colour = 'rgba(0,0,0,1)') ->
		flat_path = 'M0 0H0.43V9.91L0 9.96ZM0.43 5.48C1.05 4.51 2.48 4.\
27 3.24 5.21 4.59 6.7 1.62 9.61 0.43 9.91m0-0.38C1.16 9.53 3.13 6.75 2.19 5.62 \
1.51 4.81 0.43 5.56 0.43 6.08'
		sharp_path = 'M0.97 12.66H1.4V0.65H0.97ZM3.13 11.99H3.56V0H3.13\
ZM0 3.86V5.43L4.56 4.13V2.56ZM0 8.53v1.57L4.56 8.77V7.23'
		fl = (accidental_type.charAt 0).toUpperCase()
		ex = (accidental_type.slice 1).toLowerCase()
		accidental_type = fl + ex
		path = switch accidental_type
			when 'Flat' then flat_path
			when 'Sharp' then sharp_path
			else ''
		super path, canvas, editor, colour
		if not (['Sharp'].includes accidental_type)
			@accidental_type = 'Flat'
		else
			@accidental_type = accidental_type
		@flat_path = flat_path
		@sharp_path = sharp_path
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: =>
					{
						name: 'Editorial' + @accidental_type
					}
				set: (dat) ->
				type: {
					name: 'label'
				}
			}
			position: {
				get: =>
					{
						top: ((@rect.top - @offset[1]) * zoom)
						left: ((@rect.left - @offset[0]) * zoom)
						scale: (@rect.scaleX * zoom)
					}
				set: (dat) =>
					border = -@rect.strokeWidth
					s = {
						top: ((dat.top / zoom) + @offset[1])
						left: ((dat.left / zoom) + @offset[0])
						scaleX: (dat.scale / zoom)
						scaleY: (dat.scale / zoom)
					}
					@rect.set s
					@rect.setCoords()
				type: {
					top: 'int'
					left: 'int'
					scale: 'float'
				}
			}
			centrepoint: {
				get: =>
					{
						x: (@centrepoint.getx() * zoom)
						y: (@centrepoint.gety() * zoom)
					}
				set: (dat) =>
					@centrepoint.setpos (dat.x / zoom), (dat.y / zoom)
				type: {
					x: 'int'
					y: 'int'
				}
			}
			type: {
				get: =>
					{
						type: @accidental_type
					}
				set: (dat) =>
					fl = (@accidental_type.charAt 0).toUpperCase()
					ex = (@accidental_type.slice 1).toLowerCase()
					accidental_type = fl + ex
					@set_path (switch accidental_type
						when 'Sharp' then @sharp_path
						else @flat_path)
					if not (['Sharp'].includes accidental_type)
						@accidental_type = 'Flat'
					else
						@accidental_type = accidental_type
				type: {
					type: 'label'
				}
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.EditorialAccidental = EditorialAccidental
