class BackgroundImage
	@canvas = null
	@fabric = null
	@bg_items = null
	@zoom = 1
	constructor: (canvas, url, callback = null, rect = null, zoom = 1) ->
		console.debug 'Creating new background image', zoom
		@zoom = zoom
		@rect = rect
		@fabric = new fabric.Image.fromURL url, (i) =>
			console.debug 'Image loaded callback'
			if not @rect
				@rect = new window.Candr.Quad [
					[0, 0],
					[i.width, 0],
					[i.width, i.height]
					[0, i.height]
				]
			@i = i
			console.log 'Rect:', @rect
			console.log 'Zoom:', zoom
			canvas.setdimensions (@rect.width / zoom), (@rect.height / zoom)
			canvas.setbackgroundimage i, (-@rect.x / zoom), (-@rect.y / zoom), zoom
			if callback
				callback()
		, {selectable: false, evented: false}
		@canvas = canvas
		return this
	visible: (visible) ->
		if not visible
			@canvas?.fabric?.backgroundImage?.opacity = 0
		else
			@canvas?.fabric?.backgroundImage?.opacity = 1
	get_pos: ->
		dim = [@canvas.getwidth() * @zoom, @canvas.getheight() * @zoom]
		xy = @canvas.getbackgroundimagepos()
		return {
			"dim": dim
			"xy": xy
		}
	set_pos: (it) ->
		@canvas.setdimensions it.dim[0], it.dim[1]
		@canvas.setbackgroundimagepos it.xy[0], it.xy[1]
	redraw: ->
		@canvas.setbackgroundimage @i, (-@rect.x / @zoom), (-@rect.y / @zoom), @zoom
window.Candr.BackgroundImage = BackgroundImage
