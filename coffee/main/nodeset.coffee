class NodeSet
	@id = null
	@fill = null
	@nodes = []
	@hyperlink = null
	@canvas = null
	@offset = null
	@pos = 1
	constructor: (id, canvas, mid_image, selectable, hyperlink = null,
	mid_hyperlink = '', pos = 1, up_hyperlink = '', down_hyperlink = '',
	fill = 'rgba(102,153,255,0.5)') ->
		@id = id
		@pos = pos
		@fill = fill
		@nodes = []
		@offset = [0, 0]
		@hyperlink = hyperlink
		@mid_hyperlink = mid_hyperlink
		@up_hyperlink = up_hyperlink
		@down_hyperlink = down_hyperlink
		@canvas = canvas
		@selectable = selectable
		@mid_image = mid_image
	get_nodes: (zoom = 1) ->
		nodes = []
		for node in @nodes
			x = Math.round (node.getx() * zoom)
			y = Math.round (node.gety() * zoom)
			nodes.push [x, y]
		return [@id, nodes]
	add: (node, canvas) ->
		@nodes.push node
	add_to_canvas: (canvas) ->
		for node in @nodes
			canvas.add node
	set_fill: (fill) ->
		@fill = fill
		for node in @nodes
			node.set_fill fill
	translate: (x, y) ->
		for node in @nodes
			newx = node.getx() + x
			newy = node.gety() + y
			node.setpos newx, newy
	rad2deg: (r) ->
		r * (180 / (Math.PI))
	set_offset: (x, y) ->
		@offset[0] = x
		@offset[1] = y
		for node in @nodes
			node.set_offset x, y
		@connect_polygon()
	connect_polygon: ->
		console.debug 'Connecting polygon'
		console.debug 'Sorting nodes'
		@nodes.sort (b, a) ->
			cmp = b.gety() - a.gety()
			if cmp isnt 0
				return cmp
			cmp = b.getx() - a.getx()
			return cmp
		console.debug 'Calculating polar coordinates of nodes'
		fn = @nodes.pop()
		fx = fn.getx()
		fy = fn.gety()
		for node in @nodes
			ax = node.getx() - fx
			ay = node.gety() - fy
			node.angle = Math.atan2 ay, ax
			if node.angle < 0
				node.angle += Math.PI * 2
			node.scalar = ax * ax + ay * ay
		console.debug 'Sorting nodes by polar coordinate'
		@nodes.sort (a, b) ->
			ret = 0
			if a.angle > b.angle
				ret = -1
			else if a.angle < b.angle
				ret = 1
			else if a.scalar > b.scalar
				ret = -1
			else if a.scalar < b.scalar
				ret = 1
			return ret
		@nodes.push fn
		console.debug 'Finding top line and creating coordinate output'
		coords = []
		mincoords = (new Array 2).fill {x: Infinity, y: Infinity}
		min = {x: Infinity, y: Infinity}
		for node in @nodes
			coord = {
				x: (node.getx() + @offset[0]),
				y: (node.gety() + @offset[1]),
				angle: (@rad2deg node.angle),
				scalar: node.scalar
			}
			if coord.x < min.x
				min.x = coord.x
			if coord.y < min.y
				min.y = coord.y
			coords.push coord
			mincoords.push coord
			mincoords.sort (b, a) ->
				cmp = b.y - a.y
				if cmp isnt 0
					return cmp
				cmp = b.x - a.x
				return cmp
			mincoords.pop()
		centroid = @centroid coords
		console.debug 'Updating polygon'
		@canvas.fabric.remove @polygon
		@polygon = new fabric.Polygon coords, {
			left: min.x
			top: min.y
			fill: @fill
			selectable: false
			hoverCursor: 'default'
		}
		@canvas.fabric.add @polygon
		console.debug 'Calculating midpoint of top line'
		midpoint = [
			(mincoords[0].x + mincoords[1].x) / 2,
			(mincoords[0].y + mincoords[1].y) / 2
		]
		console.debug 'Updating midpoint image'
		if @midpoint and @midpoint.setCoords
			@midpoint.set {
				left: midpoint[0]
				top: midpoint[1]
			}
			@midpoint.setCoords()
			@midpoint.bringToFront()
		console.debug 'Adding central text'
		@canvas.fabric.remove @central_text
		@central_text = new fabric.Text ('' + @pos), {
			left: centroid.x
			top: centroid.y
			fill: 'white'
			originX: 'center'
			originY: 'center'
			fontWeight: 800
			fontSize: 48
			selectable: false
			evented: false
		}
		@canvas.fabric.add @central_text
		@central_text.bringToFront()
		no_controls = {
			tl: false
			tr: false
			br: false
			bl: false
			ml: false
			mt: false
			mr: false
			mb: false
			mtr: false
		}
		arrow_pad = @central_text.height / 2
		if @up_hyperlink
			@canvas.fabric.remove @up_arrow
			@up_arrow = new fabric.Triangle {
				left: centroid.x
				top: (centroid.y - arrow_pad)
				fill: 'white'
				originX: 'center'
				originY: 'bottom'
				width: 40
				height: 34.64
				angle: 0
				selectable: true
				hoverCursor: 'pointer'
				transparentCorners: false
				lockMovementX: true
				lockMovementY: true
				lockRotation: true
				lockScalingFlip: true
				lockScalingX: true
				lockScalingY: true
				lockSkewingX: true
				lockSkewingY: true
			}
			@up_arrow.setControlsVisibility no_controls
			@up_arrow.on 'selected', =>
				console.log 'Up arrow selected ' + @up_hyperlink
				window.location.assign @up_hyperlink
			@canvas.fabric.add @up_arrow
		if @down_hyperlink
			@canvas.fabric.remove @down_arrow
			@down_arrow = new fabric.Triangle {
				left: centroid.x
				top: (centroid.y + @central_text.height / 2 + arrow_pad)
				fill: 'white'
				originX: 'center'
				originY: 'top'
				width: 40
				height: 34.64
				angle: 180
				selectable: true
				hoverCursor: 'pointer'
				transparentCorners: false
				lockMovementX: true
				lockMovementY: true
				lockRotation: true
				lockScalingFlip: true
				lockScalingX: true
				lockScalingY: true
				lockSkewingX: true
				lockSkewingY: true
			}
			@down_arrow.setControlsVisibility no_controls
			@down_arrow.on 'selected', =>
				console.log 'Down arrow selected ' + @down_hyperlink
				window.location.assign @down_hyperlink
			@canvas.fabric.add @down_arrow
		for node in @nodes
			if node.fabric.canvas
				node.fabric.bringToFront()
		@canvas.fabric.requestRenderAll()
	centroid: (pts) ->
		first = pts[0]
		last = pts[pts.length - 1]
		if first.x isnt last.x or first.y isnt last.y
			pts.push first
		twicearea = 0
		x = 0
		y = 0
		nPts = pts.length
		j = nPts - 1
		for i in [0...nPts]
			p1 = pts[i]
			p2 = pts[j]
			f = (p1.y - first.y) * (p2.x - first.x) - (p2.y -
				first.y) * (p1.x - first.x)
			twicearea += f
			x += (p1.x + p2.x - 2 * first.x) * f
			y += (p1.y + p2.y - 2 * first.y) * f
			j = i
		f = twicearea * 3
		return {
			x: (x / f + first.x)
			y: (y / f + first.y)
		}
	create_polygon: =>
		@polygon = new fabric.Polygon [], {
			left: 0
			top: 0
			fill: @fill
			selectable: false
			hoverCursor: 'default'
		}
		if @mid_image
			midpoint = new fabric.Image.fromURL @mid_image, (i) =>
				console.debug 'Midpoint image loaded'
				console.debug 'Adding image to canvas'
				box = new fabric.Rect {
					fill: 'rgba(0,0,0,0)'
					width: (i.width * @selectable)
					height: (i.height * @selectable)
					hasBorders: false
					hasControls: false
					originX: 'center'
					originY: 'center'
					selectable: false
				}
				@midpoint = new fabric.Group [box, i], {
					hasBorders: true
					hasControls: false
					originX: 'center'
					originY: 'center'
					selectable: false
					hoverCursor: 'pointer'
				}
				@canvas.fabric.add @midpoint
				@connect_polygon()
			, {
				hasBorders: false
				hasControls: false
				selectable: false
				originX: 'center'
				originY: 'center'
			}
		console.debug 'Adding polygon to canvas'
		@canvas.fabric.add @polygon
		@connect_polygon()
	boundingrectangle: (points) ->
		minx = miny = Infinity
		maxx = maxy = -Infinity
		for point in points
			if point[0] < minx
				minx = point[0]
			if point[1] < miny
				miny = point[1]
			if point[0] > maxx
				maxx = point[0]
			if point[1] > maxy
				maxy = point[1]
		[[minx, miny], [maxx, miny], [minx, maxy], [maxx, maxy]]
	nodepoints: (scale, left, top) =>
		ret = []
		for node in @nodes
			nx = node.getx()
			ny = node.gety()
			dl = Math.round ((nx - left) / scale)
			dt = Math.round ((ny - top) / scale)
			ret.push [dl, dt]
		return ret
window.Candr.NodeSet = NodeSet
