class EditorialText
	@canvas = null
	@path = null
	@offset = null
	@in_progress = null
	@assocnode = null
	@colour = null
	constructor: (canvas, editor, colour = 'rgba(0,0,0,1)') ->
		console.debug 'EditorialText constructor'
		@canvas = canvas
		@offset = [0, 0]
		@colour = colour
		@in_progress = false
		p = new fabric.Path 'm 11.5,3.06 c 0.08,0.86 0.11,1.46 0.11\
,2.44 h 0.47 l 0.06,-1.2 c 0.02,-0.36 0.18,-0.5 0.61,-0.5 h 2.35 c 0.05,0.52 0.\
06,0.86 0.06,1.52 v 6.42 c 0,1.2 -0.08,1.32 -0.77,1.37 l -0.73,0.05 v 0.46 c 0.\
74,-0.03 1.47,-0.05 2.21,-0.05 0.74,0 1.49,0.02 2.23,0.05 V 13.15 L 17.37,13.11
C 16.67,13.06 16.6,12.94 16.6,11.74 V 5.32 c 0,-0.68 0.02,-1.02 0.06,-1.52 h 2.\
35 c 0.42,0 0.59,0.14 0.61,0.5 l 0.06,1.2 h 0.47 c 0,-0.99 0.03,-1.58 0.11,-2.4\
4 l -3.28,0.05 h -2.4 z'
		p.set {
			fill: @colour
			top: 0
			left: 2.3
		}
		t = new fabric.Triangle {
			width: 2
			height: 2
			angle: -45
			top: 0
			left: 0
			fill: @colour
		}
		@path = new fabric.Group [p, t]
		@path.set {
			left: 0
			top: 0
			originX: 'left'
			originY: 'top'
			lockUniScaling: true
			lockSkewingX: true
			lockSkewingY: true
			lockRotation: true
			noScaleCache: false
		}
		@path.setControlsVisibility {
			bl: true
			br: true
			mb: false
			ml: false
			mr: false
			mt: false
			tl: true
			tr: true
			mtr: false
		}
		@path.scaleToHeight Math.max (@canvas.fabric.height / 4), 32
		@path.on 'deselected', ->
			editor.save_optionspane()
		@canvas.fabric.add @path
		@canvas.fabric.trigger 'object:modified', {
			target: @path
		}
	get_bounds: ->
		ax = @path.left
		ay = @path.top
		bx = ax + @path.width
		_by = ay + @path.height
		[
			[ax, ay],
			[bx, ay],
			[bx, _by],
			[ax, _by]
		]
	is_component: (obj) ->
		return obj is @path or (@path.contains obj)
	remove: ->
		console.debug 'Removing EditorialText'
		@canvas.fabric.remove @path
		return null
	visibility: (visible) ->
		@path.visible = visible
	set_offset: (x, y) ->
		oldx = @offset[0]
		oldy = @offset[1]
		@offset[0] = x
		@offset[1] = y
		dx = x - oldx
		dy = y - oldy
		@path.left += dx
		@path.top += dy
		@path.setCoords()
	set_colour: (colour, force = false) ->
		console.debug 'Setting EditorialText colour to: ', colour
		if colour is @colour and not force
			return false
		@colour = colour
		@path.forEachObject (e) ->
			e.set 'fill', @colour
		return true
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: ->
					{
						name: 'EditorialText'
					}
				set: (dat) ->
				type: {
					name: 'label'
				}
			}
			position: {
				get: =>
					{
						top: ((@rect.top - @offset[1]) * zoom)
						left: ((@rect.left - @offset[0]) * zoom)
						width: (@rect.getScaledWidth() * zoom)
						height: (@rect.getScaledHeight() * zoom)
						angle: @rect.angle
					}
				set: (dat) =>
					border = -@rect.strokeWidth
					s = {
						top: ((dat.top / zoom) + @offset[1])
						left: ((dat.left / zoom) + @offset[0])
						angle: dat.angle
						width: ((dat.width / zoom) + border)
						height: ((dat.height / zoom) + border)
						scaleX: 1
						scaleY: 1
					}
					@rect.set s
					@rect.setCoords()
				type: {
					top: 'int'
					left: 'int'
					scale: 'float'
				}
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.EditorialText = EditorialText
