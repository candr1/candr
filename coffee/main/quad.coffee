# Container for 4 points with useful x, y, width and height properties
class Quad
	@nodes = null
	@width = 0
	@height = 0
	@x = 0
	@y = 0
	constructor: (nodes) ->
		if nodes.length isnt 4
			console.error "4 points are required"
			return null
		@nodes = nodes
		minx = miny = Infinity
		maxx = maxy = -Infinity
		for node in @nodes
			if node[0] < minx
				minx = node[0]
			if node[0] > maxx
				maxx = node[0]
			if node[1] < miny
				miny = node[1]
			if node[1] > maxy
				maxy = node[1]
		@width = maxx - minx
		@height = maxy - miny
		@x = minx
		@y = miny
window.Candr.Quad = Quad
