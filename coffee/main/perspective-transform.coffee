# Porting jlouthan/perspective-transform
class PerspectiveTransform
	@grid = null
	constructor: (srcPts, dstPts) ->
		r1 = [
			srcPts[0], srcPts[1], 1,
			0, 0, 0,
			-1 * dstPts[0] * srcPts[0], -1 * dstPts[0] * srcPts[1]
		]
		r2 = [
			0, 0, 0,
			srcPts[0], srcPts[1], 1,
			-1 * dstPts[1] * srcPts[0], -1 * dstPts[1] * srcPts[1]
		]
		r3 = [
			srcPts[2], srcPts[3], 1,
			0, 0, 0,
			-1 * dstPts[2] * srcPts[2], -1 * dstPts[2] * srcPts[3]
		]
		r4 = [
			0, 0, 0,
			srcPts[2], srcPts[3], 1,
			-1 * dstPts[3] * srcPts[2], -1 * dstPts[3] * srcPts[3]
		]
		r5 = [
			srcPts[4], srcPts[5], 1,
			0, 0, 0, -1 * dstPts[4] * srcPts[4], -1 * dstPts[4] * srcPts[5]
		]
		r6 = [
			0, 0, 0,
			srcPts[4], srcPts[5], 1,
			-1 * dstPts[5] * srcPts[4], -1 * dstPts[5] * srcPts[5]
		]
		r7 = [
			srcPts[6], srcPts[7], 1,
			0, 0, 0,
			-1 * dstPts[6] * srcPts[6], -1 * dstPts[6] * srcPts[7]
		]
		r8 = [
			0, 0, 0,
			srcPts[6], srcPts[7], 1,
			-1 * dstPts[7] * srcPts[6], -1 * dstPts[7] * srcPts[7]
		]
		matA = [r1, r2, r3, r4, r5, r6, r7, r8]
		matATranspose = numeric.transpose matA
		matB = dstPts
		try
			matC = numeric.inv (numeric.dotMMsmall matATranspose, matA)
		catch e
			return [1, 0, 0, 0, 1, 0, 0, 0]
		matD = numeric.dotMMsmall matC, matATranspose
		matX = numeric.dotMV matD, matB
		matX[8] = 1
		@grid = matX
	transform: (x, y) ->
		coordinates = []
		d = @grid[6] * x + @grid[7] * y + 1
		coordinates[0] = (@grid[0] * x + @grid[1] * y + @grid[2]) / d
		coordinates[1] = (@grid[3] * x + @grid[4] * y + @grid[5]) / d
		return coordinates
window.Candr.PerspectiveTransform = PerspectiveTransform
