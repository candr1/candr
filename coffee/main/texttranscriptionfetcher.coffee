class TextTranscriptionFetcher
	@url = null
	@output = null
	constructor: (url, output) ->
		@url = url
		@output = u(output)
	fetch: ->
		loader = u '<span>'
			.html "Loading text transcription..."
		@output.empty().append(loader)
		fetch @url, {
			mode: 'same-origin',
			method: 'GET',
			credentials: 'same-origin',
		}
			.then ((response) ->
				if not response.ok
					throw new Error ("HTTP error " + response.status)
				return response.text()
			)
			.then ((text) ->
				patt = /^[0-9]+\./gm
				return text.replace patt, '<b>$&</b>'
			)
			.then ((text) =>
				@output.empty().html(text)
			)
window.Candr.TextTranscriptionFetcher = TextTranscriptionFetcher
