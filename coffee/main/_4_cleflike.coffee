class ClefLike extends window.Candr.RectWithCentrepoint
	@guidelines = null
	constructor: (canvas, editor, colour = 'rgba(102,153,255,1)', stroke = 5) ->
		super canvas, editor, colour, stroke
		@guidelines = []
	rectmovefun: =>
		super()
		console.debug 'ClefLike moving'
		cpx = @centrepoint.getx() + @centrepoint.offset[0]
		cpy = @centrepoint.gety() + @centrepoint.offset[1]
		@highlight_closest_guideline cpx, cpy
	rectselfun: =>
		super()
		console.debug 'ClefLike selected'
		# the stafflines will just get in the way
		@editor.view_stafflines false
		# remove old guidelines
		for line in @guidelines
			@canvas.fabric.remove line
		@guidelines = []
		# Start and end points of each staffline
		stafflines = @editor.get_staffline_points()
		# The midpoints of each staffline set
		midlines = @get_centrelines stafflines
		console.debug 'Staffline points:', lines, midlines
		lines = stafflines.concat midlines
		for line in lines
			for point in line
				point[0] += @editor.offset[0]
				point[1] += @editor.offset[1]
			# Extend each line to the extent of the canvas
			gl = @make_infinite_guideline line[0], line[1]
			@canvas.fabric.add gl
			@guidelines.push gl
		# Sort guidelines by their average y-coordinate
		@guidelines.sort (a, b) ->
			pa = a.originalSegment
			pb = b.originalSegment
			cavy = (p) ->
				avy = 0
				for i in [1...p.length] by 2
					avy += p[i]
				return avy / p.length
			pavy = cavy pa
			pbvy = cavy pb
			return pbvy - pavy
		console.debug 'Made lines:', @guidelines
		@bringToFront()
		@rectmovefun()
		@canvas.fabric.renderAll()
	deselect_fun: ->
		for line in @guidelines
			@canvas.fabric.remove line
		@guidelines = []
	rectdeselfun: =>
		super()
		console.debug 'ClefLike deselected'
		newsel = @canvas.fabric.getActiveObject()
		if newsel isnt @centrepoint.fabric
			# Remove guidelines
			for line in @guidelines
				@canvas.fabric.remove line
			# Should stafflines be visible?
			if @editor.checkboxes.hasOwnProperty 'stafflines'
				vis = @editor.checkboxes['stafflines'].checked
				@editor.view_stafflines vis
			@canvas.fabric.renderAll()
			return @guidelines = []
	remove: ->
		console.debug 'Removing cleflike'
		super()
		for line in @guidelines
			@canvas.fabric.remove line
		return null
	set_coords: ->
		super()
		for line in @guidelines
			line.setCoords()
		return true
	centrepoint_visibility: (visible) ->
		super visible
		# rehighlight guidelines on centrepoint move
		movehfn = =>
			cpx = @centrepoint.getx() + @centrepoint.offset[0]
			cpy = @centrepoint.gety() + @centrepoint.offset[1]
			@highlight_closest_guideline cpx, cpy
		if visible
			@centrepoint.fabric.on 'moving', movehfn
		else
			@centrepoint.fabric.off 'moving', movehfn
	highlight_closest_guideline: (x, y, colour = 'rgba(0,255,0,1)', shift = 0) ->
		if not @guidelines
			return null
		# idk how this works but it does, can't remember where I copied
		# it from whoops but ¯\_(ツ)_/¯
		distToSeg = (px, py, sax, say, sbx, sby) ->
			dist = (pax, pay, pbx, pby) ->
				dx = pbx - pax
				dy = pby - pay
				return Math.sqrt (dx * dx + dy * dy)
			dx = sbx - sax
			dy = sby - say
			l2 = dx * dx + dy * dy
			if l2 is 0
				return dist px, py, sax, say
			t = ((px - sax) * dx + (py - say) * dy) / l2
			t = Math.max 0, (Math.min 1, t)
			return dist px, py, (sax + t * dx), (say + t * dy)
		# find the minimum distance to a line
		mindist = Infinity
		closest = null
		for i in [0...@guidelines.length]
			guideline = @guidelines[i]
			os = guideline.originalSegment
			d = distToSeg x, y, os[0], os[1], os[2], os[3]
			if d < mindist
				mindist = d
				closest = i
		if closest is null
			return null
		# apply shift of guidelines if note is shifted by steps
		closest += shift
		if closest >= @guidelines.length
			closest = @guidelines.length - 1
		if closest < 0
			closest = 0
		closest = @guidelines[closest]
		# recolour guideline
		closest.set 'oldStroke', closest.stroke
		closest.set 'stroke', colour
		closest.set 'oldDashArray', closest.strokeDashArray
		closest.set 'strokeDashArray', null
		# reset colour for other guidelines
		for guideline in @guidelines
			if guideline is closest
				continue
			if guideline.oldDashArray
				guideline.set 'strokeDashArray', guideline.oldDashArray
			else
				guideline.set 'strokeDashArray', [5, 5]
			if guideline.oldStroke
				guideline.set 'stroke', guideline.oldStroke
			else
				guideline.set 'stroke', 'rgba(0,255,0,0.5)'
		@canvas.fabric.renderAll()
	make_infinite_guideline: (pa, pb, width = 2, colour = 'rgba(0,255,0,0.5)') ->
		console.debug 'Making infinite guideline passing through points\
			:', pa, pb
		# Calculate straight line equation for two points
		dy = pb[1] - pa[1]
		dx = pb[0] - pa[0]
		if dx is 0
			m = Infinity
		else if dy is 0
			m = 0
		else
			m = dy / dx
		# c = y - mx
		if pb[0] is 0
			c = pa[1] - m * pa[0]
		else
			c = pb[1] - m * pb[0]
		# Calculate points on that line at x = 0 and x = width
		maxx = @canvas.getwidth()
		maxy = @canvas.getheight()
		miny = if pa[1] < pb[1] then pa[1] else pb[1]
		if m is Infinity
			ppa = [0, miny]
			ppb = [maxx, maxy]
		else
			ppa = [0, m + c] # point when x = 0
			ppb = [maxx, m * maxx + c] # point when x = width
		console.debug 'Infinite guideline also passes through points:',
		ppa, ppb
		coords = ppa.concat ppb
		return new fabric.Line coords, {
			stroke: colour
			strokeDashArray: [5, 5]
			hasBorders: false
			hasControls: false
			evented: false
			selectable: false
			originX: 'left'
			originY: 'top'
			top: miny
			left: 0
			strokeWidth: width
			originalSegment: coords
		}
	get_centrelines: (lines) ->
		ret = []
		if lines.length < 2
			return ret
		# sort lines by their average y-coord
		lines.sort (a, b) ->
			get_mean_y = (line) ->
				cum = 0
				for point in line
					cum += point[1]
				return (cum / line.length)
			return ((get_mean_y b) - (get_mean_y a))
		# sort points in each line
		for line in lines
			line.sort (a, b) -> a[0] - b[0]
		get_midpoint = (a, b) -> [(a[0] + b[0]) / 2, (a[1] + b[1]) / 2]
		cumdy = 0
		div = (lines.length - 1) * 4
		for i in [1...lines.length]
			a = lines[i - 1]
			b = lines[i]
			paa = a[0]
			pab = a[1]
			pba = b[0]
			pbb = b[1]
			mida = get_midpoint paa, pba
			midb = get_midpoint pab, pbb
			cumdy += Math.abs (pba[1] - paa[1])
			cumdy += Math.abs (pbb[1] - pab[1])
			ret.push [mida, midb]
		havdy = cumdy / div
		fl = lines[0]
		ll = lines[lines.length - 1]
		fla = fl[0]
		flb = fl[1]
		lla = ll[0]
		llb = ll[1]
		ret.push [[fla[0], fla[1] + havdy], [flb[0], flb[1] + havdy]]
		ret.push [[lla[0], lla[1] - havdy], [llb[0], llb[1] - havdy]]
		return ret
window.Candr.ClefLike = ClefLike
