class NodeEdit
	@canvas = null
	@nodes = null
	@im = null
	@offset = null
	@editable = null
	constructor: (canvasid, url, warpnodes, editable = true, zoom = 1) ->
		@canvas = new window.Candr.Canvas canvasid
		@nodes = []
		@zoom = zoom
		@offset = [0, 0]
		@editable = editable
		console.debug 'Adding image'
		@canvas.set_info_text 'Loading image...'
		if warpnodes
			iw = new window.Candr.ImageWarp url, warpnodes, (i, src, rect) =>
				@im = new window.Candr.BackgroundImage @canvas, src,
					((i) =>
						@canvas.clear_info_text()
						oldjson = JSON.stringify @offset
						offset = JSON.parse(window.Candr.Cookie.get 'offset', oldjson)
						if window.Candr.correctOffsetCookie offset
							@alter_border offset[0]
						else
							window.Candr.Cookie.set 'offset', oldjson
					), rect, zoom
		else
			@im = new window.Candr.BackgroundImage @canvas, url, ((i) =>
				@canvas.clear_info_text()
			), null, zoom
		@canvas.fabric.on 'mouse:down', (opt) =>
			if not opt.target
				return
			for ns in @nodes
				if opt.target is ns.polygon
					console.debug 'Polygon clicked'
					if ns.hyperlink
						console.debug 'Going to href: ' + ns.hyperlink
						window.location.assign ns.hyperlink
					return
				if opt.target is ns.midpoint
					console.debug 'Midpoint clicked'
					if ns.mid_hyperlink
						console.debug 'Going to href: ' + ns.mid_hyperlink
						window.location.assign ns.mid_hyperlink
	increase_border: (px = 10) ->
		@alter_border px
	decrease_border: (px = 10) ->
		@alter_border -px
	alter_border: (amount) ->
		dim = [@canvas.getwidth(), @canvas.getheight()]
		dim[0] += amount * 2
		dim[1] += amount * 2
		@canvas.setdimensions dim[0], dim[1]
		xy = @canvas.getbackgroundimagepos()
		xy[0] += amount
		xy[1] += amount
		@offset[0] += amount
		@offset[1] += amount
		window.Candr.Cookie.set 'offset', (JSON.stringify @offset)
		@canvas.setbackgroundimagepos xy[0], xy[1]
		for node in @nodes
			node.set_offset @offset[0], @offset[1]
	get_nodes: ->
		nodes = {}
		for ns in @nodes
			ret = ns.get_nodes @zoom
			nodes[ret[0]] = ret[1]
		return nodes
	add_polygon: (id, nodes = null, hyperlink, del, delete_hyperlink = '',
	pos = 1, up_hyperlink = '', down_hyperlink = '') =>
		console.debug 'Adding polygon'
		hex2rgb = (h) ->
			r = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec h
			if not r
				return null
			return {
				r: (parseInt r[1], 16),
				g: (parseInt r[2], 16),
				b: (parseInt r[3], 16)
			}
		deleteicon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAE\
			AAAABAQMAAAAl21bKAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0\
			AcxV8/pCIVETuIOGSoDmJFVMRRq1CECqFWaNXB5NIvaNKQpLg4Cq4FB\
			z8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLGg+N+vLv3uHsH+OtlpprB\
			cUDVLCOViAuZ7KoQekUQYfRiDCMSM/U5UUzCc3zdw8fXuxjP8j735+h\
			WciYDfALxLNMNi3iDeHrT0jnvE0dYUVKIz4lHDbog8SPXZZffOBcc9v\
			PMiJFOzRNHiIVCG8ttzIqGSjxFHFVUjfL9GZcVzluc1XKVNe/JXxjOa\
			SvLXKc5iAQWsQQRAmRUUUIZFmK0aqSYSNF+3MM/4PhFcsnkKoGRYwEV\
			qJAcP/gf/O7WzE9OuEnhONDxYtsfQ0BoF2jUbPv72LYbJ0DgGbjSWv5\
			KHZj5JL3W0qJHQM82cHHd0uQ94HIH6H/SJUNypABNfz4PvJ/RN2WBvl\
			uga83trbmP0wcgTV0lb4CDQ2C4QNnrHu/ubO/t3zPN/n4AmDxytj8Sd\
			rUAAAADUExURQAAAKd6PdoAAAABdFJOUwBA5thmAAAAAWJLR0QAiAUd\
			SAAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+QEDBEyK6J/jjo\
			AAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAACk\
			lEQVQIHWNgAAAAAgABz8g15QAAAABJRU5ErkJggg=="
		# it will always be a mystery to me why this transparent 1x1
		# pixel PNG is a larger base64 string than the delete icon below
		if del
			deleteicon = "data:image/png;base64,iVBORw0KGgoAAAANSUh\
				EUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA8ABnAGcvOc\
				J8AAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4wsBD\
				xExmjPx7wAAAHpJREFUOMutk8ENgCAMRR9wcxgubugWbEPY\
				ApfBSzUIBhT8yUsI6acFWqhlAQdEIAlR9iwNLRKUOjiJrcz\
				hhfkklIe4D+a8kuvOaRBrgA1YGZOheO2vRCWLXKqT9RavmZ\
				QG9ocMLXLtGvATBfjpb/ylkX5p5elhGh7nA87ziI6v2UVQA\
				AAAAElFTkSuQmCC"
		ns = new window.Candr.NodeSet id, @canvas, deleteicon, 2,
		hyperlink, delete_hyperlink, pos, up_hyperlink, down_hyperlink
		r = 6
		for node in nodes
			if node.length isnt 2
				continue
			x = node[0] / @zoom
			y = node[1] / @zoom
			cn = (new window.Candr.Node 'rgba(102,153,255,1)', r).setpos x, y
			cn.fabric.on 'moving', ->
				ns.connect_polygon()
			ns.add cn
			if @editable
				@canvas.add cn
		ns.create_polygon()
		@nodes.push ns
		div = palette('tol-rainbow', @nodes.length)
		for i in [0...@nodes.length]
			rgb = hex2rgb div[i]
			@nodes[i].set_fill ('rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ',0.5)')
window.Candr.NodeEdit = NodeEdit
