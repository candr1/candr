class Editor
	@canvas = null
	@manuscript = null
	@items = null
	@offset = null
	@checkboxes = null
	@save_optionspane = null
	@optionspane = null
	@cover = null
	@colours = null
	@colourkey = null
	@backend = null
	@block_save_timerid = null
	@zoom = 1
	constructor: (edit, coverid, canvasid, endpoint, staveid, optionspaneid,
	colourkeyid, url, warpnodes, checkboxes, last_updated = 0, zoom = 1) ->
		@canvas = new window.Candr.Canvas canvasid
		@backend = new window.Candr.EditorBackend endpoint, staveid, edit, zoom
		@request_all()
		@items = []
		@colours = new Map()
		@zoom = zoom
		@offset = [0, 0]
		@checkboxes = checkboxes
		@canvas.set_info_text 'Loading image...'
		# Warp image
		iw = new window.Candr.ImageWarp url, warpnodes, (i, src, rect) =>
			@manuscript = new window.Candr.BackgroundImage @canvas, src,
				((i) =>
					@canvas.clear_info_text()
					oldjson = JSON.stringify @offset
					offset = JSON.parse(window.Candr.Cookie.get 'offset', oldjson)
					if window.Candr.correctOffsetCookie offset
						@alter_border offset[0]
					else
						window.Candr.Cookie.set 'offset', oldjson
				), rect, zoom
			, last_updated
		# Bind keyboard shortcuts
		(u document).on 'keydown', (e) =>
			sel = @canvas.fabric.getActiveObject()
			tag = e.target.tagName.toLowerCase()
			if tag is 'input' or tag is 'textarea'
				return
			console.debug 'Key pressed:', e.key
			# Find the object the selection relates to
			it = @find_object sel
			handled = false
			if it instanceof window.Candr.Note
				handled = (->
					switch e.key
						when 'ArrowUp', 'Up' then it.shift_up()
						when 'ArrowDown', 'Down' then it.shift_down()
						when '^' then it.plica_up()
						when 'v' then it.plica_down()
						else
							return false
					return true)()
			# Either not a note selected or wasn't one of the
			# note-specific shortcuts
			if not handled
				switch e.key
					when 'a' then (if edit then @add_accidental())
					when 'c' then (if edit then @add_clef())
					when 'd' then (if edit then @add_divisione())
					when 'g' then (if edit then @add_ligature())
					when 'l' then (if edit then @add_staffline())
					when 'p'
						if not edit
							break
						console.debug 'Ligature checkbox keypress'
						chop = @checkboxes.hasOwnProperty 'ligature'
						cboxstate = => @checkboxes['ligature'].checked
						setcbox = (s) => @checkboxes['ligature'].checked = s
						if chop
							if cboxstate() is false
								console.debug 'We have an unchecked ligature checkbox'
								setcbox true
								@append_to_ligature()
							else
								console.debug 'We have a checked ligature checkbox'
								setcbox false
								@cancel_in_progress()
					when 'm'
						console.debug 'Toggling multiinput'
						if @checkboxes.hasOwnProperty 'multiinput'
							c = @checkboxes['multiinput'].checked
							@checkboxes['multiinput'].checked = not c
					when 'n' then (if edit then @add_note())
					when 's' then (if edit then @add_syllable())
					when 'Delete' then (if edit then @delete_item())
					when '+' then @increase_border()
					when '-' then @decrease_border()
					when 'Tab'
						ta = it instanceof window.Candr.Syllable
						tb = it instanceof window.Candr.EditorialSyllable
						tc = it instanceof window.Candr.Ligature
						td = it instanceof window.Candr.Clef
						te = it instanceof window.Candr.Accidental
						tf = it instanceof window.Candr.EditorialAccidental
						if ta or tb
							txt = u('#text')?.first()
							txt?.focus()
							txt?.select()
						if tc or td or te or tf
							typ = u('#type')?.first()
							if not typ
								break
							typ.focus()
							idx = typ.selectedIndex + 1
							if idx >= typ.options.length
								idx = 0
							typ.selectedIndex = idx
							u(typ).trigger 'change'
			e.preventDefault()
		@canvas.fabric.on 'selection:created', (e) ->
			if e.target? and ((not edit) or e.target.type is 'activeSelection')
				e.target.set {
					hasControls: false
					hasRotatingPoint: false
					lockMovementX: true
					lockMovementY: true
					lockRotation: true
					lockScalingX: true
					lockScalingY: true
					lockSkewingX: true
					lockSkewingY: true
				}
		@canvas.fabric.on 'object:moving', (e) =>
			if not e.target?
				return
			@reset_block_save()
		if not edit
			unedit = (e) ->
				if not e.target?
					return
				e.target.set {
					hasControls: false
					hasRotatingPoint: false
					lockMovementX: true
					lockMovementY: true
					lockRotation: true
					lockScalingX: true
					lockScalingY: true
					lockSkewingX: true
					lockSkewingY: true
				}
			@canvas.fabric.on {
				'object:added': unedit
				'object:selected': unedit
				'object:modified': unedit
				'selection:created': unedit
			}
		@optionspane = u ('#' + optionspaneid)
		@colourkey = u ('#' + colourkeyid)
		@cover = u ('#' + coverid)
		@set_cover true
		# save what we had originally
		original_optionspane_html = @optionspane.clone()
		old_selected = null
		@save_optionspane = (e) =>
			# Saving the optionspane elements is as simple as
			# triggering their change and blur events
			oldels = @optionspane.find 'input, select'
			(oldels.trigger 'blur').trigger 'change'
		update_props_base = {
			# The base elements for each type
			html: (u '<div>')
			label: (u '<span>')
			strel: (u '<input type="text" style="width:8rem;">')
			intel: (u '<input type="number" style="width:3rem;">')
			floatel: (u '<input type="number" style="width:5rem;" m\
in="0.01" step=".01">')
			boolel: (u '<input type="checkbox">')
		}
		update_props = (e) =>
			html = update_props_base.html.clone()
			label = update_props_base.label.clone()
			strel = update_props_base.strel.clone()
			intel = update_props_base.intel.clone()
			floatel = update_props_base.floatel.clone()
			boolel = update_props_base.boolel.clone()
			console.debug 'Updating options pane'
			sel = e.target
			if not sel
				return
			item = @find_object sel
			if not item
				return
			props = item.properties @zoom
			if item instanceof window.Candr.Ligature
				console.debug 'Item is ligature'
				note = @find_object sel, item.get_notes()
				noteprops = note.properties @zoom
				props = Object.assign noteprops, props
			console.debug 'Props are:', props
			if (Object.keys props).length is 0
				console.debug 'Has no props'
				return
			if not edit
				its = [strel, intel, floatel, boolel]
				for it in its
					it.nodes[0].disabled = true
			for name, prop of props
				vals = prop.get()
				console.debug name, prop, 'vals are:', vals
				types = prop.type
				if not (types? and (Object.keys types).length > 0)
					continue
				# New group (a fieldset)
				group = u '<fieldset>'
				group.append ((u '<legend>').append (name + ':'))
				for propname, type of types
					val = vals[propname]
					# if 'type' is an array, then it must be
					# select
					if Array.isArray type
						el = u '<select>'
						for opt in type
							o = ((u '<option>').attr 'value', opt).append opt
							if opt is val
								o.nodes[0].selected = true
							el.append o
					else
						el = switch type
							when 'label' then (label.clone().append val)
							when 'string' then (strel.clone().attr 'value', val)
							when 'int' then (intel.clone().attr 'value', (parseInt val))
							when 'float' then (floatel.clone().attr 'value', (parseFloat val))
							when 'bool' then (->
								c = boolel.clone()
								c.nodes[0].checked = val
								return c
							)()
					el.attr 'id', propname
					# Wrapping the change in a
					# function-in-a-function so that the
					# prop gets copied so that it is
					# correctly referenced in the closure
					cbk = (p, s, i, parse) ->
						fun = ->
							vals = p.get()
							j = u this
							id = j.attr 'id'
							v = parse j.first().value
							vals[id] = v
							console.debug 'Setting: ', vals
							p.set vals
							s i
						return fun
					p = prop
					s = (i) => @make_save i
					i = item
					switch type
						when 'int'
							el.on "change", (cbk p, s, i, ((e) -> parseInt e, 10))
						when 'float'
							el.on "change", (cbk p, s, i, ((e) -> parseFloat e))
						when 'string'
							el.on "blur", (cbk p, s, i, ((e) -> e))
						when 'bool'
							el.on "change", ((p, s, i) -> (->
								vals = p.get()
								j = u this
								id = j.attr 'id'
								v = j.is ':checked'
								vals[id] = v
								console.debug 'Setting: ', vals
								p.set vals
								s i
							))(p, s, i)
						else
							el.on "change", (cbk p, s, i, ((e) -> e))
					propspan = (u '<span>').append propname
					w = (u '<label>').addClass "h-10"
					wrapped_el = (w.append propspan).append el
					group.append wrapped_el
				html.append group
			@optionspane.html ''
			@optionspane.append html
		@canvas.fabric.on 'object:selected', update_props
		@canvas.fabric.on 'object:modified', update_props
		@canvas.fabric.on 'selection:cleared', =>
			console.debug 'Optionspane:', @optionspane
			@optionspane.html ''
			@optionspane.append original_optionspane_html
		@canvas.fabric.on 'object:modified', (e) =>
			item = @find_object e.target
			@make_save item
	reset_block_save: ->
		clearTimeout @block_save_timerid
		@block_save_timerid = setTimeout =>
			@block_save_timerid = null
			@backend.unblock_save()
		, 5000
		@backend.block_save()
	set_cover: (toggle) ->
		if toggle is true
			@cover.addClass 'flex'
			@cover.removeClass 'hidden'
		else
			@cover.addClass 'hidden'
			@cover.removeClass 'flex'
	push_saves: (callback = ->) ->
		@set_cover true
		msg_callback = (msg) =>
			@canvas.set_info_text msg
		update_callback = (ret) =>
			@set_cover false
			callback ret
		@backend.push_saves msg_callback, update_callback
	make_save: (items...) ->
		if @backend.edit
			@canvas.set_info_text 'Unsaved changes'
		else
			@canvas.clear_info_text()
		msg_callback = (msg) =>
			@canvas.set_info_text msg
		update_callback = (update) =>
			@update_items update
		if @block_save_timerid
			@backend.block_save()
		else
			@backend.unblock_save()
		@backend.save_item msg_callback, update_callback, items...
	make_delete: (items...) ->
		if @backend.edit
			@canvas.set_info_text 'Unsaved changes'
		else
			@canvas.clear_info_text()
		msg_callback = (msg) =>
			@canvas.set_info_text msg
		update_callback = (update) =>
			@update_items update
		if @block_save_timerid
			@backend.block_save()
		else
			@backend.unblock_save()
		@backend.delete_item msg_callback, update_callback, items...
	add_predictions: ->
		if @items.length > 0
			alert 'Cannot add predictions when there are currently items on the stave'
			return false
		@canvas.clear_info_text()
		@set_cover true
		msg_callback = (msg) =>
			@canvas.set_info_text msg
		update_callback = (update) =>
			@set_cover true
			@update_items update, true
			@canvas.fabric.discardActiveObject()
			@set_cover false
		@backend.get_predictions msg_callback, update_callback
	request_all: ->
		@canvas.clear_info_text()
		msg_callback = (msg) =>
			@canvas.set_info_text msg
		update_callback = (update) =>
			@set_cover true
			@update_items update, true
			#@backend.empty_queue()
			@canvas.fabric.discardActiveObject()
			for item in @items
				if item.deselect_fun? and typeof item.deselect_fun is 'function'
					item.deselect_fun()
			@set_cover false
		@backend.request_all msg_callback, update_callback
	redraw: ->
		@set_cover true
		xy = @canvas.getbackgroundimagepos()
		@canvas.fabric.clear()
		@manuscript.redraw()
		@canvas.setbackgroundimagepos xy[0], xy[1]
		@items = []
		@request_all()
		@update_colours()
	update_items: (update, immediate = false) =>
		callback_fun = (f) -> f()
		if (not immediate) and ('requestIdleCallback' in window)
			callback_fun = (f) -> requestIdleCallback f
		console.debug 'Update items: ', update
		callback_fun =>
			# disable all events
			was_evented = []
			@canvas.fabric.forEachObject (e, i) ->
				if not e.evented?
					return
				if e.evented is false
					was_evented[i] = false
				else
					was_evented[i] = true
			updater = new window.Candr.EditorUpdater this, @items, @zoom
			for upd in update
				updater.apply_update upd
			@reset_block_save()
			@update_colours()
			# reenable events
			@canvas.fabric.forEachObject (e, i) ->
				if not (e.evented? and was_evented[i]?)
					return
				e.evented = was_evented[i]
			for cname, cbox of @checkboxes
				fun = switch cname
					when 'facsimile' then @view_facsimile
					when 'stafflines' then @view_stafflines
					when 'clefs' then @view_clefs
					when 'accidentals' then @view_accidentals
					when 'divisiones' then @view_divisiones
					when 'syllables' then @view_syllables
					when 'notes' then @view_notes
					else null
				if fun?
					(fun.bind this) cbox.checked
			@canvas.fabric.renderAll()
	find_object: (obj, items = @items) ->
		console.debug 'Trying to find object:', obj
		logmsg = (i) -> console.debug 'Object is component of ', i
		for item in items
			if typeof item.is_component is 'function'
				if item.is_component obj
					logmsg item
					return item
		return null
	find_object_by_id: (id, items = @items) ->
		console.debug 'Finding object by ID:', id
		for item in items
			if not (item.properties? and typeof item.properties is 'function')
				continue
			props = item.properties @zoom
			if not (props.id? and props.id.get? and typeof props.id.get is 'function')
				continue
			ids = props.id.get()
			aids = []
			if ids.id?
				aids.push ids.id
			if ids.local_id?
				aids.push ids.local_id
			if aids.includes id
				return item
		console.debug 'Object not found'
		return null
	increase_border: (px = 10) ->
		@alter_border px
	decrease_border: (px = 10) ->
		@alter_border -px
	alter_border: (amount) ->
		@canvas.fabric.discardActiveObject()
		dim = [@canvas.getwidth(), @canvas.getheight()]
		dim[0] += amount * 2
		dim[1] += amount * 2
		@canvas.setdimensions dim[0], dim[1]
		xy = @canvas.getbackgroundimagepos()
		xy[0] += amount
		xy[1] += amount
		@offset[0] += amount
		@offset[1] += amount
		window.Candr.Cookie.set 'offset', (JSON.stringify @offset)
		@canvas.setbackgroundimagepos xy[0], xy[1]
		for item in @items
			item.set_offset @offset[0], @offset[1]
	get_item_name: (item) ->
		if not typeof item.properties is 'function'
			console.error 'Item does not have properties'
			return null
		props = item.properties @zoom
		if not (props.name? and typeof props.name.get is 'function')
			console.error 'Item does not have name property'
			return null
		nameget = props.name.get()
		if not nameget.name?
			console.error 'Name getting function did not return name'
			return null
		name = nameget.name
		if not name
			console.error 'Item does not have non-null name'
			return null
		return name
	update_colours: =>
		hex2rgb = (h) ->
			r = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec h
			if not r
				return null
			return {
				r: (parseInt r[1], 16),
				g: (parseInt r[2], 16),
				b: (parseInt r[3], 16)
			}
		# make unique array of instance types
		instancetype = new Set()
		for item in @items
			name = @get_item_name item
			if not name
				continue
			instancetype.add name
		# optimisation: we don't need to update the colours if we've got
		# the same number of items as before
		if instancetype.size is @colours.size
			return false
		# entries() returns [val, val] we just want val for each
		instancetypes = (Array.from instancetype.entries()).map (e) -> e[0]
		# generate new colour palette
		colour_palette = palette 'tol-rainbow', instancetypes.length
		colour_palette.shuffle()
		colours = colour_palette.map (c) ->
			h = hex2rgb c
			return 'rgba(' + h.r + ',' + h.g + ',' + h.b + ',0.5)'
		# add colours to map of instance name -> colour
		@colours = new Map()
		for i in [0...colours.length]
			# zip instance names and colours together in map
			@colours.set instancetypes[i], colours[i]
		console.debug 'Colours:', Array.from @colours.entries()
		if @colours.size is 0
			@colourkey.html ''
			return false
		for item in @items
			# lookup colour in map
			name = @get_item_name item
			if not @colours.has name
				console.error 'Item was not found in colour map'
				continue
			col = @colours.get name
			if col and (typeof item.set_colour is 'function')
				item.set_colour col
		@colourkey.html '<fieldset class="inline-block border-black bor\
der-t p-0 mr-1"><legend class="text-xs pr-2">Key:</legend></fieldset>'
		@colours.forEach (colour, name) =>
			pcol = parseColor colour
			colbody = [pcol.red, pcol.green, pcol.blue].join ','
			corrcolour = 'rgba(' + colbody + ',1)'
			spn = u '<span class="mr-3">'
			label = (u '<span class="mr-1">').append (name + ':')
			spn.append label
			box = (u '<span>').addClass 'w-4 h-4 inline-block'
			for node in box.nodes
				node.style.backgroundColor = corrcolour
			spn.append box
			(@colourkey.children ':first-child').append spn
		@canvas.fabric.renderAll()
		return true
	delete_item: ->
		console.debug 'Deleting item'
		sel = @canvas.fabric.getActiveObject()
		if not sel
			alert "Nothing selected"
			return
		# make unique set of items to delete
		to_delete = new Set()
		if sel.type is 'activeSelection'
			console.debug 'Deleting all objects of active selection'
			for obj in sel._objects
				item = @find_object obj
				if item
					to_delete.add item
		else
			item = @find_object sel
			if item
				to_delete.add item
		@canvas.fabric.discardActiveObject()
		to_delete.forEach (e) =>
			console.debug 'Removing ', e
			@remove_item e
		@update_colours()
		return true
	view_facsimile: (visible) ->
		if @manuscript?
			@manuscript.visible visible
		@canvas.fabric.renderAll()
	remove_item: (item) ->
		@canvas.fabric.discardActiveObject()
		ret = item.remove()
		@items.remove item
		@make_delete item
		if ret
			console.debug 'Yielded objects: ', ret
			@make_save ret...
			@items.push ret...
		@update_colours()
		@canvas.fabric.renderAll()
	cancel_in_progress: ->
		if @items.length is 0
			return
		last = @items[@items.length - 1]
		if last.in_progress
			# Remove if in progress
			console.debug 'Cancelling:', last
			@remove_item last
	condition_update_colours: (item) ->
		name = @get_item_name item
		if not name?
			console.error 'Item does not have name'
			return false
		if not @colours.has name
			return @update_colours()
		colour = @colours.get name
		if not colour
			console.warn 'Item returned colour from cache is invalid'
			return @update_colours()
		if colour and (typeof item.set_colour is 'function')
			ret = item.set_colour colour
			@canvas.fabric.renderAll()
			return ret
		console.warn 'Item colour cannot be set'
		return false
	add_staffline: (retrigger = (-> false), bare = false) ->
		console.debug 'Adding staffline'
		@cancel_in_progress()
		sl = new window.Candr.Staffline @canvas, this, =>
			if retrigger()
				@add_staffline retrigger
		, 'rgba(102, 153,255,1)', @offset
		sl.local_id = @backend.get_temp_id()
		@items.push sl
		if not bare
			@reset_block_save()
			@condition_update_colours sl
		return sl
	create_staffline: ->
		console.debug 'Creating staffline'
		sl = @add_staffline (-> false), true
		sl.cancel_create()
		return sl
	view_stafflines: (visible) ->
		for item in @items
			if item instanceof window.Candr.Staffline
				item.visibility visible
		@staffline_visibility = visible
		@canvas.fabric.renderAll()
	get_staffline_points: ->
		console.debug 'Getting points of stafflines'
		nodes = []
		for item in @items
			if item instanceof window.Candr.Staffline
				# for each staffline
				points = []
				for node in item.nodes
					points.push [node.getx(), node.gety()]
				if points.length > 1
					nodes.push points
		return nodes
	get_note_points: ->
		console.debug 'Getting points of notes'
		nodes = []
		for item in @items
			if item instanceof window.Candr.Note
				nodes.push [item.node.getx(), item.node.gety()]
			else if item instanceof window.Candr.Ligature
				ns = item.get_notes()
				for note in ns
					nodes.push [note.node.getx(), note.node.gety()]
		return nodes
	add_clef: (retrigger = false, bare = false) ->
		console.debug 'Adding clef'
		@cancel_in_progress()
		c = new window.Candr.Clef @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_clef: ->
		console.debug 'Creating clef'
		c = @add_clef false, true
		c.cancel_create()
		return c
	add_editorial_c_clef: (retrigger, bare = false) ->
		console.debug 'Adding editorial C clef'
		@cancel_in_progress()
		c = new window.Candr.EditorialClef 'C', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_editorial_c_clef: ->
		console.debug 'Creating editorial C clef'
		c = @add_editorial_c_clef false, true
		c.cancel_create()
		return c
	add_editorial_d_clef: (retrigger, bare = false) ->
		console.debug 'Adding editorial D clef'
		@cancel_in_progress()
		c = new window.Candr.EditorialClef 'D', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_editorial_d_clef: ->
		console.debug 'Creating editorial D clef'
		c = @add_editorial_d_clef false, true
		c.cancel_create()
		return c
	create_editorial_f_clef: ->
		console.debug 'Creating editorial F clef'
		c = @add_editorial_f_clef false, true
		c.cancel_create()
		return c
	add_editorial_f_clef: (retrigger, bare = false) ->
		console.debug 'Adding editorial F clef'
		@cancel_in_progress()
		c = new window.Candr.EditorialClef 'F', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	add_editorial_a_clef: (retrigger, bare = false) ->
		console.debug 'Adding editorial A clef'
		@cancel_in_progress()
		c = new window.Candr.EditorialClef 'A', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_editorial_a_clef: ->
		console.debug 'Creating editorial A clef'
		c = @add_editorial_a_clef false, true
		c.cancel_create()
		return c
	add_editorial_e_clef: (retrigger, bare = false) ->
		console.debug 'Adding editorial E clef'
		@cancel_in_progress()
		c = new window.Candr.EditorialClef 'E', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_editorial_e_clef: ->
		console.debug 'Creating editorial A clef'
		c = @add_editorial_a_clef false, true
		c.cancel_create()
		return c
	view_clefs: (visible) ->
		test = (e) ->
			a = e instanceof window.Candr.Clef
			b = e instanceof window.Candr.EditorialClef
			return a or b
		for item in @items
			if test item
				item.visibility visible
		@canvas.fabric.renderAll()
	add_accidental: (retrigger, bare = false) ->
		console.debug 'Adding accidental'
		@cancel_in_progress()
		a = new window.Candr.Accidental @canvas, this
		a.set_offset @offset[0], @offset[1]
		a.local_id = @backend.get_temp_id()
		@items.push a
		if not bare
			a.set_to_mouse()
			@reset_block_save()
			@condition_update_colours a
		return a
	create_accidental: ->
		console.debug 'Creating accidental'
		a = @add_accidental false, true
		a.cancel_create()
		return a
	add_editorial_flat: (retrigger, bare = false) ->
		console.debug 'Adding editorial Flat'
		@cancel_in_progress()
		c = new window.Candr.EditorialAccidental 'Flat', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_editorial_flat: ->
		console.debug 'Creating editorial flat'
		a = @add_editorial_flat false, true
		a.cancel_create()
		return a
	add_editorial_sharp: (retrigger, bare = false) ->
		console.debug 'Adding editorial Sharp'
		@cancel_in_progress()
		c = new window.Candr.EditorialAccidental 'Sharp', @canvas, this
		c.set_offset @offset[0], @offset[1]
		c.local_id = @backend.get_temp_id()
		@items.push c
		if not bare
			c.set_to_mouse()
			@reset_block_save()
			@condition_update_colours c
		return c
	create_editorial_sharp: ->
		console.debug 'Creating editorial sharp'
		a = @add_editorial_sharp false, true
		a.cancel_create()
		return a
	view_accidentals: (visible) ->
		test = (e) ->
			a = e instanceof window.Candr.Accidental
			b = e instanceof window.Candr.EditorialAccidental
			return a or b
		for item in @items
			if test item
				item.visibility visible
		@canvas.fabric.renderAll()
	add_divisione: (retrigger = (-> false), bare = false) ->
		console.debug 'Adding divisione'
		@cancel_in_progress()
		d = new window.Candr.Divisione @canvas, this, =>
			if retrigger()
				@add_divisione retrigger, bare
		, 'rgba(102, 153,255,1)', @offset
		d.local_id = @backend.get_temp_id()
		@items.push d
		if not bare
			@reset_block_save()
			@condition_update_colours d
		return d
	create_divisione: ->
		console.debug 'Creating divisione'
		d = @add_divisione (-> false), true
		d.cancel_create()
		return d
	view_divisiones: (visible) ->
		for item in @items
			if item instanceof window.Candr.Divisione
				item.visibility visible
		@canvas.fabric.renderAll()
	add_note: (retrigger = (-> false), bare = false) ->
		console.debug 'Adding note'
		@cancel_in_progress()
		n = new window.Candr.Note @canvas, =>
			if retrigger()
				@add_note retrigger
		, this
		n.set_offset @offset[0], @offset[1]
		n.local_id = @backend.get_temp_id()
		@items.push n
		if not bare
			@reset_block_save()
			@condition_update_colours n
		return n
	create_note: ->
		console.debug 'Creating note'
		n = @add_note (-> false), true
		n.cancel_create()
		return n
	view_notes: (visible) ->
		test = (e) ->
			if e instanceof window.Candr.Note
				return true
			if e instanceof window.Candr.Ligature
				return true
			return false
		for item in @items
			if test item
				item.visibility visible
		@canvas.fabric.renderAll()
	add_syllable: (retrigger, bare = false) ->
		console.debug 'Adding syllable'
		@cancel_in_progress()
		s = new window.Candr.Syllable @canvas, this
		s.set_offset @offset[0], @offset[1]
		s.local_id = @backend.get_temp_id()
		@items.push s
		if not bare
			s.set_to_mouse()
			@reset_block_save()
			@condition_update_colours s
		return s
	create_syllable: ->
		console.debug 'Creating syllable'
		s = @add_syllable false, true
		s.cancel_create()
		return s
	add_editorial_syllable: (retrigger, bare = false) ->
		console.debug 'Adding editorial syllable'
		@cancel_in_progress()
		s = new window.Candr.EditorialSyllable @canvas, this
		s.set_offset @offset[0], @offset[1]
		s.local_id = @backend.get_temp_id()
		@items.push s
		if not bare
			s.set_to_mouse()
			@reset_block_save()
			@condition_update_colours s
		return s
	create_editorial_syllable: ->
		console.debug 'Creating editorial syllable'
		s = @add_editorial_syllable false, true
		s.cancel_create()
		return s
	view_syllables: (visible) ->
		test = (e) ->
			a = e instanceof window.Candr.Syllable
			b = e instanceof window.Candr.EditorialSyllable
			return a or b
		for item in @items
			if test item
				item.visibility visible
		@canvas.fabric.renderAll()
	add_editorial_text: (retrigger, bare = false) ->
		console.debug 'Adding editorial text'
		@cancel_in_progress()
		t = new window.Candr.EditorialText @canvas, this
		t.set_offset @offset[0], @offset[1]
		t.local_id = @backend.get_temp_id()
		@items.push t
		if not bare
			t.set_to_mouse()
			@reset_block_save()
			@condition_update_colours t
		return t
	add_ligature: (add_note = true, bare = false) ->
		console.debug 'Adding ligature'
		@cancel_in_progress()
		if add_note is true
			sel = @canvas.fabric.getActiveObject()
			item = @find_object sel
			if not (item and item instanceof window.Candr.Note)
				alert 'No note selected'
				return false
		l = new window.Candr.Ligature @canvas
		l.set_offset @offset[0], @offset[1]
		l.local_id = @backend.get_temp_id()
		@items.remove item
		@items.push l
		if not bare
			@reset_block_save()
		if add_note is true
			l.add_note item
		if not bare
			@condition_update_colours l
		return l
	create_ligature: ->
		console.debug 'Creating ligature'
		l = @add_ligature false, true
		l.cancel_create()
		return l
	append_to_ligature: ->
		console.debug 'Adding note to ligature'
		@cancel_in_progress()
		sel = @canvas.fabric.getActiveObject()
		it = @find_object sel
		# save the previous html
		past_optionspane = @optionspane.clone()
		# helper function for setting the options pane to some useful
		# text
		set_optionspane_text = (text) =>
			outdiv = (u '<div>').addClass 'h-16 flex flex-col justify-center'
			indiv = (u '<div>').addClass 'h-10 flex flex-col justify-center'
			p = (u '<p>').addClass 'text-lg'
			i = (u '<i>').append text
			un = outdiv.append (indiv.append (p.append i))
			@optionspane.html ''
			@optionspane.append un
		chop = @checkboxes.hasOwnProperty 'ligature'
		cboxstate = => @checkboxes['ligature'].checked
		if chop and cboxstate() isnt true
			@checkboxes['ligature'].checked = true
		after = =>
			if chop and cboxstate() isnt false
				@checkboxes['ligature'].checked = false
		if it instanceof window.Candr.Ligature
			set_optionspane_text 'Select note to append'
			notesel = (e) =>
				console.debug 'Notesel'
				if e.target is sel
					console.debug 'Selected already selected note'
					@canvas.fabric.discardActiveObject()
					return
				item = @find_object e.target
				if item instanceof window.Candr.Ligature
					alert 'Note already part of a ligature.
					Remove it from the other ligature first'
					@canvas.fabric.off 'object:selected', notesel
				else if not item instanceof window.Candr.Note
					alert 'Not a note'
				else
					@items.remove item
					it.add_note item
				@canvas.fabric.off 'object:selected', notesel
				@optionspane.html ''
				@optionspane.append past_optionspane
				@make_save it
				after()
			@canvas.fabric.on 'object:selected', notesel
			cboxnsel = =>
				if @checkboxes['ligature'].checked is false
					@canvas.fabric.off 'object:selected', notesel
				@checkboxes['ligature'].removeEventListener 'change', cboxnsel
			if @checkboxes.hasOwnProperty 'ligature'
				@checkboxes['ligature'].addEventListener 'change', cboxnsel
			@canvas.fabric.discardActiveObject()
		else if it instanceof window.Candr.Note
			set_optionspane_text 'Select ligature to append'
			ligsel = (e) =>
				console.debug 'Ligsel'
				item = @find_object e.target
				if e.target is sel
					console.debug 'Selected already selected note'
					@canvas.fabric.discardActiveObject()
					return
				if item instanceof window.Candr.Note
					alert 'Selected note not part of ligatu\
re. Make a new ligature for this note first.'
				else if not item instanceof window.Candr.Ligature
					alert 'Not a ligature'
				else
					@items.remove it
					item.add_note it
					@make_save item
				@canvas.fabric.off 'object:selected', ligsel
				@optionspane.html ''
				@optionspane.append past_optionspane
				after()
			cboxlsel = =>
				if @checkboxes['ligature'].checked is false
					@canvas.fabric.off 'object:selected', ligsel
				@checkboxes['ligature'].removeEventListener 'change', cboxlsel
			if @checkboxes.hasOwnProperty 'ligature'
				@checkboxes['ligature'].addEventListener 'change', cboxlsel
			@canvas.fabric.on 'object:selected', ligsel
			@canvas.fabric.discardActiveObject()
		else
			alert 'No note selected'
			return
		@reset_block_save()
		@condition_update_colours it
window.Candr.Editor = Editor
