class ImageWarp
	@canvas = null
	@im = null
	@idb_store = null
	constructor: (url, nodeset, callback, last_updated = 0) ->
		@idb_store = new idbKeyval.Store 'CANDR', 'imagewarp'
		cache_callback = (c) =>
			if not c
				@do_warp url, nodeset, ((im, src, crect) ->
					(callback.bind im) im, src, crect
				), ((cdat, crect) =>
					now = Date.now()
					@set_cache {
						src: cdat
						crect: crect
					}, url, nodeset, now
				)
				return this
			# we are cached
			im = new window.Image()
			im.onload = -> callback this, c.data.src, c.data.crect
			im.src = c.data.src
		if nodeset and nodeset.length > 0 and nodeset[0] isnt null
			cached = @get_cache url, nodeset, last_updated
			cached.then cache_callback
		else
			cache_callback null
		return this
	get_image: ->
		return @im
	make_key: (url, nodeset) ->
		cloned = [nodeset...]
		for nodes in cloned
			nodes.sort (a, b) ->
				xdiff = b[0] - a[0]
				if xdiff isnt 0
					return xdiff
				return b[1] - a[1]
		cloned.sort (a, b) ->
			breaka = a.length < b.length
			minlen = if breaka then a.length else b.length
			for i in [0...minlen]
				if a[i][0] < b[i][0]
					return -1
				if a[i][0] > b[i][0]
					return 1
				if a[i][1] < b[i][1]
					return -1
				if a[i][1] > b[i][1]
					return 1
			return breaka
		console.debug 'String:', cloned
		JSON.stringify {
			url: url
			cloned: nodeset
		}
	set_cache: (data, url, nodeset, last_updated = 0) ->
		key = @make_key url, nodeset
		record = {
			data: data
			last_updated: last_updated
		}
		return idbKeyval.set key, record, @idb_store
			.catch (e) ->
				console.error 'Failed to set cache', e
				return false
	get_cache: (url, nodeset, last_updated = 0) ->
		key = @make_key url, nodeset
		vldt = (c) ->
			a = (c) -> c? and c.data? and c.last_updated >= last_updated
			(a c) and c.data.src? and c.data.crect?
		console.debug key, @idb_store
		return idbKeyval.get key, @idb_store
			.then (val) ->
				if not val?
					console.debug 'Not in cache'
					return null
				if not vldt val
					console.error 'Cache data was invalid', key, val
					return null
				return val
			.catch (e) ->
				console.error 'Failed to get cache data', e
				return null
	do_warp: (url, nodeset, callback, cache_callback) =>
		try
			canvas = fx.canvas()
		catch e
			console.error e
			return
		im = new window.Image()
		iwthis = this
		im.onload = ->
			offset = [0, 0]
			texture = canvas.texture this
			w = canvas.draw texture
			# for each set of nodes
			crect = null
			for nodes in nodeset
				console.debug 'Doing warp:', nodes
				# add offset
				for node in nodes
					node[0] += offset[0]
					node[1] += offset[1]
				# find the minimum rectangle to warp to
				r = new window.Candr.MinRect nodes
				# do perspective transform
				w = w.perspective (r.points.flat()), (r.rect.flat())
				# get new offsets
				crect = new window.Candr.Quad r.rect
				offset = [crect.x, crect.y]
			w.update()
			# export to PNG
			canvas.toBlob ((blob) =>
				src = window.URL.createObjectURL blob
				@onload = -> callback this, src, crect
				@src = src
				dat = canvas.toDataURL('image/png')
				cache_callback dat, crect
				texture.destroy()
			), 'image/png'
		im.src = url
		return this
window.Candr.ImageWarp = ImageWarp
