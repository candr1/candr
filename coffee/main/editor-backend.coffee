class EditorBackend
	@worker = null
	@config = null
	@temp_id = null
	@edit = null
	constructor: (endpoint, staveid, edit = false, zoom = 1
	worker = '/js/editor-backend-worker.js') ->
		console.debug 'Creating new editor backend'
		@config = {}
		@edit = edit
		@temp_id = 0
		@zoom = zoom
		try
			@worker = new Worker worker
		catch e
			console.error 'Web worker cannot be started'
			@worker = null
			return null
		@update_config {
			'endpoint': endpoint
			'stave': staveid
		}
	get_temp_id: ->
		return --@temp_id
	resolve_props: (e) ->
		i = {}
		if not e?
			return null
		if typeof e.properties isnt 'function'
			console.error 'Item cannot be saved as it does \
			not return properties'
			return i
		p = e.properties @zoom
		for frame, props of p
			if typeof props.get is 'function'
				i[frame] = props.get()
		return i
	update_config: (config) ->
		console.debug 'Updating config'
		Object.assign @config, config
		@worker.postMessage [{'config': @config}]
	save_item: (msg_callback, update_callback, items...) ->
		if not @edit
			console.debug 'Not saving as edit = false'
			return
		proppeditems = (items.map (e) => @resolve_props e).filter (e) -> e?
		if proppeditems.length is 0
			return
		console.debug 'Saving item'
		@worker.postMessage (proppeditems.map (i) -> {'edit': i})
		@worker_attach_callback msg_callback, (e) =>
			update_callback e
			@worker.onmessage = (e) ->
	push_saves: (msg_callback, update_callback) ->
		if not @edit
			console.debug 'Not saving as edit = false'
			return
		@worker.postMessage ["pushsaves"]
		@worker_attach_callback msg_callback, (e) =>
			update_callback e
			@worker.onmessage = (e) ->
	delete_item: (msg_callback, update_callback, items...) ->
		if not @edit
			console.debug 'Not deleting as edit = false'
			return
		proppeditems = (items.map (e) => @resolve_props e).filter (e) -> e?
		if proppeditems.length is 0
			return
		console.debug 'Deleting item'
		@worker.postMessage (proppeditems.map (i) -> {'delete': i})
		@worker_attach_callback msg_callback, (e) =>
			update_callback e
			@worker.onmessage = (e) ->
	request_all: (msg_callback, update_callback) ->
		console.debug 'Requesting all'
		@worker.postMessage [["recall"]]
		@worker_attach_callback msg_callback, (e) =>
			update_callback e
			@worker.onmessage = (e) ->
	get_predictions: (msg_callback, update_callback) ->
		console.debug 'Fetching predictions'
		@worker.postMessage [["predict"]]
		@worker_attach_callback msg_callback, (e) =>
			update_callback e
			@worker.onmessage = (e) ->
	empty_queue: ->
		console.debug 'Emptying queue'
		@worker.postMessage ["emptyqueue"]
	block_save: ->
		@worker.postMessage ["blocksave"]
	unblock_save: ->
		@worker.postMessage ["unblocksave"]
	worker_attach_callback: (msg_callback, update_callback) ->
		@worker.onmessage = (e) ->
			dat = e.data
			console.debug 'Worker sent me a message:', dat
			if not (dat? and dat.status?)
				return false
			switch dat.status
				when 'saving' then msg_callback 'Saving...'
				when 'saved' then msg_callback 'Saved'
				when 'error' then msg_callback 'Error!'
			if dat.update?
				update_callback dat.update
window.Candr.EditorBackend = EditorBackend
