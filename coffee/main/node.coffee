class Node
	@circle = null
	@fabric = null
	@box = null
	@offset = null
	@dir = null
	@r = null
	@fill = null
	constructor: (fill = 'rgba(102,153,255,1)', r = 6, selectable = 2) ->
		console.debug 'Creating new node'
		@angle = 0
		@r = r
		@radius = r * selectable
		@scalar = 0
		@fill = fill
		@dir = 0
		@offset = [0, 0]
		@circle = new fabric.Circle {
			fill: @fill,
			hasBorders: false
			hasControls: false
			radius: @r
			selectable: false
			originX: 'center'
			originY: 'center'
			top: 0
			left: 0
		}
		# larger transparent box to enclose the node
		@box = new fabric.Rect {
			fill: 'rgba(0,0,0,0)'
			width: (2 * @radius)
			height: (2 * @radius)
			hasBorders: false
			hasControls: false
			selectable: false
			originX: 'center'
			originY: 'center'
			top: 0
			left: 0
		}
		@fabric = new fabric.Group [@box, @circle], {
			hasBorders: true
			hasControls: false
			selectable: true
		}
		@set_dir 0
		return this
	set_coords: ->
		@fabric.setCoords()
	get_dir: ->
		return @dir
	set_dir: (dir) ->
		console.debug 'Setting node dir:', dir
		if dir >= 1
			dir = 1
		else if dir <= -1
			dir = -1
		else
			dir = 0
		@dir = dir
		@fabric.remove @circle
		switch dir
			when -1
				# Triangle pointing down
				@circle = new fabric.Triangle {
					# Radius of inscribed circle of
					# equilateral triangle is 1/root3, so
					# our triangles need to be root3 times
					# bigger to fully enclose the original
					# circle
					width: (@r * 2 * Math.sqrt 3)
					height: (@r * 2 * Math.sqrt 3)
					angle: 180
				}
			when 1
				# Triangle pointing up
				@circle = new fabric.Triangle {
					width: (@r * 2 * Math.sqrt 3)
					height: (@r * 2 * Math.sqrt 3)
				}
			else
				# Normal circle
				@circle = new fabric.Circle {
					radius: @r
				}
		# Common properties
		@circle.set {
			top: 0
			left: 0
			originX: 'center'
			originY: 'center'
			fill: @fill,
			hasBorders: false
			hasControls: false
			selectable: false
		}
		@fabric.add @circle
	get_bounds: ->
		[@getx(), @gety()]
	is_component: (obj) ->
		return obj is @circle or obj is @fabric or obj is @box
	visibility: (visible) ->
		console.debug 'Altering node visibility to: ', visible
		@fabric.set 'visible', visible
		@circle.set 'visible', visible
	setpos: (x, y) ->
		# Got rid of this, even for debug it was spam
		#console.debug 'Setting node positions'
		@fabric.set {
			left: (x - @radius + @offset[0])
			top: (y - @radius + @offset[1])
		}
		@fabric.setCoords()
		return this
	set_offset: (x, y) ->
		oldx = @getx()
		oldy = @gety()
		@offset[0] = x
		@offset[1] = y
		@setpos oldx, oldy
	set_fill: (fill) ->
		@fill = fill
		console.debug 'Node setting fill: ', @fill
		@circle.set 'fill', @fill
		return true
	getx: ->
		@fabric.left + @radius - @offset[0]
	gety: ->
		@fabric.top + @radius - @offset[1]
window.Candr.Node = Node
