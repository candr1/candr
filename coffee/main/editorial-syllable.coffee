class EditorialSyllable extends window.Candr.Syllable
	properties: (zoom) ->
		return {
			id: {
				get: =>
					{
						id: @id
						local_id: @local_id
					}
				set: (dat) =>
					@id = dat.id
					@local_id = dat.local_id
				type: {}
			}
			name: {
				get: ->
					{
						name: 'EditorialSyllable'
					}
				set: (dat) ->
				type: {
					name: 'label'
				}
			}
			position: {
				get: =>
					{
						top: ((@rect.top - @offset[1]) * zoom)
						left: ((@rect.left - @offset[0]) * zoom)
						width: (@rect.getScaledWidth() * zoom)
						height: (@rect.getScaledHeight() * zoom)
						angle: @rect.angle
					}
				set: (dat) =>
					border = -@rect.strokeWidth
					s = {
						top: ((dat.top / zoom) + @offset[1])
						left: ((dat.left / zoom) + @offset[0])
						angle: dat.angle
						width: ((dat.width / zoom) + border)
						height: ((dat.height / zoom) + border)
						scaleX: 1
						scaleY: 1
					}
				type: {
					top: 'int'
					left: 'int'
					width: 'int'
					height: 'int'
					angle: 'int'
				}
			}
			centrepoint: {
				get: =>
					{
						x: (@centrepoint.getx() * zoom)
						y: (@centrepoint.gety() * zoom)
					}
				set: (dat) =>
					@centrepoint.setpos (dat.x / zoom), (dat.y / zoom)
				type: {
					x: 'int'
					y: 'int'
				}
			}
			text: {
				get: =>
					{
						text: @syllable_text
						wordstart: @wordstart
						stanza: @stanza
					}
				set: (dat) =>
					@syllable_text = dat.text
					@wordstart = dat.wordstart
					@stanza = dat.stanza
				type: {
					text: 'string'
					wordstart: 'bool'
					stanza: 'int'
				}
			}
			'editorial comment': {
				get: =>
					{
						'editorial comment': @editorial_comment
					}
				set: (dat) =>
					@editorial_comment = dat['editorial comment']
				type: {
					'editorial comment': 'string'
				}
			}
		}
window.Candr.EditorialSyllable = EditorialSyllable
