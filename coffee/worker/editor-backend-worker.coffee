importScripts '/js/worker-plugins.js'

# Polyfill for Object.assign
if typeof Object.assign isnt 'function'
	Object.defineProperty Object, 'assign', {
		value: (target, varArgs) ->
			if not target?
				throw new TypeError 'Cannot convert undefined or null to object'
			to = Object target
			for index in [1...arguments.length]
				nextSource = arguments[index]
				if nextSource?
					for nextKey, nextVal of nextSource
						if Object::hasOwnProperty.call nextSource, nextKey
							to[nextKey] = nextVal
			return to
		writable: true
		configurable: true
	}
# Polyfill for Object.values
if typeof Object.values isnt 'function'
	Object.defineProperty Object, 'values', {
		value: (object) ->
			(Object.keys object).map (e) -> object[e]
		writable: true
		configurable: true
	}
# I know, you're not supposed to extend native objects. Shoot me.
Array::remove = (val) ->
	idx = @indexOf val
	if idx > -1
		@splice idx, 1
# Polyfill components
for key, val of exports
	if typeof self[key] is 'undefined'
		console.warn 'Polyfilling self.' + key
		self[key] = val
queue = []
delay_ms = 5000
consuming = false
save_blocked = false
timerid = null
config = undefined
is_cmd = (e) ->
	(Array.isArray e) and (e.length is 1) and (typeof e[0] is 'string')
is_cmd_msg = (e, text) ->
	(is_cmd e) and (e[0] is text)
is_recall_msg = (e) -> is_cmd_msg e, "recall"
is_empty_queue_msg = (e) -> is_cmd_msg e, "emptyqueue"
is_blocksave_msg = (e) -> is_cmd_msg e, "blocksave"
is_unblocksave_msg = (e) -> is_cmd_msg e, "unblocksave"
is_pushsaves_msg = (e) -> is_cmd_msg e, "pushsaves"
is_predict_msg = (e) -> is_cmd_msg e, "predict"
compress_queue = ->
	compressor = (acc, curr) ->
		console.debug 'Compressing:', acc, curr
		notes_contained = (a) ->
			if not (('notes' of a) and ('notes' of a.notes))
				return null
			console.debug 'Item contains notes'
			return a.notes.notes
		same_items = (a, b) ->
			console.debug 'Same items called'
			pa = (Object.values a)[0]
			pb = (Object.values b)[0]
			is_obj = (e) ->
				(typeof e) is 'object' and e isnt null
			if not ((is_obj pa) and (is_obj pb))
				return false
			if not (('id' of pa) and ('id' of pb))
				console.debug 'Doesn\'t have id'
				return false
			paids = []
			if 'id' of pa.id and pa.id.id?
				paids.push pa.id.id
			if 'local_id' of pa.id and pa.id.local_id?
				paids.push pa.id.local_id
			pbids = []
			if 'id' of pb.id and pb.id.id?
				pbids.push pb.id.id
			if 'local_id' of pb.id and pb.id.local_id?
				pbids.push pb.id.local_id
			intersection = paids.filter (x) -> pbids.includes x
			if intersection.length > 0
				console.debug 'Intersection of ids'
				return true
			console.debug 'No intersection of ids'
			return false
		contained_in = (item, container) ->
			console.debug 'Contained in called'
			container_notes = notes_contained container
			if not container_notes?
				console.debug 'No container notes in:', container
				return false
			for note in container_notes
				if same_items item, note
					console.debug 'Notes are the same:', item, note
					return true
			console.debug 'Not contained:', item, container
			return false
		if not curr?
			return acc
		if is_cmd curr
			acc.push curr
			return acc
		for itemc in curr
			console.debug 'Checking:', itemc
			if not same_items itemc, itemc
				console.warn 'Invalid item encountered in compressor:', itemc
				# an invalid item hey ho
				acc.push itemc
				continue
			pushitemc = true
			for i in [0...acc.length]
				console.debug 'Checking with:', acc[i]
				if is_cmd acc[i]
					continue
				if same_items acc[i], itemc
					console.debug 'These are the same:', acc[i], itemc
					# These have the same id
					if 'delete' of acc[i]
						console.debug 'Item already deleted'
						# Item is already deleted, ignore the
						# rest in the most recent message
						pushitemc = false
						break
					if 'delete' of itemc
						console.debug 'Itemc instructs delete'
						# Itemc is a delete command, overwrite
						# whatever was in itema. Push
						# itemc below
						acc.remove acc[i]
						break
					# merge curr item into acc item
					console.debug 'Merge'
					itemc = Object.assign acc[i], itemc
					acc.remove acc[i]
					break
				else if contained_in acc[i], itemc
					console.debug 'acc[i] in itemc'
					# Item is now contained in itemc, so we
					# no longer need item. Push itemc below.
					acc.remove acc[i]
					break
				# Note the opposite is not true: if itemc is
				# contained in acc[i], then itemc has newer
				# data, so update acc[i] the update that update
				# with itemc
			if pushitemc
				console.debug 'Push itemc'
				# acc has not been merged into itemc or itemc
				# not wiped out, so add itemc
				acc.push itemc
			else
				console.debug 'No push itemc'
		return acc
	if queue.length > 1
		oldlen = queue.length
		queue = [queue.reduce compressor, []]
		newlen = queue.length
		console.debug 'Compressed queue from ' + oldlen + ' to ' + newlen
queue_add = (dat) ->
	filter_null = (obj) ->
		(Object.entries obj).reduce (r, [k, v]) ->
			if not v?
				return r
			if Array.isArray v
				r[k] = v.map (e) -> filter_null e
			else if typeof v is 'object'
				r[k] = filter_null v
			else
				r[k] = v
			return r
		, {}
	delay = true
	if is_empty_queue_msg dat
		console.debug 'Received Empty Queue message'
		queue = []
		consuming = false
		return false
	if is_blocksave_msg dat
		console.debug 'Received Block Save message'
		save_blocked = true
	else if is_unblocksave_msg dat
		console.debug 'Received Unblock Save message'
		save_blocked = false
		delay = true
	else if is_recall_msg dat
		console.debug 'Received Recall message'
		new_dat = dat
		delay = false
	else if is_predict_msg dat
		console.debug 'Received Predict message'
		new_dat = dat
		delay = false
	else if is_pushsaves_msg dat
		console.debug 'Received Push saves message'
		save_blocked = false
		delay = false
	else
		console.debug 'Dat is actually:', dat
		new_dat = dat.map (e) -> filter_null e
	console.debug 'Adding:', new_dat
	queue.push new_dat
	return delay
begin_consume = (delay = true) ->
	consuming = true
	if delay
		console.debug 'Consuming with delay'
		return setTimeout consume_item, delay_ms
	else
		console.debug 'Consuming without delay'
		consume_item()
		return null
consume_item = ->
	compress_queue()
	if save_blocked or not consuming
		console.debug 'Save blocked'
		consuming = false
		return
	item = null
	timerid = null
	while (not item?) or (item.length < 1)
		item = queue.shift()
		console.debug 'Consume got item:', item
		if not item?
			consuming = false
			postMessage {
				status: 'saved'
				update: []
			}
			queue = []
			return true
		if (is_recall_msg item) or (is_predict_msg item)
			break
		if is_pushsaves_msg item
			continue
		item = item.filter (e) ->
			if not ('config' of e)
				return true
			config = e.config
			return false
	check_config = (c) -> c? and c.endpoint? and c.stave?
	if not check_config config
		console.error 'Config not valid'
		postMessage {status: 'error'}
		return false
	# make request to server
	r = new XMLHttpRequest()
	r.onreadystatechange = ->
		if @readyState is 4
			if @status isnt 200
				postMessage {status: 'error'}
				console.error 'Response (' + @status + '):', @responseText
			else
				resp = JSON.parse @responseText
				console.debug 'Response:', resp
				ta = (e) -> e?
				tb = (e) -> 'update' of e
				tc = (e) -> Array.isArray e.update
				if not ((ta resp) and (tb resp) and (tc resp))
					ret = {status: 'error'}
					if 'message' of resp
						ret.message = resp.message
					postMessage ret
				postMessage {
					status: 'saved'
					update: resp.update
				}
				consume_item()
	r.open 'POST', config.endpoint, true
	r.setRequestHeader 'Content-Type', 'application/json;charset=UTF-8'
	postMessage {status: 'saving'}
	r.send JSON.stringify {
		data: item
		stave: config.stave
	}
console.debug 'Worker loaded'
onmessage = (e) ->
	delay = queue_add e.data
	if delay
		if not consuming
			timerid = begin_consume delay
	else
		clearTimeout timerid
		begin_consume false
self.addEventListener 'message', onmessage, false
