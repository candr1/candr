<?php

error_reporting(E_ALL);
set_error_handler(function ($severity, $message, $file, $line) {
	if (error_reporting() & $severity) {
		throw new \ErrorException($message, 0, $severity, $file, $line);
	}
});

//session_cache_limiter('private_no_expire');
session_start();
date_default_timezone_set('Europe/London');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../PHPMusicTools/src/PHPMusicTools/PHPMusicTools.php';
require __DIR__ . '/dependencies.php';
require __DIR__ . '/../app/middleware.php';
require __DIR__ . '/../app/routes.php';

?>
