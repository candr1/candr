<?php

$settings = require __DIR__ . '/settings.php';

$app = new \Slim\App($settings);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager();
$capsule->addConnection($container['connections'][$container['default']]);
$capsule->setAsGlobal();
$capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher(new \Illuminate\Container\Container));
$capsule->connection()->enableQueryLog();
/*
$capsule->connection()->listen(function($query) use (&$settings) {
	$setting_log = $settings['settings']['logger'];
	$logger = new \Monolog\Logger($setting_log['name']);
	$logger->pushProcessor(new \Monolog\Processor\UidProcessor());
	$logger->pushHandler(new \Monolog\Handler\StreamHandler($setting_log['path'], $setting_log['level']));
	$logger->debug($query->sql);
});
*/
$capsule->bootEloquent();

$container['twig_profile'] = function() {
	return new Twig_Profiler_Profile();
};

$container['logger'] = function($c) {
	$settings = $c->get('settings')['logger'];
	$logger = new \Monolog\Logger($settings['name']);
	$logger->pushProcessor(new \Monolog\Processor\UidProcessor());
	$logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
	//$logger->pushHandler(new \Monolog\Handler\NullHandler());
	return $logger;
};

$container['view'] = function($c) use (&$container) {
	$settings = $c->get('settings')['view'];
	$view = new \Slim\Views\Twig($settings['path'], [
		'cache' => $settings['cache'],
		'debug' => true,
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$c->router,
		$c->request->getUri()
	));
	$view->addExtension(new \Twig_Extension_Profiler($c['twig_profile']));
	$view->addExtension(new \Twig_Extension_Debug());
	$view->addExtension(new \Twig_Extensions_Extension_Array());

	$alphasplit = new Twig_SimpleFilter('alphasplit', function($arr) {
		$splititems = [];
		$currentalpha = '';
		$currentlist = [];
		foreach($arr as $item) {
			if(!$item || !array_key_exists('name', $item)) {
				continue;
			}
			if(!$item['name']) {
				$thisfirst = '';
			} else {
				$thisfirst = strtolower($item['name'][0]);
			}
			if($thisfirst != $currentalpha) {
				$currentalpha = $thisfirst;
				if(!empty($currentlist)) {
					$splititems[] = $currentlist;
					$currentlist = [];
				}
			}
			$currentlist[] = $item;
		}
		if(!empty($currentlist)) {
			$splititems[] = $currentlist;
		}
		return $splititems;
	});
	$timeago = new Twig_SimpleFilter('timeago', function($strtime) {
		$timago = new Westsworld\TimeAgo();
		return $timago->inWords(new DateTime("@$strtime"));
	});

	$view->getEnvironment()->addFilter($alphasplit);
	$view->getEnvironment()->addFilter($timeago);
	$view->getEnvironment()->addGlobal('current_url', $c->get('request')->getUri());
	return $view;
};

$container['HomeController'] = function($c) {
	return new \App\Controllers\HomeController($c->get('logger'), $c->view, $c->get('settings')['logintime']);
};
$browse_controllers = [
	'BrowseRootController' => \App\Controllers\BrowseRootController::class,
	'BrowseSourceController' => \App\Controllers\BrowseSourceController::class,
	'BrowseFolioController' => \App\Controllers\BrowseFolioController::class,
	'BrowseFacsimileController' => \App\Controllers\BrowseFacsimileController::class,
	'BrowseFacsimileSetController' => \App\Controllers\BrowseFacsimileSetController::class,
	'BrowseImageController' => \App\Controllers\BrowseImageController::class,
	'BrowseSystemController' => \App\Controllers\BrowseSystemController::class,
	'BrowseSystemSyncController' => \App\Controllers\BrowseSystemSyncController::class,
	'BrowseStaveController' => \App\Controllers\BrowseStaveController::class,
	'BrowseSettingController' => \App\Controllers\BrowseSettingController::class,
];
foreach($browse_controllers as $key => $class) {
	$container[$key] = function($c) use ($class) {
		return new $class($c->get('logger'), $c->view, $c->get('settings')['imagestore'], $c->get('settings')['thumbstore'], $c->get('settings')['logintime']);
	};
}
$xml_controllers = [
	'XMLItemController' => \App\Controllers\XMLItemController::class,
	'XMLLinkController' => \App\Controllers\XMLLinkController::class,
	'XMLSettingController' => \App\Controllers\XMLSettingController::class,
	'XMLStaveController' => \App\Controllers\XMLStaveController::class,
	'XMLSystemController' => \App\Controllers\XMLSystemController::class,
	'XMLWarpController' => \App\Controllers\XMLWarpController::class,
	'XMLChunkController' => \App\Controllers\XMLChunkController::class,
	'XMLContextController' => \App\Controllers\XMLContextController::class,
	'XMLFacsimileSetController' => \App\Controllers\XMLFacsimileSetController::class,
	'XMLFacsimileController' => \App\Controllers\XMLFacsimileController::class,
];
foreach($xml_controllers as $key => $class) {
	$container[$key] = function($c) use ($class) {
		return new $class($c->get('logger'), $c->view, $c->router, $c->get('settings')['xsltstore']);
	};
}
$edit_controllers = [
	'EditSourceController' => \App\Controllers\EditSourceController::class,
	'EditFolioController' => \App\Controllers\EditFolioController::class,
	'EditFacsimileController' => \App\Controllers\EditFacsimileController::class,
	'EditFacsimileSetController' => \App\Controllers\EditFacsimileSetController::class,
	'EditSystemController' => \App\Controllers\EditSystemController::class,
	'EditSystemSyncController' => \App\Controllers\EditSystemSyncController::class,
	'EditStaveController' => \App\Controllers\EditStaveController::class,
	'EditSettingController' => \App\Controllers\EditSettingController::class,
	'EditItemController' => \App\Controllers\EditItemController::class,
	'UtilityController' => \App\Controllers\UtilityController::class,
];
foreach($edit_controllers as $key => $class) {
	$container[$key] = function($c) use ($class) {
		return new $class($c->get('logger'), $c->view, $c->get('settings')['imagestore'], $c->get('settings')['thumbstore'], $c->get('settings')['thumbnail'][0], $c->get('settings')['thumbnail'][1], $c->router, $c->get('settings')['logintime']);
	};
}
$container['UserController'] = function($c) {
	return new \App\Controllers\UserController($c->get('logger'), $c->view, $c->get('settings')['logintime']);
};
$container['TranscribeController'] = function($c) {
	return new \App\Controllers\TranscribeController($c->get('logger'), $c->view, $c->router, $c->get('settings')['logintime']);
};
$container['LoginController'] = function($c) {
	return new \App\Controllers\LoginController($c->get('logger'), $c->view, $c->get('settings')['logintime']);
};
$container['SearchController'] = function($c) {
	return new \App\Controllers\SearchController($c->get('logger'), $c->view);
};
$container['MachineLearningController'] = function($c) {
	return new \App\Controllers\MachineLearningController($c->get('logger'), $c->get('settings')['public_key']);
};
$container['ImageDownloadController'] = function($c) {
	return new \App\Controllers\ImageDownloadController($c->get('logger'), $c->get('settings')['imagestore']);
};
