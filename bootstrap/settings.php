<?php
use Tracy\Debugger;

defined('DS') || define('DS', DIRECTORY_SEPARATOR);
define('DIR', realpath(__DIR__ . '/../') . DS);
Debugger::enable(Debugger::DEVELOPMENT, DIR . 'logs');
//
$secrets = require __DIR__ . '/../secrets.php';

return [
	'settings' => [
		'addContentLengthHeader' => false,
		'displayErrorDetails' => true,
		'view' => [
			'path' => __DIR__ . '/../resources/views',
			#'cache' => __DIR__ . '/../resources/cache',
			'cache' => false
		],
		'imagestore' => __DIR__ . '/../../candr-data/imagestore',
		'thumbstore' => __DIR__ . '/../../candr-data/thumbstore',
		'public_key' => $secrets['public_key'],
		'logintime' => 86400,
		'logger' => [
			'name' => 'CANDR',
			'path' => __DIR__ . '/../logs/app.log',
			'level' => \Monolog\Logger::DEBUG,
		],
		'xsltstore' => __DIR__ . '/../xslt',
		'thumbnail' => [320, 1024],
		'tracy' => [
			'showPhpInfoPanel' => 0,
			'showSlimRouterPanel' => 0,
			'showSlimEnvironmentPanel' => 0,
			'showSlimRequestPanel' => 1,
			'showSlimResponsePanel' => 1,
			'showSlimContainer' => 0,
			'showTwigPanel' => 0,
			'showIdiormPanel' => 0,
			'showDoctrinePanel' => 0,
			'showEloquentORMPanel' => 1,
			'showProfilerPanel' => 0,
			'showVendorVersionsPanel' => 0,
			'showXDebugHelper' => 0,
			'showIncludedFiles' => 0,
			'showConsolePanel' => 0,
			'configs' => [
				'ConsoleNoLogin' => 0,
				'ConsoleAccounts' => [
					'dev' => sha1('dev'),
				],
				'ConsoleHashAlgorithm' => 'sha1',
				'ConsoleTerminalJs' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.terminal/2.8.0/js/jquery.terminal.min.js',
				'ConsoleTerminalCss' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.terminal/2.8.0/css/jquery.terminal.min.css',
				'ProfilerPanel' => [
					'show' => [
						'memoryUsageChart' => 1,
						'shortProfiles' => true,
						'timeLines' => true,
					],
				],
			],
		],
	],
	'default' => 'production',
	'connections' => [
		'development' => [
			'driver' => 'sqlite',
			'database' => __DIR__ .'/../resources/db/development.sqlite',
			'prefix' => '',
			'paths' => [
				'migrations' => __DIR__ . '/../resources/db/migrations',
				'seeds' => __DIR__ . '/../resources/db/seeds',
			],
			'migration_base_class' => '\App\Migration\Migration',
		],
		'production' => [
			'driver' => 'pgsql',
			'database' => $secrets['database'],
			'username' => $secrets['username'],
			'host' => $secrets['host'],
			'password' => $secrets['password'],
			'prefix' => '',
			'paths' => [
				'migrations' => __DIR__ . '/../resources/db/migrations',
				'seeds' => __DIR__ . '/../resources/db/seeds',
			],
			'migration_base_class' => '\App\Migration\Migration',
		],
	],
];
