#!/bin/bash
if ! [ -x "$(command -v abcm2ps)" ]; then
	echo "Cannot find abcm2ps. Please install abcm2ps and try again"
	exit 1
fi
if ! [ -x "$(command -v ps2pdf)" ]; then
	echo "Cannot find ps2pdf. Please install ps2pdf and try again"
	exit 1
fi
echo "All shell commands installed"
exit 0
