.PHONY: all watch clean bootstrap project
all: project
bootstrap:
	npm install
	node_modules/bower/bin/bower --allow-root --config.cwd=bower/main install
	node_modules/bower/bin/bower --allow-root --config.cwd=bower/worker install
	composer install
	sh check_shell_commands.sh
update:
	npm update
	node_modules/bower/bin/bower --allow-root --config.cwd=bower/main update
	node_modules/bower/bin/bower --allow-root --config.cwd=bower/worker update
	composer update
project: gulpfile.js bootstrap
	node_modules/gulp/bin/gulp.js
gulpfile.js: gulpfile.coffee bootstrap
	node_modules/coffeescript/bin/coffee -c gulpfile.coffee
migrate:
	php vendor/bin/phinx migrate -c config-phinx.php
seed:
	php vendor/bin/phinx seed:run -c config-phinx.php
watch:
	node_modules/gulp/bin/gulp.js watch
server:
	php -S localhost:8080 -t build/
cloc:
	cloc app/ bootstrap/ coffee/ html/ sass/ resources/views/ resources/db/migrations/ resources/db/seeds/ xslt/
clean:
	rm -rf bower/main/bower_components bower/worker/bower_components build gulpfile.js node_modules vendor
